<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Change Password Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <b>Admin</b>LTE
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Change Password</p>

    <form id="form-input">
      <input type="hidden" name="token" value="<?php echo $token; ?>" />
      <input type="hidden" name="phone" value="<?php echo $phone; ?>" />
      <div class="form-group has-feedback validation-input">
        <input type="password" class="form-control" placeholder="New Password" name="newpassword" id="newpassword">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessageNewPassword">Please input new password</p>
      </div>
      <div class="form-group has-feedback validation-input">
        <input type="password" class="form-control" placeholder="Retype new password" name="renewpassword" id="renewpassword">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessageReNewPassword">You must retype with same password</p>
      </div>
      <div class="row">
        <div class="col-xs-6">

        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $("#form-input").on("submit",
        function()
        {
          $("#errorMessageNewPassword").hide();
          $("#errorMessageReNewPassword").hide();

          var check = true;
          var newpassword = $("#newpassword").val();
          if (newpassword.length == 0)
          {
            check = false;

            $("#errorMessageNewPassword").html("Please type new password");
            $("#errorMessageNewPassword").show();
          }
          else
          if (newpassword.length < 6)
          {
            check = false;
            $("#errorMessageNewPassword").html("New password length is minimum 6");
            $("#errorMessageNewPassword").show();
          }
          else
          {
            var re = /\s/
            if (newpassword.match(re))
            {
              check = false;
              $("#errorMessageNewPassword").html("New password can't contain space character");
              $("#errorMessageNewPassword").show();
            }
          }

          if (check)
          {
            var renewpassword = $("#renewpassword").val();     
            if (renewpassword != newpassword)
            {
                $("#errorMessageReNewPassword").show();
                check = false;
            }
          }     

          if (! check) return false;

          $.ajax({
             type: "POST"
            ,url: "<?php echo base_url(); ?>user/dochangepassword"
            ,data: $('#form-input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                clear();
               
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );

          return false;
        }
      );

    function clear()
    {
      $("#newpassword").val("");
      $("#renewpassword").val("");
    }

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
