<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>warningstock/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "categoryId": $("#categoryID").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "item_id"
            }, {
                "data": "description"
            }, {
                "data": "category_name"
            }, {
                "data": "stock"
            }, {
                "data": "idealstock"
            }, {
                "data": "minstock"
            },
			{
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    return "<input onclick=\"showOrder()\" class=\"checkbox\" value="+data.item_id+" style=\"width:20px;height:20px;align:center\" type=\"checkbox\" name=\"checkID[]\"/>"
               }
              
            }

			],
            "order": [
                [1, 'asc']
            ]
        });

		$("#categoryID").on("change", function() {
			dt.ajax.reload();
		});
    });
	
	function pdf() {
		var keyword 	= $('.dataTables_filter input').val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_warning_stock?keyword='+keyword+'&number='+number,'_blank');		
	}
	
	function check() {
		var data_order 	= $('[name="checkID[]"]:checked').length;		
		if(data_order == '0') {
			alert("Please select at least one data!");
		}
		else {
			 $.ajax({
				url			: "<?php echo base_url(); ?>warningstock/insertDataOrder",
				type		: "POST",
				data		: $("input:checkbox[name='checkID[]']:checked").serialize(),
				dataType	: "JSON",
				success: function (result) {
					alert(result.message);
					window.location.href = '<?php echo base_url(); ?>admin/orderstock';
				}
			  });
		}
	}
	
	showOrder();
	function showOrder() {
		var data_order = $('[name="checkID[]"]:checked').length;		
		if(data_order != 0) {
			$("#btnOrder").show();
		}
		else {
			$("#btnOrder").hide();		
		}
	}
	
</script>