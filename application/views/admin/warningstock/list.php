    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Warning Stock List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row" style="margin-top:-55px">
                     <div class="col-md-4">
                     </div>
                     <div class="col-md-4">
                     </div>					 
                     <div class="col-md-4">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" onclick="pdf()" class="btn btn-danger pull-right"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Print to PDF</button>
						  <button type="button" id="btnOrder" onclick="check()" style="margin-right:5px;display:none"  class="btn btn-primary pull-right"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Order</button>
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Ideal Stock</th>
                    <th>Min. Stock</th>
                    <th style="width:6%">Check</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Ideal Stock</th>
                    <th>Min. Stock</th>
                    <th style="width:6%">Check</th>
                  </tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
