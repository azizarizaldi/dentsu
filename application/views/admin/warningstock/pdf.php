<html>
	<head>
		<title>Warning Stock</title>
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			
		</style>
	</head>
	<body onload="window.print()">
		<table style="width:100%;border:none">
			<tr>
				<td style="width:50%;vertical-align:bottom;padding-bottom:30px;border:none"><h2>Warning Stock Item</h2></td>
			</tr>
		</table>		
		<hr style="margin-top:-30px"/>
		<hr style="margin-top:-7px"/>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%">No</th>
					<th style="width:30%">Item Description</th>
					<th style="width:10%">Category</th>
					<th style="width:10%">Stock</th>
					<th style="width:10%">Ideal Stock</th>
					<th style="width:10%">Min. Stock</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
				?>
				<tr>
					<td style="width:2%;text-align:center"><?=++$nomor?></td>
					<td style="width:30%"><?=$row->item_description?></td>
					<td style="width:10%;text-align:center"><?=$row->category_name?></td>
					<td style="width:10%;text-align:center"><?=$row->item_stock?> <?=$row->item_unit_measure?></td>
					<td style="width:10%;text-align:center"><?=$row->item_ideal_stock?> <?=$row->item_unit_measure?></td>
					<td style="width:10%;text-align:center"><?=$row->item_min_stock?> <?=$row->item_unit_measure?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="6">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>