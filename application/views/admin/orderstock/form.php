<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
              New Item
          </h3>
        </div>
        <form id="form_input" role="form" method="post">
          <div class="box-body">
              <div class="form-group">
                  <label for="exampleInputEmail1">Add Item</label>&nbsp;&nbsp;
                  <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success" ><i class="fa fa-plus"></i> Choose Item</button>
              </div>

              <?php
              $this->db->select("stationary_order_item_temp.* , stationary_item.item_description , stationary_item.item_unit_measure");
              $this->db->join("stationary_item","stationary_item.item_id = stationary_order_item_temp.stationary_item");
              $this->db->where("stationary_order_item_temp.created_by",$session->user_id);
              $getTempItem = $this->db->get("stationary_order_item_temp");
              if($getTempItem->num_rows() > 0) {
              ?>
              <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th style="text-align:center;width:5%">No</th>
                    <th>Item Description</th>
                    <th>QTY</th>
                    <th style="text-align:center;width:5%">Delete</th>
                 </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 0;
                  foreach($getTempItem->result() as $row) {
                  ?>
                  <tr>
                      <td style="text-align:center;width:5%"><?=++$nomor?></td>
                      <td><?=$row->item_description?></td>
                      <td><?=$row->qty.' '.$row->item_unit_measure?></td>
                      <td><button class="btn btn-danger btn-sm" type="button" onclick="deleteTemp(<?=$row->id?>)"><i class="fa fa-trash"></i> </button></td>
                  </tr>
                  <?php }?>
                </tbody>
            </table>
            <hr/>
            <button class="btn btn-primary" type="submit" id="btnSubmit"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Order</button>
              <?php }
              else { ?>
              <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th style="text-align:center;width:5%">No</th>
                    <th>Item Description</th>
                    <th>QTY</th>
                 </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="7">No data has been selected</td>
                  </tr>
                </tbody>
               </table>
              <?php }?>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal" role="dialog">
    <form id="form_item" role="form" method="post">
    <div class="modal-dialog modal-lg">    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Please select items and enter stock</h4>
        </div>
        <div class="modal-body">
              <?php
                $getItem = $this->db->query("
                  SELECT *
                  FROM stationary_item
                  WHERE stationary_item.item_id NOT IN (
                    SELECT stationary_order_item_temp.stationary_item
                    FROM stationary_order_item_temp
                    WHERE stationary_order_item_temp.created_by = '".$session->user_id."'
                  )
                ")->result();
              ?>
              <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th style="width:4%;text-align:center">#</th>
                    <th style="width:4%;text-align:center">No</th>
                    <th>Item Description</th>
                    <th style="width:20%;text-align:left">Current Stock</th>
                    <th style="width:10%;text-align:left">Qty</th>
                 </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    foreach($getItem as $get) {
                    ?>
                    <tr>
                      <td><input type="checkbox" name="checkID[]" value="<?=$get->item_id?>"></td>
                      <td><?=++$no?></td>
                      <td><?=$get->item_description?></td>
                      <td><?=$get->item_stock.' '.$get->item_unit_measure?></td>
                      <td><input type="number" name="qtyNumber_<?=$get->item_id?>" min=1  class="form-control"></td>
                    </tr>
                    <?php }?>
                </tbody>
              </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Add Item</button>
        </div>
      </div>      
    </div>
    </form>
  </div>
</section>

<script>
function deleteTemp(id) {
  var answer = confirm("Are you sure you want to cancel this item order ?");
  if (answer) {
			$.ajax({
				url			: "<?php echo base_url(); ?>orderstock/deleteTempItem",
				type		: "POST",
				data		: {
					id							: id
				},
				dataType	: "JSON",
				success: function (result) {
					if(result.status == true) {						
						alert(result.message);
						location.reload();
					}
					else {
						alert(result.message);						
					}
				}
			});
  }
}
</script>