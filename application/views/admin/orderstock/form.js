<script>
(function ($) {
    $('#form_input').on('submit',function(){
        var answer = confirm("Are you sure you want to add this item order?");
        if (answer) {
            var form_data = new FormData(document.getElementById('form_imput'));
            $.ajax({
               url: "<?php echo base_url(); ?>orderstock/saveNewItem",
               type: "POST",
               data: form_data,
               processData: false,
               contentType: false,
               dataType	: "JSON",
               success: function (result) {
                   if(result.status == true) {						
                       alert(result.message);
                       window.location.href = '<?php echo base_url(); ?>admin/orderstock';
                   }
                   else {
                       alert(result.message);						
                   }
               }
            });    
        }
        return false;
    });

    $('#form_item').on('submit',function(){
        var data_deleted = $('[name="checkID[]"]:checked').length;		
        if(data_deleted != 0) {
            var form_data = new FormData(document.getElementById('form_item'));
            $.ajax({
               url: "<?php echo base_url(); ?>orderstock/addTempItem",
               type: "POST",
               data: form_data,
               processData: false,
               contentType: false,
               dataType	: "JSON",
               success: function (result) {
                   if(result.status == true) {						
                       alert(result.message);
                       window.location.href = '<?php echo base_url(); ?>admin/orderstock/add';
                   }
                   else {
                       alert(result.message);						
                   }
               }
            });    
        }
        else {
            alert("please select at least one data!");
        }
        return false;
    });

})(jQuery);
</script>