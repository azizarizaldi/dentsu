<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#stationary_order_id").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>orderstock/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "stationary_order_id": $("#stationary_order_id").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "item_id"
            }, {
                "data": "description"
            }, {
                "data": "category_name"
            }, {
                "data": "stock"
            }, {
                "data": "minstock"
            },
			{
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    return "<input id=\"qty_"+data.stationary_order_detail_id+"\" onblur=\"changeQty("+data.stationary_order_detail_id+")\" class=\"form-control numericc\" value="+data.qty+" type=\"text\" name=\"qty[]\"/>"
               }
              
            }

			],
            "order": [
                [1, 'asc']
            ]
        });

    });
	
	$("#stationary_order_id").on("change", function() {
		dt.ajax.reload();
	});
	
	function pdf() {
		var keyword 	= $('.dataTables_filter input').val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_warning_stock?keyword='+keyword+'&number='+number,'_blank');		
	}
		
	function changeQty(stationary_order_detail_id) {
		var qty 	= $('#qty_'+stationary_order_detail_id).val();				
		 $.ajax({
			url			: "<?php echo base_url(); ?>orderstock/changeQty",
			type		: "POST",
			data		: {
				qty							: qty,
				stationary_order_detail_id 	: stationary_order_detail_id
			},
			dataType	: "JSON",
			success: function (result) {
			}
		  });
	}
	
	function deleteOrder() {
		var getID 	= $("#stationary_order_id").val();
		var getText = $("#stationary_order_id option:selected").text();
		var answer = confirm("Are you sure you want to delete the order no : "+getText+" ?");
		if (answer) {
			$.ajax({
				url			: "<?php echo base_url(); ?>orderstock/deleteOrder",
				type		: "POST",
				data		: {
					stationary_order_id							: getID
				},
				dataType	: "JSON",
				success: function (result) {
					if(result.status == true) {						
						alert(result.message);
						location.reload();
					}
					else {
						alert(result.message);						
					}
				}
			});
		}
	}
	
	function record() {
		var getID 	= $("#stationary_order_id").val();
		var getText = $("#stationary_order_id option:selected").text();
		var answer = confirm("Are you sure you want to record the order no : "+getText+" ?");
		if (answer) {
			$.ajax({
				url			: "<?php echo base_url(); ?>orderstock/recordOrder",
				type		: "POST",
				data		: {
					stationary_order_id							: getID					
				},
				dataType	: "JSON",
				success: function (result) {
					if(result.status == true) {						
						var printData = confirm(result.message+"\nAre you sure you want to print data ?");
						if (printData) {
							pdf();
							window.location.href = '<?php echo base_url(); ?>admin/receivestock';
						}
						else {
							window.location.href = '<?php echo base_url(); ?>admin/receivestock';
						}
					}
					else {
						alert(result.message);						
					}
				}
			});
		}
	}
	
	function pdf() {
		var keyword 	= $('.dataTables_filter input').val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		var getID 		= $("#stationary_order_id").val();		
		
		window.open('<?php echo base_url(); ?>report/pdf_order_stock?keyword='+keyword+'&number='+number+'&id='+getID,'_blank');				
	}

	function newItem() {
		window.location.href = '<?php echo base_url(); ?>admin/orderstock/add';
	}

</script>