<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#company_id").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>visitor_report/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "range_date"	: $("#range_date").val(),
                        "brand_unit_id"	: $("#brand_unit_id").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "order_id"
            }, {
                "data": "order_date"
            }, {
                "data": "visitorName"
            }, {
                "data": "visitingName"
            }, {
                "data": "order_receiver_note"
            }, {
                "data": "status"
            }, {
                "data": "order_rejected_note"
            }, {
                "data": "signInTime"
            }, {
                "data": "signOutTime"
            }
			],
            "order": [
                [1, 'asc']
            ]
        });

		$("#range_date").on("change", function() {
			dt.ajax.reload();
		});

		$("#company_id").on("change", function() {
			dt.ajax.reload();
		});

		$("#brand_unit_id").on("change", function() {
			dt.ajax.reload();
		});
		
    });
		
	function print() {
		var keyword 		= $('.dataTables_filter input').val();
		var range_date 		= $('#range_date').val();
		var brand_unit_id 	= $('#brand_unit_id').val();
		var number 			= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_visitor_report?keyword='+keyword+'&number='+number+'&range_date='+range_date+'&brand_unit_id='+brand_unit_id,'_blank');		
	}
	
	function brandUnit() {
		var company_id = $("#company_id").val();
		$.ajax({
			url: 'visitor_report/changeBrandUnit',
			type: "post",
			data: {
				company_id : company_id
			}			,
			success: function (response) {
				$("#brand_unit_id").html(response);
				$("#brand_unit_id").select2();
			}
		});		
	}
	$('#range_date').daterangepicker();
</script>