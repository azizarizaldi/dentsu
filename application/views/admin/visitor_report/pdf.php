<html>
	<head>
		<title>VISITOR MANAGEMENT REPORT</title>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			

			@media print {
				h5 {
					color: #ffffff !important;
				}
			}

		</style>
	</head>
	<body onload="window.print()">
		<table style="border:none;" align="right">
			<tr>
				<td class="title-top" style="vertical-align:bottom;border:none;color:white!important;border: 1px solid black!important;background-color:black!important"><h5 style="margin-left:5px;margin-right:5px">VISITOR MANAGEMENT REPORT</h5></td>
			</tr>
		</table>
		<br/>
		<hr style="margin-top:30px"/>
		<hr style="margin-top:-20px"/>
		<p style="text-align:right">Date : <?=tgl(date("Y-m-d"),'02')?></p>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02').' s/d '.tgl($lastDate,'02')?></td>
			</tr>
			<tr>
				<td style="vertical-align:bottom;border:none">Number Of Guest</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=count($query)?></td>
			</tr>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Order ID</th>
                    <th rowspan="2">Date Of Visit</th>
                    <th rowspan="2">Visitor Name</th>
                    <th rowspan="2">Host Name</th>
                    <th rowspan="2">Purpose</th>
                    <th rowspan="2">Note</th>
					<th colspan="2">Time</th>
                    <th rowspan="2">Status</th>
				</tr>
				<tr>
					<th>Sign In</th>
                    <th>Sign Out</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
				?>
				<tr>
					<td style="width:2%;text-align:center;vertical-align:top"><?=++$nomor?></td>
					<td style="vertical-align:top"><?=$row->order_id?></td>
					<td style="vertical-align:top"><?=tgl($row->order_date,'2')?></td>
					<td style="vertical-align:top"><?=$row->visitorName;?></td>
					<td style="vertical-align:top"><?=$row->visitingName?></td>
					<td style="vertical-align:top"><?=$row->order_receiver_note?></td>
					<td style="vertical-align:top"><?=$row->order_rejected_note?></td>
					<td style="vertical-align:top"><?=$row->signInTime?></td>
					<td style="vertical-align:top"><?=($row->signOutTime == '00:00' ? '-':''.$row->signOutTime)?></td>
					<td style="vertical-align:top"><?=$row->status?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="8">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>