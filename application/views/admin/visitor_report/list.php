    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Report Book / Month Meeting Room</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Periode</label>
						<input type="text" class="form-control pull-right" id="range_date">
                     </div>
                     <div class="col-md-3" style="margin-top:10px">
                        <label>Company</label>
                        <select class="form-control" id="company_id" style="width: 100%;" onchange="brandUnit()">
                           <option value="">Select company </option>
						   <?php						   
							foreach($this->db->get('company')->result() as $get) { ?>
							<option value="<?=$get->company_id?>"><?=$get->company_name?></option>
							<?php } ?>
						</select>
                     </div>
                     <div class="col-md-3" style="margin-top:10px">
                        <label>Brand Unit</label>
                        <select class="form-control" id="brand_unit_id" style="width: 100%;">
                           <option value="">Select the first company </option>
						</select>
                     </div>
                     <div class="col-md-2" style="margin-top:10px">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" id="btnOrder" onclick="print()"  class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>						  
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Created Date</th>
                    <th>Visitor</th>
                    <th>Host</th>
                    <th>Purpose</th>
                    <th>Status</th>
                    <th>Note</th>
                    <th>Sign In</th>
                    <th>Sign Out</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Created Date</th>
                    <th>Visitor</th>
                    <th>Host</th>
                    <th>Purpose</th>
                    <th>Status</th>
                    <th>Note</th>
                    <th>Sign In</th>
                    <th>Sign Out</th>
                 </tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

