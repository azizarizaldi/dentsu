<section class="content">
  <div class="row">

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>
            <?=$warning_stock?>
          </h3>

          <p>Warnig Stock</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="<?=base_url().'admin/warningstock'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>
            <?=$meeting_room?>
          </h3>

          <p>Meeting Room Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="<?=base_url().'admin/bookingmeetingroom'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>
            <?=$operational_car?>
          </h3>

          <p>Operational Car Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-cart-outline"></i>
        </div>
        <a href="<?=base_url().'admin/bookingcar'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>
            <?=$doc_in?>
          </h3>

          <p>Document Internal Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-cart-outline"></i>
        </div>
        <a href="<?=base_url().'admin/documentinternal'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>
            <?=$doc_ex?>
          </h3>

          <p>Document External Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-cart-outline"></i>
        </div>
        <a href="<?=base_url().'admin/documentexternal'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>
            <?=$courier?>
          </h3>

          <p>Courier Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-person"></i>
        </div>
        <a href="<?=base_url().'admin/documentin'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>
            <?=$visitors?>
          </h3>

          <p>Visitors Active</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-people-outline"></i>
        </div>
        <a href="<?=base_url().'admin/appointmentorder?data=1'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>
            <?=$stationary_order?>
            <!--sup style="font-size: 20px">%</sup-->
          </h3>
          <p>Stationary Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="<?=base_url().'admin/stationaryorder'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div
  >
  <!-- Main row -->
  <div class="row">
    <section class="col-md-12 connectedSortable">
      <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">To Do List</h3>
        </div>
        <div class="box-body">
          <ul class="todo-list">
            <li>
              <input type="checkbox" value="">
              <span class="text">Design a nice theme</span>
              <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
              <div class="tools">
                <i class="fa fa-edit"></i>
                <i class="fa fa-trash-o"></i>
              </div>
            </li>
          </ul>
        </div>
        <div class="box-footer clearfix no-border">
          <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#modalTodolist" onclick="addItem()"><i class="fa fa-plus"></i> Add item</button>
        </div>
      </div>
      </div>

      <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <i class="fa fa-envelope"></i>
          <h3 class="box-title">Quick Email</h3>
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form action="#" method="post" id="formSendEmail">
            <div class="form-group">
              <input type="email" class="form-control" name="emailto" placeholder="Email to:" id="emailto">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" placeholder="Subject">
            </div>
            <div>
              <textarea name="message" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </form>
        </div>
        <div class="box-footer clearfix">
          <button type="button" onclick="sendEmail()" class="pull-right btn btn-default" id="sendEmail">Send
            <i class="fa fa-arrow-circle-right"></i></button>
        </div>
      </div>
      </div>
    </section>
  </div>

<!-- Modal -->
<div class="modal fade" id="modalTodolist" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="#" method="post" id="formTodolist">      
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
          <input type="hidden" name="id" id="id" value=""/>
          <label for="title">Title : </label>
          <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
          <label for="pwd">Priority :</label>
          <select class="form-control" name="priority" id="priority">
            <option value="low">Low</option>
            <option value="medium">Medium</option>
            <option value="high">High</option>
          </select>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveTodolist()" id="btnSaveTodolist">Save Data</button>
        </div>
      </div>          
    </form>
    </div>
  </div>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <script>
    function sendEmail() {
      var values = $("#formSendEmail").serialize();
          $.ajax({
            url       : "<?=base_url().'/welcome/sendEmailDashboard'?>",
            type      : "post",
            data      : values ,
            dataType  : "json" ,
            beforeSend: function(){
              $("#sendEmail").html('<i class="fa fa-spin fa-spinner"></i> Sending...');
            },
            complete: function(){
              $("#sendEmail").html('Send <i class="fa fa-arrow-circle-right"></i>');
            },          
            success: function (response) {
              if(response.status == 'success') {
                  Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  });
                  $('#formSendEmail')[0].reset();    
                }
              else {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  $("#emailto").focus();
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });      
    }

    todolist();
    function todolist() {
      $.ajax({
          type      : "get",
          url       : "<?=base_url().'/welcome/todolist'?>",
          data      : {name: 'Wayne'},
          beforeSend: function(){
            $(".todo-list").html('<p style="text-align:center"><i class="fa fa-spin fa-spinner"></i> Getting data...</p>');
          },
          success: function(data){
            $(".todo-list").html(data);
          }
      });
    }


    function changeDone(id,status) {
      $.ajax({
          type      : "post",
          url       : "<?=base_url().'/welcome/update_todolist_status'?>",
          data      : {
              id    : id,
              status:status
          },
          success: function(data){
            $.ajax({
                type      : "get",
                url       : "<?=base_url().'/welcome/todolist'?>",
                data      : {name: ''},
                success: function(data){
                  $(".todo-list").html(data);
                }
            });
          }
      });
    }

    function deleteTodo(id) {
      if (confirm('Are you sure you want to deleted data?')) {
        $.ajax({
            type      : "post",
            url       : "<?=base_url().'/welcome/delete_todolist'?>",
            data      : {
                id    : id
            },
            dataType  : "json",
            success: function(data){
              Swal.fire({
                position: 'top',
                type: 'success',
                title: data.message,
                showConfirmButton: false,
                timer: 1500
              });
              todolist();              
            }
        });
      }
    }

    function addItem() {
      $('#id').val("");
      $('#title').val("");
      $(".modal-title").html("Add Item");
    }

    function saveTodolist() {
      var values  = $("#formTodolist").serialize();
      var title   = $("#title").val();
      if(title == '') {
        alert("Title can't empty!");
      }
      else {
          $.ajax({
            url       : "<?=base_url().'/welcome/saveTodolist'?>",
            type      : "post",
            data      : values ,
            dataType  : "json" ,
            beforeSend: function(){
              $("#btnSaveTodolist").html('<i class="fa fa-spin fa-spinner"></i> Saving...');
            },
            complete: function(){
              $("#btnSaveTodolist").html('Save Data');
            },          
            success: function (response) {
                Swal.fire({
                  position: 'top',
                  type: 'success',
                  title: response.message,
                  showConfirmButton: false,
                  timer: 1500
                });
                todolist();    
                $("#modalTodolist").modal("hide");          
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });      
      }
    }

    function editTodo(id) {
      var title     = $("#textTitle_"+id).val();
      var priority  = $("#textPriority_"+id).val();
      $("#modalTodolist").modal("show");          
      $(".modal-title").html("Edit Item");
      $("#id").val(id);
      $("#title").val(title);
      $("#priority").val(priority).change();    
    }
  </script>