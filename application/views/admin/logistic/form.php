<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Logistic Company" : "Add Logistic Company"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="logistic_id" value="<?php echo $row->logistic_id; ?>" />
                <?php } ?>
              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->logistic_id; ?>" disabled>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label>Company Name</label>
                  <div class="input-group input-group-lg">
                     <div class="input-group-btn">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Companies
                            <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu" id="companies">
                        </ul>
                    </div>
                        <!-- /btn-group -->
                    <input type="text" class="form-control validate-input" placeholder="Company Name" id="logistic_company" name="logistic_company" value="<?php echo isset($row) ? htmlspecialchars($row->logistic_company, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                </div>
                    <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input company name</p>
                </div>
                   <div class="form-group">
                  <label>Branch</label>
                  <input type="text" class="form-control validate-input" placeholder="Branch" name="logistic_branch" value="<?php echo isset($row) ? htmlspecialchars($row->logistic_branch, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input external courier branch</p>
                </div>
                                      <div class="form-group">
                  <label>Contact Person</label>
                  <input type="text" class="form-control" placeholder="Contact Person" name="logistic_cp" value="<?php echo isset($row) ? htmlspecialchars($row->logistic_cp, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input contact person</p>
                </div>

                   <div class="form-group">
                  <label>Phone</label>
                  <input type="text" class="form-control" placeholder="Phone" name="logistic_phone" value="<?php echo isset($row) ? htmlspecialchars($row->logistic_phone, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input phone no</p>
                </div>
                   
                   <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" name="logistic_address" rows="3" placeholder="Address ..."><?php echo isset($row) ? htmlspecialchars($row->logistic_address, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>