<script>

(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        })
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/orderpriority/save"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/orderpriority";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   
    $("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/orderpriority/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/orderpriority";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });

})(jQuery);
</script>