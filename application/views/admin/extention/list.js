<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $("#id_category").select2();
    
    var dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "<?php echo base_url(); ?>admin/extention/get",
            data: function ( d ) {
           return $.extend( {}, d, {
             "id_category"		: $("#id_category").val()
           } );
        }
          },
        "columns": [
           
            { "data": "extention_id" },
            { "data": "category" },
            { "data": "extention_name" },
            { "data": "website" }
        ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/extention/edit?id=" + tr.find('td:first').html();
    } );

    $("#id_category").on("change", function() {
		dt.ajax.reload();
	});

  
  })


  function printData() {
		var keyword 	  	= $('.dataTables_filter input').val();
    var number 			  = $('select[name="example1_length"] option:selected').val();
    var id_category 	= $('#id_category').val();    
    
		window.open('<?php echo base_url(); ?>report/pdf_extention?keyword='+keyword+'&number='+number+'&id_category='+id_category,'_blank');		
  }
</script>