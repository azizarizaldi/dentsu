<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php echo ($action == "edit") ? "Edit extention" : "Add extention"; ?>
          </h3>
        </div>
        <form id="form_input" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?=$action?>" />
          <?php if ($action == "edit") { ?>
          <input type="hidden" name="extention_id" value="<?php echo $row->extention_id; ?>" />
          <?php } ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ID extention</label>
                  <?php if ($action == "edit") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="extention_id_text"
                    value="<?php printf(" %05d", $row->extention_id);?>"
                  <?php echo $canModify ? "" : "disabled" ; ?> />
                  <?php }
                  else if ($action == "add") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="extention_id_text"
                    value="" <?php echo $canModify ? "" : "disabled" ; ?> />
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select id="id_category" style="width:100%;" class="form-control validate-input select2" name="id_category"
                    <?php if (! $canModify) { echo " disabled" ; }?>>
                    <option value="">--- Select a category ---</option>
                    <?php foreach($data_category as $category) { ?>
                    <option value="<?php echo $category->id; ?>" <?php if (isset($row) && $category->id ==
                      $row->id_category) { echo "selected"; } ?>>
                      <?php echo htmlspecialchars($category->name, ENT_QUOTES); ?>
                    </option>
                    <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please
                    select a category</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Extention Name</label>
                  <input type="text" class="form-control validate-input" id="extention_name" name="extention_name"
                    placeholder="Extention Name" value="<?php echo isset($row) ? htmlspecialchars($row->extention_name, ENT_QUOTES) : ""; ?>">
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input
                    extention name</p>
                </div>
              </div>
            </div>
            <hr />
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Process Duration</label>
                  <input type="text" class="form-control" id="process_duration" name="process_duration" placeholder="process duration..."
                    value="<?php echo isset($row) ? htmlspecialchars($row->process_duration, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Website</label>
                  <input type="text" class="form-control" id="website" name="website" placeholder="Website (example : www.denstu.com)..."
                    value="<?php echo isset($row) ? htmlspecialchars($row->website, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Attachment Doc</label>
                  <input type="file" id="file_attachment" name="file_attachment">
                  <?php
                    if($action == 'edit') {
                      if(!empty($row->file_attachment)) { ?>
                  Previous File : <a target="blank" href="<?=base_url().'uploads/extention/'.$row->file_attachment?>">Click
                    Here!</a>
                  <?php }
                  }?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Estimated Cost</label>
                  <input type="number" min="1" class="form-control" id="estimated_cost" name="estimated_cost" placeholder="Estimated cost..."
                    value="<?php echo isset($row) ? htmlspecialchars($row->estimated_cost, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Terms & Condition</label>
                  <textarea class="form-control" id="terms_condition" name="terms_condition" rows="6" placeholder="terms & condition..."><?php echo isset($row) ? htmlspecialchars($row->terms_condition, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Note</label>
                  <textarea class="form-control" id="note" name="note" rows="3" placeholder="Notes..."><?php echo isset($row) ? htmlspecialchars($row->note, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" id="btnSubmit">
                <?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
              <?php if ($action == "edit") { ?>
              &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              <?php } ?>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>