<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/car/get",
        "columns": [
           
            { "data": "car_id" },
            { "data": "no" },
            { "data": "car_brand" },
            { "data": "car_model" },
            { "data": "car_color" },
            { "data": "car_year" },
            { "data": "car_seat" },
            { "data": "user_name"},
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                  //if (data.order_status == 1)
                  //{
                    
                    return "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.car_id+"); \">Edit</button>"
                      + "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:driver("+data.car_id+","+data.user_id+",'"+data.car_driver_start_date_fmt+"','"+data.car_driver_end_date_fmt+"');\">Driver</button>";
                  //}
               }
              
            }
        ],
        "order": [[1, 'asc']]
    } );
   
   $('#datepicker1').datepicker({
      autoclose: true
    });
   
      $('#datepicker2').datepicker({
      autoclose: true
    });
  })
  
    function edit(carId)
    {
        location.href = "<?php echo base_url(); ?>admin/car/edit?id=" + carId;
    }
    
    var carId;
    
    function driver(id, driverId, startDate, endDate)
    {
      carId = id;
      
      if (startDate != "null")
      {
        $("#datepicker1").val(startDate);
      }
      else
      {
        $("#datepicker1").val("");
      }
      
      if ((endDate != "31/12/2999") && (endDate != "null"))
      {
        $("#datepicker2").val(endDate);
      }
      else
      {
        $("#datepicker2").val("");
      }
      
      $("#driverId").val(driverId);
      
      $("#modal-info").modal('show');
    }
    
    function getDateInt(date)
    {
      //01/04/2018
      var dates =  date.split("/");
      if (dates.length != 3) return 0;
      
      return dates[2]*10000 + dates[1]*100 + dates[0]*1;
    }
    
    function setDriver(date1, date2, driverId)
    {
      
      var ndate1 = getDateInt(date1);
      
      if ((date1.length == 0) || (ndate1 == 0))
      {
        alert("Please select start date");
        return;
      }
      
      var ndate2;
      if (date2.length > 0)
      {
          var ndate2 = getDateInt(date2);
          if (ndate1 > ndate2)
          {
              alert("Invalid date range");
              return;
          }
      }
      
      if (driverId == "")
      {
        alert("Please select a driver");
        return;
      }
      
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/car/setDriver"
            ,data: {"carId": carId, "date1": ndate1, "date2": ndate2, "driverId": driverId}
            ,success: function (data)
            {
                alert(data.message);
                if (data.status)
                {
                  $("#modal-info").modal('hide');
                    dt.ajax.reload(function() {
                        dt.columns.adjust()
                    }, false);
                }
            }
            ,error: function(request, error)
            {
                alert(error);
            }
            ,dataType: "json"
          }
        );
    }
</script>