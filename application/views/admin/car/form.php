<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Car" : "Add Car"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="car_id" value="<?php echo $row->car_id; ?>" />
                <?php } ?>
                                <input type="hidden" id="car_pic" name="car_pic" value="" />
                <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />

              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->car_id; ?>" disabled>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label>Vehicle Registration Number</label>
                  <input type="text" class="form-control validate-input" placeholder="Vehicle Registration Number" name="car_no" value="<?php echo isset($row) ? htmlspecialchars($row->car_no, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vehicle registration number</p>
                </div>
                <div class="form-group">
                  <label>Brand</label>
                  <div class="input-group input-group-lg">
                     <div class="input-group-btn">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Brands
                            <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu" id="brands">
                        </ul>
                    </div>
                        <!-- /btn-group -->
                    <input type="text" class="form-control validate-input" placeholder="Brand" id="car_brand" name="car_brand" value="<?php echo isset($row) ? htmlspecialchars($row->car_brand, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                </div>
                    <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vehicle brand</p>
                </div>
                <div class="form-group">
                  <label>Model</label>
                  <div class="input-group input-group-lg">
                     <div class="input-group-btn">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Models
                            <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu" id="models">
                        </ul>
                    </div>
                        <!-- /btn-group -->
                    <input type="text" class="form-control validate-input" placeholder="Model" id="car_model" name="car_model" value="<?php echo isset($row) ? htmlspecialchars($row->car_model, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                </div>
                    <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vehicle model</p>
                </div>
                  <div class="form-group">
                  <label>Vehicle Production Year</label>
                  <input type="text" class="form-control validate-input" placeholder="Vehicle Registration Number" name="car_year" value="<?php echo isset($row) ? htmlspecialchars($row->car_year, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vehicle production year</p>
                </div>
                <div class="form-group">
                  <label>Color</label>
                  <input type="text" class="form-control validate-input" placeholder="Vehicle Color" name="car_color" value="<?php echo isset($row) ? htmlspecialchars($row->car_color, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vehicle color</p>
                </div>
                  <div class="form-group">
                  <label>Vehicle Picture</label>
                  <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->car_pic ? base_url().$row->car_pic : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->car_pic ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->car_pic) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input picture</p> 
                </div>
                   <div class="form-group">
                  <label>No of Seat</label>
                  <input type="text" class="form-control validate-input" placeholder="No of Seat" name="car_seat" value="<?php echo isset($row) ? htmlspecialchars($row->car_seat, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input no of seat</p>
                </div>
                <div class="form-group">
                  <label>Rent Company</label>
                  <div class="input-group input-group-lg">
                     <div class="input-group-btn">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Rent Companies
                            <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu" id="rents">
                        </ul>
                    </div>
                        <!-- /btn-group -->
                    <input type="text" class="form-control" placeholder="Rent Company" id="car_company_rent" name="car_company_rent" value="<?php echo isset($row) ? htmlspecialchars($row->car_company_rent, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>