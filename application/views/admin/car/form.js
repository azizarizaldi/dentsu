<script>

(function ($) {
    "use strict";

    loadBrands();
    loadModels();
    loadRentCompanies();
    
    function loadBrands()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>car/getBrands"
            ,success: function (data)
            {
                $("#brands").empty();
                
                for(var i=0; i < data.length; i++)
                {
                    $("#brands").append("<li><a href='#' onClick='javascript:$(\"#car_brand\").val(\""+data[i].car_brand+"\")'>"+data[i].car_brand+"</a><li>");
                }
            }
            ,error: function(request, error)
            {
                alert(error);
            }
            ,dataType: "json"
          }
        );
    }
    
    function loadModels()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>car/getModels"
            ,success: function (data)
            {
                $("#models").empty();
                
                for(var i=0; i < data.length; i++)
                {
                    $("#models").append("<li><a href='#' onClick='javascript:$(\"#car_model\").val(\""+data[i].car_model+"\")'>"+data[i].car_model+"</a><li>");
                }
            }
            ,error: function(request, error)
            {
                alert(error);
            }
            ,dataType: "json"
          }
        );
    }

    function loadRentCompanies()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>car/getRentCompanie"
            ,success: function (data)
            {
                $("#rents").empty();
                
                for(var i=0; i < data.length; i++)
                {
                    $("#rents").append("<li><a href='#' onClick='javascript:$(\"#car_company_rent\").val(\""+data[i].car_company_rent+"\")'>"+data[i].car_company_rent+"</a><li>");
                }
            }
            ,error: function(request, error)
            {
                alert(error);
            }
            ,dataType: "json"
          }
        );
    }
    

    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        })

        $("#car_pic").val("");
        $("#previewPhoto").hide();

        $("#car_company_rent").val("");
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/car/save"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                loadBrands();
                loadModels();
                loadRentCompanies();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/car";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   
    $("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/car/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/car";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });

    var files;
    $("#photo").on("change", function(event)
                                      {
                                        event.preventDefault();
                                        var formData = new FormData($("#form_input")[0]);
                                        $.ajax({
        url: '<?php echo base_url(); ?>admin/car/savePhoto',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(! data.error)
            {
                // Success so call function to process the form
                $("#previewPhoto").attr("src", "<?php echo base_url(); ?>"+data.src);
                $("#previewPhoto").show();
                
                $("#tmp_photo").val(data.src);
                $("#car_pic").val(data.photo);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
                                        
                                        
                                        
    });

    $("#btnResetPhoto").on("click", function()
                       {
                            $("#car_pic").val("");
                            $("#previewPhoto").attr("src", "<?php echo base_url(); ?><?php echo isset($row) ? $row->car_pic : ""; ?>");
                       });

})(jQuery);
</script>