<script>
(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        var $inputs = $('#form_input :input');
        $inputs.each(function() {
            $(this).val("");
        });

        var $inputs = $('#form_input :select');
        $inputs.each(function() {
            $(this).val("");
        });


        $("#user_photo").val("");
        $("#previewPhoto").hide();
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/visitor/save"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/visitor";
                <?php } ?>
            }
            ,error: function(data)
            {
                alert("Terjadi kesalahan")
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){

        var check = true;

        for(var i=0; i<input.length; i++) {
            if (input[i].name == "user_email") continue;
            if (input[i].name == "user_password") continue;
            if (input[i].name == "confirm_password") continue;
            
            if(validate(input[i]) == false){
                showValidate(input[i], "");
                check=false;
            }
        }
        
        if (check)
        {
            if ($("#user_email").val().trim() == "")
            {
                showValidate($("#user_email"), "Please input email");
                check=false;
            }
            else
            if (! validateEmail($("#user_email").val()))
            {
                showValidate(input[0], "Invalid email format");
                check=false;
            }
            <?php if ($action == "add")  { ?>
            if ($("#user_password").val().trim() == "")
            {
                showValidate($("#user_password"), "Please input password");
                check=false;
            }
            else
            if ($("#user_password").val().trim().length < 6)
            {
                showValidate($("#user_password"), "Password too short");
                check=false;
            }
            else
            if ($("#user_password").val().trim() != $("#confirm_password").val().trim())
            {
                showValidate($("#confirm_password"), "Confirm Password is different");
                check=false;
            }
            <?php } ?>
        }
        
        if (check)
        {
             post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input, text) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
        if (text.length > 0)
        {
            $(thisAlert).find('#errorMessage').html(text);
        }
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
    
    function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

$("#btnResetPhoto").on("click", function()
                       {
                            $("#user_photo").val("");
                            $("#previewPhoto").attr("src", "<?php echo base_url(); ?><?php echo isset($row) ? $row->user_photo : ""; ?>");
                       });
   
$("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/visitor/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/visitor";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });
                       
                       var files;
                       $("#photo").on("change", function(event)
                                      {
                                        event.preventDefault();
                                        var formData = new FormData($("#form_input")[0]);
                                        $.ajax({
        url: '<?php echo base_url(); ?>admin/visitor/savePhoto',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(! data.error)
            {
                // Success so call function to process the form
                $("#previewPhoto").attr("src", "<?php echo base_url(); ?>"+data.src);
                $("#previewPhoto").show();
                
                $("#tmp_photo").val(data.src);
                $("#user_photo").val(data.photo);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
                                        
                                        
                                        
                                      });
                       
                       $("#btnChangePassword").on("click", function()
                                                                   {
                                                                    var check = true;
            if ($("#user_password").val().trim() == "")
            {
                showValidate($("#user_password"), "Please input password");
                check=false;
            }
            else
            if ($("#user_password").val().trim().length < 6)
            {
                showValidate($("#user_password"), "Password too short");
                check=false;
            }
            else
            if ($("#user_password").val().trim() != $("#confirm_password").val().trim())
            {
                showValidate($("#confirm_password"), "Confirm Password is different");
                check=false;
            }
            
            if (check)
            {
             $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/visitor/changePassword"
            ,data: $('#form_password').serialize()
            ,success: function (data)
            {
                alert(data.message)
               <?php if ($action != "edit") { ?>
                              $("#user_password").val("");
                $("confirm_password").val("");
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/user";
                <?php } ?>
            }
            ,error: function(data)
            {
                alert("Terjadi kesalahan")
                <?php if ($action != "edit") { ?>
                $("#user_password").val("");
                $("confirm_password").val("");
                <?php } ?>
            }
            ,dataType: "json"
          }
        );   
            }
            
                                                                    });

})(jQuery);
</script>