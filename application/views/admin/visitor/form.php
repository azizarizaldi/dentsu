<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Visitor" : "Add Visitor"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="user_id" value="<?php echo $row->user_id; ?>" />
                <?php } ?>
              <input type="hidden" id="user_photo" name="user_photo" value="" />
              <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />
              <input type="hidden" id="user_type" name="user_type" value="50" />
              <input type="hidden" id="user_status" name="user_status" value="2" />
              
              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->user_id; ?>" disabled>
                </div>
                <?php } ?>
                   
                   <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control validate-input" placeholder="Name" name="user_name" value="<?php echo isset($row) ? htmlspecialchars($row->user_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input name</p>
                </div>
                   <div class="form-group">
                  <label>Phone</label>
                  <input type="text" class="form-control validate-input" placeholder="Phone" name="user_phone" value="<?php echo isset($row) ? htmlspecialchars($row->user_phone, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input phone no</p>
                </div>
                                <div class="form-group">
                  <label>Company</label>
                  <input type="text" class="form-control validate-input" placeholder="Company" name="user_visitor_company" value="<?php echo isset($row) ? htmlspecialchars($row->user_visitor_company, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input visitor company</p>
                </div>
                 
                 <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control validate-input" name="user_email" placeholder="Enter email" id="user_email" value="<?php echo isset($row) ? htmlspecialchars($row->user_email, ENT_QUOTES) : ""; ?>" />
                   <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input email</p>
                </div>
                 <div class="form-group">
                  <label for="exampleInputFile">Photo</label>
                  <input type="file" id="photo" name="photo" value="" />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->user_photo ? base_url().$row->user_photo : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->user_photo ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->user_photo) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                </div>                            
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

       
      <!-- /.row -->

    </section>