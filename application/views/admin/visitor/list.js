<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/visitor/get",
        "columns": [
           
            { "data": "user_name" },
            { "data": "user_phone" },
            { "data": "user_email" },
            { "data": "user_visitor_company" }
        ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/visitor/edit?id=" + row.data().user_id;
    } );
   
  })
</script>