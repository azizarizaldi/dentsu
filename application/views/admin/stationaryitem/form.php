<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Stationary" : "Add Stationary"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="item_id" value="<?php echo $row->item_id; ?>" />
                <?php } ?>
                                <input type="hidden" id="item_picture" name="item_picture" value="" />
                <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />

              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->item_id; ?>" disabled>
                </div>
                <?php } ?>
				<div class="row">
				<div class="col-md-6">
                <div class="form-group">
                  <label>Item Description</label>
                  <input type="text" class="form-control validate-input" placeholder="Item Description" name="item_description" value="<?php echo isset($row) ? htmlspecialchars($row->item_description, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input item description</p>
                </div>              
                </div>              
				<div class="col-md-6">
                <div class="form-group">
                  <label>Item Category</label>
                  <select class="form-control  validate-input" name="item_category"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a category ---</option>
                    <?php foreach($categories as $category) { ?>
                    <option value="<?php echo $category->category_id; ?>" <?php if (isset($row) && $category->category_id == $row->item_category) { echo "selected"; } ?>><?php echo htmlspecialchars($category->category_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
					<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a item category</p>  
                </div>
                </div>
				</div>
				<div class="row">
                <div class="col-md-6">
				<div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control validate-input" placeholder="Price" name="item_price" value="<?php echo isset($row) ? htmlspecialchars($row->item_price, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input item price</p>
                </div>  
                </div>  
                <div class="col-md-6">
                <div class="form-group">
                  <label>Stock</label>
                  <input type="text" class="form-control validate-input" placeholder="Stock" name="item_stock" value="<?php echo isset($row) ? htmlspecialchars($row->item_stock, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input item stock</p>
                </div>
                </div>
				</div>
				<div class="row">
				<div class="col-md-6">
                <div class="form-group">
                  <label>Unit Measure</label>
                  <input type="text" class="form-control validate-input" placeholder="Unit Measure" name="item_unit_measure" value="<?php echo isset($row) ? htmlspecialchars($row->item_unit_measure, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input unit measure</p>
                </div>  
                </div>  
				<div class="col-md-6">
                  <div class="form-group">
                  <label>Ideal Stock</label>
                  <input type="text" class="form-control validate-input" placeholder="Ideal Stock" name="item_ideal_stock" value="<?php echo isset($row) ? htmlspecialchars($row->item_ideal_stock, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input ideal stock</p>
                </div> 
                </div> 
                </div>
				<div class="row">
				  <div class="col-md-6">
                  <div class="form-group">
                  <label>Min. Stock</label>
                  <input type="text" class="form-control validate-input" placeholder="Min. Stock" name="item_min_stock" value="<?php echo isset($row) ? htmlspecialchars($row->item_min_stock, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input min. stock</p>
                </div>
                </div>
			  <div class="col-md-6">
                <div class="form-group">
                  <label>Item Picture</label>
                  <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->item_picture ? base_url().$row->item_picture : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->item_picture ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->item_picture) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select item picture</p> 
                </div>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>