    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Stationary Item List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php $stationary_category =  $this->db->get("stationary_category")->result(); ?>
				<div class="form-group">
                  <div class="row">
                     <div class="col-md-4">
                        <label>Category</label>
                        <select class="form-control" id="categoryID" style="width: 100%;">
                           <option value="">Category List</option>
						   <?php
								foreach($stationary_category as $row) { ?>
								<option value="<?=$row->category_id?>"><?=$row->category_name?></option>
								<?php }
						   ?>
                        </select>
                     </div>
                     <div class="col-md-4">
                     </div>					 
                     <div class="col-md-4">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" onclick="pdf()" class="btn btn-danger pull-right"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Print to PDF</button>
						  <!--button class="btn btn-danger pull-right" style="margin-right:5px"><i class="fa fa-file-pdf-o"></i> Print to Excel</button-->
						  <a style="margin-right:5px" href="<?php echo base_url(); ?>admin/stationaryitem/add" class="btn btn-primary pull-right">New Item</a>
						</div>
                     </div>
                  </div>
               </div>
				<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Ideal Stock</th>
                    <th>Min. Stock</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Ideal Stock</th>
                    <th>Min. Stock</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <div class="modal modal-info fade" id="modal-info" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Select a driver</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Start Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start date driver">
                </div>
                <div class="form-group">
                  <label>End Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker2" placeholder="Fill empty if car always drive by him">
                </div>
                <div class="form-group">
                  <label>Driver</label>
                  <select class="form-control" id="driverId">
                     <option value="">--- Select a driver ---</option>
                    <?php foreach($drivers as $user) { ?>
                    <option value="<?php echo $user->user_id; ?>"><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" id="btnClose">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:setDriver($('#datepicker1').val(), $('#datepicker2').val(), $('#driverId').val());">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
