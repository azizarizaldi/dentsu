<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
    $("#categoryID").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>stationaryitem/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "categoryId": $("#categoryID").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "item_id"
            }, {
                "data": "description"
            }, {
                "data": "category_name"
            }, {
                "data": "item_price"
            }, {
                "data": "stock"
            }, {
                "data": "idealstock"
            }, {
                "data": "minstock"
            }],
            "order": [
                [1, 'asc']
            ]
        });

		$("#categoryID").on("change", function() {
			dt.ajax.reload();
		});
		
        $('#example1 tbody').on('click', 'tr', function() {

            var tr = $(this).closest('tr');
            var row = dt.row(tr);

            location.href = "<?php echo base_url(); ?>admin/stationaryitem/edit?id=" + tr.find('td:first').html();
        });
    });
	
	function pdf() {
		var keyword 	= $('.dataTables_filter input').val();
		var categoryID 	= $("#categoryID").val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_stationary_item?keyword='+keyword+'&categoryID='+categoryID+'&number='+number,'_blank');		
	}
</script>