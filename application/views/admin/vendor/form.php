<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php echo ($action == "edit") ? "Edit Vendor" : "Add Vendor"; ?>
          </h3>
        </div>
        <form id="form_input" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?=$action?>"/>
          <?php if ($action == "edit") { ?>
          <input type="hidden" name="vendor_id" value="<?php echo $row->vendor_id; ?>" />
          <?php } ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ID Vendor</label>
                  <?php if ($action == "edit") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="vendor_id_text" value="<?php printf("%05d", $row->vendor_id);?>"
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php }
                  else if ($action == "add") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="vendor_id_text" value=""
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select id="id_category" style="width:100%;" class="form-control validate-input select2" name="id_category"<?php if (! $canModify) { echo " disabled"; }?>>
                        <option value="">--- Select a category ---</option>
                        <?php foreach($data_category as $category) { ?>
                        <option value="<?php echo $category->id; ?>" <?php if (isset($row) && $category->id == $row->id_category) { echo "selected"; } ?>><?php echo htmlspecialchars($category->name, ENT_QUOTES); ?></option>
                        <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a category</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Vendor Name</label>
                  <input type="text" class="form-control validate-input" id="vendor_name" name="vendor_name" placeholder="Vendor Name" value="<?php echo isset($row) ? htmlspecialchars($row->vendor_name, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input vendor name</p>              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>NPWP No.</label>
                  <input type="text" class="form-control validate-input" id="npwp" name="npwp" placeholder="NPWP No..." value="<?php echo isset($row) ? htmlspecialchars($row->npwp, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input npwp number</p>              
                </div>
              </div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Person in charge</label>
                  <input type="text" class="form-control validate-input" id="contact_person" name="contact_person" placeholder="Person in charge..." value="<?php echo isset($row) ? htmlspecialchars($row->contact_person, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input Person in charge</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control validate-input" id="email" name="email" placeholder="Email..." value="<?php echo isset($row) ? htmlspecialchars($row->email, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input email</p>              
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone (Primary)</label>
                  <input type="text" class="form-control validate-input" id="phone_primary" name="phone_primary" placeholder="Phone (Primary)..." value="<?php echo isset($row) ? htmlspecialchars($row->phone_primary, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input phone primary</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone (Alternative)</label>
                  <input type="text" class="form-control" id="phone_alternative" name="phone_alternative" placeholder="Phone (Alternative)..." value="<?php echo isset($row) ? htmlspecialchars($row->phone_alternative, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" id="address" name="address" rows="3" placeholder="Address..."><?php echo isset($row) ? htmlspecialchars($row->address, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Note</label>
                  <textarea class="form-control" id="note" name="note" rows="3" placeholder="Address..."><?php echo isset($row) ? htmlspecialchars($row->note, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Attachment</label>
                  <input type="file" id="file_attachment" name="file_attachment" >
                  <?php
                  if($action == 'edit') {
                    if(!empty($row->file_attachment)) { ?>
                    Previous File : <a target="blank" href="<?=base_url().'uploads/vendor/'.$row->file_attachment?>">Click Here!</a>
                  <?php }
                 }?>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">
                <?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
              <?php if ($action == "edit") { ?>
              &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              <?php } ?>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>