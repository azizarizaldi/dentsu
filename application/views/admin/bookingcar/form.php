<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $action=="add" ? "Create Order Car" : "Edit Order Car"; ?></h3>
            </div>
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
                <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
                <?php } ?>
                <input type="hidden" id="order_sender_latitude" name="order_sender_latitude" value="<?php if (isset($row) && $row->order_sender_latitude) { echo $row->order_sender_latitude; }; ?>" />
                <input type="hidden" id="order_sender_longitude" name="order_sender_longitude" value="<?php if (isset($row) && $row->order_sender_latitude) { echo $row->order_sender_longitude; }; ?>" />
                <input type="hidden" id="order_receiver_latitude" name="order_receiver_latitude" value="<?php if (isset($row) && $row->order_receiver_latitude) { echo $row->order_receiver_latitude; }; ?>" />
                <input type="hidden" id="order_receiver_longitude" name="order_receiver_longitude" value="<?php if (isset($row) && $row->order_receiver_latitude) { echo $row->order_receiver_longitude; }; ?>" />
                <input type="hidden" id="order_photo" name="order_photo" value="" />
                <input type="hidden" name="order_name" value="Booking Car" />
                <div class="box-body">
                    <?php if ($action == "edit") { ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Order ID</label>
                        <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
                    </div>
                    <?php } ?>
					<div class="row">
					<div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Car</label>
						<button style="width:30%" type="button" class="btn btn-block btn-primary" onclick="javascript:car()"><?php echo $action == "edit" ? "Change Car" : "Find car"; ?></button>
                        <div id="dvCar">
                            <?php if ($action == "edit") {
                               printf("%s %s %s %s %s", $row->car_no, $row->car_brand, $row->car_model, $row->order_start_date_fmt,  $row->order_end_date_fmt); 
                            }?>
                        </div>
                        <input class="validate-input" type="hidden" id="order_ref" name='order_ref' value="<?php echo isset($row) ? htmlspecialchars($row->order_ref, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />
                        <input type="hidden" id="order_start_date" name='order_start_date' value="<?php echo isset($row) ? htmlspecialchars($row->order_start_date_fmt, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />
                        <input type="hidden" id="order_end_date" name='order_end_date' value="<?php echo isset($row) ? htmlspecialchars($row->order_end_date_fmt, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />

                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a car</p>              
                    </div>
                    </div>
					<div class="col-md-6">
                    <div class="form-group">
                        <label>Book a car</label>
                        <select id="sender" style="width:100%;" class="form-control validate-input select2" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                            <option value="">--- Select a sender ---</option>
                            <?php foreach($senders as $user) { ?>
                            <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
                    </div>
                    </div>
					</div>
                    <?php if ($canModify) { ?>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Pickup Location</label>
                        <div class="radio">
                            <label>
                                <input type="radio" id="pickupLocation" name="pickupLocation" value="1" <?php echo ! isset($row) || $row->order_sender_latitude == 0 ? "checked" : ""; ?>> At Office
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" id="pickupLocation" name="pickupLocation" value="2" <?php echo isset($row) && $row->order_sender_latitude != 0 ? "checked" : ""; ?>> At Location
                            </label>
                        </div>
                        <div id="dvPickUpLocation">
                            <div>Enter a location: <input id="mapPickupInput" type="text" placeholder="Enter a location" style="width: 250px;"></div>
                            <div id="mapPickup" style="width: 100%;height: 300px;"></div>
                            <textarea class="form-control" name="order_sender_address" id="order_sender_address" rows="3" placeholder="Pickup Location address ..."><?php echo isset($row) ? htmlspecialchars($row->order_sender_address, ENT_QUOTES) : ""; ?></textarea>
                        </div>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessageMarkerPickup">Please select pickup location</p>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Destination</label>                  
                        <div>Enter a location: <input id="mapInput" type="text" placeholder="Enter a location" style="width: 250px;"></div>
                        <div id="map" style="width: 100%;height: 300px;margin-top:20px"></div>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessageMarker">Please select destination map</p>
                        <br />
                        <textarea class="form-control validate-input" name="order_receiver_address" id="order_receiver_address" rows="3" placeholder="Destination Address ..."><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?></textarea>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input destination address</p>              
                    </div>                    
					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputEmail1">Detail Destination</label>
							<textarea class="form-control validate-input" name="order_receiver_address_note" rows="3" placeholder="Detail Destination..."><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address_note, ENT_QUOTES) : ""; ?></textarea>
							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input Detail Destination</p>              
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputEmail1">Client Name</label>
							<input type="text" class="form-control validate-input" id="order_receiver_name" name="order_receiver_name" placeholder="Client Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input client name</p>              
						</div>
						                    <div class="form-group">
                        <input type="text" class="form-control validate-input" id="order_receiver_phone" name="order_receiver_phone" placeholder="No of passenger" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_phone, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input no of passenger</p>              
                    </div>

						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
                    <?php if ($canModify) { ?>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Service</label>
                        <?php
                        $val = isset($row)  ? $row->order_priority : 1;
                        for($i=0; $i < count($services); $i++) {                                   
                        ?>
                        <div class="radio">
                            <label>
                                <input type="radio" name="order_priority" value="<?php echo $i+1; ?>" <?php echo $val == $i+1 ? "checked" : ""; ?>><?php echo $services[$i]; ?>
                            </label>
                        </div>
                         <?php } ?>
                    </div>
                    </div>
                    <?php } ?>
					<div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Note</label>
                        <textarea class="form-control" id="order_note" name="order_note" rows="3" placeholder="Note..."><?php echo isset($row) ? htmlspecialchars($row->order_note, ENT_QUOTES) : ""; ?></textarea>
                    </div>
                    </div>
                    </div>
                </div>
                <?php if ($canModify) { ?>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
                </div>
                <?php } ?>
            </form>
          </div>
        </div>
    </div>
</section>
<div class="modal modal-info fade" id="modal-info" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Select a car</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">                    
                    <label>Start</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation1" readonly>
                    </div>    
                </div>
                <div class="form-group">                    
                    <label>End</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation2" readonly>
                    </div>    
                </div>
                <div class="form-group">
                    <label>Available Car</label>
                    <div id="availCar"></div>
                    <label>Suggestion Car</label>
                    <div id="suggestCar"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" id="btnClose">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:save();">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    var markerPickUp = null;
    var marker;
    
    function initMap() {
        var geocoder = new google.maps.Geocoder;
        
        // pickup location
        
        var mapPickup = new google.maps.Map(document.getElementById('mapPickup'), {
            <?php if (isset($row) && isset($row->order_sender_latitude)
                      && isset($row->order_sender_longitude)
                      && $row->order_sender_latitude
                      && $row->order_sender_longitude) { ?>
                      center: {lat: <?php echo $row->order_sender_latitude; ?>, lng: <?php echo $row->order_sender_longitude; ?>},
            <?php } else { ?>
                center: {lat: -6.1751, lng: 106.8650},
            <?php } ?>
          zoom: 16
        });
        
        google.maps.event.addListenerOnce(mapPickup, 'idle',
                                          function()
                                          {
                                                <?php if (! isset($row) || $row->order_sender_latitude == 0) { ?>
                                                $("#dvPickUpLocation").hide();
                                                <?php } else { ?>
                                                var position = new google.maps.LatLng(<?php echo $row->order_sender_latitude; ?>, <?php echo $row->order_sender_longitude; ?>);
                                                markerPickUp = new google.maps.Marker({
                                                    position: position,
                                                    map: mapPickup});
                        
                                                mapPickup.panTo(position);
                                                <?php } ?>
                                          });
        
        var mapPickupInput = document.getElementById('mapPickupInput');
        var autocompletePickup = new google.maps.places.Autocomplete(mapPickupInput);
        
        autocompletePickup.setComponentRestrictions({'country': ['id']});
        autocompletePickup.bindTo('bounds', mapPickup);
        autocompletePickup.addListener('place_changed', function() {
            var place = autocompletePickup.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            
            if (place.geometry.viewport) {
                mapPickup.fitBounds(place.geometry.viewport);
            }
            else {
                mapPickup.setCenter(place.geometry.location);
                mapPickup.setZoom(17);  // Why 17? Because it looks good.
            }
          
            if (markerPickUp != null)
            {
                markerPickUp.setMap(null);
            }
            
            var address = place.formatted_address;
            if (place.name)
            {
                address = place.name + " " + address;
            }
            
            $("#mapPickupInput").val("");
            $("#order_sender_address").val(address);
            $("#order_sender_latitude").val(place.geometry.location.lat());
            $("#order_sender_longitude").val(place.geometry.location.lng());
            
            markerPickUp = new google.maps.Marker({
                position: place.geometry.location,
                map: mapPickup
            });
            mapPickup.panTo(place.geometry.location);
        });
        
        mapPickup.addListener('click', function(e) {
            $("#errorMessageMarkerPickup").hide();
            if (markerPickUp != null)
            {
                markerPickUp.setMap(null);
            }
            $("#order_sender_latitude").val(e.latLng.lat());
            $("#order_sender_longitude").val(e.latLng.lng());
            
            markerPickUp = new google.maps.Marker({
                position: e.latLng,
                map: mapPickup
            });
            
             geocoder.geocode({'location': e.latLng}, function(results, status) {
                    if (status === 'OK')
                    {
                        if (results[0]) {
                            $("#order_sender_address").val(results[0].formatted_address);
                        } 
                    }
            });
        });
        
        // map destination
        
        var map = new google.maps.Map(document.getElementById('map'), {
            <?php if (isset($row) && isset($row->order_receiver_latitude)
                      && isset($row->order_receiver_longitude)
                      && $row->order_receiver_latitude
                      && $row->order_receiver_longitude) { ?>
                      center: {lat: <?php echo $row->order_receiver_latitude; ?>, lng: <?php echo $row->order_receiver_longitude; ?>},
            <?php } else { ?>
                center: {lat: -6.1751, lng: 106.8650},
            <?php } ?>
          zoom: 16
        });
        
        var mapInput = document.getElementById('mapInput');
        var autocomplete = new google.maps.places.Autocomplete(mapInput);
        
        autocomplete.setComponentRestrictions({'country': ['id']});
        autocomplete.bindTo('bounds', map);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            }
            else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
          
            if (marker != null)
            {
                marker.setMap(null);
            }
            
            var address = place.formatted_address;
            if (place.name)
            {
                address = place.name + " " + address;
            }
            
            $("#mapInput").val("");
            $("#order_receiver_address").val(address);
            $("#order_receiver_latitude").val(place.geometry.location.lat());
            $("#order_receiver_longitude").val(place.geometry.location.lng());
            
            marker = new google.maps.Marker({
                position: place.geometry.location,
                map: map
            });
            map.panTo(place.geometry.location);
        });
        
        google.maps.event.addListenerOnce(map, 'idle', function(){
            <?php if (isset($row) && isset($row->order_receiver_latitude)
                      && isset($row->order_receiver_longitude)
                      && $row->order_receiver_latitude
                      && $row->order_receiver_longitude) { ?>
                      
                        var position = new google.maps.LatLng(<?php echo $row->order_receiver_latitude; ?>, <?php echo $row->order_receiver_longitude; ?>);
                        marker = new google.maps.Marker({
                        position: position,
                        map: map});
                        
                        map.panTo(position);
            <?php } ?>
        });
        
        map.addListener('click', function(e) {
            $("#errorMessageMarker").hide();
            if (marker != null)
            {
                marker.setMap(null);
            }
            $("#order_receiver_latitude").val(e.latLng.lat());
            $("#order_receiver_longitude").val(e.latLng.lng());
            
            marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });
            
            geocoder.geocode({'location': e.latLng}, function(results, status) {
                    if (status === 'OK')
                    {
                        if (results[0]) {
                            $("#order_receiver_address").val(results[0].formatted_address);
                        } 
                    }
            });
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item("gMapKey"); ?>&libraries=places&callback=initMap" async defer></script>
