<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   $("#carID").select2();
	
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>bookingcar/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status"		: $("#order_status").val(),
           "orderDate"	: $("#datepicker").val(),
           "carId"		: $("#carID").val()
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "receiver" },
            { "data": "courier" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                  var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";

                  if (data.order_status == 1)
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:approve("+data.order_id+");\">Approve</button>";
                  }

                  if ((data.order_status == 2) || (data.order_status == 3))
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:done("+data.order_id+");\">Done</button>";
                  }

                  if ((data.order_status == 3) || (data.order_status == 4))
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-default\" onclick=\"javascript:showMap("+data.order_id+");\">Lacak</button>";
                  }

                  if ((data.order_status != 4) && (data.order_status != 5))
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                  }

                  return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
  
    
    $("#order_status").on("change", function() {
		dt.ajax.reload();
	});
  
	$("#datepicker").on("change", function() {
		dt.ajax.reload();
	});

	$("#carID").on("change", function() {
		dt.ajax.reload();
	});

  var orderId;  
  function showMap(id, lat, lng)
    {
        orderId = id;
      
        $("#modal-default").modal('show');
        
    }
    
    $('#modal-default').on('shown.bs.modal', function (e) {
      initMap();
      
      google.maps.event.trigger(map, "resize");
      
      reloadMap();

    });
    
    var senderMarker = null;
    var receiverMarker = null;
    var pickupMarker = null;
    function reloadMap()
    {
      $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>bookingcar/getById"
            ,data: {"orderId": orderId}
            ,success: function (data)
            {
              
              if (senderMarker != null)
              {
                senderMarker.setMap(null);
              }
              
              if (receiverMarker != null)
              {
                receiverMarker.setMap(null);
              }
              
              
              if (data.order_sender_latitude != 0)
              {
                var position = new google.maps.LatLng(data.order_sender_latitude, data.order_sender_longitude);  
                senderMarker = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "<?php echo base_url(); ?>assets/app/engineer.png"});
                    
              }

              var position = new google.maps.LatLng(data.order_receiver_latitude, data.order_receiver_longitude);  
              receiverMarker = new google.maps.Marker({
                        position: position,
                        map: map});
              
              if (data.order_pickup_latitude != null)
              {
                var position = new google.maps.LatLng(data.order_pickup_latitude, data.order_pickup_longitude);  
                senderMarker = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "<?php echo base_url(); ?>assets/app/car-compact.png"});
                
                map.panTo(position);
              }
              else
              {                
                  map.panTo(position);
              }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
  
  function approve(oId)
  {
    if (confirm("Are you sure to approve booking car?"))
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>bookingcar/approve"
            ,data: {"orderId": oId}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
  }
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/bookingcar/edit?id=" + orderId;
    }
    
    function done(id)
    {
      if (confirm("Are you sure to complete booking car?"))
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>bookingcar/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

        //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>