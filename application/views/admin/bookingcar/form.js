<script>
    var startDate = "";
    var endDate = "";
    
    $(function () {
        "use strict";
        
        $("#sender").select2();

        // date range booking car
        
        var date = new Date();
        date.setMinutes(0);

        $('#reservation1').daterangepicker({ timePicker: true, singleDatePicker: true, timePickerIncrement: 30, timePicker24Hour:true, locale: { format: 'DD/MM/YYYY HH:mm' } });       
        $('#reservation1').on('cancel.daterangepicker', function(ev, picker) {
            $('#reservation1').val(startDate);
        });
        $('#reservation1').on('apply.daterangepicker', function(ev, picker) {
            startDate = $('#reservation1').val();
            getCar();
        });
        $('#reservation1').on('show.daterangepicker', function(ev, picker) {
            var h = getHour($('#reservation1').val())+1;
            $(".hourselect").find('option:nth-child(' + h + ')').prop('selected',true);   

            var d = getDate($('#reservation1').val()); 
            $("td[data-title^='r']").each(
                function() {
                    if ($(this).is(':visible'))
                    {
                        var dataTitle = $(this).attr('data-title');
                        var re1 = /r[0-9]c[0-9]/
                        var re2 = /off/
                        if (! re2.test($(this).attr('class')) && re1.test(dataTitle) && $(this).html() == d)
                        {
                            $(this).trigger("mousedown.daterangepicker")
                        }
                    }
                }
            ); 
        });
        $('#reservation1').on('outsideClick.daterangepicker', function(ev, picker) {
            $('#reservation1').val(startDate);
        });
        
        startDate = dateFormat(date);
        $("#reservation1").val(startDate);
 
        $('#reservation2').daterangepicker({ timePicker: true, singleDatePicker: true, timePickerIncrement: 30, timePicker24Hour:true, locale: { format: 'DD/MM/YYYY HH:mm' } });
        $('#reservation2').on('cancel.daterangepicker', function(ev, picker) {
            $('#reservation2').val(endDate);
        });
        $('#reservation2').on('apply.daterangepicker', function(ev, picker) {
            endDate = $('#reservation2').val();
            getCar();
        });
        $('#reservation2').on('outsideClick.daterangepicker', function(ev, picker) {
            $('#reservation2').val(endDate);
        });
        $('#reservation2').on('show.daterangepicker', function(ev, picker) {
            var h = getHour($('#reservation2').val())+1;
            $(".hourselect").find('option:nth-child(' + h + ')').prop('selected',true);   

            var d = getDate($('#reservation2').val()); 
            $("td[data-title^='r']").each(
                function() {
                    if ($(this).is(':visible'))
                    {
                        var dataTitle = $(this).attr('data-title');
                        var re1 = /r[0-9]c[0-9]/
                        var re2 = /off/
                        if (! re2.test($(this).attr('class')) && re1.test(dataTitle) && $(this).html() == d)
                        {
                            $(this).trigger("mousedown.daterangepicker")
                        }
                    }
                }
            );       
        });
        
        date.setHours(23);
        date.setMinutes(0);
        
        endDate = dateFormat(date);
        $("#reservation2").val(endDate);
        
        // pickup location
        
        $("input[id=pickupLocation]").each(function() {
                                $(this).change(function() {
                                                if ($(this).val() == 1)
                                                {
                                                    $("#dvPickUpLocation").hide();
                                                }
                                                else
                                                {
                                                    $("#dvPickUpLocation").show();
                                                }
                                               });
                              });
        
        // submit
        
        var input = $('.validate-input');
        $('#form_input').on('submit',function(){            
                var check = true;

                for(var i=0; i<input.length; i++) {
                    if(validate(input[i]) == false){
                        showValidate(input[i]);
                        check=false;
                    }
                }
                
                if ($("input[id=pickupLocation]:checked").val() == 2) {
                    if (markerPickUp == null) {
                        $("#errorMessageMarkerPickup").show();
                        check = false;
                    }
                }
                
                if (marker == null) {
                    $("#errorMessageMarker").show();
                    check = false;
                }
                
                return false;
            
            });
    });

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }
    
    function validate (input) {
          return $(input).val().trim() != '';
    }

    function car()
    {
        $('#modal-info').on('shown.bs.modal',
                               function() {
                                    if ($("#order_start_date").val() != "")
                                    {

                                        startDate = $("#order_start_date").val();

                                        $('#reservation1').data('daterangepicker').setStartDate(startDate)
                                        $("#reservation1").val($("#order_start_date").val());
                                    }
                                    if ($("#order_end_date").val() != "")
                                    {
                                        endDate = $("#order_end_date").val()

                                        $('#reservation2').data('daterangepicker').setStartDate(endDate)
                                        $("#reservation2").val($("#order_end_date").val());
                                    }
                                    getCar();
                                        
                                });
        $("#modal-info").modal("show");
    }
    
    function getCar()
    {
        $.ajax({
             type: "GET"
            ,url: "<?php echo base_url(); ?>bookingcar/getBooking"
            ,data: {"date1": $('#reservation1').val(), "date2": $('#reservation2').val()<?php if (isset($row)) {  printf(", \"bookingId\": %d", $row->order_id); } ?>}
            ,success: function (data)
            {
                if (! data.status)
                {
                    $("#dvCar").html("");
                    $("#order_ref").val("");
                    
                    alert(data.message);
                    return;
                }
                
                if (data.availables.length == 0)
                {
                    $("#availCar").html("No car available for this time");
                }
                else
                {
                    var html = "";
                    for(var i=0; i < data.availables.length; i++)
                    {
                        html += '<div class="radio">';
                        html += '   <label>';
                        html += '        <input type="radio" name="car_id" id="car_id" value="'+data.availables[i].car_id+'" />'+data.availables[i].car_no
                            + ' ' + data.availables[i].car_brand + ' '  + data.availables[i].car_model;
                        html += '    </label>';
                        html += '</div>';
                    }
                    
                    $("#availCar").html(html);  
                }

                if (data.suggestions.length == 0)
                {
                    $("#suggestCar").html("No car suggest for this time");
                }
                else
                {
                    var html = "";
                    for(var i=0; i < data.suggestions.length; i++)
                    {
                        if (data.suggestions[i].isstart == 1)
                        {
                            html += '<div class="radio">';
                            html += '   <label>';
                            html += '        <input type="radio" name="car_id" id="car_id" value="'+data.suggestions[i].car_id+'|'+data.suggestions[i].startdate_fmt+'|'+data.suggestions[i].order_start_date_fmt+'" />'+data.suggestions[i].car_no
                                        + ' ' + data.suggestions[i].car_brand + ' '  + data.suggestions[i].car_model + '   ';
                            html += data.suggestions[i].startdate_fmt + " - " + data.suggestions[i].order_start_date_fmt;
                            html += '    </label>';
                            html += '</div>';
                        }
                        
                        if (data.suggestions[i].isend == 1)
                        {
                            html += '<div class="radio">';
                            html += '   <label>';
                            html += '        <input type="radio" name="car_id" id="car_id" value="'+data.suggestions[i].car_id+'|'+data.suggestions[i].order_end_date_fmt+'|'+data.suggestions[i].enddate_fmt+'" />'+data.suggestions[i].car_no
                                        + ' ' + data.suggestions[i].car_brand + ' '  + data.suggestions[i].car_model + '   ';
                            html += data.suggestions[i].order_end_date_fmt + " - " + data.suggestions[i].enddate_fmt;
                            html += '    </label>';
                            html += '</div>';
                            
                        }
                    }
                    
                    $("#suggestCar").html(html);
                }

                    <?php if (isset($row)) { ?>
                        $("input[id='car_id']").each(function() {
                            $(this).prop("checked", $(this).val() == <?php echo $row->order_ref; ?>);
                        }
                        );
                    <?php } ?> 
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
    function save()
    {
        var carId = $("input[id=car_id]:checked").val();
        if (! carId)
        {
            alert("Please select a car");
            return;
        }
        
        var carIds = carId.split("|");
        if (carIds.length == 1)
        {
            $("#order_start_date").val($("#reservation1").val() );
            $("#order_end_date").val($("#reservation2").val() );
            $("#order_ref").val(carId);
        
            var carname = $("input[id=car_id]:checked").parent('label').text();
            var dvCarHtml = carname + " " + $("#reservation1").val() + "-" + $("#reservation2").val();
        
            $("#dvCar").html(dvCarHtml);
        }
        else
        {
            $("#order_start_date").val(carIds[1]);
            $("#order_end_date").val(carIds[2]);
            $("#order_ref").val(carIds[0]);
        
            var carname = $("input[id=car_id]:checked").parent('label').text();
            var dvCarHtml = carname;
            $("#dvCar").html(dvCarHtml);
        }
        
        $("#modal-info").modal("hide");
    }
    
    function getDate(s)
    {

        var ss = s.split(" ");
        var sss = ss[0].split("/")
 
        return sss[0]*1;
    }

    function getHour(s)
    {
        var ss = s.split(" ");
        var sss = ss[1].split(":")

        return sss[0]*1;
    }

    function leadingZero(n)
    {
        if (n < 10) return '0'+n;
        
        return n;
    }
    
    function dateFormat(date)
    {
        return leadingZero(date.getDate()) + "/" + leadingZero(date.getMonth()+1) + "/" + leadingZero(date.getFullYear()) + " " + leadingZero(date.getHours()) + ":" + leadingZero(date.getMinutes());
    }
    
    $('#form_input').on('submit',function(){
        var input = $('.validate-input');
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if ($("input[id=pickupLocation]:checked").val() == 2)
        {
            if (markerPickUp == null)
            {
                $("#errorMessageMarkerPickup").show();
                check = false;
            }
        }
        
        if (marker == null)
        {
            $("#errorMessageMarker").show();
            check = false;
        }
       
        if (check)
        {
            post();
        }

        return false;
        
        });
    
    function post()
    {
        $.ajax({
             type: "POST"
            ,url: "<?php echo base_url(); ?>bookingcar/create"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/bookingcar";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        });

        $("#dvCar").hide();
        $("#sender").val("").trigger("change");

        if (marker != null)
        {
            marker.setMap(null);
            marker = null;
        }

        if (markerPickUp != null)
        {
            markerPickUp.setMap(null);
            markerPickUp = null;
        }

        $("#order_sender_address").val("");
        $("#order_note").val("");
    }
    
</script>