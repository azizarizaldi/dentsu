<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php echo ($action == "edit") ? "Edit document" : "Add document"; ?>
          </h3>
        </div>
        <form id="form_input" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?=$action?>"/>
          <?php if ($action == "edit") { ?>
          <input type="hidden" name="document_id" value="<?php echo $row->document_id; ?>" />
          <?php } ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ID document</label>
                  <?php if ($action == "edit") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="document_id_text" value="<?php printf("%05d", $row->document_id);?>"
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php }
                  else if ($action == "add") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="document_id_text" value=""
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select id="id_category" style="width:100%;" class="form-control validate-input select2" name="id_category"<?php if (! $canModify) { echo " disabled"; }?>>
                        <option value="">--- Select a category ---</option>
                        <?php foreach($data_category as $category) { ?>
                        <option value="<?php echo $category->id; ?>" <?php if (isset($row) && $category->id == $row->id_category) { echo "selected"; } ?>><?php echo htmlspecialchars($category->name, ENT_QUOTES); ?></option>
                        <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a category</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Document Name</label>
                  <input type="text" class="form-control validate-input" id="document_name" name="document_name" placeholder="document Name" value="<?php echo isset($row) ? htmlspecialchars($row->document_name, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input document name</p>              
                </div>
              </div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Validate Period</label>
                  <input type="text" class="form-control" name="range_date" id="range_date" placholder="insert your range date...">
                  <br/>
                  <label>Attachment Doc</label>
                  <input type="file" id="file_attachment" name="file_attachment" >
                  <?php
                  if($action == 'edit') {
                    if(!empty($row->file_attachment)) { ?>
                    Previous File : <a target="blank" href="<?=base_url().'uploads/document/'.$row->file_attachment?>">Click Here!</a>
                  <?php }
                 }?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Terms & Condition</label>
                  <textarea class="form-control" id="terms_condition" name="terms_condition" rows="6" placeholder="terms & condition..."><?php echo isset($row) ? htmlspecialchars($row->terms_condition, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Note</label>
                  <textarea class="form-control" id="note" name="note" rows="3" placeholder="Notes..."><?php echo isset($row) ? htmlspecialchars($row->note, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Vendor</label>
                  <select id="vendor_id" style="width:100%;" class="form-control select2" name="vendor_id"<?php if (! $canModify) { echo " disabled"; }?>>
                        <option value="">--- Select a vendor ---</option>
                        <?php
                        $data_vendor = $this->db->get("vendor")->result();
                        foreach($data_vendor as $vendor) { ?>
                        <option value="<?php echo $vendor->vendor_id; ?>" <?php if (isset($row) && $vendor->vendor_id == $row->vendor_id) { echo "selected"; } ?>><?php echo htmlspecialchars($vendor->vendor_name, ENT_QUOTES); ?></option>
                        <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Reminder Days</label>
                  <select id="reminder_day" style="width:100%;" class="form-control select2" name="reminder_day"<?php if (! $canModify) { echo " disabled"; }?>>
                    <option value="">--- Select a reminder days ---</option>
                    <option value="15"  <?php if (isset($row) && $row->reminder_day == "15") { echo "selected"; } ?> >15 Days before expired</option>
                    <option value="30" <?php if (isset($row) && $row->reminder_day == "30") { echo "selected"; } ?>>30 Days before expired</option>
                    <option value="60" <?php if (isset($row) && $row->reminder_day == "60") { echo "selected"; } ?>>60 Days before expired</option>
                    <option value="90" <?php if (isset($row) && $row->reminder_day == "90") { echo "selected"; } ?>>90 Days before expired</option>
                    <option value="180" <?php if (isset($row) && $row->reminder_day == "180") { echo "selected"; } ?>>180 Days before expired</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" id="btnSubmit">
                <?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
              <?php if ($action == "edit") { ?>
              &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              <?php } ?>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>

<?php
  $start_date = "";
  $end_date = "";
  if($action == 'edit') {
    $start_date = $row->start_date;
    $end_date   = $row->end_date;  
  }
?>
<script>
$(document).ready(function() {
    var action = "<?=$action?>";
    if(action == 'edit') {
      var startDate = "<?=$start_date?>";
      var endDate   = "<?=$end_date?>";

      $('#range_date').daterangepicker();

      var date_one = new Date(startDate);
      var start = date_one.toLocaleDateString();

      var date_two = new Date(endDate);
      var end = date_two.toLocaleDateString();
      
      // bulan/tanggal/tahun
      $("#range_date").data('daterangepicker').setStartDate(start);
      $("#range_date").data('daterangepicker').setEndDate(end);      
    }
    else {
      $('#range_date').daterangepicker();
    }
});
</script>
)