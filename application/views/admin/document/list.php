<?php
   $check_document = $this->db->query("
      SELECT *
      FROM (
      SELECT send_reminder , document.document_id , document.document_name ,  setting_category.name AS category, vendor.vendor_name , start_date , DATE_ADD(end_date,INTERVAL -reminder_day DAY) AS sendDate , end_date , reminder_day
      FROM document
      INNER JOIN setting_category ON setting_category.id = document.id_category
      INNER JOIN vendor ON vendor.vendor_id = document.vendor_id
      ) getDocument
      WHERE send_reminder = 'yes' and curdate() BETWEEN sendDate AND end_date
  ")->result();
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Document List</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-4">
                <label>Filter By Category</label>
                <select class="form-control" id="id_category" style="width: 100%;">
                  <option value="">Select Category List</option>
                    <?php
                    $this->db->where("type_category","2");
                    foreach($this->db->get('setting_category')->result() as $get) { ?>
                    <option value="<?=$get->id?>">
                      <?=$get->name?>
                    </option>
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-4">
                    <a href="<?php echo base_url(); ?>admin/document/add" class="btn btn-primary" style="margin-top:25px"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add document</a>
<!--                    <button type="button" onclick="printData()" class="btn btn-default" style="margin-top:25px"><i class="fa fa-print"></i> &nbsp;&nbsp;Print Data</button>
-->              </div>
              <div class="col-md-4">
              </div>
            </div>
          </div>

          <table id="example1" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th>ID document</th>
                <th>Category</th>
                <th>Document Name</th>
                <th>Validate Period</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID document</th>
                <th>Category</th>
                <th>Document Name</th>
                <th>Validate Period</th>
              </tr>
            </tfoot>
          </table>
          <hr/>
          <?php
          if(count($check_document) > 0) {
          ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Reminder !</h4>
            <ul>
              <?php
                foreach($check_document as $row) {
                    echo "<li>Your document '".$row->document_name."' will be expired on ".date('l\,jS F Y',strtotime($row->end_date)).'</li>';
                }
              ?>
            </ul>
          </div>
          <?php }?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>