<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>

var dtAddCart = null;
var dtCart = null;

(function ($) {
    "use strict";

    $("#sender").select2();

    <?php if (isset($row)) { 

      printf("loadCart(%d);", $row->order_id);

    } else {
      echo "updateCart();";
    } ?>
    

    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        })

        $("#sender").val("").trigger("change");

        updateCart();
    }
    
    function loadCart(id)
    {
      $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>stationaryorder/getCart"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                var newItemIds = new Array();
                var newItemQtys = new Array();

                for(var i=0; i < data.length; i++)
                {
                  newItemIds.push(data[i].order_stationary_item); 
                  newItemQtys.push(data[i].order_stationary_qty); 
                }

                if (newItemIds.length == 0)
                {
                  $("#itemIds").val("");
                  $("#itemQtys").val("");
                }
                else
                {
                  $("#itemIds").val(newItemIds.join(","));
                  $("#itemQtys").val(newItemQtys.join(","));
                }

                updateCart();
            }
            ,error: function(request, error)
            {
                alert(error);
            }
            ,dataType: "json"
          }
        );
    }

    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>stationaryorder/create"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/stationaryorder";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   

})(jQuery);
   function cart()
   {
     $('#modal-info').on('shown.bs.modal',
                                   function() {

                                    if (dtAddCart != null)
                                    {
                                        dtAddCart.destroy();
                                        $("#example1").empty();
                                    }

      dtAddCart = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "bPaginate": false,
        "paging": false,
        "bInfo": false,
        "iDisplayLength": 1000,
        "searching": false,
        "ajax": {
          url: "<?php echo base_url(); ?>stationaryitem/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "notInIds": $("#itemIds").val()
         } );
      }
        },
        "columns": [
           
            { "data": "description" },
            { "data": "category_name" },
            { "data": "stock" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    return '<div class="input-group">'
          + '<span class="input-group-btn">'
          + '    <button type="button" onClick="javascript:minus('+data.item_id+')" class="btn btn-danger btn-number" data-type="minus" data-field="quant">'
          + '      <span class="glyphicon glyphicon-minus"></span>'
          + '    </button>'
          + '</span>'
          + '<input type="text" id="qty'+data.item_id+'" class="form-control input-number" value="0" min="1" max="'+data.item_stock+'" style="width:50px;">'
          + '<span class="input-group-btn">'
          + '    <button type="button" onClick="javascript:plus('+data.item_id+')" class="btn btn-success btn-number" data-type="plus" data-field="quant">'
           + '       <span class="glyphicon glyphicon-plus"></span>'
           + '   </button>'
          + '</span>'
      + '</div>';

                    
               }
              
            }
        ],
        "order": [[1, 'asc']]
    } );  
                                         
                                    });
    $("#modal-info").modal("show");
   }

  function minus(id)
   {
        var val  = $("#qty"+id).val()*1;

        if ((val-1) < 0) return;

        $("#qty"+id).val(val-1)
   }

  function minusUpdate(id)
   {
        var val  = $("#qtyUpdate"+id).val()*1;

        if ((val-1) < 1) return;

        $("#qtyUpdate"+id).val(val-1)

        addItem(id, val-1);
   }
   

   function plus(id)
   {
        var max = $("#qty"+id).attr("max")*1;
        var val  = $("#qty"+id).val()*1;

        if ((val+1) > max) return;

        $("#qty"+id).val(val+1)
   }

   function plusUpdate(id)
   {
        var max = $("#qtyUpdate"+id).attr("max")*1;
        var val  = $("#qtyUpdate"+id).val()*1;

        if ((val+1) > max) return;

        $("#qtyUpdate"+id).val(val+1)

        addItem(id, val+1);
   }

   function save()
   {
      var check = false;
      $("input[id^=qty]").each(
        function()
        {
            if ($(this).val() > 0)
            {
              var id = $(this).attr("id").substring(3);
              addItem(id, $(this).val());
              check = true;
            }
        }
      );

      if (! check)
      {
        alert("Please select a stationary item");
        return;
      }

      updateCart();
      $("#modal-info").modal("hide");
   }

   function addItem(id, qty)
   {
      if ($("#itemIds").val() == "")
      {
        $("#itemIds").val(id);
        $("#itemQtys").val(qty);
        return;
      }

      var found = false;

      var itemIds = $("#itemIds").val().split(",");
      var itemQtys = $("#itemQtys").val().split(",");

      for(var i=0; i < itemIds.length; i++)
      {
          if (itemIds[i] == id)
          {
              itemQtys[i] = qty;

              $("#itemQtys").val(itemQtys.join(","));

              return;
          }
      }

      $("#itemIds").val($("#itemIds").val()+","+id);
      $("#itemQtys").val($("#itemQtys").val()+","+qty);
   }

   function updateCart()
   {
    if ($("#itemIds").val() == "")
    {
      $("#dvCart").hide();
    }
    else
    {
      $("#dvCart").show();
    }

    if (dtCart != null)
    {
      dtCart.destroy();
      $("#example2").empty();
    }

    dtCart = $('#example2').DataTable( {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "bPaginate": false,
        "paging": false,
        "bInfo": false,
        "iDisplayLength": 1000,
        "retrieve": true,
        "ajax": {
          url: "<?php echo base_url(); ?>stationaryitem/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "inIds": $("#itemIds").val()
         } );
      }
        },
        "columns": [
           
            { "data": "description" },
            { "data": "category_name" },
            { "data": "stock" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                  var qty = getQty(data.item_id);
                    return '<div class="input-group">'
          + '<span class="input-group-btn">'
          + '    <button type="button" onClick="javascript:minusUpdate('+data.item_id+')" class="btn btn-danger btn-number" data-type="minus" data-field="quant">'
          + '      <span class="glyphicon glyphicon-minus"></span>'
          + '    </button>'
          + '</span>'
          + '<input type="text" id="qtyUpdate'+data.item_id+'" class="form-control input-number" value="'+qty+'" min="1" max="'+data.item_stock+'" style="width:50px;">'
          + '<span class="input-group-btn">'
          + '    <button type="button" onClick="javascript:plusUpdate('+data.item_id+')" class="btn btn-success btn-number" data-type="plus" data-field="quant">'
           + '       <span class="glyphicon glyphicon-plus"></span>'
           + '   </button>'
          + '</span>'
      + '</div>';

                    
               }
              
            },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                  return "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:remove("+data.item_id+",);\">Remove</button>";
               }
             }
        ],
        "order": [[1, 'asc']]
    } ); 
   }

   function getQty(id)
   {
      var itemIds = $("#itemIds").val().split(",");
      var itemQtys = $("#itemQtys").val().split(",");

      for(var i=0; i < itemIds.length; i++)
      {
          if (itemIds[i] == id)
          {
            return itemQtys[i];
          }
      }

      return 0;
   }

   function remove(id)
   {
      var itemIds = $("#itemIds").val().split(",");
      var itemQtys = $("#itemQtys").val().split(",");

      var newItemIds = new Array();
      var newItemQtys = new Array();

      var j = 0;
      for(var i=0; i < itemIds.length; i++)
      {
          if (itemIds[i] != id)
          {
            newItemIds[j] =itemIds[i];
            newItemQtys[j] =itemQtys[i];

            j++; 
          }
      }

      if (newItemIds.length == 0)
      {
        $("#itemIds").val("");
        $("#itemQtys").val("");
      }
      else
      {
       $("#itemIds").val(newItemIds.join(","));
       $("#itemQtys").val(newItemQtys.join(","));
      }

     updateCart();
   }
</script>