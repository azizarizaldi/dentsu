<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $action=="add" ? "Create Stationary Order" : "Edit Stationary Order"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
              <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
              <?php } ?>
              <input type="hidden" id="order_name" name="order_name" value="Stationary" />
                <input type="hidden" id="order_photo" name="order_photo" value="" />
              <div class="box-body">
                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Order ID</label>
                  <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
                </div>
                <?php } ?>
                
				<div class="row">
				<div class="col-md-10">
                <div class="form-group">
                  <label>Order By</label>
                  <select id="sender" style="width: 100%"class="form-control  validate-input" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a sender ---</option>
                    <?php foreach($senders as $user) { ?>
                    <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
				  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
                </div>
                </div>
				<div class="col-md-2">
                <div class="form-group">
                  <label>Cart</label>
					<button type="button" class="btn btn-block btn-primary" onclick="javascript:cart()">Add to cart</button>
                </div>
                </div>
                </div>
                <div class="form-group">
					<span id="dvCart">
			  <table id="example2" class="table table-bordered" width="100%">
                <thead>
                  <tr>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Qty</th>
                    <th></th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Qty</th>
                    <th></th>
                  </tr>
                </tfoot>
              </table>
                        </span>
                <input type="hidden" class="validate-input" id="itemIds" name="itemIds" value="" />
                <input type="hidden" id="itemQtys" name="itemQtys" value="" />
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Cart is empty</p>              
                    </div>
                        
                  <?php if ($canModify) { ?>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Delivery</label>
                        <div class="radio">
                            <label>
                                <input type="radio" id="orderDelivery" name="orderDelivery" value="2" <?php echo isset($row) && $row->order_sender_latitude == 2 ? "checked" : ""; ?>> Delivery by Office Boy
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" id="orderDelivery" name="orderDelivery" value="1" <?php echo ! isset($row) || $row->order_sender_latitude == 1 ? "checked" : ""; ?>> Take it by your Self
                            </label>
                        </div>
                    </div>
                    <?php } ?>
              </div>
              <!-- /.box-body -->
<?php if ($canModify) { ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
              </div>
              <?php } ?>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <div class="modal modal-success fade" id="modal-info" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Cart</h4>
            </div>
            <div class="modal-body">          
                <div class="form-group">                    
                    <label>Add to cart</label>
                     <table id="example1" class="table table-bordered" width="100%">
                <thead>
                  <tr>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Qty</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Qty</th>
                  </tr>
                </tfoot>
              </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" id="btnClose">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:save();">Save changes</button>
            </div>
        </div>
    </div>
</div>
    
