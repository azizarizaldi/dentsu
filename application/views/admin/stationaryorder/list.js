<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
var dtCart = null;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>stationaryorder/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status": $("#order_status").val(),
           "orderDate"	: $("#datepicker").val()		   
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
          { "data": "order_created_date" },
            { "data": "sender" },
            { "data": "courier" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";
                    if (data.order_status == 1)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:approve("+data.order_id+");\">Approve</button>";
                    }

                    if ((data.order_status == 2) && (data.order_sender_latitude == 2))
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:pickUp("+data.order_id+",);\">Pick Up</button>";
                    }
                    if (((data.order_status == 3) || (data.order_sender_latitude == 1)) && data.order_status != 4)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:receive("+data.order_id+");\">Done</button>";
                    }

                    if (data.order_status < 4)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                    }

                    if (data.order_status != 5)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-default\" onclick=\"javascript:cart("+data.order_id+");\">Cart</button>";
                    }
                    return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
  
   function approve(oId)
  {
    if (confirm("Are you sure to approve stationary order?"))
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>stationaryorder/approve"
            ,data: {"orderId": oId}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
  } 


    $("#order_status").on("change", function() {
		dt.ajax.reload();
	});
  
	$("#datepicker").on("change", function() {
		dt.ajax.reload();
	});
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/stationaryorder/edit?id=" + orderId;
    }
    
    function receive(id)
    {
      if (confirm("Are you sure to complete stationary order?"))
      {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>stationaryorder/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      }
    }
    
    var orderId;
    function pickUp(id)
    {
        orderId = id;

        $("#modal-pickup").modal("show");
    }

    function changeOfficeBoy(id)
    {
      if (id  == "")
      {
        alert("Please select a office boy");
        return;
      }

        $.ajax(
            {
               type: "POST"
              ,url: "<?php echo base_url(); ?>stationaryorder/pickup"
              ,data: {"orderId": orderId, "officeBoy": id}
              ,success: function (data)
              {
                  alert(data.message)
                  if (data.status)
                  {
                      dt.ajax.reload(function() {
                            dt.columns.adjust()
                        }, false);
                      
                      $("#modal-pickup").modal("hide");   
                      return;
                  }
                  
                  
              }
              ,error: function(request, error)
              {
                  alert(error);
                  clear();
              }
              ,dataType: "json"
            }
          );
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

    function cart(id)
    {
      orderId = id;
      $('#modal-default').on('shown.bs.modal',
                                   function() {
                                      if (dtCart != null)
                                      {
                                        dtCart.destroy();
                                        $("#example2").empty();
                                      }


                                      loadCart();
                                   }
                                   );
      $("#modal-default").modal("show");

    }

    function loadCart()
    {
      dtCart = $('#example2').DataTable( {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "bPaginate": false,
        "bSort" : false,
        "paging": false,
        "bInfo": false,
        "iDisplayLength": 1000,
        "ajax": "<?php echo base_url(); ?>stationaryorder/getCart?dt=1&orderId="+orderId,
        "columns": [
           
            { "data": "description" },
            { "data": "category_name" },
            { "data": "qty" }
        ],
        "order": [[1, 'asc']]
    } ); 
    }

        //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });    
</script>