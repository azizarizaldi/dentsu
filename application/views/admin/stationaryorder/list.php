    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<div class="form-group">
			  <div class="row">
				 <div class="col-md-4">
					<label>Order Date</label>
					<input type="text" class="form-control pull-right" id="datepicker">
				 </div>
				 <div class="col-md-4">
					<label>&nbsp;</label>
				 </div>
				 <div class="col-md-4">
					<label>Status</label>
					<select class="form-control" id="order_status" style="width: 100%;">
					   <option value="">All</option>
					   <option value="1">New</option>
					   <option value="2">Approved</option>
					   <option value="3">Pickup</option>
					   <option value="4">Done</option>
					   <option value="5">Rejected</option>
					</select>
				 </div>
			  </div>
			</div>
			   <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Created Date</th>
                    <th>Sender</th>
                    <th>Delivery</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Created Date</th>
                    <th>Sender</th>
                    <th>Delivery</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Reject</h4>
              </div>
              <div class="modal-body">
                 <div class="form-group">
                  <label for="exampleInputEmail1">Note</label>
                  <textarea class="form-control" id="note" rows="3" placeholder="Note ..."></textarea>

                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:rejectNote($('#note').val());">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal modal-info fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Cart</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <table id="example2" class="table table-bordered" width="100%">
                    <thead>
                      <tr>
                        <th>Item Description</th>
                        <th>Category</th>
                        <th>Qty</th>
                     </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Item Description</th>
                        <th>Category</th>
                        <th>Qty</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal modal-info fade" id="modal-pickup" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Select a office boy</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Office Boy</label>
                  <select class="form-control" id="officeBoyId">
                     <option value="">--- Select a office boy ---</option>
                    <?php foreach($officeboys as $user) { ?>
                    <option value="<?php echo $user->user_id; ?>"><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                </div>
                
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" id="btnClose">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:changeOfficeBoy($('#officeBoyId').val());">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
    
