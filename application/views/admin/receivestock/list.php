    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Receive Stock List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-4">
                        <label>Order No </label>
                        <select class="form-control" id="stationary_order_id" style="width: 100%;">
						   <?php
								foreach($stationary_order as $row) { ?>
								<option value="<?=$row->stationary_order_id?>"><?=$row->code.'-'.$row->stationary_order_id.'-'.$row->order_date?></option>
								<?php }
						   ?>
                        </select>						
                     </div>
                     <div class="col-md-4">
                     </div>					 
                     <div class="col-md-4">
                        <label>&nbsp;</label>
						<div>
              <button type="button" id="btnPrint" onclick="pdf()" style="margin-right:5px;<?=(count($stationary_order) == '0' ? 'display:none':'')?>"  class="btn btn-default pull-right"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Print</button>						  
						  <button style="margin-right:5px;<?=(count($stationary_order) == '0' ? 'display:none':'')?>"type="button" onclick="deleteReceive()" class="btn btn-danger pull-right"><i class="fa fa-close"></i>&nbsp;&nbsp;Delete Order</button>
						  <button type="button" id="btnOrder" onclick="receive()" style="margin-right:5px;<?=(count($stationary_order) == '0' ? 'display:none':'')?>"  class="btn btn-primary pull-right"><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Receive & Print</button>						  
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Min. Stock</th>
                    <th style="width:10%">Order Qty</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Item Description</th>
                    <th>Category</th>
                    <th>Stock</th>
                    <th>Min. Stock</th>
                    <th style="width:10%">Order Qty</th>
                  </tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

