<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Inventory report List</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4" style="margin-top:10px">
              <label>Periode</label>
              <input type="text" class="form-control pull-right" id="range_date">
            </div>
            <div class="col-md-4" style="margin-top:10px">
              <label>Brand Unit</label>
              <select class="form-control" id="brand_unit_id" style="width: 100%;">
                <option value="">Select Brand Unit </option>
                <?php						   
							foreach($this->db->get('brand_unit')->result() as $get) { ?>
                <option value="<?=$get->brand_unit_id?>">
                  <?=$get->brand_unit_name?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-4" style="margin-top:10px">
              <label>Company</label>
              <select class="form-control" id="company_id" style="width: 100%;">
                <option value="">Select Company </option>
                <?php						   
							foreach($this->db->get('company')->result() as $get) { ?>
                <option value="<?=$get->company_id?>">
                  <?=$get->company_name?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <hr />
          <button type="button" id="btnPrint" onclick="print()" class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>
          <br />
          <br />
          <br />
          <table id="example1" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th>Item Description</th>
                <th>Category</th>
                <th>Qty</th>
                <th>Cost</th>
                <th>Total Cost</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Item Description</th>
                <th>Category</th>
                <th>Qty</th>
                <th>Cost</th>
                <th>Total Cost</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>