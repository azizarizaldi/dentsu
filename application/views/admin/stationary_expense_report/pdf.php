<html>
	<head>
		<title>Stationary Expense Report</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			

			@media print {
				h5 {
					color: #ffffff !important;
				}
			}

		</style>
	</head>
	<body onload="window.print()">
		<table style="border:none;" align="right">
			<tr>
				<td class="title-top" style="vertical-align:bottom;border:none;color:white!important;border: 1px solid black!important;background-color:black!important"><h5 style="margin-left:5px;margin-right:5px">Stationary Expense Report</h5></td>
			</tr>
		</table>
		<br/>
		<hr style="margin-top:30px"/>
		<hr style="margin-top:-20px"/>
		<p style="text-align:right">Date : <?=tgl(date("Y-m-d"),'02')?></p>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02')?> s/d <?=tgl($lastDate,'02')?></td>
			</tr>
			<?php
			if($company_id) {
				$this->db->where("company_id",$company_id);
				$company = $this->db->get("company")->row();?>
			<tr>
				<td style="vertical-align:bottom;border:none">Company</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=$company->company_name?></td>
			</tr>
			<?php }?>
			<?php
			if($brand_unit_id) {
				$this->db->where("brand_unit_id",$brand_unit_id);
				$brand_unit = $this->db->get("brand_unit")->row();?>
			<tr>
				<td style="vertical-align:bottom;border:none">Brand Unit</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=$brand_unit->brand_unit_name?></td>
			</tr>
			<?php }?>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%;text-align:center">No</th>
					<th style="width:15%;text-align:center">Item Description</th>
					<th style="width:10%;text-align:center">Category</th>
					<th style="width:10%;text-align:center">Qty</th>
					<th style="width:10%;text-align:center">Cost</th>
					<th style="width:10%;text-align:center">Total Cost</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					$total_all_cost = 0;
					foreach($query as $row) {
						$total_cost				= 0;
						$total_cost				= ($row->jumlah_qty*$row->item_price);
						$total_all_cost			= $total_all_cost+$total_cost;
						?>
						<tr>
							<td style="text-align:center"><?=++$nomor?></td>
							<td style="text-align:left"><?=$row->item_description?></td>
							<td style="text-align:left"><?=$row->category_name?></td>
							<td style="text-align:left"><?=$row->jumlah_qty?></td>
							<td style="text-align:right"><?="Rp. ".number_format($row->item_price, 0, '', '.').''; ?></td>
							<td style="text-align:right"><?="Rp. ".number_format($total_cost, 0, '', '.').''; ?></td>
						</tr>
					<?php }
				}
				else {
				?>
				<tr>
					<td colspan="6">No item</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
		<br/>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Number of item</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=count($query)?></td>
			</tr>
			<tr>
				<td style="vertical-align:bottom;border:none">Total Cost</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?="Rp. ".number_format($total_all_cost, 0, '', '.').'';?></td>
			</tr>
		</table>
	</body>
</html>