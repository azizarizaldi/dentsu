<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $("#brand_unit_id").select2();
    $("#company_id").select2();
    dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>admin/stationary_expense_report/get",
          data: function(d) {
              return $.extend({}, d, {
                  "range_date"		: $("#range_date").val(),
                  "company_id"		: $("#company_id").val(),
                  "brand_unit_id"		: $("#brand_unit_id").val()
              });
            }
          },
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": ["_all"]
        }],
        "columns": [
           
            { "data": "item_description" },
            { "data": "category_name" },
            { "data": "jumlah_qty" },
            { "data": "item_price" },
            { "data": "total_cost" }
        ],
        "order": [[1, 'asc']]
    });  

		$("#range_date").on("change", function() {
			dt.ajax.reload();
		});

		$("#company_id").on("change", function() {
			dt.ajax.reload();
		});

		$("#brand_unit_id").on("change", function() {
			dt.ajax.reload();
		});

  })



function print() {
		var keyword 		    = $('.dataTables_filter input').val();
		var range_date 		  = $('#range_date').val();
		var company_id 		  = $('#company_id').val();
		var brand_unit_id 	= $('#brand_unit_id').val();
		var number 			  = $('select[name="example1_length"] option:selected').val();
    window.open('<?php echo base_url(); ?>report/pdf_stationary_expense?keyword='+keyword+'&number='+number+'&company_id='+company_id+'&brand_unit_id='+brand_unit_id+'&range_date='+range_date,'_blank');		
}

$(document).ready(function() {
  $('#range_date').daterangepicker();
});

</script>