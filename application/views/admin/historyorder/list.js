<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#stationary_order_id").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>historyorder/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "range_date": $("#range_date").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "order_no"
            }, {
                "data": "order_date"
            }, {
                "data": "receive_date"
            }, {
                "data": "item_order"
            }, {
                "data": "note"
            },
			{
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    return "<button onclick=\"printDetail("+data.stationary_order_id+")\"type=\"button\" class=\"btn btn-default btn-sm\"><i class=\"fa fa-print\"></i> Print</button>"
               }
              
            }
			],
            "order": [
                [1, 'asc']
            ]
        });

		$("#range_date").on("change", function() {
			dt.ajax.reload();
		});
		
    });
		
	function printAll() {
		var keyword 	= $('.dataTables_filter input').val();
		var range_date 	= $('#range_date').val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_history_stock?keyword='+keyword+'&number='+number+'&range_date='+range_date,'_blank');		
	}
	
	function printDetail(stationary_order_id) {
		window.open('<?php echo base_url(); ?>report/pdf_history_stock_detail?stationary_order_id='+stationary_order_id,'_blank');		
	}
	$('#range_date').daterangepicker();
</script>