<html>
	<head>
		<title>History Stock</title>
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			
		</style>
	</head>
	<body onload="window.print()">
		<table style="width:100%;border:none">
			<tr>
				<td style="vertical-align:bottom;padding-bottom:30px;border:none"><h2>History Stock Item</h2></td>
			</tr>
		</table>
		<hr style="margin-top:-30px"/>
		<hr style="margin-top:-7px"/>
		<br/>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order No</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=$stationary_order->code.'-'.$stationary_order->stationary_order_id.'-'.$stationary_order->order_date?></td>
			</tr>
			<tr>
				<td style="vertical-align:bottom;border:none;padding-top:-10px">Order Date</td>
				<td style="vertical-align:bottom;border:none;padding-top:-10px">:</td>
				<td style="vertical-align:bottom;border:none;padding-top:-10px"><?=tgl($stationary_order->order_date,'02')?></td>
			</tr>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%">No</th>
					<th style="width:30%">Item Description</th>
					<th style="width:10%">Qty</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
				?>
				<tr>
					<td style="width:2%;text-align:center"><?=++$nomor?></td>
					<td style="width:30%"><?=$row->item_description?></td>
					<td style="width:10%;text-align:center"><?=$row->qty?> <?=$row->item_unit_measure?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="3">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>