<html>
	<head>
		<title>History Order Stock</title>
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			
		</style>
	</head>
	<body onload="window.print()">
		<table style="width:100%;border:none">
			<tr>
				<td style="vertical-align:bottom;padding-bottom:30px;border:none"><h2>History Stock Item</h2></td>
			</tr>
		</table>
		<hr style="margin-top:-30px"/>
		<hr style="margin-top:-7px"/>
		<br/>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">History Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02').' s/d '.tgl($lastDate,'02')?></td>
			</tr>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%">No</th>
					<th style="width:15%">Order No</th>
					<th style="width:15%">Order Date</th>
					<th style="width:15%">Receive Date</th>
					<th style="width:20%">item Order</th>
					<th style="width:10%">Note</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $rows) {
				?>
				<tr>
					<td style="width:2%;text-align:center;vertical-align:top"><?=++$nomor?></td>
					<td style="vertical-align:top"><?=$rows->code.'-'.$rows->stationary_order_id.'-'.$rows->order_date;?></td>
					<td style="vertical-align:top"><?=tgl($rows->order_date,'02')?></td>
					<td style="vertical-align:top"><?=tgl($rows->receive_date,'02')?></td>
					<td style="vertical-align:top"><?='- '.str_replace(',','<br/>- ',$rows->item_description);?></td>
					<td style="text-align:center">-</td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="5">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>