    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">History Order Stock</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-8">
                        <label>Periode</label>
						<input type="text" class="form-control pull-right" id="range_date">
                     </div>
                     <div class="col-md-4">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" id="btnOrder" onclick="printAll()"  class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print All</button>						  
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Order No</th>
                    <th>Order Date</th>
                    <th>Receive Date</th>
                    <th>item Order</th>
                    <th>Note</th>
                    <th>Action</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Order No</th>
                    <th>Order Date</th>
                    <th>Receive Date</th>
                    <th>item Order</th>
                    <th>Note</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

