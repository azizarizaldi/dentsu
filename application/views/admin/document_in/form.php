<section class="content">
   <div class="row">
   <!-- left column -->
   <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
         <div class="box-header with-border">
            <h3 class="box-title"><?php echo $action=="add" ? "Create Document In" : "Edit Document In"; ?></h3>
         </div>
         <!-- /.box-header -->
         <!-- form start -->
         <form id="form_input" role="form" method="post">
            <?php if ($action == "edit") { ?>
            <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
            <?php } ?>
            <input type="hidden" id="order_receiver_latitude" name="order_receiver_latitude" value="-1" />
            <input type="hidden" id="order_receiver_longitude" name="order_receiver_longitude" value="-1" />
            <input type="hidden" name="order_receiver_phone" value="null" />
            <input type="hidden" id="order_photo" name="order_photo" value="" />
            <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />
            <div class="box-body">
               <?php if ($action == "edit") { ?>
               <div class="form-group">
                  <label for="exampleInputEmail1">Order ID</label>
                  <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control validate-input" id="order_name" name="order_name" placeholder="Document Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input document name</p>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Sender</label>
                        <?php if (count($documentInSenders)) { ?>
                        <div class="input-group input-group">
                           <div class="input-group-btn">
                              <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Sender <?php count($senders); ?>
                              <span class="fa fa-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu" id="senders">
                                 <?php foreach($documentInSenders as $sender) { ?>
                                 <li><a href="javascript:$('#order_receiver_name').val('<?php echo htmlspecialchars($sender->order_receiver_name, ENT_QUOTES); ?>')"><?php echo $sender->order_receiver_name; ?></li>
                                 </a>
                                 <?php } ?>
                              </ul>
                           </div>
                           <!-- /btn-group -->                    
                           <input type="text" class="form-control validate-input" placeholder="Sender" id="order_receiver_name" name="order_receiver_name" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                        </div>
                        <?php } else { ?>
                        <input type="text" class="form-control validate-input" placeholder="Sender" id="order_receiver_name" name="order_receiver_name" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                        <?php } ?>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input sender</p>
                     </div>
                     <div class="form-group">
                        <label>Courier Name</label>
                        <?php if (count($documentInCouriers)) { ?>
                        <div class="input-group input-group">
                           <div class="input-group-btn">
                              <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Courier
                              <span class="fa fa-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu" id="couriers">
                                 <?php foreach($documentInCouriers as $courier) { ?>
                                 <li><a href="javascript:$('#order_receiver_address').val('<?php echo htmlspecialchars($courier->order_receiver_address, ENT_QUOTES); ?>')"><?php echo $courier->order_receiver_address; ?></li>
                                 </a>
                                 <?php } ?>
                              </ul>
                           </div>
                           <!-- /btn-group -->
                           <input type="text" class="form-control validate-input" placeholder="Courier Name" id="order_receiver_address" name="order_receiver_address" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                        </div>
                        <?php } else { ?>
                        <input type="text" class="form-control validate-input" placeholder="Courier Name" id="order_receiver_address" name="order_receiver_address" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                        <?php } ?>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input courier name</p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="exampleInputFile">Photo</label>
                        <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                        <br />
                        <img id="previewPhoto" src="<?php echo isset($row) && $row->order_photo ? base_url().$row->order_photo : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->order_photo ? "" : 'style="display: none;"'; ?> />
                        <?php if (isset($row) && $row->order_photo) { ?>
                        <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                        <?php } ?>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input photo</p>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label>Receiver</label>
                  <select id="receiver" class="form-control  validate-input" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a receiver ---</option>
                     <?php foreach($senders as $user) { ?>
                     <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                     <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
               </div>
               <!-- /.box-body -->
               <?php if ($canModify) { ?>
               <div class="box-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
               </div>
               <?php } ?>
         </form>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <!--/.col (right) -->
   </div>
   <!-- /.row -->
</section>