<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>documentin/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status"		: $("#order_status").val(),
           "orderDate"	: $("#datepicker").val()
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "receiver" },
            { "data": "courier" },
            { "data": "sender_name" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";

                    if (data.order_status < 4)
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:receive("+data.order_id+");\">Receive</button>";
                    }

                    if (data.order_status != 4)
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                    }

                    return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
  
    
     $("#order_status").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
     $("#datepicker").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });  
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/documentin/edit?id=" + orderId;
    }
    
    function receive(id)
    {
      if (confirm("Are you sure to set order status to completed?"))
      {
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentin/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {                  
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      }
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentin/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
  
          //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    
</script>