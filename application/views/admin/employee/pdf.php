<html>
	<head>
		<title>Employee List</title>
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			
		</style>
	</head>
	<body onload="window.print()">
		<table style="width:100%;border:none">
			<tr>
				<td style="vertical-align:bottom;padding-bottom:30px;border:none"><h2>Employee List</h2></td>
			</tr>
		</table>
		<hr style="margin-top:-30px"/>
		<hr style="margin-top:-7px"/>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%">No</th>
					<th style="width:10%">ID employee</th>
					<th style="width:10%">Category</th>
					<th style="width:10%">employee Name</th>
					<th style="width:10%">Phone Primary</th>
					<th style="width:10%">Phone Alternative</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
				?>
				<tr>
					<td style="width:2%;text-align:center"><?=++$nomor?></td>
					<td style="width:10%;text-align:left"><?=sprintf("%05d", $row->employee_id);?></td>
					<td style="width:10%;text-align:left"><?=$row->category?></td>
					<td style="width:10%;text-align:left"><?=$row->employee_name?></td>
					<td style="width:10%;text-align:left"><?=$row->phone_primary?></td>
					<td style="width:10%;text-align:left"><?=$row->phone_alternative?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="5">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>