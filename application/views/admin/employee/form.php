<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php echo ($action == "edit") ? "Edit Employee" : "Add Employee"; ?>
          </h3>
        </div>
        <form id="form_input" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?=$action?>"/>
          <?php if ($action == "edit") { ?>
          <input type="hidden" name="employee_id" value="<?php echo $row->employee_id; ?>" />
          <?php } ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ID Team</label>
                  <?php if ($action == "edit") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="employee_id_text" value="<?php printf("%05d", $row->employee_id);?>"
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php }
                  else if ($action == "add") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="employee_id_text" value=""
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select id="id_category" style="width:100%;" class="form-control validate-input select2" name="id_category"<?php if (! $canModify) { echo " disabled"; }?>>
                        <option value="">--- Select a category ---</option>
                        <?php foreach($data_category as $category) { ?>
                        <option value="<?php echo $category->id; ?>" <?php if (isset($row) && $category->id == $row->id_category) { echo "selected"; } ?>><?php echo htmlspecialchars($category->name, ENT_QUOTES); ?></option>
                        <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a category</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Employee Name</label>
                  <input type="text" class="form-control validate-input" id="employee_name" name="employee_name" placeholder="employee Name" value="<?php echo isset($row) ? htmlspecialchars($row->employee_name, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input employee name</p>              
                </div>
              </div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Position / Title</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Position / Title..." value="<?php echo isset($row) ? htmlspecialchars($row->title, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone (Primary)</label>
                  <input type="text" class="form-control validate-input" id="phone_primary" name="phone_primary" placeholder="Phone (Primary)..." value="<?php echo isset($row) ? htmlspecialchars($row->phone_primary, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input phone primary</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Photo</label>
                  <input type="file" name="file_photo" style="display:none" onchange="loadFile(event)">
                  <br/>
                  <?php if($action == 'add') { ?>
                    <img id="cover-thumbnail-control" src="<?=base_url().'uploads/employee/default.png'?>" style="width:30%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;" title="your photo"/>                    
                  <?php }
                  else {
                    $yourPhoto = "default.png";
                    if(!empty($row->file_photo)) {
                      $yourPhoto = $row->file_photo;                      
                    }
                    ?>
                    <img id="cover-thumbnail-control" src="<?=base_url().'uploads/employee/'.$yourPhoto?>" style="width:30%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;" title="your photo"/>                    
                  <?php } ?>
                  <br/>
                  <button onclick="$('[type=file]').click()" style="margin-top:10px" type="button" class="btn btn-success btn-sm"><i class="fa fa-image"></i> Change Image</button>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone (Alternative)</label>
                  <input type="text" class="form-control" id="phone_alternative" name="phone_alternative" placeholder="Phone (Alternative)..." value="<?php echo isset($row) ? htmlspecialchars($row->phone_alternative, ENT_QUOTES) : ""; ?>">
                </div>
                <div class="form-group">
                  <label>Assignment Location</label>
                  <input type="text" class="form-control" id="address" name="address" placeholder="Assignment Location..." value="<?php echo isset($row) ? htmlspecialchars($row->address, ENT_QUOTES) : ""; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Note</label>
                  <textarea class="form-control" id="note" name="note" rows="3" placeholder="Address..."><?php echo isset($row) ? htmlspecialchars($row->note, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">
                <?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
              <?php if ($action == "edit") { ?>
              &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              <?php } ?>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
	var loadFile = function(event) {
		var output = document.getElementById('cover-thumbnail-control');
		output.src = URL.createObjectURL(event.target.files[0]);
	};
</script>