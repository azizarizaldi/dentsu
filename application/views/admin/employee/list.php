<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Employee List</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-4">
                <label>Filter By Category</label>
                <select class="form-control" id="id_category" style="width: 100%;">
                  <option value="">Select Category List</option>
                    <?php
                    $this->db->where("type_category","4");
                    foreach($this->db->get('setting_category')->result() as $get) { ?>
                    <option value="<?=$get->id?>">
                      <?=$get->name?>
                    </option>
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-4">
                    <a href="<?php echo base_url(); ?>admin/employee/add" class="btn btn-primary" style="margin-top:25px"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add employee</a>
                    <button type="button" onclick="printData()" class="btn btn-default" style="margin-top:25px"><i class="fa fa-print"></i> &nbsp;&nbsp;Print Data</button>
              </div>
              <div class="col-md-4">
              </div>
            </div>
          </div>

          <table id="example1" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th>ID employee</th>
                <th>Category</th>
                <th>Employee Name</th>
                <th>Phone Primary</th>
                <th>Phone Alternative</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID employee</th>
                <th>Category</th>
                <th>Employee Name</th>
                <th>Employee Person</th>
                <th>Phone</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>