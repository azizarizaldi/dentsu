<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#order_ref").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>report_meeting_room/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "range_date": $("#range_date").val(),
                        "order_ref"	: $("#order_ref").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "order_id"
            }, {
                "data": "order_date"
            }, {
                "data": "user_name"
            }, {
                "data": "room"
            }, {
                "data": "meeting"
            }, {
                "data": "periode"
            }, {
                "data": "status"
            }
			],
            "order": [
                [1, 'asc']
            ]
        });

		$("#range_date").on("change", function() {
			dt.ajax.reload();
		});

		$("#order_ref").on("change", function() {
			dt.ajax.reload();
		});
		
    });
		
	function print() {
		var keyword 	= $('.dataTables_filter input').val();
		var range_date 	= $('#range_date').val();
		var order_ref 	= $('#order_ref').val();
		var number 		= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_report_meeting_room?keyword='+keyword+'&number='+number+'&range_date='+range_date+'&order_ref='+order_ref,'_blank');		
	}
	
	$('#range_date').daterangepicker();
</script>