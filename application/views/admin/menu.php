<?php
   $check_document = $this->db->query("
      SELECT *
      FROM (
      SELECT send_reminder , document.document_id , document.document_name ,  setting_category.name AS category, vendor.vendor_name , start_date , DATE_ADD(end_date,INTERVAL -reminder_day DAY) AS sendDate , end_date , reminder_day
      FROM document
      INNER JOIN setting_category ON setting_category.id = document.id_category
      INNER JOIN vendor ON vendor.vendor_id = document.vendor_id
      ) getDocument
      WHERE send_reminder = 'yes' and curdate() BETWEEN sendDate AND end_date
   ")->result();
?>
<ul class="sidebar-menu" data-widget="tree">
   <li class="header">MAIN NAVIGATION</li>
   <li>
      <a href="<?php echo base_url();?>admin/dashboard">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
   </li>
   <li class="treeview">
      <a href="#">
      <i class="fa fa-th-large"></i> 
      <span>Order</span>
      <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
      </span>
      </a>
      <ul class="treeview-menu" >
         <li class="treeview">
            <a href="#"><i class=""></i> Operational Car
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/bookingcar/add"><i class=""></i> Create Order</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/bookingcar"><i class=""></i> Booking List</a></li>
               <li><a href="<?php echo base_url(); ?>admin/car"><i class=""></i> Car List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Meeting Room
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/bookingmeetingroom/add"><i class=""></i> Create Order</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/bookingmeetingroom"><i class=""></i> Booking List</a></li>
               <li><a href="<?php echo base_url(); ?>admin/meetingroom"><i class=""></i> Meeting Room List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Stationery
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/stationaryorder/add"><i class=""></i> Create Order</a></li>
               <?php } ?>
				<li><a href="<?php echo base_url(); ?>admin/stationaryorder"><i class=""></i> Order List</a></li>
				<li><a href="<?php echo base_url(); ?>admin/stationaryitem"><i class=""></i> Stock List</a></li>
				<li><a href="<?php echo base_url(); ?>admin/warningstock"><i class=""></i> Warning Stock</a></li>
				<li><a href="<?php echo base_url(); ?>admin/orderstock"><i class=""></i> Order Stock</a></li>
				<li><a href="<?php echo base_url(); ?>admin/receivestock"><i class=""></i> Receive Stock</a></li>
				<li><a href="<?php echo base_url(); ?>admin/historyorder"><i class=""></i> History Order</a></li>
				<!--li><a href="<?php echo base_url(); ?>admin/stationary_by_category"><i class=""></i> History Receive Stock</a></li-->
            </ul>
         </li>
		<li class="treeview">
            <a href="#"><i class=""></i> Visitor Management
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/appointmentorder/add"><i class=""></i> Create Appointment</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/appointmentorder"><i class=""></i> Appointment List</a></li>
            </ul>
         </li>
		<li class="treeview">
            <a href="#"><i class=""></i> Courier
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<li class="treeview">
				<a href="#"><i class=""></i> Internal Courier
				<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
				</a>
				<ul class="treeview-menu" >
					<li><a href="<?php echo base_url(); ?>admin/documentinternal/add"><i class="fa fa-plus"></i> Create Order</a></li>
					<li><a href="<?php echo base_url(); ?>admin/documentinternal"><i class="fa fa-table"></i> Order List</a></li>
					<li><a href="<?php echo base_url(); ?>admin/listofmessenger"><i class="fa fa-users"></i> List Of Messenger</a></li>
				</ul>
			 </li>
			<li class="treeview">
				<a href="#"><i class=""></i> External Courier
				<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
				</a>
				<ul class="treeview-menu" >
					<li><a href="<?php echo base_url(); ?>admin/documentexternal/add"><i class="fa fa-plus"></i> Create Order</a></li>
					<li><a href="<?php echo base_url(); ?>admin/documentexternal"><i class="fa fa-table"></i> Order List</a></li>
				</ul>
			 </li>
			<li class="treeview">
				<a href="#"><i class=""></i> Incoming Doc & Goods
				<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
				</a>
				<ul class="treeview-menu" >
					<li><a href="<?php echo base_url(); ?>admin/documentin/add"><i class="fa fa-plus"></i> Create Order</a></li>
					<li><a href="<?php echo base_url(); ?>admin/documentin"><i class="fa fa-table"></i> Order List</a></li>
				</ul>
			 </li>
            </ul>
         </li>
      </ul>
   </li>
   <li class="treeview">
      <a href="#">
      <i class="fa fa-file-text-o"></i> <span>Report</span>
      <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
      </span>
      </a>
      <ul class="treeview-menu" >
		<!--li><a href="<?php echo base_url(); ?>admin/stationary_by_category"><i class=""></i>Stationary</a></li-->        
		<li><a href="<?php echo base_url(); ?>admin/inventory_report"><i class=""></i>Inventory Report</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/low_level_stock"><i class=""></i>Low Level Stock</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/stationary_expense_report"><i class=""></i>Stationary Expense Report</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_meeting_room"><i class=""></i> Meeting Room</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_operational_car"><i class=""></i> Operational Car</a></li>		
		<li><a href="<?php echo base_url(); ?>admin/visitor_report"><i class=""></i> Visitor Management</a></li>
		<li><a href="<?php echo base_url(); ?>admin/report_internal_courier"><i class=""></i> Internal Courier</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_external_courier"><i class=""></i> External Courier</a></li>
		<li><a href="<?php echo base_url(); ?>admin/report_document_in"><i class=""></i> Receive Doc. & Goods</a></li>
      </ul>
   </li>
   <li class="treeview">
      <a href="#">
      <i class="fa fa-gear"></i> <span>Setting</span>
      <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
      </span>
      </a>
      <ul class="treeview-menu" >
         <li class="treeview">
            <a href="#"><i class=""></i> Company
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<?php if ($canModify) { ?>
			<li><a href="<?php echo base_url(); ?>admin/company/add"><i class=""></i> Add Company</a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>admin/company"><i class=""></i> Company List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Brand Unit
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<?php if ($canModify) { ?>
			<li><a href="<?php echo base_url(); ?>admin/brandunit/add"><i class=""></i> Add Brand Unit</a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>admin/brandunit"><i class=""></i> Brand Unit List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Departement
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<?php if ($canModify) { ?>
			<li><a href="<?php echo base_url(); ?>admin/dept/add"><i class=""></i> Add Departement</a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>admin/dept"><i class=""></i> Departement List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> User
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<?php if ($canModify) { ?>
			<li><a href="<?php echo base_url(); ?>admin/user/add"><i class=""></i> Add User</a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>admin/user"><i class=""></i> User List</a></li>
			<li><a href="<?php echo base_url(); ?>admin/user/toolsimport"><i class=""></i> Import</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Banner
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
			<?php if ($canModify) { ?>
			<li><a href="<?php echo base_url(); ?>admin/billboard/add"><i class=""></i> Add Banner</a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>admin/billboard"><i class=""></i> Banner List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Visitor
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/visitor/add"><i class=""></i> Add Visitor</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/visitor"><i class=""></i> Visitor List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Stationary
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/categorystationary/add"><i class=""></i> Add Category</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/categorystationary"><i class=""></i> Category List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Meeting Room
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/meetingroom/add"><i class=""></i> Add Meeting Room</a></li>
               <?php } ?>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Operational Car
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/car/add"><i class=""></i> Add Car</a></li>
               <?php } ?>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Courier
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/orderpriority/add"><i class=""></i> Add Order Priority</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/orderpriority"><i class=""></i> Order Priority List</a></li>
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/logistic/add"><i class=""></i> Add Logistic Company</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/logistic"><i class=""></i> Logistic Company List</a></li>
               <?php if ($canModify) { ?>
               <li><a href="<?php echo base_url(); ?>admin/packagetype/add"><i class=""></i> Add Courier Package</a></li>
               <?php } ?>
               <li><a href="<?php echo base_url(); ?>admin/packagetype"><i class=""></i> Courier Package List</a></li>
            </ul>
         </li>
      </ul>
   </li>
   <li class="treeview">
      <a href="#">
      <i class="fa fa-database"></i> 
      <span>New Module</span>
      <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
      </span>
      </a>
      <ul class="treeview-menu" >
         <li class="treeview">
            <a href="#"><i class=""></i> Setting Category
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <li><a href="<?php echo base_url(); ?>admin/setting_category/add"><i class=""></i> Add Category</a></li>
               <li><a href="<?php echo base_url(); ?>admin/setting_category"><i class=""></i> Category List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Vendor Database
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <li><a href="<?php echo base_url(); ?>admin/vendor/add"><i class=""></i> Add Vendor</a></li>
               <li><a href="<?php echo base_url(); ?>admin/vendor"><i class=""></i> Vendor List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Important Contact Numbers
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <li><a href="<?php echo base_url(); ?>admin/contact/add"><i class=""></i> Add Contact</a></li>
               <li><a href="<?php echo base_url(); ?>admin/contact"><i class=""></i> Contact List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> General Affair Team
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu" >
               <li><a href="<?php echo base_url(); ?>admin/employee/add"><i class=""></i> Add Employee</a></li>
               <li><a href="<?php echo base_url(); ?>admin/employee"><i class=""></i> Employee List</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class=""></i> Company Document
            <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
               <?php
               if(!empty(count($check_document))) { ?>
                  <small class="label pull-right bg-red" style="margin-top:2px"><?=count($check_document)?></small>
               <?php }
               ?>
            </span>
            </a>
            <ul class="treeview-menu" >
               <li><a href="<?php echo base_url(); ?>admin/document/add"><i class=""></i> Add Document</a></li>
               <li><a href="<?php echo base_url(); ?>admin/document">
               <i class=""></i>
               Document List
               <?php
               if(!empty(count($check_document))) { ?>
                  <small class="label pull-right bg-red" style="margin-top:2px"><?=count($check_document)?></small>
               <?php }
               ?>               
               </a></li>
               <li><a href="<?php echo base_url(); ?>admin/extention/add"><i class=""></i> New Procedure</a></li>
               <li><a href="<?php echo base_url(); ?>admin/extention"><i class=""></i> Extention Procedure</a></li>
            </ul>
         </li>
      </ul>
   </li>

</ul>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    var url = window.location;
// for sidebar menu but not for treeview submenu
$('ul.sidebar-menu a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');
// for treeview which is like a submenu
$('ul.treeview-menu a').filter(function() {
    return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active menu-open').end().addClass('active menu-open');
</script>