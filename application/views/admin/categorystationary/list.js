<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>categorystationary/get",
        "columns": [
           
            { "data": "category_id" },
            { "data": "category_name" },
            { "data": "picture", "orderable": false }
        ],
        "order": [[1, 'asc']]
    } );

    $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/categorystationary/edit?id=" + tr.find('td:first').html();
    } );  
  });
</script>