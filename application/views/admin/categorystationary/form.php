<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Stationary Category" : "Add Stationary Category"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="category_id" value="<?php echo $row->category_id; ?>" />
                <?php } ?>
                                <input type="hidden" id="category_pic" name="category_pic" value="" />
                <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />

              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->category_id; ?>" disabled>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control validate-input" placeholder="Category Name" name="category_name" value="<?php echo isset($row) ? htmlspecialchars($row->category_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input category name</p>
                </div>              
                <div class="form-group">
                  <label>Category Picture</label>
                  <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->category_pic ? base_url().$row->category_pic : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->category_pic ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->category_pic) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select category picture</p> 
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>