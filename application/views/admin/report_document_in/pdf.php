<html>
	<head>
		<title>INCOMING DOCUMENT & GOODS</title>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style>
			body {
				font-family:arial;
				-webkit-print-color-adjust:exact;
			}
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			

			@media print {
				h5 {
					color: #ffffff !important;
				}
			}
		</style>
	</head>
	<body onload="window.print()">
		<table style="border:none;" align="right">
			<tr>
				<td class="title-top" style="vertical-align:bottom;border:none;color:white!important;border: 1px solid black!important;background-color:black!important"><h5 style="margin-left:5px;margin-right:5px">INCOMING DOCUMENT & GOODS</h5></td>
			</tr>
		</table>
		<br/>
		<hr style="margin-top:30px"/>
		<hr style="margin-top:-20px"/>
		<p style="text-align:right">Date : <?=tgl(date("Y-m-d"),'02')?></p>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02').' s/d '.tgl($lastDate,'02')?></td>
			</tr>
			<?php
			if(!empty($order_ref)) {
				$this->db->where("user_id",$order_ref);
				$user = $this->db->get("_user")->row();
			?>
			<tr>
				<td style="vertical-align:bottom;border:none">Courier</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=$user->user_name?></td>
			</tr>
			<?php }?>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
                    <th rowspan="2" style="text-align:center">No</th>
                    <th rowspan="2" style="width:8%;text-align:center">Order ID</th>
                    <th rowspan="2" style="width:8%;text-align:center">Order Date</th>
                    <th rowspan="2" style="width:20%;text-align:center">Item Description</th>
                    <th rowspan="2" style="width:20%">Receiver</th>
                    <th colspan="2" style="width:16%;text-align:center">Sender</th>
                    <th rowspan="2">Status</th>
				</tr>
				<tr>
                    <th style="width:8%;text-align:center">Name</th>
                    <th style="width:8%;text-align:center">Schedule</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
					$message = "";
					if($row->status_order == 'Done') {
						$message = "<br/>Received Date : ".$row->order_received_date;
					}
					else if($row->status_order == 'Rejected') {
						$message = "<br/>Rejected Date : ".$row->order_rejected_date;				
					}	
				?>
				<tr>
					<td style="width:3%;text-align:center;vertical-align:top;"><?=++$nomor?></td>
					<td style="vertical-align:top;;text-align:center"><?=$row->order_id?></td>
					<td style="vertical-align:top;;text-align:center"><?=tgl($row->order_date,'2')?></td>
					<td style="vertical-align:top;text-align:left;">
						<center>
						<?='<img width="80" height="80" style="margin-top:5px" src="'.base_url().$row->item.'"/>'?>
						</center>
						<br/>
						<?=$row->order_name?>
					</td>
					<td style="vertical-align:top"><?=$row->user_name?></td>
					<td style="vertical-align:top"><?=$row->order_receiver_name?></td>
					<td style="vertical-align:top"><?=$row->order_receiver_name?></td>
					<td style="vertical-align:top"><?='<b>'.$row->status_order.'</b><br/>By : '.$row->user_name.'<br/>'.$message.'<br/>Note : '.$row->order_note?>
					</td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="10">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>

<!--
	<td style="vertical-align:top"><?="Document Name : ".$row->order_name.'<br/>Sender : '.$row->order_receiver_name.'<br/>Courier : '.$row->order_receiver_address;?></td>
	<td style="vertical-align:top"><?='<b>'.$row->status_order.'</b>'.$message;?></td>
	$message = "";
	if($row->status_order == 'Done') {
		$message = "<br/>Received Date : ".$row->order_received_date;
	}
	else if($row->status_order == 'Rejected') {
		$message = "<br/>Rejected Date : ".$row->order_rejected_date;				
	}
-->