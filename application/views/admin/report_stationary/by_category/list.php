    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Report Stationary Item</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Periode</label>
						<input type="text" class="form-control pull-right" id="range_date">
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Category</label>
                        <select class="form-control" id="item_category" style="width: 100%;">
                           <option value="">Select Category </option>
						   <?php						   
							foreach($this->db->get('stationary_category')->result() as $get) { ?>
							<option value="<?=$get->category_id?>"><?=$get->category_name?></option>
							<?php } ?>
						</select>
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Departement</label>
                        <select class="form-control" id="departement_id" style="width: 100%;">
                           <option value="">Select Departement </option>
						   <?php						   
							foreach($this->db->get('departement')->result() as $get) { ?>
							<option value="<?=$get->departement_id?>"><?=$get->departement_name?></option>
							<?php } ?>
						</select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Brand Unit</label>
                        <select class="form-control" id="brand_unit_id" style="width: 100%;">
                           <option value="">Select Brand Unit </option>
						   <?php						   
							foreach($this->db->get('brand_unit')->result() as $get) { ?>
							<option value="<?=$get->brand_unit_id?>"><?=$get->brand_unit_name?></option>
							<?php } ?>
						</select>
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Company</label>
                        <select class="form-control" id="company_id" style="width: 100%;">
                           <option value="">Select Company </option>
						   <?php						   
							foreach($this->db->get('company')->result() as $get) { ?>
							<option value="<?=$get->company_id?>"><?=$get->company_name?></option>
							<?php } ?>
						</select>
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>&nbsp;</label>
						<div>
                     <!--button type="button" id="btnPrint" onclick="print()"  class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button-->
                     <div class="input-group-btn">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-print"></i>&nbsp;&nbsp;Print
                        <span class="fa fa-caret-down"></span></button>
                        <ul class="dropdown-menu">
                           <li><a onclick="inventoryReport()"> Inventory Report&nbsp;&nbsp;</a></li>
                           <li><a onclick="lowLevelReport()"> Low Level Stock&nbsp;&nbsp;</a></li>
                           <li><a href="#"> Expense Report&nbsp;&nbsp;</a></li>
                        </ul>
                     </div>
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<div class="box-body table-responsive no-padding">
			<table id="example1" class="table table-bordered table-hover table" width="100%">
                <thead>
                  <tr>
                    <th>ID Order</th>
                    <th>Order Date</th>
                    <th>Item Description</th>
                    <th>Qty</th>
                    <th>Name</th>
                    <th>Brand Unit</th>
                    <th>Departement</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID Order</th>
                    <th>Order Date</th>
                    <th>Item Description</th>
                    <th>Qty</th>
                    <th>Name</th>
                    <th>Brand Unit</th>
                    <th>Departement</th>
                 </tr>
                </tfoot>
              </table>
			  </div>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

