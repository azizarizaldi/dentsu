<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#item_category").select2();
	$("#departement_id").select2();
	$("#company_id").select2();
	$("#brand_unit_id").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>stationary_by_category/get",
                data: function(d) {
                    return $.extend({}, d, {
                        "range_date"		: $("#range_date").val(),
                        "item_category"		: $("#item_category").val(),
                        "departement_id"	: $("#departement_id").val(),
                        "company_id"		: $("#company_id").val(),
                        "brand_unit_id"		: $("#brand_unit_id").val()
					});
                }
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "order_no"
            }, {
                "data": "order_date"
            }, {
                "data": "item_description"
            }, {
                "data": "qty"
            }, {
                "data": "user_name"
            }, {
                "data": "brand_unit_name"
            }, {
                "data": "departement_name"
            }
			],
            "order": [
                [1, 'asc']
            ]
        });

		$("#range_date").on("change", function() {
			dt.ajax.reload();
		});

		$("#item_category").on("change", function() {
			dt.ajax.reload();
		});
		
		$("#departement_id").on("change", function() {
			dt.ajax.reload();
		});

		$("#company_id").on("change", function() {
			dt.ajax.reload();
		});

		$("#brand_unit_id").on("change", function() {
			dt.ajax.reload();
		});
		
    });
		
	function print() {
		var keyword 		= $('.dataTables_filter input').val();
		var range_date 		= $('#range_date').val();
		var item_category 	= $('#item_category').val();
		var departement_id 	= $('#departement_id').val();
		var company_id 		= $('#company_id').val();
		var brand_unit_id 	= $('#brand_unit_id').val();
		var number 			= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_stationary_by_category?keyword='+keyword+'&number='+number+'&range_date='+range_date+'&item_category='+item_category+'&departement_id='+departement_id+'&company_id='+company_id+'&brand_unit_id='+brand_unit_id,'_blank');		
	}

	function inventoryReport() {
		var keyword 		= $('.dataTables_filter input').val();
		var range_date 		= $('#range_date').val();
		var item_category 	= $('#item_category').val();
		var departement_id 	= $('#departement_id').val();
		var company_id 		= $('#company_id').val();
		var brand_unit_id 	= $('#brand_unit_id').val();
		var number 			= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_inventory_report?keyword='+keyword+'&number='+number+'&range_date='+range_date+'&item_category='+item_category+'&departement_id='+departement_id+'&company_id='+company_id+'&brand_unit_id='+brand_unit_id,'_blank');		
    }
    
	function lowLevelReport() {
		var keyword 		= $('.dataTables_filter input').val();
		var range_date 		= $('#range_date').val();
		var item_category 	= $('#item_category').val();
		var departement_id 	= $('#departement_id').val();
		var company_id 		= $('#company_id').val();
		var brand_unit_id 	= $('#brand_unit_id').val();
		var number 			= $('select[name="example1_length"] option:selected').val();
		window.open('<?php echo base_url(); ?>report/pdf_low_Level_report?keyword='+keyword+'&number='+number+'&range_date='+range_date+'&item_category='+item_category+'&departement_id='+departement_id+'&company_id='+company_id+'&brand_unit_id='+brand_unit_id,'_blank');		
	}


	$(document).ready(function() {
		$('#range_date').daterangepicker();
	});
</script>