<html>
	<head>
		<title>Low Level Stock</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			

			@media print {
				h5 {
					color: #ffffff !important;
				}
			}

		</style>
	</head>
	<body onload="window.print()">
		<table style="border:none;" align="right">
			<tr>
				<td class="title-top" style="vertical-align:bottom;border:none;color:white!important;border: 1px solid black!important;background-color:black!important"><h5 style="margin-left:5px;margin-right:5px">Low Level Stock</h5></td>
			</tr>
		</table>
		<br/>
		<hr style="margin-top:30px"/>
		<hr style="margin-top:-20px"/>
		<p style="text-align:right">Date : <?=tgl(date("Y-m-d"),'02')?></p>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Number Of Item</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=count($query)?></td>
			</tr>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%;text-align:center">No</th>
					<th style="width:15%;text-align:center">Item Description</th>
					<th style="width:10%;text-align:center">Category</th>
					<th style="width:10%;text-align:center">Stock</th>
					<th style="width:10%;text-align:center">Min Stock</th>
					<th style="width:10%;text-align:center">Price</th>
					<th style="width:10%;text-align:center">Total Value</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {					
					$nomor = 0;
					$total = 0;
					foreach($query as $row) {
					$total = $total + $row->total_value;	
					?>
					<tr>
						<td style="text-align:center"><?=++$nomor?></td>
						<td style="text-align:center"><?=$row->item_description?></td>
						<td style="text-align:center"><?=$row->category?></td>
						<td style="text-align:center"><?=$row->item_stock?></td>
						<td style="text-align:center"><?=$row->item_min_stock?></td>
						<td style="text-align:right"><?="Rp. ".number_format($row->item_price, 0, '', '.').',- '; ?></td>
						<td style="text-align:right"><?="Rp. ".number_format($row->total_value, 0, '', '.').',- '; ?></td>
				 	</tr>
					<?php } ?>
					<tr>
						<td colspan="6">Total Value</td>
						<td style="text-align:right"><?="Rp. ".number_format($total, 0, '', '.').',- '; ?></td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>