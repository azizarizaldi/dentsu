<html>
	<head>
		<title>Stationary Item By Category</title>
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}			
		</style>
	</head>
	<body onload="window.print()">
		<table style="width:100%;border:none">
			<tr>
				<td style="vertical-align:bottom;padding-bottom:30px;border:none"><h2>Stationary Order Item</h2></td>
			</tr>
		</table>
		<hr style="margin-top:-30px"/>
		<hr style="margin-top:-7px"/>
		<br/>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02').' s/d '.tgl($lastDate,'02')?></td>
			</tr>
			<?php if($item_category) { ?>
				<tr>
					<td style="vertical-align:bottom;border:none">Category</td>
					<td style="vertical-align:bottom;border:none">:</td>
					<td style="vertical-align:bottom;border:none"><?=$item_category->category_name?></td>
				</tr>
			<?php }?>

			<?php if($departement) { ?>
				<tr>
					<td style="vertical-align:bottom;border:none">Departement</td>
					<td style="vertical-align:bottom;border:none">:</td>
					<td style="vertical-align:bottom;border:none"><?=$departement->departement_name?></td>
				</tr>
			<?php }?>
			
			<?php if($brand_unit) { ?>
				<tr>
					<td style="vertical-align:bottom;border:none">Brand Unit</td>
					<td style="vertical-align:bottom;border:none">:</td>
					<td style="vertical-align:bottom;border:none"><?=$brand_unit->brand_unit_name?></td>
				</tr>
			<?php }?>

			<?php if($company) { ?>
				<tr>
					<td style="vertical-align:bottom;border:none">Company</td>
					<td style="vertical-align:bottom;border:none">:</td>
					<td style="vertical-align:bottom;border:none"><?=$company->company_name?></td>
				</tr>
			<?php }?>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
					<th style="width:2%">No</th>
					<th style="width:10%">Order ID</th>
					<th style="width:15%">Order Date</th>
					<th style="width:15%">Item Description</th>
					<th style="width:5%">Qty</th>
					<th style="width:10%">Name</th>
					<th style="width:10%">Brand Unit</th>
					<th style="width:10%">Departement</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $rows) {
				?>
				<tr>
					<td style="width:2%;text-align:center;vertical-align:top"><?=++$nomor?></td>
					<td style="vertical-align:top"><?=str_pad ($rows->order_id, 5, '0', STR_PAD_LEFT);?></td>
					<td style="vertical-align:top"><?=tgl($rows->order_date,'02')?></td>
					<td style="vertical-align:top"><?=$rows->item_description;?></td>
					<td style="text-align:center;vertical-align:top"><?=$rows->qty?></td>
					<td style="text-align:left;vertical-align:top"><?=$rows->user_name?></td>
					<td style="text-align:left;vertical-align:top"><?=$rows->brand_unit_name?></td>
					<td style="text-align:left;vertical-align:top"><?=$rows->departement_name?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="8">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>