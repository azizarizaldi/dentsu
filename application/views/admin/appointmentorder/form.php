<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $action=="add" ? "Create Appointment" : "Edit Appointment"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
              <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
              <?php } ?>
              <input type="hidden" name="order_name" value="Appointment" />
              <div class="box-body">
                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Order ID</label>
                  <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label>Visitor</label>
                  <select id="dvVisitor" class="form-control  validate-input" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a visitor ---</option>
                    <?php foreach($visitors as $user) { ?>
                    <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                                <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a visitor</p>
                </div>
                                <div class="form-group">
                  <label>Host</label>
                  <select id="dvHost" class="form-control  validate-input" name="order_pickup_by"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a host ---</option>
                    <?php foreach($senders as $user) { ?>
                    <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_pickup_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                                <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a host</p>
                </div>                      
                                                 
                <div class="form-group">
                  <label for="exampleInputEmail1">Purpose</label>
                  
                  <textarea class="form-control validate-input" name="order_receiver_address" rows="3" placeholder="Appointment Purposes"><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?></textarea>
                                 <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input appointment purpose</p>              
                </div>
                
              <!-- /.box-body -->
<?php if ($canModify) { ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
              </div>
              <?php } ?>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    
