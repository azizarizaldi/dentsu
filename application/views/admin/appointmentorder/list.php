    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Appointment Order List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-1">
                  <label>Status</label>
                  </div>
                                        <div>
<?php
	$get = $this->input->get();
	$selected = "";
	if (isset($get["data"])) {
		if($get["data"] == '1') {
			$selected = "selected";
		}
	}
?>
				<select class="form-control" id="order_status" style="width: 300px;">
                     <option value="">All</option>
                     <option <?=$selected?> value="1">New</option>
                     <option value="4">Done</option>
                     <option value="5">Rejected</option>
                  </select>
                  </div>
                    </div>
                </div>
              <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Created Date</th>
                    <th>Visitor</th>
                    <th>Host</th>
                    <th>Purpose</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Created Date</th>
                    <th>Visitor</th>
                    <th>Host</th>
                    <th>Purpose</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Reject</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                                  <label for="exampleInputPassword1">Reason</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="order_sender_latitude" value="1" <?php echo ! isset($row) || $row->order_sender_latitude == 1 ? "checked" : ""; ?>>
                      In a meeting
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="order_sender_latitude" value="2" <?php echo isset($row) && $row->order_sender_latitude == 2 ? "checked" : ""; ?>>
                      Leave
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="order_sender_latitude" value="3" <?php echo isset($row) && $row->order_sender_latitude == 3 ? "checked" : ""; ?>>
                      Out of Office
                    </label>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Note</label>
                  <textarea class="form-control" id="note" rows="3" placeholder="Note ..."></textarea>

                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:rejectNote($('#note').val(), $('input[name=order_sender_latitude]:checked').val());">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal modal-warning fade" id="modal-pickup">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Accept</h4>
              </div>
              <div class="modal-body">
                 <div class="form-group">
                  <label for="exampleInputEmail1">Note</label>
                  <div class="radio">
                    <label>
                      <input type="radio" id="pickupNoteRadio" name="pickupNoteRadio" value="On the Way" checked>
                      On the Way
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" id="pickupNoteRadio" name="pickupNoteRadio" value="Please wait 10 Minutes">
                      Please wait 10 Minutes
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" id="pickupNoteRadio" name="pickupNoteRadio" value="Please wait 30 Minutes">
                      Please wait 30 Minutes
                    </label>
                  </div>
                  <textarea class="form-control" id="pickupNote" rows="3" placeholder="Note ..."></textarea>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:doPickup($('#pickupNote').val(), $('#pickupNoteRadio:checked').val());">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
