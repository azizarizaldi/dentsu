<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>appointmentorder/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status": $("#order_status").val()
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "sender" },
            { "data": "receiver" },
            { "data": "courier" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";
                    if (data.order_status <= 2)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:pickup("+data.order_id+");\">Accept</button>";
                    }
                    if (data.order_status == 3)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:receive("+data.order_id+");\">Done</button>";
                    }

                    if (data.order_status < 3)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                    }
                    return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;  
    
     $("#order_status").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/appointmentorder/edit?id=" + orderId;
    }
    
    function receive(id)
    {
      orderId = id;
      $("#modal-receive").modal('show');
    }

    function receive(id)
    {
      if (confirm("Are you sure to complete a order?"))
      {
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>appointmentorder/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-pickup").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      }
    }

    function pickup(id)
    {
      orderId = id;
      $("#modal-pickup").modal('show');
    }
    
    function doPickup(note, noteChoice)
    {
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>appointmentorder/pickup"
            ,data: {"orderId": orderId, "note" : note, "addressNote": noteChoice}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-pickup").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      
    }
    
    function rejectNote(note, reason)
    {       
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note, "reason": reason}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

    
</script>