<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url();?>admin/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
        <a href="#">
            <i class="fa fa-download"></i> <span>Dashboard Banner</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/billboard/add"><i class="fa fa-plus"></i> Add Banner</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/billboard"><i class="fa fa-table"></i> Banner List</a></li>
          </ul>
        </li>
        <li class="header">ACCOUNT MANAGEMENT</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Company</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/company/add"><i class="fa fa-plus"></i> Add Company</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/company"><i class="fa fa-table"></i> Company List</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i> <span>Brand Unit</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/brandunit/add"><i class="fa fa-plus"></i> Add Brand Unit</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/brandunit"><i class="fa fa-table"></i> Brand Unit List</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i> <span>Departement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/dept/add"><i class="fa fa-plus"></i> Add Departement</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/dept"><i class="fa fa-table"></i> Departement List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/user/add"><i class="fa fa-plus"></i> Add User</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/user"><i class="fa fa-table"></i> User List</a></li>
            <li><a href="<?php echo base_url(); ?>admin/user/toolsimport"><i class="fa fa-file-excel-o"></i> Import</a></li>
          </ul>
        </li>
        <li class="header">COURIER & DOCUMENT MANAGEMENT</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-sort-alpha-desc"></i> <span>Order Priority Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/orderpriority/add"><i class="fa fa-plus"></i> Add Order Priority Data</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/orderpriority"><i class="fa fa-table"></i> Order Priority Data List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Logistic Company</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/logistic/add"><i class="fa fa-plus"></i> Add Logistic Company</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/logistic"><i class="fa fa-table"></i> Logistic Company List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Courier Package Type</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/packagetype/add"><i class="fa fa-plus"></i> Add Courier Package Type</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/packagetype"><i class="fa fa-table"></i> Courier Package Type List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i> <span>Internal Courier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>admin/documentinternal/add"><i class="fa fa-plus"></i> Create Order</a></li>
            <li><a href="<?php echo base_url(); ?>admin/documentinternal"><i class="fa fa-table"></i> Order List</a></li>
            <li><a href="<?php echo base_url(); ?>admin/listofmessenger"><i class="fa fa-users"></i> List Of Messenger</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i> <span>External Courier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>admin/documentexternal/add"><i class="fa fa-plus"></i> Create Order</a></li>
            <li><a href="<?php echo base_url(); ?>admin/documentexternal"><i class="fa fa-table"></i> Order List</a></li>
          </ul>
        </li>
                <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i> <span>Document In</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>admin/documentin/add"><i class="fa fa-plus"></i> Create Document In</a></li>
            <li><a href="<?php echo base_url(); ?>admin/documentin"><i class="fa fa-table"></i> Document In List</a></li>
          </ul>
        </li>
      <li class="header">OPERATIONAL CAR MANAGEMENT</li>
              <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Car</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/car/add"><i class="fa fa-plus"></i> Add Car</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/car"><i class="fa fa-table"></i> Car List</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Booking </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/bookingcar/add"><i class="fa fa-plus"></i> Create Booking</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/bookingcar"><i class="fa fa-table"></i> Booking List</a></li>
          </ul>
        </li>
         <li class="header">MEETING ROOM MANAGEMENT</li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i> <span>Meeting Room</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/meetingroom/add"><i class="fa fa-plus"></i> Add Meeting Room</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/meetingroom"><i class="fa fa-table"></i> Meeting Room List</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Booking </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/bookingmeetingroom/add"><i class="fa fa-plus"></i> Create Booking</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/bookingmeetingroom"><i class="fa fa-table"></i> Booking List</a></li>
          </ul>
        </li>
        <li class="header">STATIONARY MANAGEMENT</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Stationary Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/categorystationary/add"><i class="fa fa-plus"></i> Add Category</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/categorystationary"><i class="fa fa-table"></i> Category List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Stationary Item</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/stationaryitem/add"><i class="fa fa-plus"></i> Add Stationary Item</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/stationaryitem"><i class="fa fa-table"></i> Stationary Item List</a></li>
            <li><a href="<?php echo base_url(); ?>admin/warningstock"><i class="fa fa-warning"></i> Warning Stock</a></li>
            <li><a href="<?php echo base_url(); ?>admin/orderstock"><i class="fa fa-table"></i> Order Stock</a></li>
            <li><a href="<?php echo base_url(); ?>admin/receivestock"><i class="fa fa-check-square-o"></i> Receive Stock</a></li>
            <li><a href="<?php echo base_url(); ?>admin/historyorder"><i class="fa fa-history"></i> History Order</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Stationary Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/stationaryorder/add"><i class="fa fa-plus"></i> Create Order</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/stationaryorder"><i class="fa fa-table"></i> Order List</a><br /></li>
          </ul>
        </li>
        <li class="header">VISITOR MANAGEMENT</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Visitor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/visitor/add"><i class="fa fa-plus"></i> Add Visitor</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/visitor"><i class="fa fa-table"></i> Visitor List</a></li>
          </ul>
        </li>
                <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar-plus-o"></i> <span>Appointment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($canModify) { ?>}
            <li><a href="<?php echo base_url(); ?>admin/appointmentorder/add"><i class="fa fa-plus"></i> Create Appointment</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>admin/appointmentorder"><i class="fa fa-table"></i> Appointment List</a></li>
          </ul>
        </li>
		
        <li class="header">REPORTING MANAGEMENT</li>
		
                <li class="treeview">
          <a href="#">
            <i class="fa fa-file"></i> <span>Reporting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
		<li><a href="<?php echo base_url(); ?>admin/stationary_by_category"><i class="fa fa-table"></i> Report Stationary Item</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_meeting_room"><i class="fa fa-table"></i> Meeting Room</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_operational_car"><i class="fa fa-table"></i> Operational Car</a></li>		
		<li><a href="<?php echo base_url(); ?>admin/report_internal_courier"><i class="fa fa-table"></i> Internal Courier</a></li>        
		<li><a href="<?php echo base_url(); ?>admin/report_external_courier"><i class="fa fa-table"></i> External Courier</a></li>
		<li><a href="<?php echo base_url(); ?>admin/report_document_in"><i class="fa fa-table"></i> Incoming Package & Doc</a></li>
		<li><a href="<?php echo base_url(); ?>admin/visitor_report"><i class="fa fa-table"></i> Visitor</a></li>
          </ul>
        </li>
				
      </ul>
