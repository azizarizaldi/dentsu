<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Import User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">  
            <div class="box-body">  
              <div class="form-group">
                  <label for="exampleInputFile">File (csv)</label>
                  <input type="file" id="file" name="file" value="" />
              </div>
              <div class="form-group">
                                  <label for="exampleInputPassword1">Existing Data</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="method" value="1" checked>
                      Skip
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="method" value="2" >
                      Replace
                    </label>
                  </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
    </section>