<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/user/get",
        "columns": [
           
            { "data": "user_nik" },
            { "data": "user_name" },
            { "data": "user_phone" },
            { "data": "user_email" },
            { "data": "company_name" },
            { "data": "brand_unit_name" },
            { "data": "departement_name" },
            { "data": "type_name" }
        ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/user/edit?id=" + row.data().user_id;
    } );
   
  })
</script>