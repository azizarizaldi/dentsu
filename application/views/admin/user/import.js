<script>
(function ($) {
    "use strict";


    $("#form_input").on("submit", function(event) {
        event.preventDefault();
        var formData = new FormData($("#form_input")[0]);
        $.ajax({
            url: '<?php echo base_url(); ?>admin/user/doimport',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR) {
                alert(data.message)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

})(jQuery);
</script>