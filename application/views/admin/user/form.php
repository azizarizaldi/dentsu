<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit User" : "Add User"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="user_id" value="<?php echo $row->user_id; ?>" />
                <?php } ?>
              <input type="hidden" id="user_photo" name="user_photo" value="" />
              <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />
              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->user_id; ?>" disabled>
                </div>
                <?php } ?>
                   <div class="form-group">
                  <label>NIK</label>
                  <input type="text" class="form-control validate-input" placeholder="NIK" name="user_nik" value="<?php echo isset($row) ? htmlspecialchars($row->user_nik, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?>  />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input NIK</p>
                </div>
                   <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control validate-input" placeholder="Name" name="user_name" value="<?php echo isset($row) ? htmlspecialchars($row->user_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input name</p>
                </div>
                   <div class="form-group">
                  <label>Phone</label>
                  <input type="text" class="form-control validate-input" placeholder="Phone" name="user_phone" value="<?php echo isset($row) ? htmlspecialchars($row->user_phone, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input phone no</p>
                </div>
                   <div class="form-group">
                  <label for="exampleInputFile">Photo</label>
                  <input type="file" id="photo" name="photo" value="" />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->user_photo ? base_url().$row->user_photo : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->user_photo ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->user_photo) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                </div>
                 <div class="form-group">
                  <label>Company</label>
                  <select class="form-control" id="company">
                     <option value="">--- Select a company ---</option>
                    <?php foreach($companies as $company) { ?>
                    <option value="<?php echo $company->company_id; ?>" <?php echo isset($row) && ($row->brand_unit_company == $company->company_id) ? "selected" : "" ?>><?php echo htmlspecialchars($company->company_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                   <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a company</p>
                </div>
                 <div class="form-group">
                  <label>Brand Unit</label>
                  <select class="form-control"  id="brandUnit">
                     <option>--- Select a brand unit ---</option>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a brand unit</p>
                </div>
                 <div class="form-group">
                  <label>Departement</label>
                  <select class="form-control"  id="departement" name="user_departement">
                     <option value="">--- Select a departement ---</option>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select departement</p>
                </div>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control validate-input" name="user_email" placeholder="Enter email" id="user_email" value="<?php echo isset($row) ? htmlspecialchars($row->user_email, ENT_QUOTES) : ""; ?>" />
                   <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input email</p>
                </div>
                 <?php if ($action == "add")  { ?>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control validate-input" placeholder="Password" name="user_password" id="user_password">
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input password</p>
                </div>
                                 <div class="form-group">
                  <label for="exampleInputPassword1">Confirm Password</label>
                  <input type="password" class="form-control validate-input" placeholder="Confirm Password" name="confirm_password" id="confirm_password" />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input confirm password</p>
                </div>
                                 <?php } ?>
                                 <div class="form-group">
                                  <label for="exampleInputPassword1">Level</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="1" <?php echo ! isset($row) || $row->user_type == 1 ? "checked" : ""; ?>>
                      Karyawan
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="2" <?php echo isset($row) && $row->user_type == 2 ? "checked" : ""; ?>>
                      Office Boy
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="3" <?php echo isset($row) && $row->user_type == 3 ? "checked" : ""; ?>>
                      Driver
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="4" <?php echo isset($row) && $row->user_type == 4 ? "checked" : ""; ?>>
                      Kurir
                    </label>
                  </div>
                                    <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="5" <?php echo isset($row) && $row->user_type == 5 ? "checked" : ""; ?>>
                      Respsionis
                    </label>
                  </div>
                     <?php if ($canModify) { ?>               
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="97" <?php echo isset($row) && $row->user_type == 97 ? "checked" : ""; ?>>
                      Staff GA
                    </label>
                  </div>
                                                      <?php } ?>
                    <?php if ($canModify) { ?>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_type" value="98" <?php echo isset($row) && $row->user_type == 98 ? "checked" : ""; ?>>
                      Supervisor GA
                    </label>
                  </div>
                                       <?php } ?>
                </div>
                                 <?php if ($canModify) { ?>
                                 <div class="form-group">
                                  <label for="exampleInputPassword1">Status</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_status" value="1" <?php echo isset($row) && $row->user_status == 1 ? "checked" : ""; ?>>
                      New
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_status" value="2" <?php echo ! isset($row) || isset($row) && $row->user_status == 2 ? "checked" : ""; ?>>
                      Verified
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="user_status" value="3" <?php echo isset($row) && $row->user_status == 3 ? "checked" : ""; ?>>
                      Disabled
                    </label>
                  </div>
                                 <?php } ?>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        <?php if ($action == "edit" && $canModify) { ?>
      <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Password</h3>
                </div>
        
            
                <form id="form_password" role="form" method="post">
                    <input type="hidden" name="user_id" value="<?php echo $row->user_id; ?>" />
                    <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control validate-input" placeholder="Password" name="user_password" id="user_password">
                      <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input password</p>
                    </div>
                                     <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input type="password" class="form-control validate-input" placeholder="Confirm Password" name="confirm_password" id="confirm_password" />
                      <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input confirm password</p>
                    </div>
                    </div>
                </form>
                
                <div class="box-footer">
                <button type="button" id="btnChangePassword" class="btn btn-primary">Change</button>
              </div>
            </div>
        </div>
      </div>
      <?php } ?>
      </div>
      <!-- /.row -->

    </section>