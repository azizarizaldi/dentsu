<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login - Universe General Affair Assistant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/png" href="assets/login/images/icons/favicon.ico"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/login_new/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/login_new/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/login_new/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/login_new/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/login_new/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/login_new/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/login_new/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="assets/login_new/pages/css/themes/corporate.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="assets/login_new/pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header menu-pin menu-behind">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img style="width:100%" src="assets/login_new/img/bg_login_dentsu.jpg" data-src="assets/login_new/img/bg_login_dentsu.jpg" data-src-retina="assets/login_new/img/bg_login_dentsu.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-up sm-pull-up text-white p-l-20 m-t-20">          
        <img style="width:100%;" src="assets/login_new/img/Tagline.png" alt="logo" data-src="assets/login_new/img/Tagline.png" data-src-retina="assets/login_new/img/Tagline.png" style="width:60%">
        <!--<h2 class="semi-bold text-white">
					WE MAKE YOUR BUSINESS</h2>
          <p class="small">
    			BETTER THAN OTHERS
          </p>-->
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
		      <center>
          <img src="assets/login_new/img/LogoNew.png" alt="logo" data-src="assets/login_new/img/LogoNew.png" data-src-retina="assets/login_new/img/LogoNew.png" style="width:40%">
    		  </center>
          <form id="form-login" class="p-t-15" role="form" method="post">
            <div class="form-group form-group-default has-error">
              <label><b>Login</b></label>
              <div class="controls">
                <input type="text" name="username" placeholder="User Name" class="form-control " required>
              </div>
            </div>
            <div class="form-group form-group-default has-error">
              <label>Password</label>
              <div class="controls">
                <input type="password" class="form-control" name="pass" placeholder="Password" required>
              </div>
            </div>
            <button style="width:100%" class="btn btn-info btn-cons m-t-10" type="submit" id="btnLogin"><i class="fa fa-enter"></i>SIGN IN</button>
          </form>
        </div>
      </div>
    </div>
    <script src="assets/login_new/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/tether/js/tether.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/login_new/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/login_new/plugins/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/login_new/plugins/classie/classie.js"></script>
    <script src="assets/login_new/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/login_new/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <script src="assets/login_new/pages/js/pages.min.js"></script>
    <script >
        $(function () {
            $('#form-login').validate({ // initialize the plugin
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                submitHandler: function (form) {
                    $("#btnLogin").html('<i class="fa fa-spin fa-spinner"></i> Please wait...');
                    $("#btnLogin").attr('disabled',true);
                    var values = $("#form-login").serialize();
                    $.ajax({
                        url: "login/loginAdmin",
                        type: "post",
                        data: values,
                        dataType: "json",
                        success: function (response) {
                          if(response.status == true) {
                              location.href = "admin/dashboard";
                              $("#btnLogin").html('Login success!');
                              $("#btnLogin").removeAttr('disabled');
                          }
                          else {
                              alert(response.message);
                              $("#btnLogin").html('Sign in');
                              $("#btnLogin").removeAttr('disabled');
                          }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    });
                }
            });
        }) 
    </script>    
  </body>
</html>