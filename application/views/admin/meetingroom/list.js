<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    
   var dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/meetingroom/get",
        "columns": [
           
            { "data": "meeting_room_id" },
            { "data": "name" },
            { "data": "meeting_room_location" },
            { "data": "meeting_room_capacity" },
            { "data": "meeting_room_phone" },
            { "data": "meeting_room_desc" }
        ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/meetingroom/edit?id=" + tr.find('td:first').html();
    } );
   
  })
</script>