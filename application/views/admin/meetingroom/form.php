<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Meeting Room" : "Add Meeting Room"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="meeting_room_id" value="<?php echo $row->meeting_room_id; ?>" />
                <?php } ?>
              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->meeting_room_id; ?>" disabled>
                </div>
                <?php } ?>
                                                <input type="hidden" id="meeting_room_pic" name="meeting_room_pic" value="" />
                <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />


                
                   <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control validate-input" placeholder="Meeting Room Name" name="meeting_room_name" value="<?php echo isset($row) ? htmlspecialchars($row->meeting_room_name, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input meeting room name</p>
                </div>
                <div class="form-group">
                  <label>Room</label>
                  <input type="text" class="form-control validate-input" placeholder="Room" name="meeting_room_location" value="<?php echo isset($row) ? htmlspecialchars($row->meeting_room_location, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input meeting room phone no</p>
                </div>

                                      <div class="form-group">
                  <label>Capacity</label>
                  <input type="text" class="form-control validate-input" placeholder="Capacity" name="meeting_room_capacity" value="<?php echo isset($row) ? htmlspecialchars($row->meeting_room_capacity, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input meeting room capacity</p>
                </div>

                   <div class="form-group">
                  <label>Ext. Number</label>
                  <input type="text" class="form-control" placeholder="Phone" id="meeting_room_phone" name="meeting_room_phone" value="<?php echo isset($row) ? htmlspecialchars($row->meeting_room_phone, ENT_QUOTES) : ""; ?>" <?php echo $canModify ? "" : "disabled"; ?> />
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input meeting room ext. number no</p>
                </div>
                <div class="form-group">
                  <label>Room Picture</label>
                  <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->meeting_room_pic ? base_url().$row->meeting_room_pic : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->meeting_room_pic ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->meeting_room_pic) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input picture</p> 
                </div>
                   
                   <div class="form-group">
                  <label>Facility</label>
                  <textarea class="form-control validate-input" name="meeting_room_desc" rows="3" placeholder="Description ..."><?php echo isset($row) ? htmlspecialchars($row->meeting_room_desc, ENT_QUOTES) : ""; ?></textarea>
                </div>                   
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>