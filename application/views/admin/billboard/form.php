<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Banner" : "Add Banner"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="billboard_id" value="<?php echo $row->billboard_id; ?>" />
                <?php } ?>
              <input type="hidden" id="billboard_banner" name="billboard_banner" value="" />
              <input type="hidden" id="tmp_banner" name="tmp_banner" value="" />
              <div class="box-body">
                                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->billboard_id; ?>" disabled>
                </div>
                <?php } ?>
                   <div class="form-group">
                  <label for="exampleInputFile">Banner</label>
                  <input type="file" id="photo" name="photo" value="" />
                  <br />
                  <img id="previewPhoto" src="<?php echo isset($row) && $row->billboard_banner ? base_url().$row->billboard_banner : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->billboard_banner ? "" : 'style="display: none;"'; ?> />
                  <?php if (isset($row) && $row->billboard_banner) { ?>
                  <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                  <?php } ?>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
                                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
        
      </div>
      <!-- /.row -->

    </section>