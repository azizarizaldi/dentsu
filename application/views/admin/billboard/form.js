<script>
(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================    [ Validate ]*/

    
    function clear()
    {
               $("#previewPhoto").hide();
        $("#billboard_banner").val("");
    }

    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/billboard/save"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/billboard";
                <?php } ?>
            }
            ,error: function(data)
            {
                alert("Terjadi kesalahan")
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){

        if ($("#photo").val() == "")
        {
            alert("Please input banner");
            return;
        }

        post();

        return false;
    });


    $("#btnResetPhoto").on("click", function()
                       {
                            $("#billboard_banner").val("");
                            $("#previewPhoto").attr("src", "<?php echo base_url(); ?><?php echo isset($row) ? $row->billboard_banner : ""; ?>");
                       });
   
    $("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/billboard/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/billboard";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });

                       
        var files;
        $("#photo").on("change", function(event)
                                      {
                                        event.preventDefault();
                                        var formData = new FormData($("#form_input")[0]);
                                        $.ajax({
        url: '<?php echo base_url(); ?>admin/billboard/savePhoto',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(! data.error)
            {
                // Success so call function to process the form
                $("#previewPhoto").attr("src", "<?php echo base_url(); ?>"+data.src);
                $("#previewPhoto").show();
                
                $("#tmp_banner").val(data.src);
                $("#billboard_banner").val(data.photo);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
                                        
                                        
                                        
                                      });
                       
                       

})(jQuery);
</script>