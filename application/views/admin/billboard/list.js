<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/billboard/get",
        "columns": [
           
            { "data": "billboard_created_date" },
            { "data": "billboard_banner_url" }
        ],
        "order": [[0, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/billboard/edit?id=" + row.data().billboard_id;
    } );
   
  })
</script>