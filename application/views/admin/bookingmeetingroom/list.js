<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
   $("#meetingRoomID").select2();
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>bookingmeetingroom/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status"		: $("#order_status").val(),
           "orderDate"	: $("#datepicker").val(),
           "meetingRoomID"	: $("#meetingRoomID").val()
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "receiver" },
            { "data": "courier" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                  var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";

                  if (data.order_status == 1)
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:approve("+data.order_id+");\">Approve</button>";
                  }

                  if (data.order_status == 2)
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:done("+data.order_id+");\">Done</button>";
                  }

                  if ((data.order_status != 4) && (data.order_status != 5))
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                  }

                  if (data.order_status != 5)
                  {
                    buttons += "<button type=\"button\" class=\"btn btn-block btn-default\" onclick=\"javascript:attendance("+data.order_id+");\">Attendance List</button>";
                  }

                  return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
      
    $("#order_status").on("change", function() {
		dt.ajax.reload();
	});
  
	$("#datepicker").on("change", function() {
		dt.ajax.reload();
	});

	$("#meetingRoomID").on("change", function() {
		dt.ajax.reload();
	});
  

  var orderId;  
  function attendance(id, lat, lng)
    {
        orderId = id;
      
        $("#modal-default").modal('show');
        
    }
    
    $('#modal-default').on('shown.bs.modal', function (e) {
      getAttendanceList();
    });
    
    function getAttendanceList()
    {
      $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>bookingmeetingroom/getAttendanceList"
            ,data: {"orderId": orderId}
            ,success: function (data)
            {
                if (data.length == 0)
                {
                  $("#dvAttendanceList").html("No attendance List");
                }
                else
                {
                  var html = "";
                  for(var i=0; i < data.length; i++)
                  {
                    html += '<div class="form-group"><label>'+(i+1)+". "+data[i].user_name+'</div></label>';
                  }
                  
                  $("#dvAttendanceList").html(html);
                }
              
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
  
  function approve(oId)
  {
    if (confirm("Are you sure to approve booking meeting room?"))
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>bookingmeetingroom/approve"
            ,data: {"orderId": oId}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
  }
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/bookingmeetingroom/edit?id=" + orderId;
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

  function done(id)
    {
      if (confirm("Are you sure to complete booking meeting room?"))
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>bookingmeetingroom/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    }
    
        //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });    
</script>