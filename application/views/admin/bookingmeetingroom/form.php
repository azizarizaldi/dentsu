<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $action=="add" ? "Create Order Meeting Room" : "Edit Order Meeting Room"; ?></h3>
            </div>
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
                <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
                <?php } ?>
                <div class="box-body">
                    <?php if ($action == "edit") { ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Order ID</label>
                        <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
                    </div>
                    <?php } ?>
					<div class="row">
					<div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Meeting Room <button type="button" class="btn btn-block btn-primary" onclick="javascript:meetingroom()"><?php echo $action == "edit" ? "Change Meeting Room" : "Find Meeting Room"; ?></button></label>
                        <div id="dvCar">
                            <?php if ($action == "edit") {
                               printf("%s %s %s %s %s", $row->meeting_room_name, $row->car_brand, $row->car_model, $row->order_start_date_fmt,  $row->order_end_date_fmt); 
                            }?>
                        </div>
                        <input class="validate-input" type="hidden" id="order_ref" name='order_ref' value="<?php echo isset($row) ? htmlspecialchars($row->order_ref, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />
                        <input type="hidden" id="order_start_date" name='order_start_date' value="<?php echo isset($row) ? htmlspecialchars($row->order_start_date_fmt, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />
                        <input type="hidden" id="order_end_date" name='order_end_date' value="<?php echo isset($row) ? htmlspecialchars($row->order_end_date_fmt, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?> />
                        <input type="hidden" id="order_receiver_latitude" name="order_receiver_latitude" value="-1" />
                        <input type="hidden" id="order_receiver_longitude" name="order_receiver_longitude" value="-1" />
                        <input type="hidden" id="order_receiver_address" name="order_receiver_address" value="meetingroom" />
                        <input type="hidden" id="order_photo" name="order_photo" value="" />

                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a meeting room</p>              
                    </div>
                    </div>
					<div class="col-md-6">
                    <div class="form-group">
                        <label>Book a meeting room</label>
                        <select style="width:100%" id="booker" class="form-control  validate-input" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                            <option value="">--- Select a sender ---</option>
                            <?php foreach($senders as $user) { ?>
                            <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
                    </div>
                    </div>
                    </div>
					<div class="row">
                    <div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Meeting Topics</label>
						<div class="col-sm-10">
                        <input type="text" class="form-control validate-input" id="order_name" name="order_name" placeholder="Meeting Topics" value="<?php echo isset($row) ? htmlspecialchars($row->order_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input meeting topics</p>              
                    </div>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Meeting with</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="order_sender_latitude" value="2" <?php echo isset($row) && $row->order_sender_latitude == 2 ? "checked" : ""; ?>>
                                Internal 
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="order_sender_latitude" value="1" <?php echo isset($row) && $row->order_sender_latitude == 1 ? "checked" : ""; ?>>
                                Vendor 
                            </label>
                        </div>
                        <div class="row" style="margin-top:-15px">
                            <div class="col-lg-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="order_sender_latitude" value="0" <?php echo ! isset($row) || $row->order_sender_latitude == 0 ? "checked" : ""; ?>>
                                        Client 
                                    </label>                
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="order_receiver_name" name="order_receiver_name" placeholder="Client Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_sender_latitude == 0 ? $row->order_receiver_name : "", ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                            </div>
                        </div>						
                                                           
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input client name</p>              
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No of Attendance</label>
                        <input type="text" class="form-control validate-input" id="order_receiver_phone" name="order_receiver_phone" placeholder="No of attandance" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_phone, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input no of attendance</p>              
                    </div>
                    <div class="form-group">
                        <label>Attendee List</label>
                        <select id="attendanceId" class="form-control select2 select2-hidden-accessible" name="attendaces[]" multiple="" data-placeholder="Select a attende" style="width: 100%;" tabindex="-1" aria-hidden="true">
                            <?php foreach($senders as $user) { ?>
                            <option value="<?php echo $user->user_id; ?>"<?php if (isset($attendances['user'.$user->user_id])) { echo " selected";} ?> ><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select attendee List</p>
                    </div>
                </div>
                <?php if ($canModify) { ?>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
                </div>
                <?php } ?>
            </form>
          </div>
        </div>
    </div>
</section>
<div class="modal modal-info fade" id="modal-info" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Meeting Room Booking Period</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">                    
                    <label>Start</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation1" readonly>
                    </div>    
                </div>
                <div class="form-group">                    
                    <label>End</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation2" readonly>
                    </div>    
                </div>
                <div class="form-group">
                    <label>Available Meeting Room</label>
                    <div id="availCar"></div>
                    <br />
                    <label>Suggestion Meeting Roomr</label>
                    <div id="suggestCar"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" id="btnClose">Close</button>
                <button type="button" class="btn btn-outline" onclick="javascript:save();">Save changes</button>
            </div>
        </div>
    </div>
</div>
