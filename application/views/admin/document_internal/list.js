<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
$("#idCourier").select2();
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>documentinternal/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status"		: $("#order_status").val(),
           "orderDate"	: $("#datepicker").val(),
           "courierId"	: $("#idCourier").val(),  
           "category"	: "1",  
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "sender" },
            { "data": "receiver" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";
                    if (data.order_status == 1)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:courier("+data.order_id+","+data.order_courier_by+");\">Select a courier</button>";
                    }
                    if (data.order_status == 2)
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:pickUp("+data.order_id+",);\">Pick Up</button>";
                    }    
   
                    if ((data.order_status == 3) || (data.order_status == 4))
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-default\" onclick=\"javascript:showMap("+data.order_id+", "+data.order_receiver_latitude+","+data.order_receiver_longitude+");\">Lacak</button>";
                    }
                    if (data.order_status == 3)
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:receive("+data.order_id+");\">Receive</button>";
 
                    }

                    if ((data.order_status != 4) && (data.order_status != 5))
                    {
                        buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                    }
                    
                    return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
  
  var receiverMarker = null;
  var receiverLat = null;
  var receiverLng = null;

  var courierLat = null;
  var courierLng = null;
  var courierMarker = null;
  
     $("#order_status").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
								 
     $("#datepicker").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });  

	 $("#idCourier").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
  
  function courier(oId, cId)
  {
      orderId = oId;
      courierId - cId;
    
      $("#modal-info").modal('show');      
      $("#courierId").val(cId);
  }
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/documentinternal/edit?id=" + orderId;
    }
    
    function receive(id)
    {
      if (confirm("Are you sure to set order status to completed?"))
      {
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/complete"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {                  
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      }
    }
    
    function pickUp(id)
    {
      if (confirm("Are you sure to pick up order?"))
      {
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/pickUp"
            ,data: {"orderId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {                  
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      }
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

    function changeCourier(id)
    {
      if (id == "")
      {
        alert("Please select a courier");
        return;
      }
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/setCourier"
            ,data: {"orderId": orderId,  "courierId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-info").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
    $('#modal-default').on('shown.bs.modal', function (e) {
      initMap();
      
      google.maps.event.trigger(map, "resize");
      
      var position = new google.maps.LatLng(receiverLat, receiverLng);      
      receiverMarker = new google.maps.Marker({
                        position: position,
                        map: map});
 
      map.panTo(position);      
      map.setCenter(position);
      
      showCourierPosition();
});
    
    function showMap(id, lat, lng)
    {
        orderId = id;
        receiverLat = lat;
        receiverLng = lng;
      
        $("#modal-default").modal('show');
        
    }
    
    function reloadMap()
    {
      courierMarker.setMap(null);
      showCourierPosition();
    }
    
    function showCourierPosition()
    {
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/getCourierPosition"
            ,data: {"orderId": orderId}
            ,success: function (data)
            {
                if (data.status)
                {
                    if (data.order_pickup_latitude == null && data.order_pickup_longitude == null)
                    {
                      alert("Courier location is not define")
                      return;
                    }
                    
                    var position = new google.maps.LatLng(data.order_pickup_latitude, data.order_pickup_longitude);  
                    courierMarker = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "<?php echo base_url(); ?>assets/app/motorcycle.png"});
                    
                    map.panTo(position); 
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      
    }
	
        //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
	
</script>