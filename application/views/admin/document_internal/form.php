<section class="content">
   <div class="row">
   <!-- left column -->
   <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
         <div class="box-header with-border">
            <h3 class="box-title"><?php echo $action=="add" ? "Create Internal Courier" : "Edit Internal Courier"; ?></h3>
         </div>
         <!-- /.box-header -->
         <!-- form start -->
         <form id="form_input" role="form" method="post">
            <?php if ($action == "edit") { ?>
            <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
            <?php } ?>
            <input type="hidden" id="order_receiver_latitude" name="order_receiver_latitude" value="<?php if (isset($row) && $row->order_receiver_latitude) { echo $row->order_receiver_latitude; }; ?>" />
            <input type="hidden" id="order_receiver_longitude" name="order_receiver_longitude" value="<?php if (isset($row) && $row->order_receiver_latitude) { echo $row->order_receiver_longitude; }; ?>" />
            <input type="hidden" id="order_photo" name="order_photo" value="" />
            <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />
            <div class="box-body">
               <?php if ($action == "edit") { ?>
               <div class="form-group">
                  <label for="exampleInputEmail1">Order ID</label>
                  <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="exampleInputEmail1">Item Description</label>
                  <input type="text" class="form-control validate-input" id="order_name" name="order_name" placeholder="Document Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input Item Description</p>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Sender</label>
                        <select class="form-control select2 validate-input" style="width: 100%;"  id="sender" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                           <option value="">--- Select a sender ---</option>
                           <?php foreach($senders as $user) { ?>
                           <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                           <?php } ?>
                        </select>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="exampleInputFile">Photo</label>
                        <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                        <br />
                        <img id="previewPhoto" src="<?php echo isset($row) && $row->order_photo ? base_url().$row->order_photo : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->order_photo ? "" : 'style="display: none;"'; ?> />
                        <?php if (isset($row) && $row->order_photo) { ?>
                        <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                        <?php } ?>
                        <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input photo</p>
                     </div>
                  </div>
               </div>
               <?php if ($canModify) { ?>
               <div class="form-group">
                  <label for="exampleInputPassword1">Priority</label>
                  <?php foreach($priorities as $priority)
                     {
                     ?>
                  <div class="radio">
                     <label>
                     <input type="radio" name="order_priority" value="1" <?php echo ! isset($row) || $row->order_priority == $priority->priority_id ? "checked" : ""; ?>>
                     <?php echo $priority->priority_name; ?>
                     </label>
                  </div>
                  <?php
                     } ?>
                  <?php } ?>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Name</label>
                           <input type="text" class="form-control validate-input" id="order_receiver_name" name="order_receiver_name" placeholder="Receiver Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                           <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver name</p>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Phone No.</label>
                           <input type="text" class="form-control validate-input" id="order_receiver_phone" name="order_receiver_phone" placeholder="Receiver Phone No." value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_phone, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                           <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver phone</p>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Address</label>
                     <div>Enter a location: <input id="mapInput" type="text" placeholder="Enter a location" style="width: 250px;"></div>
                     <div id="map" style="width: 100%;height: 300px;"></div>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessageMarker">Please select receiver marker</p>
                     <br />
                     <textarea class="form-control validate-input" name="order_receiver_address" id="order_receiver_address" rows="3" placeholder="Receiver Address ..."><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?></textarea>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver address</p>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Detail Address</label>
                     <textarea class="form-control" name="order_receiver_address_note" rows="3" placeholder="Detail Receiver Address..."><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address_note, ENT_QUOTES) : ""; ?></textarea>
                  </div>
                  </div>
                     <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1"> Note</label>
                     <textarea class="form-control" name="order_note" rows="3" placeholder="Note..."><?php echo isset($row) ? htmlspecialchars($row->order_note, ENT_QUOTES) : ""; ?></textarea>
                  </div>
                  </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <?php if ($canModify) { ?>
               <div class="box-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
               </div>
               <?php } ?>
         </form>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <!--/.col (right) -->
   </div>
   <!-- /.row -->
</section>
<script>
   var marker = null;
       function initMap() {
   // Create a map object and specify the DOM element for display.
   var map = new google.maps.Map(document.getElementById('map'), {
       <?php if (isset($row) && isset($row->order_receiver_latitude)
      && isset($row->order_receiver_longitude)
      && $row->order_receiver_latitude
      && $row->order_receiver_longitude) { ?>
                 center: {lat: <?php echo $row->order_receiver_latitude; ?>, lng: <?php echo $row->order_receiver_longitude; ?>},
       <?php } else { ?>
           center: {lat: -6.1751, lng: 106.8650},
       <?php } ?>
     zoom: 16
   });
   
   var mapInput = document.getElementById('mapInput');
   var geocoder = new google.maps.Geocoder;
   var autocomplete = new google.maps.places.Autocomplete(mapInput);
   //autocomplete.setTypes(['geocode']);
   autocomplete.setComponentRestrictions(
       {'country': ['id']});
   autocomplete.bindTo('bounds', map);
   autocomplete.addListener('place_changed', function() {
       var place = autocomplete.getPlace();
       if (!place.geometry) {
           window.alert("No details available for input: '" + place.name + "'");
           return;
       }
       
       if (place.geometry.viewport) {
           map.fitBounds(place.geometry.viewport);
       }
       else {
           map.setCenter(place.geometry.location);
           map.setZoom(17);  // Why 17? Because it looks good.
       }
     
       if (marker != null)
       {
           marker.setMap(null);
       }
       
       var address = place.formatted_address;
       if (place.name)
       {
           address = place.name + " " + address;
       }
       
       $("#mapInput").val("");
       $("#order_receiver_address").val(address);
       $("#order_receiver_latitude").val(place.geometry.location.lat());
       $("#order_receiver_longitude").val(place.geometry.location.lng());
       
       marker = new google.maps.Marker({
           position: place.geometry.location,
           map: map
       });
       map.panTo(place.geometry.location);
   });
   
   google.maps.event.addListenerOnce(map, 'idle', function(){
       <?php if (isset($row) && isset($row->order_receiver_latitude)
      && isset($row->order_receiver_longitude)
      && $row->order_receiver_latitude
      && $row->order_receiver_longitude) { ?>
                 
                   var position = new google.maps.LatLng(<?php echo $row->order_receiver_latitude; ?>, <?php echo $row->order_receiver_longitude; ?>);
                   marker = new google.maps.Marker({
                   position: position,
                   map: map});
                   
                   map.panTo(position);
       <?php } ?>
   });
   
   map.addListener('click', function(e) {
       $("#errorMessageMarker").hide();
       if (marker != null)
       {
           marker.setMap(null);
       }
       $("#order_receiver_latitude").val(e.latLng.lat());
       $("#order_receiver_longitude").val(e.latLng.lng());
       
       marker = new google.maps.Marker({
           position: e.latLng,
           map: map
       });
       map.panTo(e.latLng);
       
       geocoder.geocode({'location': e.latLng}, function(results, status) {
               if (status === 'OK')
               {
                   if (results[0]) {
                       $("#order_receiver_address").val(results[0].formatted_address);
                   } 
               }
       });
   });
   }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item("gMapKey"); ?>&libraries=places&callback=initMap" async defer></script>