<script>

(function ($) {
    "use strict";

    $("#sender").select2();

    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        });

        $("#order_photo").val("");
        $("#previewPhoto").hide();

        if (marker != null)
        {
            marker.setMap(null);
            marker = null;
        }

        $("#sender").val("").trigger("change");

        $('textarea').each(function(){
            $(this).val("");    
        });
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentinternal/create"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/documentinternal";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if (marker == null)
        {
            $("#errorMessageMarker").show();
            check = false;
        }
        
        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   
    $("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/company/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/company";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });
    
    $("#btnResetPhoto").on("click", function()
                       {
                            $("#order_photo").val("");
                            $("#previewPhoto").attr("src", "<?php echo base_url(); ?><?php echo isset($row) ? $row->order_photo : ""; ?>");
                       });
    
    var files;
                       $("#photo").on("change", function(event)
                                      {
                                        event.preventDefault();
                                        var formData = new FormData($("#form_input")[0]);
                                        $.ajax({
        url: '<?php echo base_url(); ?>admin/documentinternal/savePhoto',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(! data.error)
            {
                // Success so call function to process the form
                $("#previewPhoto").attr("src", "<?php echo base_url(); ?>"+data.src);
                $("#previewPhoto").show();
                
                $("#tmp_photo").val(data.src);
                $("#order_photo").val(data.photo);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
                                        
                                        
                                        
                                      });

})(jQuery);
</script>