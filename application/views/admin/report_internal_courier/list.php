    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Report Book / Month Internal Courier</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Periode</label>
						<input type="text" class="form-control pull-right" id="range_date">
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Courier</label>
                        <select class="form-control" id="order_ref" style="width: 100%;">
                           <option value="">Select Courier </option>
						   <?php						
							$this->db->where("user_type","4");
							foreach($this->db->get('_user')->result() as $get) { ?>
							<option value="<?=$get->user_id?>"><?=$get->user_name?></option>
							<?php } ?>
						</select>
                     </div>					 
                     <div class="col-md-4" style="margin-top:10px">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" id="btnOrder" onclick="print()"  class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>						  
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Order Date</th>
                    <th>Sender</th>
                    <th>Item</th>
                    <th>Description</th>
                    <th>Receiver</th>
                    <th>Schedule</th>
                    <th>Courier</th>
                    <th>Status</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Order Date</th>
                    <th>Sender</th>
                    <th>Item</th>
                    <th>Description</th>
                    <th>Receiver</th>
                    <th>Schedule</th>
                    <th>Courier</th>
                    <th>Status</th>
				 </tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

