<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    var dt;
	$("#stationary_order_id").select2();
    $(function() {
        dt = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url(); ?>listofmessenger/get",
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["_all"]
            }],
            "columns": [{
                "data": "user_nik"
            }, {
                "data": "user_photo"
            }, {
                "data": "user_name"
            }, {
                "data": "user_phone"
            }, {
                "data": "user_email"
            }
			],
            "order": [
                [1, 'asc']
            ]
        });
    });
</script>