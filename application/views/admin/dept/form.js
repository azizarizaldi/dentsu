<script>
var init = true;
(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        })
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/dept/save"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/dept";
                <?php } ?>
            }
            ,error: function(data)
            {
                alert("Terjadi kesalahan")
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   
$("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/dept/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/dept";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });

                       $("#company").on("change", function()
                                        {
 
                                           if ($(this).val() == "")
                                           {
                                                     $('#departement_brand_unit')
    .find('option')
    .remove()
    .end()
    .append('<option value="">--- Select a brand unit---</option>')
    .val('');
                                           }
                                           else
                                           {
                                                $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>admin/brandunit/getByCompany"
            ,data: {"id": $(this).val()}
            ,success: function (data)
            {
                $('#departement_brand_unit')
    .find('option')
    .remove()
    .end()
    .append('<option value="">--- Select a brand unit---</option>')
    .val('');
                
                for(var i=0; i < data.length; i++)
                {
                    <?php if ($action == "edit") { ?>
                            if (init && (<?php echo $row->brand_unit_id; ?> == data[i].brand_unit_id))
                            {
                                $('#departement_brand_unit').append('<option value="'+data[i].brand_unit_id+'" selected>'+data[i].brand_unit_name+'</option>');                                                            
                            }
                            else
                            {
                                $('#departement_brand_unit').append('<option value="'+data[i].brand_unit_id+'">'+data[i].brand_unit_name+'</option>');                    
                                
                            }
                    <?php } else { ?>
                        $('#departement_brand_unit').append('<option value="'+data[i].brand_unit_id+'">'+data[i].brand_unit_name+'</option>');                    
                    <?php } ?>
                }
                
                 init = false;
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          });
                                           }
                                        });
                       
                       <?php if ($action == "edit") { ?>
                       $("#company").change();
                       <?php } ?>

})(jQuery);
</script>