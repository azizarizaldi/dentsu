<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo ($action == "edit") ? "Edit Departement" : "Add Departement"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
                            <input type="hidden" name="departement_id" value="<?php echo $row->departement_id; ?>" />
                <?php } ?>
              <div class="box-body">
                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->departement_id; ?>" disabled>
                </div>
                <?php } ?>

                 <div class="form-group">
                  <label>Company</label>
                  <select class="form-control  validate-input" id="company"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a company ---</option>
                    <?php foreach($companies as $company) { ?>
                    <option value="<?php echo $company->company_id; ?>"<?php if (isset($row) && $company->company_id == $row->brand_unit_company) echo " selected";  ?>><?php echo htmlspecialchars($company->company_name, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                  <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a company</p>
                </div>
                 <div class="form-group">
                  <label>Brand Unit</label>
                  <select class="form-control  validate-input" name="departement_brand_unit" id="departement_brand_unit"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a brand unit ---</option>
                  </select>
                                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select brand unit</p>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Departement Name</label>
                  <input type="text" class="form-control validate-input" id="departement_name" name="departement_name" placeholder="Departement Name" value="<?php echo isset($row) ? htmlspecialchars($row->departement_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                                   <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input departement name</p>

                </div>
              </div>
              <!-- /.box-body -->
<?php if ($canModify) { ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo ($action == "edit") ? "Update" : "Submit"; ?></button>
                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
              <?php } ?>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>