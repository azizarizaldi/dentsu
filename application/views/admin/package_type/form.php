<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $action=="add" ? "Add Courier Package Type" : "Edit Courier Package Type"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
                <?php if ($action == "edit") { ?>
              <input type="hidden" name="package_type_id" value="<?php echo $row->package_type_id; ?>" />
              <?php } ?>
              <div class="box-body">
                <?php if ($action == "edit") { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->package_type_id; ?>" disabled>
                </div>
                <?php } ?>
                 <div class="form-group">
                  <label>Company</label>
                  <select class="form-control  validate-input" name="package_type_company"<?php if (! $canModify) { echo " disabled"; }?>>
                     <option value="">--- Select a company ---</option>
                    <?php foreach($companies as $company) { ?>
                    <option value="<?php echo $company->logistic_id; ?>" <?php if (isset($row) && $company->logistic_id == $row->package_type_company) { echo "selected"; } ?>><?php echo htmlspecialchars($company->logistic_company, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                                <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a company</p>
  
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Courier Package</label>
                  <input type="text" class="form-control validate-input" id="package_type_name" name="package_type_name" placeholder="Courier Package Type Name" value="<?php echo isset($row) ? htmlspecialchars($row->package_type_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                                 <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input courier package name name</p>
  
                </div>
                                
                                     <div class="form-group">
                  <label for="exampleInputEmail1"> Note</label>
                  <textarea class="form-control" name="package_type_note" rows="3" placeholder="Note..."><?php echo isset($row) ? htmlspecialchars($row->package_type_note, ENT_QUOTES) : ""; ?></textarea>
                </div>
              </div>
              <!-- /.box-body -->
<?php if ($canModify) { ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Add"; ?></button>
                <?php if ($action == "edit") { ?>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
                <?php } ?>
              </div>
              <?php } ?>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>