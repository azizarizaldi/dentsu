    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Courier Package Type List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                 <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-1">
                  <label>Company</label>
                  </div>
                                        <div>
                  <select class="form-control" id="package_type_company" style="width: 300px;">
                     <option value="">All</option>
                    <?php foreach($companies as $company) { ?>
                    <option value="<?php echo $company->logistic_id; ?>"><?php echo htmlspecialchars($company->logistic_company, ENT_QUOTES); ?></option>
                    <?php } ?>
                  </select>
                  </div>
                    </div>
                </div>
              <table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Package Type Name</th>
                    <th>Creation Date</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Package Type Name</th>
                    <th>Creation Date</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
