<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>admin/packagetype/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "company": $("#package_type_company").val()
         } );
      }
          
        },
        "columns": [
           
            { "data": "package_type_id" },
            { "data": "logistic_company" },
            { "data": "package_type_name" },
            { "data": "package_type_created_date" }
        ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/packagetype/edit?id=" + tr.find('td:first').html();
    } );
   
   $("#package_type_company").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
   
  })
</script>