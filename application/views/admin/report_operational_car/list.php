    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Report Book / Month Operational Car</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Periode</label>
						<input type="text" class="form-control pull-right" id="range_date">
                     </div>
                     <div class="col-md-4" style="margin-top:10px">
                        <label>Car</label>
                        <select class="form-control" id="order_ref" style="width: 100%;">
                           <option value="">Select Car </option>
						   <?php						   
							foreach($this->db->get('car')->result() as $get) { ?>
							<option value="<?=$get->car_id?>"><?=$get->car_brand.' - '.$get->car_model?></option>
							<?php } ?>
						</select>
                     </div>					 
                     <div class="col-md-4" style="margin-top:10px">
                        <label>&nbsp;</label>
						<div>
						  <button type="button" id="btnOrder" onclick="print()"  class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>						  
						</div>
                     </div>
                  </div>
			<hr/>
			<form method="post" id="form_data">
			<table id="example1" class="table table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Date</th>
                    <th>Username</th>
                    <th>Pickup</th>
                    <th>Destionation</th>
                    <th>Periode</th>
                    <th>Status</th>
                 </tr>
                </thead>
                <tfoot>
                  <tr>
					<th>Order ID</th>
                    <th>Date</th>
                    <th>Username</th>
                    <th>Pickup</th>
                    <th>Destionation</th>
                    <th>Periode</th>
                    <th>Status</th>
					</tr>
                </tfoot>
              </table>
			</form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
</script>

