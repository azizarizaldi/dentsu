<html>
	<head>
	<title>OPERATIONAL CAR BOOKING</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style>
			body {
				font-family:arial;
			}
						
			hr {
				display: block;
				height: 1px;
				background: transparent;
				width: 100%;
				border: none;
				border-top: solid 1px #aaa;
			}			
			
			table, td, th {
				border: 1px solid black;
				padding-left:2px;
			}

			#border {
				border-collapse: separate;
			}

			#no_border {
				border-collapse: collapse;
			}
			
			@media print {
				h5 {
					color: #ffffff !important;
				}
			}
						
		</style>
	</head>
	<body onload="window.print()">
		<table style="border:none;" align="right">
			<tr>
				<td class="title-top" style="vertical-align:bottom;border:none;color:white!important;border: 1px solid black!important;background-color:black!important"><h5 style="margin-left:5px;margin-right:5px">OPERATIONAL CAR BOOKING</h5></td>
			</tr>
		</table>
		<br/>
		<hr style="margin-top:30px"/>
		<hr style="margin-top:-20px"/>
		<p style="text-align:right">Date : <?=tgl(date("Y-m-d"),'02')?></p>
		<table style="width:auto;border:none;font-size:13px;">
			<tr>
				<td style="vertical-align:bottom;border:none">Order Date</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=tgl($firstDate,'02').' s/d '.tgl($lastDate,'02')?></td>
			</tr>
			<?php
			if(!empty($order_ref)) {
				$this->db->where("car_id",$order_ref);
				$getData = $this->db->get("car")->row();
			?>
			<tr>
				<td style="vertical-align:bottom;border:none">Type Of Car</td>
				<td style="vertical-align:bottom;border:none">:</td>
				<td style="vertical-align:bottom;border:none"><?=$getData->car_brand.' - '.$getData->car_model?></td>
			</tr>
			<?php }?>
		</table>
		<br/>
		<table style="width:100%;font-size:13px;" id="no_border">
			<thead>
				<tr>
                    <th rowspan="2" style="text-align:center;width:3%">No</th>
                    <th rowspan="2" style="text-align:center;width:10%">Order ID</th>
                    <th rowspan="2" style="text-align:center;width:10%">Booking Date</th>
                    <th rowspan="2" style="text-align:center;width:10%">Username</th>
                    <th colspan="2" style="text-align:center;width:40%">Location</th>
                    <th rowspan="2" style="text-align:center;width:10%">Schedule</th>
                    <th rowspan="2" style="text-align:center;width:10%">Status</th>
				</tr>
				<tr>
					<th>Pickup</th>
                    <th>Destination</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($query) {
					$nomor = 0;
					foreach($query as $row) {
				?>
				<tr>
					<td style="width:2%;text-align:center;vertical-align:top"><?=++$nomor?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->order_id?></td>
					<td style="vertical-align:top;text-align:center"><?=tgl($row->order_date,'2')?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->user_name;?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->pickup?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->destination?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->start_date.' - '.$row->end_date?></td>
					<td style="vertical-align:top;text-align:center"><?=$row->status_order?></td>
				</tr>
				<?php }
				}
				else { ?>
				<tr>
					<td colspan="8">Data not available</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</body>
</html>