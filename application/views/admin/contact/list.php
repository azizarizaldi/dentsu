<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Contact List</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-4">
                <label>Filter By Category</label>
                <select class="form-control" id="id_category" style="width: 100%;">
                  <option value="">Select Category List</option>
                    <?php
                    $this->db->where("type_category","5");
                    foreach($this->db->get('setting_category')->result() as $get) { ?>
                    <option value="<?=$get->id?>">
                      <?=$get->name?>
                    </option>
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-4">
                    <a href="<?php echo base_url(); ?>admin/contact/add" class="btn btn-primary" style="margin-top:25px"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add contact</a>
                    <button type="button" onclick="printData()" class="btn btn-default" style="margin-top:25px"><i class="fa fa-print"></i> &nbsp;&nbsp;Print Data</button>
              </div>
              <div class="col-md-4">
              </div>
            </div>
          </div>

          <table id="example1" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th>ID Contact</th>
                <th>Category</th>
                <th>Contact Name</th>
                <th>Phone Primary</th>
                <th>Phone Alternative</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID contact</th>
                <th>Category</th>
                <th>contact Name</th>
                <th>Contact Person</th>
                <th>Phone</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>