<script>

(function ($) {
    "use strict";

    $("#sender").select2();

    /*==================================================================
    [ Focus input ]*/
    $('.validate-input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
    
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input');
    
    function clear()
    {
        $('.validate-input').each(function(){
            $(this).val("");    
        })

        $("#sender").val("").trigger("change");

        $("#order_photo").val("");
        $("#previewPhoto").hide();
    }
    
    function post()
    {
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentexternal/create"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                
               <?php if ($action != "edit") { ?>
                clear();
                <?php } else { ?>
                location.href = "<?php echo base_url();?>admin/documentexternal";
                <?php } ?>
            }
            ,error: function(request, error)
            {
                alert(error);
                <?php if ($action != "edit") { ?>
                clear();
                <?php } ?>
            }
            ,dataType: "json"
          }
        );
    }

    $('#form_input').on('submit',function(){
        var check = true;
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        if (check)
        {
            post();
        }

        return false;
    });


    $('.validate-input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
          return $(input).val().trim() != ''
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').show();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).find('#errorMessage').hide();
    }
   
    $("#btnDelete").on("click", function()
                       {
                       if (confirm("Are you sure to delete a data?"))
                       {
                        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>admin/company/remove"
            ,data: $('#form_input').serialize()
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    location.href = "<?php echo base_url(); ?>admin/company";
                }
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
                        
                       }
                       });
    
    
    $("#courier_company").on("change", function()
                              {
                                selectPackageType();
                              });
    
    selectPackageType();
    
    function selectPackageType()
    {
        $('#order_courier_by')
            .find('option')
            .remove()
            .end()
            .val('');
        
        $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>documentexternal/getPackageTypeByCompany"
            ,data: {"company": $("#courier_company").val()}
            ,success: function (data)
            {
                for(var i=0; i < data.length; i++)
                {
                   <?php if ($action == "edit") { ?>
                   if (data[i].package_type_id == <?php echo $row->order_courier_by; ?>)
                   {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'" selected>'+data[i].package_type_name+'</option>'); 
                   }
                   else
                   {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'">'+data[i].package_type_name+'</option>'); 
                   }
                    continue;
                   <?php } ?>
                    if (i == 0)
                    {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'" selected>'+data[i].package_type_name+'</option>'); 
                    }
                    else
                    {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'">'+data[i].package_type_name+'</option>'); 
                    }
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
    $("#btnResetPhoto").on("click", function()
                       {
                            $("#order_photo").val("");
                            $("#previewPhoto").attr("src", "<?php echo base_url(); ?><?php echo isset($row) ? $row->order_photo : ""; ?>");
                       });
    
    var files;
                       $("#photo").on("change", function(event)
                                      {
                                        event.preventDefault();
                                        var formData = new FormData($("#form_input")[0]);
                                        $.ajax({
        url: '<?php echo base_url(); ?>admin/documentexternal/savePhoto',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(! data.error)
            {
                // Success so call function to process the form
                $("#previewPhoto").attr("src", "<?php echo base_url(); ?>"+data.src);
                $("#previewPhoto").show();
                
                $("#tmp_photo").val(data.src);
                $("#order_photo").val(data.photo);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
                                        
                                        
                                        
                                      });

})(jQuery);
</script>