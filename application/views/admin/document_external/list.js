<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
var dt;
  $(function () {
    
   dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
          url: "<?php echo base_url(); ?>documentexternal/get",
          data: function ( d ) {
         return $.extend( {}, d, {
           "status"		: $("#order_status").val(),
           "orderDate"	: $("#datepicker").val()
         } );
      }
        },
        "aoColumnDefs": [
{ "bSortable": false, "aTargets": [ "_all" ] }],
        "columns": [
            { "data": "document" },
            { "data": "sender" },
            { "data": "receiver" },
            { "data": "courier" },
            { "data": "status" },
            {
              "data": null,
              "defaultContent": "<button>Click!</button>",
               "render": function ( data, type, row ) {
                    var buttons = "<button type=\"button\" class=\"btn btn-block btn-primary\" onClick=\"javascript:edit("+data.order_id+"); \">Edit</button>";
                    if (data.order_status <= 2)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:courier("+data.order_id+","+data.order_courier_by+");\">Change courier</button>";
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-info\"  onclick=\"javscript:pickUp("+data.order_id+",);\">Pick Up</button>";
                    }
                    if (data.order_status == 3)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-warning\"  onclick=\"javscript:receive("+data.order_id+");\">Receive</button>";
                    }

                    if (data.order_status < 4)
                    {
                      buttons += "<button type=\"button\" class=\"btn btn-block btn-danger\" onclick=\"javascript:reject("+data.order_id+");\">Reject</button>";
                    }
                    return buttons;
               }
              
            }
            
        ],
        "order": []
    } );
      
   var detailRows = [];   
  });
  
  var orderId = null;
  var courierId = null;
  
    
     $("#order_status").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });
  
     $("#datepicker").on("change", function()
                                 {
                                  dt.ajax.reload();
                                 });  
  
  function courier(oId, cId)
  {
      orderId = oId;
      courierId - cId;
    
      $("#modal-info").modal('show');      
      $("#courierId").val(cId);
  }
  
  function reject(oId)
  {
      orderId = oId;
      $("#modal-danger").modal('show');
  }
  
    function edit(orderId)
    {
        location.href = "<?php echo base_url(); ?>admin/documentexternal/edit?id=" + orderId;
    }
    
    function receive(id)
    {
      orderId = id;
      $("#modal-receive").modal('show');
    }
    
    function doReceive(note, fee, no)
    {
      
        if (no.length == 0)
        {
          alert("Please input airway bill");
          return;
        }

        if (fee.length == 0)
        {
          alert("Please input delivery fee");
          return;
        }

        if (note.length == 0)
        {
          alert("Please input note");
          return;
        }
      
          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentexternal/complete"
            ,data: {"orderId": orderId, "note" : note, "fee": fee, "airwaybill": no}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-receive").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
      
    }
    
    function pickUp(id)
    {
      orderId = id;
      $("#modal-pickup").modal('show');
    }
    
    function changeOfficeBoy(id)
    {
      if (id  == "")
      {
        alert("Please select a office boy");
        return;
      }

          $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentexternal/pickUp"
            ,data: {"orderId": orderId, "officeBoy": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                  $("#modal-pickup").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()                        
                      }, false);
                       
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
    
    function rejectNote(note)
    {
        if (note.length == 0)
        {
          alert("Please input reject reason");
          return;
        }
        
        $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentexternal/rejectOrder"
            ,data: {"orderId": orderId,  "note": note}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-danger").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }

    function changeCourier(id)
    {
      if (id == "")
      {
        alert("Please select a courier");
        return;
      }
      $.ajax(
          {
             type: "POST"
            ,url: "<?php echo base_url(); ?>documentexternal/setCourier"
            ,data: {"orderId": orderId,  "courierId": id}
            ,success: function (data)
            {
                alert(data.message)
                if (data.status)
                {
                    $("#modal-info").modal('hide');
                    dt.ajax.reload(function() {
                          dt.columns.adjust()
                      }, false);
                    
                    
                    return;
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
  
  
  selectPackageType();
  
  function selectPackageType()
    {
        $('#order_courier_by')
            .find('option')
            .remove()
            .end()
            .val('');
        
        $.ajax(
          {
             type: "GET"
            ,url: "<?php echo base_url(); ?>documentexternal/getPackageTypeByCompany"
            ,data: {"company": $("#courier_company").val()}
            ,success: function (data)
            {
                for(var i=0; i < data.length; i++)
                {
                    if (i == 0)
                    {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'" selected>'+data[i].package_type_name+'</option>'); 
                    }
                    else
                    {
                        $('#order_courier_by').append('<option value="'+data[i].package_type_id+'">'+data[i].package_type_name+'</option>'); 
                    }
                    
                }
                
                
            }
            ,error: function(request, error)
            {
                alert(error);
                clear();
            }
            ,dataType: "json"
          }
        );
    }
	
        //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
	
    
</script>