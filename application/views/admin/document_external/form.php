<section class="content">
   <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><?php echo $action=="add" ? "Create External Courier" : "Edit External Courier"; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
               <?php if ($action == "edit") { ?>
               <input type="hidden" name="order_id" value="<?php echo $row->order_id; ?>" />
               <?php } ?>
               <input type="hidden" id="order_receiver_latitude" name="order_receiver_latitude" value="-1" />
               <input type="hidden" id="order_receiver_longitude" name="order_receiver_longitude" value="-1" />
               <input type="hidden" id="order_photo" name="order_photo" value="" />
               <input type="hidden" id="tmp_photo" name="tmp_photo" value="" />
               <div class="box-body">
                  <?php if ($action == "edit") { ?>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Order ID</label>
                     <input type="text" class="form-control" value="<?php printf("%05d", $row->order_id); ?>" disabled>
                  </div>
                  <?php } ?>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Item Description</label>
                     <input type="text" class="form-control validate-input" id="order_name" name="order_name" placeholder="Document Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input document Item Description</p>
                  </div>
				  <div class="row">
				  <div class="col-md-6">
                  <div class="form-group">
                     <label>Sender</label>
                     <select id="sender" class="form-control  validate-input" name="order_created_by"<?php if (! $canModify) { echo " disabled"; }?>>
                        <option value="">--- Select a sender ---</option>
                        <?php foreach($senders as $user) { ?>
                        <option value="<?php echo $user->user_id; ?>" <?php if (isset($row) && $user->user_id == $row->order_created_by) { echo "selected"; } ?>><?php echo htmlspecialchars($user->user_name, ENT_QUOTES); ?></option>
                        <?php } ?>
                     </select>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a employee</p>
                  </div>
                  </div>
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputFile">Photo</label>
                     <input type="file" id="photo" name="photo" value="" <?php if ($action != "edit") { ?>class="validate-input"<?php } ?> />
                     <br />
                     <img id="previewPhoto" src="<?php echo isset($row) && $row->order_photo ? base_url().$row->order_photo : ""; ?> " width="100" height="100" <?php echo isset($row) && $row->order_photo ? "" : 'style="display: none;"'; ?> />
                     <?php if (isset($row) && $row->order_photo) { ?>
                     <button type="button" id="btnResetPhoto" class="btn btn-block btn-info" style="margin-top: 5px;width: 100px;">Reset</button>
                     <?php } ?>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input photo</p>
                  </div>
                  </div>				  
                  </div>
				  <div class="row">
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Receiver Name</label>
                     <input type="text" class="form-control validate-input" id="order_receiver_name" name="order_receiver_name" placeholder="Receiver Name" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_name, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver name</p>
                  </div>
                  </div>
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Receiver Phone</label>
                     <input type="text" class="form-control validate-input" id="order_receiver_phone" name="order_receiver_phone" placeholder="Receiver Phone" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_phone, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver phone</p>
                  </div>
                  </div>
                  </div>
				  <div class="row">
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Receiver Address</label>
                     <textarea class="form-control validate-input" name="order_receiver_address" rows="3" placeholder="Receiver Address ..."><?php echo isset($row) ? htmlspecialchars($row->order_receiver_address, ENT_QUOTES) : ""; ?></textarea>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver address</p>
                  </div>
                  </div>
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Post Code</label>
                     <input type="text" class="form-control validate-input" id="order_receiver_address_note" name="order_receiver_address_note" placeholder="Receiver Post Code" value="<?php echo isset($row) ? htmlspecialchars($row->order_receiver_address_note, ENT_QUOTES) : ""; ?>"<?php if (! $canModify) { echo " disabled"; }?>>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input receiver post code</p>
                  </div>
                  </div>
                  </div>
				  <div class="row">
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Courier</label>
                     <select class="form-control" id="courier_company"<?php if (! $canModify) { echo " disabled"; }?>>
                        <?php foreach($couriers as $courier) { ?>
                        <option value="<?php echo $courier->logistic_id; ?>"<?php if (isset($row) && $row->package_type_company == $courier->logistic_id) echo " selected"; ?>><?php echo htmlspecialchars($courier->logistic_company, ENT_QUOTES); ?></option>
                        <?php } ?>
                     </select>
				  </div>
				  </div>
				  <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Package Type</label>
                     <select class="form-control  validate-input" name="order_courier_by" id="order_courier_by"<?php if (! $canModify) { echo " disabled"; }?>>
                     </select>
                     <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please select a courier</p>
                  </div>
                  </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <?php if ($canModify) { ?>
               <div class="box-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $action == "edit" ? "Update" : "Create"; ?></button>
               </div>
               <?php } ?>
            </form>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <!--/.col (right) -->
   </div>
   <!-- /.row -->
</section>