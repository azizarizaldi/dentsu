<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/inventory_report/get",
        "columns": [
           
            { "data": "item_description" },
            { "data": "category_name" },
            { "data": "item_stock" },
            { "data": "item_min_stock" },
            { "data": "item_ideal_stock" },
            { "data": "item_price" },
            { "data": "total_value" }
        ],
        "order": [[1, 'asc']]
    });  
  })


function print() {
		var keyword 		= $('.dataTables_filter input').val();
		var number 			= $('select[name="example1_length"] option:selected').val();
    window.open('<?php echo base_url(); ?>report/pdf_inventory_report?keyword='+keyword+'&number='+number,'_blank');		
}

</script>