<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Inventory report List</h3>
        </div>
        <div class="box-body">
          <button type="button" id="btnPrint" onclick="print()"  class="btn btn-primary pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>
          <br/>
          <br/>
          <br/>
          <table id="example1" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th>Item Description</th>
                <th>Category</th>
                <th>Stock</th>
                <th>Min Stock</th>
                <th>Ideal Stock</th>
                <th>Price</th>
                <th>Total Value</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
              <th>Item Description</th>
                <th>Category</th>
                <th>Stock</th>
                <th>Min Stock</th>
                <th>Ideal Stock</th>
                <th>Price</th>
                <th>Total Value</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>