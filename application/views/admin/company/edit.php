<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Company</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="form_input" role="form" method="post">
              <input type="hidden" name="company_id"  value="<?php echo $row->company_id; ?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">ID</label>
                  <input type="text" class="form-control" value="<?php echo $row->company_id; ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $canModify ? "Old Company Name" : "Company Name"; ?></label>
                  <input type="text" class="form-control" value="<?php echo htmlspecialchars($row->company_name, ENT_QUOTES); ?>" disabled>
                </div>
                <?php if ($canModify) { ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">New Company Name</label>
                  <input type="text" class="form-control validate-input" id="company_name" name="company_name" placeholder="Company Name">
                                    <p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input company name</p>
                </div>
                <?php } ?>
              </div>
              <!-- /.box-body -->
<?php if ($canModify) { ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              </div>
              <?php } ?>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>