<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php echo ($action == "edit") ? "Edit Category" : "Add Category"; ?>
          </h3>
        </div>
        <form id="form_input" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="<?=$action?>"/>
          <?php if ($action == "edit") { ?>
          <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
          <?php } ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>ID Category</label>
                  <?php if ($action == "edit") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="setting_category_id_text" value="<?php printf("%05d", $row->id);?>"
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php }
                  else if ($action == "add") { ?>
                  <input type="text" class="form-control" disabled placeholder="Auto Generate" name="setting_category_id_text" value=""
                    <?php echo $canModify ? "" : "disabled" ; ?> />
                    <?php } ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control validate-input" id="name" name="name" placeholder="Category Name" value="<?php echo isset($row) ? htmlspecialchars($row->name, ENT_QUOTES) : ""; ?>">
    							<p class="text-red" align="right" style="font-size: 8pt;display:none;" id="errorMessage">Please input name</p>              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Type Category</label>
                  <select class="form-control" name="type_category" id="type_category">
                      <option value="1" <?php if (isset($row) && $row->type_category == '1') { echo "selected"; } ?> >Vendor</option>
                      <option value="2" <?php if (isset($row) && $row->type_category == '2') { echo "selected"; } ?> >Contract & Company Doc.</option>
                      <option value="3" <?php if (isset($row) && $row->type_category == '3') { echo "selected"; } ?> >Procedure</option>
                      <option value="4" <?php if (isset($row) && $row->type_category == '4') { echo "selected"; } ?> >Employee Position</option>
                      <option value=5 <?php if (isset($row) && $row->type_category == '5') { echo "selected"; } ?> >Contact</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">
                <?php echo $action == "edit" ? "Update" : "Submit"; ?></button>
              <?php if ($action == "edit") { ?>
              &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" id="btnDelete">Delete</button>
              <?php } ?>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>