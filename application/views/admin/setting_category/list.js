<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    var dt = $('#example1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url(); ?>admin/setting_category/get",
        "columns": [
           
            { "data": "id" },
            { "data": "name" },
            { "data": "type_category" },
            { "data": "created_at" }
          ],
        "order": [[1, 'asc']]
    } );
   
   var detailRows = [];
   
   $('#example1 tbody').on( 'click', 'tr', function () {

        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        
        location.href = "<?php echo base_url(); ?>admin/setting_category/edit?id=" + tr.find('td:first').html();
    } );
  
  })

</script>