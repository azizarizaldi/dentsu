<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends CI_Model {
    function get($from, $to)
    {
        $this->db->select("*, DATE_FORMAT(chat_create_date, '%d/%m/%Y %H:%i') as chat_create_date_fmt, DATE_FORMAT(chat_create_date, '%d/%m/%Y') as chat_create_date_1_fmt, DATE_FORMAT(chat_create_date, '%H:%i') as chat_create_date_2_fmt");
        $this->db->order_by("chat_create_date", "asc");
        $this->db->where("(chat_from = ${from} OR chat_to = ${from})", null, false);
        $this->db->where("(chat_from = ${to} OR chat_to = ${to})", null, false);
        $q = $this->db->get("chat");
        
        return $q->result();
    }
    
    function getLast($to, $search="")
    {
        $this->db->distinct();
        $this->db->select("chat_from");
        $this->db->where("chat_to", $to);
        if ($search)
        {
            $this->db->like("user_name", $search);
            $this->db->join("_user sender", "sender.user_id = chat_from", "left");
        }
        $q = $this->db->get("chat");
        
        $rows = $q->result();
        
        $result = array();
        for($i=0; $i < count($rows); $i++)
        {
            $this->db->limit(1);
            $this->db->select("*, DATE_FORMAT(chat_create_date, '%d/%m/%Y %H:%i') as chat_create_date_fmt, DATE_FORMAT(chat_create_date, '%d/%m/%Y') as chat_create_date_1_fmt, DATE_FORMAT(chat_create_date, '%H:%i') as chat_create_date_2_fmt");
            $this->db->order_by("chat_create_date", "asc");
            $this->db->where("chat_to", $to);
            $this->db->where("chat_from", $rows[$i]->chat_from);
            $this->db->join("_user sender", "sender.user_id = chat_from", "left");

            $q = $this->db->get("chat");
        
            $row = $q->row();
            
            $result[] = $row;
        }
        
        return $result;
    }
    
    function save($from, $to, $message)
    {
        $this->db->where("user_id", $from);
        $q = $this->db->get("_user");
        
        if ($q->num_rows() == 0) return;
        
        $row = $q->row();
        
        $from = $row->user_id;
        $photo = $row->user_photo;
        $name = $row->user_name;
        
        if ($row->user_type == "3")
        {
            $type = "Driver";
        }
        else
        if ($row->user_type == "4")
        {
            $type = "Courier";
        }
        else
        {
            $type = "Employee";
        }
        
        $fcmMessage = sprintf("%s: %s", $name, $message);

        $this->db->where("user_id", $to);
        $q = $this->db->get("_user");
        
        if ($q->num_rows() == 0) return;
        
        $row = $q->row();

        fcm(array($row->user_fcm), sprintf("__chat__%d__%s__%s__%s", $from, $photo, $name, $type), $fcmMessage, "");
        
        $insert['chat_from'] = $from; 
        $insert['chat_to'] = $to;
        $insert['chat_message'] = $message;
        $insert['chat_create_date'] = date("Y-m-d H:i:s");
        $insert['chat_status'] = 1;
        
        $this->db->insert("chat", $insert);
    }
   
}
