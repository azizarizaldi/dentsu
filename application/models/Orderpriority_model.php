<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderPriority_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->like("priority_name", $search);
      }
      $q =  $this->db->get("order_priority");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("priority_id", $id);
      $q =  $this->db->get("order_priority");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->like("priority_name", $search);
      }
      return $this->db->count_all_results("order_priority");
   }

   public function insert($data)
   {
        $this->db->insert("order_priority", $data);
   }
   
   public function update($data)
   {
      $this->db->where("priority_id", $data["priority_id"]);
        $this->db->update("order_priority", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("priority_id", $id);
        $this->db->delete("order_priority");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["priority_id"]))
      {
         $this->db->where("priority_id <>", $data["priority_id"]);
      }
      $this->db->where("priority_name", $data["priority_name"]);
      return $this->db->count_all_results("order_priority");
   }
}
