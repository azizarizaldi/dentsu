<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listofmessenger_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      
	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(user_name LIKE ${search} OR user_nik LIKE ${search})", null, false);
      }
	  
	  $this->db->where("user_type","4");
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
      
   public function getCount($search)
   {      
	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(user_name LIKE ${search} OR user_nik LIKE ${search})", null, false);
      }
	  
	  $this->db->where("user_type","4");
      $q =  $this->db->get("_user");
      
      return $q->num_rows();
   }
   
}
