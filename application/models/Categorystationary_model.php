<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryStationary_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->like("category_name", $search);
      }
      $q =  $this->db->get("stationary_category");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("category_id", $id);
      $q =  $this->db->get("stationary_category");
      
      return $q->row();
   }

   public function getByName($name)
   {
      $this->db->where("company_name", $name);
      $q =  $this->db->get("company");
      
      return $q->row();
   }
   
   public function getCount($search)
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->like("category_name", $search);
      }
      return $this->db->count_all_results("stationary_category");
   }

   public function insert($data)
   {
        $this->db->insert("stationary_category", $data);
   }
   
   public function update($data)
   {
      $this->db->where("category_id", $data["category_id"]);
        $this->db->update("stationary_category", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("category_id", $id);
        $this->db->delete("stationary_category");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["category_id"]))
      {
         $this->db->where("category_id <>", $data["category_id"]);
      }
      $this->db->where("category_name", $data["category_name"]);
      return $this->db->count_all_results("stationary_category");
   }
}
