<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model {

   public function get($orderBy="contact_name", $dir="asc", $limit=0, $offset=0, $search="",$id_category="")
   {
      $this->db->select("contact.* , setting_category.name as category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("contact_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = contact.id_category","inner");
      $q =  $this->db->get("contact");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("contact_id", $id);
      $q =  $this->db->get("contact");
      
      return $q->row();
   }
   
   public function getCount($search,$id_category)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("contact_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = contact.id_category","inner");
      return $this->db->count_all_results("contact");
   }

   public function insert($data)
   {
        $this->db->insert("contact", $data);
   }
   
   public function update($data)
   {
      $this->db->where("contact_id", $data["contact_id"]);
        $this->db->update("contact", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("contact_id", $id);
        $this->db->delete("contact");
   }
   
}
