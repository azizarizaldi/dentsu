<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StationaryItem_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="", $inIds=false, $notInIds=false, $categoryId=0)
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
          $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(item_description LIKE ${search} OR category_name LIKE ${search})", null, false);
      }
      if ($inIds)
      {
         $this->db->where_in("item_id", explode(",", $inIds));
      }
      if ($notInIds)
      {
         $this->db->where_not_in("item_id", explode(",", $notInIds));
      }
      if ($categoryId > 0)
      {
         $this->db->where("category_id", $categoryId);
      }
      $this->db->join("stationary_category", "category_id = item_category");
      $q =  $this->db->get("stationary_item");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("item_id", $id);
      $q =  $this->db->get("stationary_item");
      
      return $q->row();
   }

   public function getByName($name)
   {
      $this->db->where("company_name", $name);
      $q =  $this->db->get("company");
      
      return $q->row();
   }
   
   public function getCount($search, $categoryId = 0)
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("item_description", $search);
         $this->db->or_like("category_name", $search);
      }
      if ($categoryId > 0)
      {
         $this->db->where("category_id", $categoryId);
      }
      $this->db->join("stationary_category", "category_id = item_category");
      return $this->db->count_all_results("stationary_item");
   }

   public function insert($data)
   {
        $this->db->insert("stationary_item", $data);
   }
   
   public function update($data)
   {
      $this->db->where("item_id", $data["item_id"]);
        $this->db->update("stationary_item", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("item_id", $id);
        $this->db->delete("stationary_item");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["item_id"]))
      {
         $this->db->where("item_id <>", $data["item_id"]);
      }
      $this->db->where("item_description", $data["item_description"]);
      $this->db->where("item_category", $data["item_category"]);

      return $this->db->count_all_results("stationary_item");
   }
   
   public function syncStock($orderId)
   {
       $this->db->where("order_stationary_order", $orderId);
       $q = $this->db->get("order_stationary");
       
       $rows = $q->result();
       for($i=0; $i < count($rows); $i++)
       {
           $itemId = $rows[$i]->order_stationary_item;
           $itemQty = $rows[$i]->order_stationary_qty;
           
           $query = sprintf("UPDATE stationary_item SET item_stock = item_stock - %d WHERE item_id = %d", $itemQty, $itemId);
           $this->db->query($query);
       }
   }
}
