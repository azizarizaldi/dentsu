<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_Model extends CI_Model {

   public function get($orderBy="car_brand", $dir="asc", $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("car_no", $search);
         $this->db->or_like("car_model", $search);
         $this->db->or_like("car_brand", $search);
      }
      
      $this->db->select("*, DATE_FORMAT(car_driver_start_date, '%d/%m/%Y') car_driver_start_date_fmt, DATE_FORMAT(car_driver_end_date, '%d/%m/%Y') car_driver_end_date_fmt");
      $this->db->join("car_driver", "car_driver_car = car_id AND car_driver_start_date <= NOW() AND (car_driver_end_date >= NOW() OR car_driver_end_date = '0000-00-00 00:00:00')", "left");
      $this->db->join("_user", "user_id = car_driver_user", "left");
      $q =  $this->db->get("car");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("car_id", $id);
      $q =  $this->db->get("car");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("car_no", $search);
         $this->db->or_like("car_model", $search);
         $this->db->or_like("car_brand", $search);
      }
      return $this->db->count_all_results("car");
   }

   public function insert($data)
   {
        $this->db->insert("car", $data);
   }
   
   public function update($data)
   {
      $this->db->where("car_id", $data["car_id"]);
        $this->db->update("car", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("car_id", $id);
        $this->db->delete("car");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["car_id"]))
      {
         $this->db->where("car_id <>", $data["car_id"]);
      }
      $this->db->where("car_no", $data["car_no"]);
      return $this->db->count_all_results("car");
   }
   
   function getBrands()
   {
      $this->db->distinct();
      $this->db->select("car_brand");
      $this->db->order_by("car_brand", "asc");
      $q = $this->db->get("car");
      
      return $q->result();
   }
   
   function getModels()
   {
      $this->db->distinct();
      $this->db->select("car_model");
      $this->db->order_by("car_model", "asc");
      $q = $this->db->get("car");
      
      return $q->result();
   }

   function getRentCompanies()
   {
      $this->db->distinct();
      $this->db->where("car_company_rent<> ", null);
      $this->db->where("car_company_rent<> ", "");
      $this->db->select("car_company_rent");
      $this->db->order_by("car_company_rent", "asc");
      $q = $this->db->get("car");
      
      return $q->result();
   }
   
}
