<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_Model extends CI_Model {
   
   public function insert($data)
   {
        $this->db->insert("billboard", $data);
   }
   
   public function update($data)
   {
      $this->db->where("billboard_id", $data["billboard_id"]);
        $this->db->update("billboard", $data);
   }
   
       public function delete($id)
   {
      $this->db->where("billboard_id", $id);
        $this->db->delete("billboard");
   }

   public function getById($id)
   {
      $this->db->where("billboard_id", $id);
      $q =  $this->db->get("billboard");
      
      return $q->row();
   }
   
   public function get($orderBy, $dir, $limit=0, $offset=0)
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
     
      $q =  $this->db->get("billboard");
      
      return $q->result();
   }
   
   public function getCount()
   {
      
      return $this->db->count_all_results("billboard");
   }
   
}
