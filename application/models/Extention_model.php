<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extention_model extends CI_Model {

   public function get($orderBy="extention_name", $dir="asc", $limit=0, $offset=0, $search="",$id_category="")
   {
      $this->db->select("extention.* , setting_category.name as category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("extention_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = extention.id_category","inner");
      $q =  $this->db->get("extention");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("extention_id", $id);
      $q =  $this->db->get("extention");
      
      return $q->row();
   }
   
   public function getCount($search,$id_category)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("extention_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = extention.id_category","inner");
      return $this->db->count_all_results("extention");
   }

   public function insert($data)
   {
        $this->db->insert("extention", $data);
   }
   
   public function update($data)
   {
      $this->db->where("extention_id", $data["extention_id"]);
        $this->db->update("extention", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("extention_id", $id);
        $this->db->delete("extention");
   }
   
}
