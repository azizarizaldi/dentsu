<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dept_Model extends CI_Model {

   public function get($orderBy, $dir, $limit, $offset, $search)
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      if ($search)
      {
         $this->db->or_like("company_name", $search);
         $this->db->or_like("brand_unit_name", $search);
         $this->db->or_like("departement_name", $search);
      }

      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id");
      $this->db->join("company", "company_id = brand_unit_company");
      $q =  $this->db->get("departement");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id");
      $this->db->where("departement_id", $id);
      $q =  $this->db->get("departement");
      
      return $q->row();
   }

   public function getByName($brandUnitId, $name)
   {
      $this->db->where("departement_name", $name);
      $this->db->where("departement_brand_unit", $brandUnitId);
      $q =  $this->db->get("departement");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      if ($search)
      {
         $this->db->or_like("company_name", $search);
         $this->db->or_like("brand_unit_name", $search);
         $this->db->or_like("departement_name", $search);
      }
      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id");
      $this->db->join("company", "company_id = brand_unit_company");
      return $this->db->count_all_results("departement");
   }

   public function insert($data)
   {
        $this->db->insert("departement", $data);
   }
   
   public function update($data)
   {
      $this->db->where("departement_id", $data["departement_id"]);
        $this->db->update("departement", $data);
   }
   
    public function delete($id)
   {
      $this->db->where("departement_id", $id);
        $this->db->delete("departement");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["departement_id"]))
      {
         $this->db->where("departement_id <>", $data["departement_id"]);
      }
      
      $this->db->where("departement_brand_unit", $data["departement_brand_unit"]);
      $this->db->where("departement_name", $data["departement_name"]);
      
      return $this->db->count_all_results("departement");
   }
   
   public function getCountByBrandUnit($brand_unit)
   {
      $this->db->where("departement_brand_unit", $brand_unit);
      return $this->db->count_all_results("departement");
   }
   
   public function getByBrandUnit($brandUnitId)
	{
		$this->db->where("departement_brand_unit", $brandUnitId);
		$q = $this->db->get("departement");
		
      return $q->result();
	}
}
