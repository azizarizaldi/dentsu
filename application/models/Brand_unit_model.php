<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_Unit_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $this->db->join("company", "company_id = brand_unit_company");
      if ($search)
      {
         $this->db->or_like("company_name", $search);
         $this->db->or_like("brand_unit_name", $search);
      }
      $q =  $this->db->get("brand_unit");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("brand_unit_id", $id);
      $q =  $this->db->get("brand_unit");
      
      return $q->row();
   }

   public function getByName($company, $name)
   {
      $this->db->where("brand_unit_company", $company);
      $this->db->where("brand_unit_name", $name);
      $q =  $this->db->get("brand_unit");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      if ($search)
      {
         $this->db->or_like("company_name", $search);
         $this->db->or_like("brand_unit_name", $search);
      }

      $this->db->join("company", "company_id = brand_unit_company");
      return $this->db->count_all_results("brand_unit");
   }

   public function insert($data)
   {
        $this->db->insert("brand_unit", $data);
   }
   
   public function update($data)
   {
      $this->db->where("brand_unit_id", $data["brand_unit_id"]);
        $this->db->update("brand_unit", $data);
   }
   
    public function delete($id)
   {
      $this->db->where("brand_unit_id", $id);
        $this->db->delete("brand_unit");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["brand_unit_id"]))
      {
         $this->db->where("brand_unit_id <>", $data["brand_unit_id"]);
      }
      
      $this->db->where("brand_unit_name", $data["brand_unit_name"]);
      $this->db->where("brand_unit_company", $data["brand_unit_company"]);
      
      return $this->db->count_all_results("brand_unit");
   }
   
   public function getCountByCompany($company)
   {
      $this->db->where("brand_unit_company", $company);
      return $this->db->count_all_results("brand_unit");
   }
   
   public function getByCompany($companyId)
	{
		$this->db->where("brand_unit_company", $companyId);
		$q = $this->db->get("brand_unit");
		
      return $q->result();
	}
}
