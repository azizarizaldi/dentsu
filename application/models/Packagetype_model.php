<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PackageType_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="", $company="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $this->db->join("logistic", "logistic_id = package_type_company");
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("((logistic_company LIKE ${search}) OR (package_type_name LIKE ${search}))", null, false);
      }
      if ($company)
      {
         $this->db->where("package_type_company", $company);
      }
      $q =  $this->db->get("courier_package_type");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("package_type_id", $id);
      $q =  $this->db->get("courier_package_type");
      
      return $q->row();
   }
   
   public function getCount($company="", $search="")
   {
      $this->db->join("logistic", "logistic_id = package_type_company");
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("((logistic_company LIKE ${search}) OR (package_type_name LIKE ${search}))", null, false);
      }
      if ($company)
      {
         $this->db->where("package_type_company", $company);
      }
      return $this->db->count_all_results("courier_package_type");
   }

   public function insert($data)
   {
        $this->db->insert("courier_package_type", $data);
   }
   
   public function update($data)
   {
      $this->db->where("package_type_id", $data["package_type_id"]);
        $this->db->update("courier_package_type", $data);
   }
   
    public function delete($id)
   {
      $this->db->where("package_type_id", $id);
        $this->db->delete("courier_package_type");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["package_type_id"]))
      {
         $this->db->where("package_type_id <>", $data["package_type_id"]);
      }
      
      $this->db->where("package_type_name", $data["package_type_name"]);
      $this->db->where("package_type_company", $data["package_type_company"]);
      
      return $this->db->count_all_results("courier_package_type");
   }
   
   public function getCourierCompanies()
   {
      $this->db->distinct("logistic_id, logistic_company");
      $this->db->select("logistic_id, logistic_company");
      $this->db->order_by("logistic_company", "asc");
      
      $this->db->join("logistic", "logistic_id = package_type_company");
      $q = $this->db->get("courier_package_type");
      
      return $q->result();
   }
   
   public function getByCompany($company)
   {
       if ($company)
       {
      $this->db->where("package_type_company", $company);
       }
      $this->db->order_by("package_type_name", "asc");
      
      $q = $this->db->get("courier_package_type");
      
      return $q->result();
   }
   
}
