<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Model extends CI_Model {

   public function get($type, $limit=0, $offset=0, $search="",$status="", $arrstatus="", $createdBy = 0, $orderTypes = false, $carId=0, $category=0, $courierId=0,$orderDate=0,$meetingRoomID=0)
   {
      $this->db->order_by("order_created_date", "desc");
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      
      $search = trim($search);
      
      if ($orderTypes)
      {
        $this->db->where_in("order_type", $orderTypes);
      }
      else
      {
          $this->db->where("order_type", $type);
      }
      
      if ($status)
      {
         $this->db->where("order_status", $status);
      }
      
      if ($category)
      {
          $this->db->where("order_type", $category);
      }
      
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(LPAD(order_id,10,'0') LIKE ${search} OR order_name LIKE ${search})", null, false);
      }
      if ($arrstatus)
      {
          $this->db->where_in("order_status", explode(",", $arrstatus));
      }

      if ($meetingRoomID)
      {
          $this->db->where_in("order_ref", explode(",", $meetingRoomID));
      }
	  
      if ($orderDate)
      {
          $this->db->where("date(order_created_date) = '$orderDate'");
      }
      
      if ($courierId)
      {
          $this->db->where("order_courier_by", $courierId);
          $this->db->where("order_type", 1);
      }
      else
      if ($carId)
      {
          $this->db->where_in("order_ref", explode(",", $carId));
          $this->db->where("order_type", 4);
      }
      else
      if ($createdBy)
      {
          $this->db->where("(order_created_by = ${createdBy} OR order_pickup_by = ${createdBy})", null, false);
      }
      
      $this->db->join("_user reject", "reject.user_id = order_rejected_by", "left");
      $this->db->join("_user sender", "sender.user_id = order_created_by", "left");
      $this->db->join("_user courier", "courier.user_id = order_courier_by", "left");
      $this->db->join("_user pickup", "pickup.user_id = order_pickup_by", "left");
      $this->db->join("departement", "departement_id = sender.user_departement", "left");
      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id", "left");
      $this->db->join("company", "brand_unit_company = company_id", "left");
      $this->db->join("order_priority", "priority_id = order_priority", "left");
      $this->db->join("courier_package_type", "package_type_id = order_courier_by", "left");
      $this->db->join("car", "car_id = order_ref", "left");
      $this->db->join("meeting_room", "meeting_room_id = order_ref", "left");
      $this->db->join("logistic", "package_type_company = logistic_id", "left");
      $this->db->select("*, sender.user_name as sender_name, sender.user_phone as sender_phone, sender.user_photo as sender_photo, sender.user_visitor_company as sender_visitor_company, courier.user_name as courier_name, courier.user_photo as courier_photo, courier.user_phone as courier_phone, reject.user_name as rejected_by, pickup.user_name as pickup_by, CONCAT(car_brand, ' ', car_model) as car_name_detail
                        , DATE_FORMAT(order_created_date, '%d.%m.%Y %H:%i') as order_created_date_fmt
                        , DATE_FORMAT(order_courier_date, '%d.%m.%Y %H:%i') as order_courier_date_fmt
                        , DATE_FORMAT(order_pickup_date, '%d.%m.%Y %H:%i') as order_pickup_date_fmt
                        , DATE_FORMAT(order_received_date, '%d.%m.%Y %H:%i') as order_received_date_fmt
                        , DATE_FORMAT(order_rejected_date, '%d/%m/%Y %H:%i') as order_rejected_date_fmt
                        , DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i') as order_start_date_fmt
                        , DATE_FORMAT(order_start_date, '%d/%m/%Y') as order_start_date_only
                        , DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i') as order_end_date_fmt
                        , DATE_FORMAT(order_end_date, '%H:%i') as order_end_time_fmt
                        , CONCAT(DATE_FORMAT(order_start_date, '%h:%i %p'), ' - ', DATE_FORMAT(order_end_date, '%h:%i %p')) as order_range_time
                        , DATEDIFF(order_end_date, order_start_date) as order_date_length ");
      $q =  $this->db->get("order");
	  
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->select("*
                        , DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i') as order_start_date_fmt
                        , DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i') as order_end_date_fmt
                        , DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i') as order_end_date_fmt
                        , DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i') as order_start_date_fmt");
      $this->db->join("car", "car_id = order_ref", "left");
      $this->db->join("courier_package_type", "package_type_id = order_courier_by", "left");
      $this->db->join("meeting_room", "meeting_room_id = order_ref", "left");
      $this->db->where("order_id", $id);
      $q =  $this->db->get("order");
      
      return $q->row();
   }
   
   public function getDocumentInternalCount($userId, $status)
   {
      $this->db->where("order_courier_by", $userId);
      $this->db->where_in("order_status", $status);
      $this->db->where("order_type", 1);
      $this->db->where("order_courier_date >=", date("Y-m-d 00:00:00"));
      $this->db->where("order_courier_date <=", date("Y-m-d 23:59:59"));
      $count = $this->db->count_all_results("order");
      
      return $count;
   }
   
   public function getBookingCarCount($carIds, $status)
   {
      if ($carIds)
      {
          $this->db->where_in("order_ref", $carIds);
          $this->db->where("order_type", 4);
      }

      $this->db->where_in("order_status", $status);
      $this->db->where("order_courier_date >=", date("Y-m-d 00:00:00"));
      $this->db->where("order_courier_date <=", date("Y-m-d 23:59:59"));
    
      $count = $this->db->count_all_results("order");
      
      return $count;
   }
   
   public function getCount($type, $status="", $search="", $arrstatus="", $createdBy = 0, $orderTypes = false, $carId=0, $category=0, $courierId=0,$orderDate=0,$meetingRoomID=0)
   {
      if ($status)
      {
         $this->db->where("order_status", $status);
      }
    if ($category)
      {
          $this->db->where("order_type", $category);
      }
      
      if ($orderDate)
      {
          $this->db->where("date(order_created_date) = '$orderDate'");
      }

      if ($meetingRoomID)
      {
          $this->db->where_in("order_ref", explode(",", $meetingRoomID));
      }
	  
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(LPAD(order_id,10,'0') LIKE ${search} OR order_name LIKE ${search})", null, false);
      }
      if ($arrstatus)
      {
          $this->db->where_in("order_status", explode(",", $arrstatus));
      }
      
      if ($courierId)
      {
          $this->db->where("order_courier_by", $courierId);
          $this->db->where("order_type", 1);
      }
      else
      if ($carId)
      {
          $this->db->where("order_ref", $carId);
          $this->db->where("order_type", 4);
      }
      else
      if ($createdBy)
      {
          $this->db->where("(order_created_by = ${createdBy} OR order_pickup_by = ${createdBy})", null, false);
      }
      
      if ($orderTypes)
      {
        $this->db->where_in("order_type", $orderTypes);
      }
      else
      {
          $this->db->where("order_type", $type);
      }
      
      $count = $this->db->count_all_results("order");
      
      return $count;
   }
   
   public function getCountByPriority($id)
   {
      $this->db->where("order_priority", $id);
      return $this->db->count_all_results("order");
   }

   public function insert($data)
   {
         if ($data['order_type'] == 2)
         {
            $data['order_status'] = 2;
         }
	if (! isset($data['order_created_date']) || ! $data['order_created_date'])
	{
		$data['order_created_date'] = date("Y-m-d H:i:s");
	}
      
        $this->db->insert("order", $data);
   }
   
   public function update($data)
   {
      $this->db->where("order_id", $data["order_id"]);
        $this->db->update("order", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("company_id", $id);
        $this->db->delete("company");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["company_id"]))
      {
         $this->db->where("company_id <>", $data["company_id"]);
      }
      $this->db->where("company_name", $data["company_name"]);
      return $this->db->count_all_results("company");
   }
   
   public function getStatusDesc($status)
   {
      switch($status)
      {
         case 2:
            return "Courier found";
         case 3:
            return "Courier pick up";
         case 4:
            return "Received";
         case 5:
            return "Rejected";
      }
      
      return "New";
   }
   
   public function getOrderById($id)
   {
      $this->db->where("order_id", $id);
      $q = $this->db->get("order");
      
      return $q->row_array();
   }
   
   public function getReceiverNames($type)
   {
      $this->db->distinct();
      $this->db->select("order_receiver_name");
      $this->db->where("order_type", $type);
      $q = $this->db->get("order");
      
      return $q->result();
   }
   
   public function getReceiverAddress($type)
   {
      $this->db->distinct();
      $this->db->select("order_receiver_address");
      $this->db->where("order_type", $type);
      $q = $this->db->get("order");
      
      return $q->result();
   }
   
   public function getBookingCarsServices()
   {
      return ['Drop Only', 'Pick up @ Client', 'Full Service'];
   }
   
   public function getBookingCarsServiceById($id)
   {
      $arr = $this->getBookingCarsServices();
      return $arr[$id];
   }
   
   public function getBooking($type, $startdate, $enddate, $bookingId)
   {
      // cari kendaraan yang dalam range

      $this->db->select("*, (order_start_date > '${startdate}') as isstart, (order_end_date < '${enddate}') as isend, DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i') order_end_date_fmt, DATE_FORMAT('${enddate}', '%d/%m/%Y %H:%i') enddate_fmt, DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i') order_start_date_fmt, DATE_FORMAT('${startdate}', '%d/%m/%Y %H:%i') startdate_fmt");
      //$this->db->where("(order_start_date < '${enddate}' OR order_end_date < '{$startdate}')", null, false);

      $this->db->where("((order_end_date >= '${startdate}' AND order_end_date <= '${enddate}') OR (order_start_date <= '${enddate}' AND order_end_date >= '${startdate}') OR (order_start_date >= '${startdate}' AND order_end_date <= '${enddate}'))", null, false);
      $this->db->where_in("order_status", array(1, 2, 3, 6));
      $this->db->where("order_id <>", $bookingId);
      $this->db->where("order_type", $type);
      if ($type == 4)
      {
         $this->db->join("car", "order_ref = car_id");
      }
      else
      if ($type == 5)
      {
         $this->db->join("meeting_room", "order_ref = meeting_room_id");
      }
      
      $q = $this->db->get("order");
      $rows = $q->result();

      for($i=0; $i < count($rows); $i++)
      {
         $refs[$rows[$i]->order_ref] = false;
      }
      
      if ($type == 4)
      {
         if (isset($refs))
         {
            $this->db->where_not_in("car_id", array_keys($refs));
         }
         
         $q = $this->db->get("car");   
         $data['avails'] = $q->result();
         
         $this->db->distinct();
         $this->db->select("order_ref");
         $this->db->where("order_start_date <=", $startdate);
         $this->db->where("order_end_date >=", $startdate);
         $this->db->where("order_start_date <=", $enddate);
         $this->db->where("order_end_date >=", $enddate);
         $this->db->where_in("order_status", array(1, 2, 3, 6));
         $this->db->where("order_type", $type);
         $q = $this->db->get("order");
         
         $rows1 = $q->result();
         
         $refs = array(0);
         for($i=0; $i < count($rows1); $i++)
         {
            $refs[] = $rows1[$i]->order_ref;
         }
         
         for($i=0; $i < count($rows); $i++)
         {
            if(in_array($rows[$i]->car_id, $refs)) continue;
         
            $suggest[] = $rows[$i];
         }
         
         $data['suggest'] = isset($suggest) ? $suggest : array();
         
      }
      else
      if ($type == 5)
      {
         if (isset($refs))
         {
            $this->db->where_not_in("meeting_room_id", array_keys($refs));
         }
         
         $q = $this->db->get("meeting_room");   
         $data['avails'] = $q->result();
         
         $this->db->distinct();
         $this->db->select("order_ref");
         $this->db->where("order_start_date <=", $startdate);
         $this->db->where("order_end_date >=", $startdate);
         $this->db->where("order_start_date <=", $enddate);
         $this->db->where("order_end_date >=", $enddate);
         $this->db->where_in("order_status", array(1, 2, 3, 6));
         $this->db->where("order_type", $type);
         $q = $this->db->get("order");
         
         $rows1 = $q->result();
         
         $refs = array(0);
         for($i=0; $i < count($rows1); $i++)
         {
            $refs[] = $rows1[$i]->order_ref;
         }
         
         for($i=0; $i < count($rows); $i++)
         {
            if(in_array($rows[$i]->meeting_room_id, $refs)) continue;
         
            $suggest[] = $rows[$i];
         }
         
         $data['suggest'] = isset($suggest) ? $suggest : array();
         
      }
      
      return $data;
   }
   
   function getAttendanceList($orderId)
   {
      $this->db->where("order_id", $orderId);
      $this->db->join("meeting_attendance", "meeting_attendance_room = order_id");
      $this->db->join("_user", "user_id = meeting_attendance_user");
      $q = $this->db->get("order");
      
      return $q->result();
   }

   function deleteStationaryCart($id)
   {
      $this->db->where("order_stationary_order", $id);
      $this->db->delete("order_stationary");
   }

   function updateStationaryCart($data)
   {
      $this->db->insert("order_stationary", $data);
   }

   function getCart($id, $createdBy=0)
   {
      
      $this->db->select("order_stationary.*, order.*, stationary_item.*, stationary_category.*, pickupUser.user_name pickupUserName, sender.user_name senderName, DATE_FORMAT(order_received_date, '%d/%m/%Y %H:%i') as order_received_date_fmt, DATE_FORMAT(order_rejected_date, '%d/%m/%Y %H:%i') as order_rejected_date_fmt");
      $this->db->where("order_stationary_order", $id);
      $this->db->join("stationary_item", "item_id = order_stationary_item");
      $this->db->join("stationary_category", "item_category = category_id");
      $this->db->join("order", "order_id = order_stationary_order");
      $this->db->join("_user pickupUser", "pickupUser.user_id = order_pickup_by", "left");
      $this->db->join("_user sender", "sender.user_id = order_created_by", "left");
      
      if ($createdBy)
      {
          $this->db->where("order_created_by", $createdBy);
      }
      
      $q = $this->db->get("order_stationary");

      return $q->result();
   }

   function getMeetingWith($row)
   {
      switch($row->order_sender_latitude)
      {
         case 0:
            return sprintf("Client '%s'", $row->order_receiver_name);
         case 1:
            return "Vendor";
         case 2:
            return "Internal";
      }
   }

   function getAppointmentRejectedReason($row)
   {
      switch($row->order_sender_latitude)
      {
         case 1:
            return "In a Meeting";
         case 2:
            return "Leave";
         case 3:
            return "Out of Office";
      }
   }
   
   public function getDocumentInSenders()
   {
       $this->db->where("order_type", 3);
       $this->db->select("order_receiver_name");
       $this->db->distinct("order_receiver_name");
       $q = $this->db->get("order");
       
       return $q->result();
   }
   
    public function getDocumentInCourier()
   {
       $this->db->where("order_type", 3);
       $this->db->select("order_receiver_address");
       $this->db->distinct("order_receiver_address");
       $q = $this->db->get("order");
       
       return $q->result();
   }
   
   public function getAppointmentToday()
   {
       $this->db->select("DATE_FORMAT(order_created_date, '%d/%m/%Y') as createdDate, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime, order_status, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
       $this->db->where("order_type", 7);
       $this->db->join("_user visitor", "visitor.user_id = order_created_by", "left");
       $this->db->join("_user visiting", "visiting.user_id = order_pickup_by", "left");
      $this->db->where("order_created_date >=", date("Y-m-d 00:00:00"));
      $this->db->where("order_created_date <=", date("Y-m-d 23:59:59"));
       $q = $this->db->get("order");
       
       return $q->result();
   }
   
   public function appointmentSignOut($orderId, $createdDate)
   {
        $data['order_end_date'] = date("Y-m-d H:i:s");
       
        $this->db->where("order_id", $orderId);
        $this->db->where("order_created_date", $createdDate);
        $this->db->update("order", $data);
        
        $row = $this->getNotifData($orderId);
        
        $subject = sprintf("Your appointment has been done");
        $message = sprintf("Client: %s\nPurpose: %s\nSign Out: %s ", $row->senderName, $row->order_receiver_address, date("d/m/Y H:i:s"));
        
        fcm(array($row->pickupUserFCM), $subject, $message, "");
   }
   
   public function notifRejectedOrder($orderId)
   {
       $row = $this->getNotifData($orderId);
       
       $subject = "";
       
       $message = "Order has been rejected at ".$row->order_rejected_date_fmt." by ".$row->rejectedUserName." with reason '".$row->order_rejected_note."'";
       
        $to = $this->getGAEmails();
        $cc = $row->senderEmail;
        
        $regIds = $this->getGARegisterIds();
       switch($row->order_type)
       {
            case 1:
               $subject = "Document Courier REJECTED. Order Id: ".$row->order_id;
               break;
            case 2:
                $regIds = array($row->senderFCM);
                $subject = "Document External Courier REJECTED. Order Id: ".$row->order_id;
                
                $cc = $this->getGAEmails();
                $to = $row->senderEmail;
                break;
            case 3:
                $subject = "Incoming Document REJECTED. Order Id: ".$row->order_id; 
                break;
            case 4:
                $subject = "Booking Car DONE. REJECTED Id: ".$row->order_id;
                break;
            case 5:
                $subject = "Booking Meeting Room REJECTED. Order Id: ".$row->order_id;
                break;
            case 6:
                $subject = "Stationary Order REJECTED. Order Id: ".$row->order_id;
                break;
            case 7:
                $cc = $this->getGAEmails();
                $to = $row->senderEmail;
                
                $regIds = false;
                
                $subject = "Appointment rejected. Id: ".$row->order_id;
                $message = "Your appointment has been rejected by ".$row->pickupUserName." Note. ".$row->order_rejected_note;
                break;
       }
       
       $this->notif($row, $to, $subject, $message, $cc, $regIds);
   }
   
   public function notifCompleteOrder($orderId, $sessionusername="")
   {
       $row = $this->getNotifData($orderId);
       
       $doner = $row->senderName;
       switch($row->order_type)
       {
           case 2:
           case 1:
               $doner = $row->pickupUserName;
               break;
            case 4:
                $doner = $sessionusername;
            break;
       }
       
       $subject = "";
       $message = "Order has been done at ".$row->order_received_date_fmt." by ".$doner;
       
       $to = $this->getGAEmails();
       $cc = $row->senderEmail;
       $regIds = $this->getGARegisterIds();
       
       switch($row->order_type)
       {
            case 1:
               $subject = "Document Courier DONE. Order Id: ".$row->order_id;
               
                $cc = $this->getGAEmails();
                $to = $row->senderEmail;
                $regIds = array($row->senderFCM);
               break;
            case 2:
                $subject = "Document External Courier DONE. Order Id: ".$row->order_id;
                
                $cc = $this->getGAEmails();
                $to = $row->senderEmail;
                $regIds = array($row->senderFCM);
                break;
            case 3:
                $subject = "Incoming Document DONE. Order Id: ".$row->order_id; 
                break;
            case 4:
                $subject = "Booking Car DONE. Order Id: ".$row->order_id;
                break;
            case 5:
                $subject = "Booking Meeting Room DONE. Order Id: ".$row->order_id;
                break;
            case 6:
                $subject = "Stationary Order DONE. Order Id: ".$row->order_id;
                break;
            case 7:
                $cc = $this->getGAEmails();
                $to = $row->senderEmail;
                $regIds = false;
                
                $subject = "Appointment Approval. Id: ".$row->order_id;
                $message = "Your appointment has been approved by ".$row->pickupUserName." Note. ".$row->order_receiver_note;
                break;
       }
       
       $this->notif($row, $to, $subject, $message, $cc, $regIds);
   }
   
   public function notifSetCourier($orderId)
   {
       $row = $this->getNotifData($orderId);
       
       $subjectSender = "Document Courier SET MESSENGER. Order Id: ".$row->order_id;
       $messageSender = "Your order will be send by ".$row->courierName;
       
       $subjectMessenger = "New Order Document Courier. Order Id: ".$row->order_id;    
       $messageMessenger = sprintf("You have a new order created by %s", $row->senderName);
      
        $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_photo);
      
       fcm(array($row->senderFCM), $subjectSender, $messageSender, $photo);
       fcm(array($row->courierFCM), $subjectMessenger, $messageMessenger, $photo);
       
        $this->notif($row, $row->senderEmail, $subjectSender, $messageSender, $this->getGAEmails());
       
       $this->notif($row, $row->courierEmail, $subjectMessenger, $messageMessenger, $this->getGAEmails());
       
      
   }
   
   public function notifPickupOrder($orderId, $sessionUserName="")
   {
       $row = $this->getNotifData($orderId);
       
       $subject = "";
       $message = "Your Order has been pick up at ".$row->order_pickup_date_fmt." by ".$row->pickupUserName;
       switch($row->order_type)
       {
            case 1:
               $subject = "Document Courier PICK UP. Order Id: ".$row->order_id;
               break;
            case 2:
                $subject = "Document Ext Courier PICK UP. Order Id: ".$row->order_id;
                break;
            case 3:
                $subject = "New Incoming Document PICK UP. Order Id: ".$row->order_id; 
                break;
            case 4:
                if ($row->order_status == 3)
                {
                    $subject = "Booking Car PROCESSED. Order Id: ".$row->order_id;
                    $message = "Your Order has been processed at ".$row->order_pickup_date_fmt." by ".$sessionUserName;
                }
                else
                {
                    $subject = "Booking Car PICK UP. Order Id: ".$row->order_id;
                    $message = "Your Order has been pick up at ".$row->order_pickup_date_fmt." by ".$sessionUserName;
                }
                break;
            case 6:
                $subject = "Stationary Order PICK UP. Order Id: ".$row->order_id;
                break;
       }
       
       $this->notif($row, $row->senderEmail, $subject, $message, $this->getGAEmails(), array($row->senderFCM));
   }
   
   public function notifApproveOrder($orderId)
   {
       $row = $this->getNotifData($orderId);
       
       if ($row->order_type == 4)
       {
            $subjectSender = "Booking Car APPROVAL. Order Id: ".$row->order_id;
            $messageSender = "Your Order has been approved at ".$row->order_courier_date_fmt;
       
            $this->notif($row, $row->senderEmail, $subjectSender, $messageSender, $this->getGAEmails(), array($row->senderFCM));
           
            $this->db->limit(1);
            $this->db->order_by('car_driver_end_date', 'DESC');
            $this->db->where("car_driver_car", $row->car_id);
            $this->db->join("_user", "user_id = car_driver_user");
            $q = $this->db->get("car_driver");
        
            $driver = $q->row();
        
           $subjectDriver = "New Booking Car. Order Id: ".$row->order_id;    
           $messageDriver = sprintf("You have a new order created by %s", $row->senderName);
           $this->notif($row, $driver->user_email, $subjectDriver, $messageDriver, $this->getGAEmails(), array($driver->user_fcm));
       
           return;
       }
       
       $subject = "";
       $message = "Your Order has been approved at ".$row->order_courier_date_fmt;
       switch($row->order_type)
       {
            case 1:
               $subject = "New Document Courier Approval. Order Id: ".$row->order_id;
               break;
            case 2:
                $subject = "New Document External Courier Approval. Order Id: ".$row->order_id;
                break;
            case 3:
                $subject = "New Incoming Document Approval. Order Id: ".$row->order_id; 
                break;
            case 5:
                $subject = "New Booking Meeting Room Approval. Order Id: ".$row->order_id;
                $this->inviteAttendees($row);
                break;
            case 6:
                $subject = "New Stationary Order Approval. Order Id: ".$row->order_id;
                break;
       }
       
       $this->notif($row, $row->senderEmail, $subject, $message, $this->getGAEmails(), array($row->senderFCM));
   }
   
   private function inviteAttendees($row)
   {
       $this->db->where("meeting_attendance_room", $row->order_id);
       $this->db->join("_user", "user_id = meeting_attendance_user");
       $q = $this->db->get("meeting_attendance");
       
       $rows = $q->result();
       
       $attendees = array();
       for($i=0; $i < count($rows); $i++)
       {
           $attendees[] = $rows[$i]->user_email;
       }
       
       $subject = sprintf("Meeting Invite");
       
       $msessage = sprintf("%s invite you to join with %s meeting. <br /><br />%s", $row->senderName, $row->order_name, $this->bookingMeetingRoomDetail($row));
       kirimEmail($attendees, $subject, $msessage);
   }
   
   public function notifNewOrder($orderId)
   {
       $row = $this->getNotifData($orderId);
       
       $subject = "";
       $message = "New Order has been created at ".$row->order_created_date_fmt." by ".$row->senderName."<br />Please check your administration web";
       
       $to = $this->getGAEmails();
       $regIds = $this->getGARegisterIds();
       
       $cc = array($row->senderEmail);
       switch($row->order_type)
       {
            case 1:
               $subject = "New Document Courier. Order Id: ".$row->order_id;
               break;
            case 2:
                $subject = "New Document Ext Courier. Order Id: ".$row->order_id;
                break;
            case 3:
                $to = array($row->senderEmail);
                $regIds = array($row->senderFCM);
                $cc = $this->getGAEmails();
                
                $subject = "New Incoming Document. Order Id: ".$row->order_id; 
                $message = "We have received you order at ".$row->order_created_date_fmt." from ".$row->order_receiver_name;
                break;
            case 4:
                $subject = "New Booking Car. Order Id: ".$row->order_id;
                break;
            case 5:
                $subject = "New Booking Meeting Room. Order Id: ".$row->order_id;
                break;
            case 6:
                $subject = "New Stationary Order. Order Id: ".$row->order_id;
                break;
            case 7:
                $to = array($row->pickupUserEmail);
                $regIds = array($row->pickupUserFCM);
                $cc = $this->getGAEmails();
                
                $subject = "New Appointment. Id: ".$row->order_id;
                $message = "You have new appointment at ".$row->order_created_date_fmt." by ".$row->senderName." with purpose '".$row->order_receiver_address."'";
                break;
       }
       
       $this->notif($row, $to, $subject, $message, $cc, $regIds);
   }
   
   private function notif($row, $to, $subject, $message, $cc, $regIds=false)
   {
       $message .= "<br /><br />";
       switch($row->order_type)
       {
            case 1:
                $message .= $this->docInternalDetail($row);
                $notifMessage = $this->docInternalDetailNotif($row);
                break;
            case 2:
                $message .= $this->docExternalDetail($row);
                $notifMessage = $this->docExternalDetailNotif($row);
                break;
            case 3:
                $message .= $this->docIncomingDetail($row);
                $notifMessage = $this->docIncomingDetailNotif($row);
                break;
            case 4:
                $message .= $this->bookingCarDetail($row);
                break;
            case 5:
               $message .= $this->bookingMeetingRoomDetail($row);
               break;
            case 6:
                $message .= $this->stationaryDetail($row);
                break;
            case 7:
                $notifMessage[0] = "You have new appointment from ".$row->senderName." with purpose '".$row->order_receiver_address."'";
                $notifMessage[1] = sprintf("%s/%s", $this->config->item("base_url"), $row->senderPhoto);
                break;
       }
       
       $data['message'] = $message;
       
       if (! isset($notifMessage))
       {
           $notifMessage[0] = $message;
           $notifMessage[1] = "";
       }
       
       
       if ($regIds)
       {
           $notifMsg = str_replace("<br />", "\n", $notifMessage[0]);
           $notifMsg = str_replace("<b>", "", $notifMsg);
           $notifMsg = str_replace("</b>", "", $notifMsg);
           
           fcm($regIds, $subject, $notifMsg, $notifMessage[1]);
       }
       
       $html = $this->load->view('email_notif', $data, TRUE);
       kirimEmail($to, $subject, $html, $cc);
   }
   
   private function getNotifData($orderId)
   {
       $this->db->select("*, DATE_FORMAT(order_created_date, '%d/%m/%Y %H:%i:%s') as order_created_date_fmt, DATE_FORMAT(order_rejected_date, '%d/%m/%Y %H:%i:%s') as order_rejected_date_fmt, DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i:%s') as order_end_date_fmt, DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i:%s') as order_start_date_fmt, DATE_FORMAT(order_courier_date, '%d/%m/%Y %H:%i:%s') as order_courier_date_fmt, DATE_FORMAT(order_received_date, '%d/%m/%Y %H:%i:%s') as order_received_date_fmt, DATE_FORMAT(order_pickup_date, '%d/%m/%Y %H:%i:%s') as order_pickup_date_fmt, sender.user_name senderName, sender.user_email senderEmail, sender.user_fcm senderFCM, sender.user_photo senderPhoto, pickupUser.user_name pickupUserName, pickupUser.user_email pickupUserEmail, pickupUser.user_fcm pickupUserFCM, reject.user_name rejectedUserName, courier.user_name courierName, courier.user_email courierEmail, courier.user_fcm courierFCM");
       
       $this->db->where("order_id", $orderId);
       
       $this->db->join("order_priority", "priority_id = order_priority", "left");
       $this->db->join("meeting_room", "meeting_room_id = order_ref", "left");
       $this->db->join("car", "car_id = order_ref", "left");
       $this->db->join("_user pickupUser", "pickupUser.user_id = order_pickup_by", "left");
       $this->db->join("_user sender", "sender.user_id = order_created_by", "left");
       $this->db->join("_user reject", "reject.user_id = order_rejected_by", "left");
       $this->db->join("_user courier", "courier.user_id = order_courier_by", "left");
       $this->db->join("courier_package_type", "package_type_id = order_courier_by", "left");
       $this->db->join("logistic", "package_type_company = logistic_id", "left");
       $q = $this->db->get("order");
       
       $row = $q->row();
       
       return $row;
   }
   
   private function getGAEmails()
   {
        $this->db->where_in("user_type", array(97, 98));
       $q = $this->db->get("_user");
       
       $rows = $q->result();
       if (count($rows) == 0) return array();
       
       $tos = array();
       for($i=0; $i < count($rows); $i++)
       {
           $tos[] = $rows[$i]->user_email;
       }
       
       return $tos;
   }
   
    public function getGARegisterIds()
   {
        $this->db->where_in("user_type", array(97, 98, 99));
       $q = $this->db->get("_user");
       
       $rows = $q->result();
       if (count($rows) == 0) return array();
       
       $tos = array();
       for($i=0; $i < count($rows); $i++)
       {
           if ($rows[$i]->user_fcm)
           {
            $tos[] = $rows[$i]->user_fcm;
           }
       }
       
       return $tos;
   }
   
   private function docInternalDetail($row)
   {
       $receiverphoto = "";
       if ($row->order_status == 4)
       {
           $receiverphoto = sprintf("Receiver Photo: %s/%s<br />", $this->config->item("base_url"), $row->order_receiver_photo);
       }
       
       return sprintf("DETAILS<br />Delivery Time: <b>%s</b><br />Pickup: <b>At Office</b><br />Destination: <b>%s (note. %s)</b><br />Photo: %s/%s<br />Item Description: <b>%s</b><br />Receiver Name: <b>%s</b><br />%sHandphone No: <b>%s</b><br />Note: <b>%s</b><br />", $row->priority_name, $row->order_receiver_address, $row->order_receiver_address_note, $this->config->item("base_url"), $row->order_photo, $row->order_name, $row->order_receiver_name, $receiverphoto, $row->order_receiver_phone, $row->order_note);
   }
   
   private function docInternalDetailNotif($row)
   {
       $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_photo);
       
       if ($row->order_status == 5)
       {
           $message = sprintf("%s rejected %s '%s'", $row->order_name, $row->rejectedUserName, $row->order_rejected_note);
       }
       else
       if ($row->order_status == 4)
       {
           $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_receiver_photo);
           
           $message = sprintf("Your order '%s' has received by %s", $row->order_name, $row->order_receiver_name);
       }
       else
       if ($row->order_status == 3)
       {
           $message = sprintf("Your order '%s' has pick up by %s", $row->order_name, $row->courierName);
       }
       else
       {
            $message = sprintf("%s by %s", $row->order_name, $row->senderName);
       }
       
       
       return array($message, $photo);
   }
   
   private function docIncomingDetail($row)
   {
        return sprintf("DETAILS<br />Photo: %s/%s<br />Receiver Name: <b>%s</b><br />Sender (Company Name): <b>%s</b><br />Courier Name: <b>%s</b><br />Description of Goods: <b>%s</b><br />", $this->config->item("base_url"), $row->order_photo, $row->senderName, $row->order_receiver_name, $row->order_receiver_address, $row->order_name);
   }
   
   private function docIncomingDetailNotif($row)
   {
       $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_photo);
       
       if ($row->order_status == 5)
       {
           $message = sprintf("Order '%s' rejected by %s: '%s'", $row->order_name, $row->rejectedUserName, $row->order_rejected_note);
       }
       else
       if ($row->order_status == 4)
       {
           $message = sprintf("Order '%s' has received by %s ", $row->order_name, $row->senderName);
       }
       else
       if ($row->order_status == 3)
       {
           $message = sprintf("Your order '%s' has pick up by %s", $row->order_name, $row->pickupUserName);
       }
       else
       {
            $message = sprintf("You have new order '%s' from", $row->order_name, $row->order_receiver_name);
       }
       
       return array($message, $photo);
   }
   
   private function docExternalDetail($row)
   {
       $receive = "";
       if ($row->order_status == 4)
       {
           $receive = sprintf("RECEIPT DESC<br />Airwaybill No: <b>%s</b><br />Delivery Fee: <b>Rp %s</b><br />Note: <b>%s</b><br /><br />", $row->order_note, number_format($row->order_receiver_latitude, 0, ",", "."), $row->order_receiver_note);
       }
       
       return sprintf("%sDETAILS<br />Photo: %s/%s<br />Courier company: <b>%s</b><br />Type of Service: <b>%s</b><br />Item Description: <b>%s</b><br />Receiver Name: <b>%s</b><br />Handphone No: <b>%s</b><br />Sent to (Address): <b>%s</b><br />Postal Code: <b>%s</b><br />", $receive, $this->config->item("base_url"), $row->order_photo, $row->logistic_company, $row->package_type_name, $row->order_name, $row->order_receiver_name, $row->order_receiver_phone, $row->order_receiver_address, $row->order_receiver_address_note);
   }
   
   private function docExternalDetailNotif($row)
   {
       $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_photo);
       
       if ($row->order_status == 5)
       {
           $message = sprintf("%s rejected: '%s'", $row->order_name, $row->order_rejected_note);
       }
       else
       if ($row->order_status == 4)
       {
           $photo = sprintf("%s/%s", $this->config->item("base_url"), $row->order_receiver_photo);
           
           $message = sprintf("Your order '%s' has sent to %s %s", $row->order_name, $row->logistic_company, $row->package_type_name);
       }
       else
       if ($row->order_status == 3)
       {
           $message = sprintf("Your order '%s' has pick up by %s", $row->order_name, $row->pickupUserName);
       }
       else
       {
            $message = sprintf("%s by %s", $row->order_name, $row->senderName);
       }
       
       
       return array($message, $photo);
   }
   
   private function stationaryDetail($row)
   {
       $this->db->where("order_stationary_order", $row->order_id);
       $this->db->join("stationary_item", "item_id = order_stationary_item");
       
       $q = $this->db->get("order_stationary");
       
       $rows = $q->result();
       
       $items = array();
       for($i=0; $i < count($rows); $i++)
       {
           $items[] = sprintf("%s %d Pcs", $rows[$i]->item_description, $rows[$i]->order_stationary_qty);
       }
       
       $shipping = $row->order_sender_latitude == 1 ? "Take it by your self" : "Delivery with Office Boy ".$row->pickupUserName;

       return sprintf("DETAILS<br />Items: <br /><b>%s</b><br />Delivery: <b>%s</b>", implode("<br /> ", $items), $shipping);
   }
   
   private function bookingMeetingRoomDetail($row)
   {
       $this->db->where("meeting_attendance_room", $row->order_id);
       $this->db->join("_user", "user_id = meeting_attendance_user");
       $q = $this->db->get("meeting_attendance");
       
       $rows = $q->result();
       
       $attendees = array();
       for($i=0; $i < count($rows); $i++)
       {
           $attendees[] = $rows[$i]->user_name;
       }
       
       return sprintf("DETAILS<br />Room: %s <b>(%s)</b><br />Schedule: <b>%s-%s</b><br />Topic: <b>%s</b><br />Meeting with: <b>%s</b><br />Attendees: <b>%s</b>", $row->meeting_room_name, $row->meeting_room_location, $row->order_start_date_fmt, $row->order_end_date_fmt, $row->order_name, $this->getMeetingWith($row), implode(", ", $attendees));
   }
   
   private function bookingCarDetail($row)
   {
        $this->db->limit(1);
        $this->db->order_by('car_driver_end_date', 'DESC');
        $this->db->where("car_driver_car", $row->car_id);
        $this->db->join("_user", "user_id = car_driver_user");
        $q = $this->db->get("car_driver");
        
        $driver = $q->row();
       
        $services = $this->getBookingCarsServices();
       
        return sprintf("DETAILS<br />Schedule: <b>%s-%s</b><br />Car: <b>%s (%s %s)</b><br />Driver: <b>%s</b><br />Service: <b>%s</b><br />Pick up Location: <b>%s (note. %s)</b><br />Destination Location: <b>%s (note. %s)</b>", $row->order_start_date_fmt, $row->order_end_date_fmt, $row->car_no, $row->car_brand, $row->car_model, $driver->user_name, $services[$row->order_priority], $row->order_sender_address, $row->order_photo, $row->order_receiver_address, $row->order_receiver_address_note);
   }
   
   private function visitorDetail($row)
   {
        return sprintf("DETAILS<br />Photo: %s/%s<br />Receiver Name: <b>%s</b><br />Sender (Company Name): <b>%s</b><br />Courier Name: <b>%s</b><br />Description of Goods: <b>%s</b><br />", $this->config->item("base_url"), $row->order_photo, $row->senderName, $row->order_receiver_name, $row->order_receiver_address, $row->order_name);
   }
   
}
