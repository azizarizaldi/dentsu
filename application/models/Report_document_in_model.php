<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_document_in_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="",$range_date,$order_ref)
   {
	  $this->db->select("
		order_id,
		DATE(order_created_date) AS order_date,
		_user.user_name,
		order.order_photo AS item,
		order_name,
		order_note,
		order_receiver_name,
		order_receiver_phone,
		order_receiver_address,
		order_receiver_address_note,
		courier.user_name AS courier,
		TIME(order_received_date) AS rec_date,
		TIME(order_pickup_date) AS pick_date,
		TIME(order_courier_date) AS found_date,
		order.order_received_date ,
		order.order_rejected_date ,
	    IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status_order		
	  ");

 	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(order_name LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
      }
	  
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
	  
	  if($order_ref) {
		  $this->db->where("order_status",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);
	
		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->where("order_type","3");
	  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
	  $this->db->join("_user courier","courier.user_id = order_courier_by ","left");
      $q =  $this->db->get("order");
      
      return $q->result();
   }
      
   public function getCount($search,$range_date,$order_ref)
   {
$this->db->select("
		  order_id,
		  DATE(order_created_date) AS order_date,
		  _user.user_name,
		  order.order_photo AS item,
		  order_name,
		  order_note,
		  order_receiver_name,
		  order_receiver_phone,
		  order_receiver_address,
		  order_receiver_address_note,
		  courier.user_name AS courier 	  
	  ");

 	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(order_name LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
      }
	  	  
	  if($order_ref) {
		  $this->db->where("order_status",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);
	
		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->where("order_type","3");
	  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
	  $this->db->join("_user courier","courier.user_id = order_courier_by ","left");
      $q =  $this->db->get("order");
      
      return $q->num_rows();
   }
   
}
