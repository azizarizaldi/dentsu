<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model {

   public function get($orderBy="document_name", $dir="asc", $limit=0, $offset=0, $search="",$id_category="")
   {
      $this->db->select("document.* , setting_category.name as category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("document_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = document.id_category","inner");
      $q =  $this->db->get("document");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("document_id", $id);
      $q =  $this->db->get("document");
      
      return $q->row();
   }
   
   public function getCount($search,$id_category)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("document_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = document.id_category","inner");
      return $this->db->count_all_results("document");
   }

   public function insert($data)
   {
        $this->db->insert("document", $data);
   }
   
   public function update($data)
   {
      $this->db->where("document_id", $data["document_id"]);
        $this->db->update("document", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("document_id", $id);
        $this->db->delete("document");
   }
   
}
