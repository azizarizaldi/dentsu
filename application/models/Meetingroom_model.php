<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeetingRoom_Model extends CI_Model {

   public function get($orderBy="meeting_room_name", $dir="asc", $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("meeting_room_name", $search);
         $this->db->or_like("meeting_room_location", $search);
      }
      $q =  $this->db->get("meeting_room");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("meeting_room_id", $id);
      $q =  $this->db->get("meeting_room");
      
      return $q->row();
   }
   
   public function getCount($search)
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("meeting_room_name", $search);
         $this->db->or_like("meeting_room_location", $search);
      }
      return $this->db->count_all_results("meeting_room");
   }

   public function insert($data)
   {
        $this->db->insert("meeting_room", $data);
   }
   
   public function update($data)
   {
      $this->db->where("meeting_room_id", $data["meeting_room_id"]);
        $this->db->update("meeting_room", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("meeting_room_id", $id);
        $this->db->delete("meeting_room");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["meeting_room_id"]))
      {
         $this->db->where("meeting_room_id <>", $data["meeting_room_id"]);
      }
      $this->db->where("meeting_room_name", $data["meeting_room_name"]);
      return $this->db->count_all_results("meeting_room");
   }
   
   function updateAttendance($id, $attandaces)
   {
      $this->db->where("meeting_attendance_room", $id);
      $this->db->delete("meeting_attendance");
      
      for($i=0; $i < count($attandaces); $i++)
      {
         unset($data);
         
         $data['meeting_attendance_user'] = $attandaces[$i];
         $data['meeting_attendance_room'] = $id;
         
         $this->db->insert("meeting_attendance", $data);
      }
   }
   
   function getBook($meetingRoomId)
   {
        $this->db->select("*
            , DATE_FORMAT(order_start_date, '%H') as booked_start_hour
            , DATE_FORMAT(order_start_date, '%i') as booked_start_minute
            , DATE_FORMAT(order_start_date, '%p') as booked_start_ampm
            , DATE_FORMAT(order_end_date, '%H') as booked_end_hour
            , DATE_FORMAT(order_end_date, '%i') as booked_end_minute
            , DATE_FORMAT(order_end_date, '%p') as booked_end_ampm
            ");
        $this->db->where("order_type", 5);
        $this->db->where("order_start_date <=", date("Y-m-d H:i:s"));
        $this->db->where("order_end_date >=", date("Y-m-d H:i:s"));
        $this->db->where("order_status <>", 5);
        $this->db->where("order_ref", $meetingRoomId);
        $q = $this->db->get("order");
        
        return $q->row();
   }
   
    function getAvailable($meetingRoomId)
   {
        $this->db->select("*, DATE_FORMAT(order_start_date, '%H') as order_available_hour, DATE_FORMAT(order_start_date, '%i') as order_available_minute, DATE_FORMAT(order_start_date, '%p') as order_available_am_pm");
        $this->db->where("order_type", 5);
        $this->db->where("order_start_date >=", date("Y-m-d H:i:s"));
        $this->db->where("order_ref", $meetingRoomId);
        $this->db->where("order_status <>", 5);
        $this->db->limit(1);
        $this->db->order_by("order_start_date", "asc");
        $q = $this->db->get("order");
        
        return $q->row();
   }
   
    function getTodays($meetingRoomId)
   {
        $this->db->select("*
            , TIMEDIFF(order_end_date, '".date("Y-m-d H:i:s")."') as remaining
            , DATE_FORMAT(order_start_date, '%H:%i') as startDate
            , DATE_FORMAT(order_end_date, '%H:%i') as endDate
            , creator.user_name sender_name
            , order_start_date <= '".date("Y-m-d H:i:s")."' AND order_end_date >= '".date("Y-m-d H:i:s")."'  as courier_id");
        $this->db->where("order_type", 5);
        $this->db->where("order_status <>", 5);
        $this->db->where("((order_start_date <= '".date("Y-m-d H:i:s")."' AND order_end_date >= '".date("Y-m-d H:i:s")."') OR (order_start_date >= '".date("Y-m-d H:i:s")."' ))", null, false);
        $this->db->where("order_ref", $meetingRoomId);
        $this->db->order_by("order_start_date", "asc");
        $this->db->join("_user creator", "creator.user_id = order_created_by", "left");
        $q = $this->db->get("order");
        
        $temps = $q->result();
        $rows = array();
        
        if (count($temps) == 0)
        {
                $obj = new stdClass;
                $obj->startDate = date("H:i");
                $obj->endDate = "23:59";
                $obj->order_name = "Available";
                $obj->courier_id = 1;
                
                $rows[] = $obj;
                
            return $rows;
        }
        
        
        if (count($temps) > 0)
        {
             $row = $temps[0];
             if ($row->courier_id != 1)
             {
                $obj = new stdClass;
                $obj->startDate = date("H:i");
                $obj->endDate = $row->startDate;
                $obj->order_name = "Available";
                $obj->courier_id = 1;
                
                $rows[] = $obj;
             }
        }
        
        for($i=1; $i < count($temps); $i++)
        {
            $prev = $temps[$i-1];
            $next = $temps[$i];
            
            $rows[] = $temps[$i-1];
            if ($prev->endDate != $next->startDate)
            {
                $obj = new stdClass;
                $obj->startDate = $prev->endDate;
                $obj->endDate = $next->startDate;
                $obj->order_name = "Available";
                
                $rows[] = $obj;
            }
        }
        
        if (count($temps) > 0)
        {
            $row = $temps[count($temps)-1];
            
            $rows[] = $row;
            
            if ($row->endDate != "23:59")
            {
                $obj = new stdClass;
                $obj->startDate = $row->endDate;
                $obj->endDate = "23:59";
                $obj->order_name = "Available";
                
                $rows[] = $obj;
            }
        }
        
        return $rows;
   }

   
   function absent($orderId, $attendees)
   {
       $update['meeting_attendance_absent'] = 10;
       $update['meeting_attendance_date'] = date("Y-m-d H:i:s");
       
       $this->db->where_in("meeting_attendance_user", $attendees);
       $this->db->where("meeting_attendance_room", $orderId);
       
       $this->db->update("meeting_attendance", $update);
   }
   
   function endNow($orderId)
   {
       $update['order_status'] = 4;
       $update['order_end_date'] = date("Y-m-d H:i:s");
       
       $this->db->where("order_type", 5);
       $this->db->where("order_id", $orderId);
       $this->db->update("order", $update);
   }
   
   function extend($orderId, $time)
   {
       $sql = sprintf("UPDATE `order` SET order_end_date = order_end_date + INTERVAL %d SECOND WHERE order_type = 5 AND order_id = %d", $time, $orderId);
       $this->db->query($sql, null, false);
   }
}
