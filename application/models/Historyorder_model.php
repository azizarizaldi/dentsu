<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HistoryOrder_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="",$range_date)
   {
	  $this->db->select("DATE(order_date) AS order_date,code,stationary_order.stationary_order_id,DATE(receive_date) AS receive_date,GROUP_CONCAT(CONCAT(stationary_item.item_description,' - ',stationary_order_detail.qty,' ',stationary_item.item_unit_measure)) AS item_description");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      
/* 	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(item_description LIKE ${search} OR category_name LIKE ${search})", null, false);
      }
 */	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);
	
		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","inner");
	  $this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
	  $this->db->group_by("stationary_order_detail.stationary_order_id");
      $q =  $this->db->get("stationary_order");
      
      return $q->result();
   }
      
   public function getCount($search,$range_date)
   {
	  $this->db->select("DATE(order_date) AS order_date,code,stationary_order.stationary_order_id,DATE(receive_date) AS receive_date,GROUP_CONCAT(CONCAT(stationary_item.item_description,' - ',stationary_order_detail.qty,' ',stationary_item.item_unit_measure)) AS item_description");      
/* 	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(item_description LIKE ${search} OR category_name LIKE ${search})", null, false);
      }
 */	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);
	
		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","inner");
	  $this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
	  $this->db->group_by("stationary_order_detail.stationary_order_id");
      $q =  $this->db->get("stationary_order");
      
      return $q->num_rows();
   }
   
}
