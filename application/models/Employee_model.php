<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

   public function get($orderBy="employee_name", $dir="asc", $limit=0, $offset=0, $search="",$id_category="")
   {
      $this->db->select("employee.* , setting_category.name as category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("employee_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = employee.id_category","inner");
      $q =  $this->db->get("employee");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("employee_id", $id);
      $q =  $this->db->get("employee");
      
      return $q->row();
   }
   
   public function getCount($search,$id_category)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("employee_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = employee.id_category","inner");
      return $this->db->count_all_results("employee");
   }

   public function insert($data)
   {
        $this->db->insert("employee", $data);
   }
   
   public function update($data)
   {
      $this->db->where("employee_id", $data["employee_id"]);
        $this->db->update("employee", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("employee_id", $id);
        $this->db->delete("employee");
   }
   
}
