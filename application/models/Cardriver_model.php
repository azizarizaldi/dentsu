<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CarDriver_Model extends CI_Model {

   function isValidData($carId, $date1, $date2, $driverId)
   {
      $this->db->where("car_driver_start_date <=", $date1);
      $this->db->where("car_driver_end_date >=", $date1);
      $this->db->where("car_driver_car", $carId);
      //$this->db->where("car_driver_user <>", $driverId);
      
      $n = $this->db->count_all_results("car_driver");
      
      if ($n > 0) return false;
      
      $this->db->where("car_driver_start_date <=", $date2);
      $this->db->where("car_driver_end_date >=", $date2);
      $this->db->where("car_driver_car", $carId);
      //$this->db->where("car_driver_user <>", $driverId);
      
      $n = $this->db->count_all_results("car_driver");
      
      return $n == 0;
   }
   
   function insert($data)
   {
      $this->db->insert("car_driver", $data);
   }
   
   function getDriver($carId)
   {
        $this->db->limit(1);
        $this->db->order_by('car_driver_end_date', 'DESC');
        $this->db->where("car_driver_car", $carId);
        $this->db->join("_user", "user_id = car_driver_user");
        $q = $this->db->get("car_driver");
        
        return $q->row();
   }
   
    function getCar($driverId)
   {
        $this->db->limit(1);
        $this->db->order_by('car_driver_end_date', 'DESC');
        $this->db->where("user_id", $driverId);
        $this->db->join("_user", "user_id = car_driver_user");
        $q = $this->db->get("car_driver");
        
        return $q->row();
   }
   
   function getCars($driverId)
   {
        $this->db->order_by('car_driver_end_date', 'DESC');
        $this->db->where("user_id", $driverId);
        $this->db->where("(car_driver_end_date >= '".date("Y-m-d 23:59:59")."' OR car_driver_end_date = '0000-00-00 00:00:00')", null, false);
        $this->db->join("_user", "user_id = car_driver_user");
        $q = $this->db->get("car_driver");
        
        return $q->result();
   }

}
