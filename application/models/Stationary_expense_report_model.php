<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stationary_expense_report_model extends CI_Model {

	public function get($orderBy, $dir, $limit=0, $offset=0, $search="",$range_date,$item_category="",$departement_id="",$company_id="",$brand_unit_id="") {
		$this->db->select("
			stationary_item.item_description,
			stationary_item.item_stock , 
			stationary_item.item_min_stock ,
			stationary_item.item_ideal_stock , 
			stationary_item.item_price ,
			stationary_category.category_name
		");
		
		if ($limit > 0) {
		 	$this->db->limit($limit, $offset);
		}

		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
		}
		
		if($item_category) {
			$this->db->where("item_category",$item_category);
		}

		if($departement_id) {
			$this->db->where("user_departement",$departement_id);
		}

		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}

		if($company_id) {
			$this->db->where("brand_unit_company",$company_id);
		}
		
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		}
		
		$this->db->join("order_stationary","order_stationary.order_stationary_order = order.order_id","inner");
		$this->db->join("stationary_item","stationary_item.item_id = order_stationary.order_stationary_item","inner");
		$this->db->join("_user","_user.user_id = order.order_created_by","inner");
		$this->db->join("departement","_user.user_departement = departement.departement_id","left");
		$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$q =  $this->db->get("order");

		return $q->result();
	}
      
   public function getCount($search,$range_date,$item_category="",$departement_id="",$company_id="",$brand_unit_id="")
   {	  
	$this->db->select("
		stationary_item.item_description,
		stationary_item.item_stock , 
		stationary_item.item_min_stock ,
		stationary_item.item_ideal_stock , 
		stationary_item.item_price ,
		stationary_category.category_name
	");

	$search = trim($search);
	if ($search) {
		$search = $this->db->escape('%'.$search.'%');
		$this->db->where("(item_description LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
	}

	if($item_category) {
		$this->db->where("item_category",$item_category);
	}

	if($departement_id) {
		$this->db->where("user_departement",$departement_id);
	}

	if($brand_unit_id) {
		$this->db->where("departement_brand_unit",$brand_unit_id);
	}

	if($company_id) {
		$this->db->where("brand_unit_company",$company_id);
	}

	if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);

		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);

		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	}

	$this->db->join("order_stationary","order_stationary.order_stationary_order = order.order_id","inner");
	$this->db->join("stationary_item","stationary_item.item_id = order_stationary.order_stationary_item","inner");
	$this->db->join("_user","_user.user_id = order.order_created_by","inner");
	$this->db->join("departement","_user.user_departement = departement.departement_id","left");
	$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
	$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
	$q =  $this->db->get("order");

	return $q->num_rows();
   }
   
}
