<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {
   
   public function insert($data, $password)
   {
        $this->db->set('user_password', 'PASSWORD("'.$password.'")', FALSE);
        $this->db->insert("_user", $data);
        
        return $this->db->insert_id();
   }
   
   public function changePassword($data)
   {
      $password = $data["user_password"];
      unset($data['user_password']);
      
      $this->db->where("user_id", $data['user_id']);
      $this->db->set('user_password', 'PASSWORD("'.$password.'")', FALSE);
      $this->db->update("_user", $data);
   }
   
   public function update($data)
   {
      $this->db->where("user_id", $data["user_id"]);
        $this->db->update("_user", $data);
   }
  
  public function updateToken($userId, $token, $fcm="")
  {
      if ($fcm)
      {
          $updateFCM['user_fcm'] = "";
          
          $this->db->where("user_fcm", $fcm);
          $this->db->update("_user", $updateFCM);
      }
      
      $data['user_token'] = $token;
      if ($fcm)
      {
          $data['user_fcm'] = $fcm;
      }
      
      $this->db->where("user_id", $userId);
      $this->db->update("_user", $data);
  }
   
  public function delete($id)
   {
      $this->db->where("user_id", $id);
        $this->db->delete("_user");
   }

   function getByLogin($username, $pass)
   {
      $password = str_replace("'", "\'", $pass);
      
      $this->db->where("user_email", $username);
      $this->db->where("user_password = PASSWORD('${password}')", null, false);
      
      $this->db->join("departement", "departement_id = user_departement", "left");
      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id", "left");
      $this->db->join("company", "brand_unit_company = company_id", "left");
      
      $q =  $this->db->get("_user");
      
      return $q->row();
   }
   
   function logout($email, $token)
   {
      $data['user_fcm'] = ""; 
       
      $this->db->where("user_email", $email);
      $this->db->where("user_token", $token);
      $this->db->update("_user", $data);
      
   }
   
   function checkToken($email, $token)
   {
      $this->db->where("user_email", $email);
      $this->db->where("user_token", $token);
      $q =  $this->db->get("_user");
      
      return $q->row();
   }

   function getByUniqValue($email, $phone, $nik)
   {
      $this->db->or_where("user_email", $email);
      $this->db->or_where("user_phone", $phone);
      if ($nik !== FALSE)
      {
        $this->db->or_where("user_nik", $nik);
      }
      $q =  $this->db->get("_user");
      
      return $q->row();
   }

   function getByVerificationCode($code, $phone)
   {
      if ($code == "") return null;

      $this->db->where("user_verification_code", $code);
      $this->db->where("user_phone", $phone);
      $q =  $this->db->get("_user");
      
      return $q->row();
   }
   
   public function getById($id)
   {
      $this->db->join("departement", "departement_id = user_departement", "left");
      $this->db->join("brand_unit", "departement_brand_unit = brand_unit_id", "left");
      $this->db->where("user_id", $id);
      $q =  $this->db->get("_user");
      
      return $q->row();
   }
   
   public function get($orderBy, $dir, $limit=0, $offset=0, $search="", $isVisitor=false)
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("((user_nik Like ${search}) OR (user_phone Like ${search}) OR (user_email Like ${search}))", null, false);
      }

      $this->db->where("user_type <>", 99);
      if ($isVisitor)
      {
         $this->db->where("user_type", 50);
      }
      else
      {
         $this->db->where("user_type <>", 50);
      }
      
      $this->db->join("departement", "user_departement = departement_id", "left");
      $this->db->join("brand_unit", "brand_unit_id = departement_brand_unit", "left");
      $this->db->join("company", "company_id = brand_unit_company", "left");
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
   
   public function getCount($search="", $isVisitor=false)
   {
      if ($search)
      {
         $search = $this->db->escape('%'.$search.'%');

         $this->db->where("((user_nik Like ${search}) OR (user_phone Like ${search}) OR (user_email Like ${search}))", null, false);
  
      }

      $this->db->where("user_type <>", 99);
      if ($isVisitor)
      {
         $this->db->where("user_type", 50);
      }
      else
      {
         $this->db->where("user_type <>", 50);
      }
      
      return $this->db->count_all_results("_user");
   }
   
   public function getEmployees()
   {
      $this->db->order_by("user_name", "asc");
      $this->db->or_where("user_type", 1);
      $this->db->or_where("user_type", 97);
      $this->db->or_where("user_type", 98);
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
   
   public function getCouriers()
   {
      $this->db->order_by("user_name", "asc");
      $this->db->or_where("user_type", 4);
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
   
   public function getDrivers()
   {
      $this->db->order_by("user_name", "asc");
      $this->db->or_where("user_type", 3);
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
   
   public function getOfficeBoys()
   {
      $this->db->order_by("user_name", "asc");
      $this->db->or_where("user_type", 2);
      $q =  $this->db->get("_user");
      
      return $q->result();
   }
   
   public function isNIKAlreadyExist($data)
   {
      if (isset($data["user_id"]))
      {
         $this->db->where("user_id <>", $data["user_id"]);
      }
      
      $this->db->where("user_nik", $data["user_nik"]);
      
      return $this->db->count_all_results("_user");
   }
   
   public function isPhoneAlreadyExist($data)
   {
      if (isset($data["user_id"]))
      {
         $this->db->where("user_id <>", $data["user_id"]);
      }
      
      $this->db->where("user_phone", $data["user_phone"]);
      
      return $this->db->count_all_results("_user");
   }
   
   public function isEmailAlreadyExist($data)
   {
      if (isset($data["user_id"]))
      {
         $this->db->where("user_id <>", $data["user_id"]);
      }
      
      $this->db->where("user_email", $data["user_email"]);
      
      return $this->db->count_all_results("_user");
   }
   
   function isAdmin($type)
   {
      return $type == "99" || $type == 98 || $type == 97;
   }
   
   function canModify($type)
   {
      return $type == "99" || $type == 98;
   }
   
   function isSuperAdmin($type)
   {
      return $type == "99";
   }
   
   function getLevelName($type)
   {
      switch($type)
      {
         case 1:
            return "Karyawan";
         case 2:
            return "Office Boy";
         case 3:
            return "Driver";
         case 4:
            return "Kurir";
        case 5:
                return "Resepsionis";
         case 97:
            return "GA Staff";
         case 99:
            return "Administrator";
      }
      
      return "GA Admin";
   }
   
   function getStatusName($status)
   {
      switch($status)
      {
         case 1:
            return "New";
         case 2:
            return "Approved";
         case 3:
            return "Disabled";
      }
      
      return "";
   }
}
