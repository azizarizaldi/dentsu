<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_model extends CI_Model {

   public function get($orderBy="vendor_name", $dir="asc", $limit=0, $offset=0, $search="",$id_category="")
   {
      $this->db->select("vendor.* , setting_category.name as category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("vendor_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = vendor.id_category","inner");
      $q =  $this->db->get("vendor");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("vendor_id", $id);
      $q =  $this->db->get("vendor");
      
      return $q->row();
   }
   
   public function getCount($search,$id_category)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("vendor_name", $search);
      }
      if ($id_category) {
         $this->db->where("id_category", $id_category);
      }
      $this->db->join("setting_category","setting_category.id = vendor.id_category","inner");
      return $this->db->count_all_results("vendor");
   }

   public function insert($data)
   {
        $this->db->insert("vendor", $data);
   }
   
   public function update($data)
   {
      $this->db->where("vendor_id", $data["vendor_id"]);
        $this->db->update("vendor", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("vendor_id", $id);
        $this->db->delete("vendor");
   }
   
}
