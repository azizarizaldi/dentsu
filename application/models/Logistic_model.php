<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logistic_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("logistic_company", $search);
         //$this->db->or_like("logistic_name", $search);
         $this->db->or_like("logistic_cp", $search);
      }
      $q =  $this->db->get("logistic");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("logistic_id", $id);
      $q =  $this->db->get("logistic");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->or_like("logistic_company", $search);
         $this->db->or_like("logistic_name", $search);
         $this->db->or_like("logistic_cp", $search);
      }
      return $this->db->count_all_results("logistic");
   }

   public function insert($data)
   {
        $this->db->insert("logistic", $data);
   }
   
   public function update($data)
   {
      $this->db->where("logistic_id", $data["logistic_id"]);
        $this->db->update("logistic", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("logistic_id", $id);
        $this->db->delete("logistic");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["logistic_id"]))
      {
         $this->db->where("logistic_id <>", $data["logistic_id"]);
      }
      $this->db->where("logistic_company", $data["logistic_company"]);
      $this->db->where("logistic_branch", $data["logistic_branch"]);
      return $this->db->count_all_results("logistic");
   }
   
   function getCompanies()
   {
      $this->db->distinct();
      $this->db->select("logistic_company");
      $this->db->order_by("logistic_company", "asc");
      $q = $this->db->get("logistic");
      
      return $q->result();
   }
}
