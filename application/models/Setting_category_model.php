<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_category_model extends CI_Model {

   public function get($orderBy="name", $dir="asc", $limit=0, $offset=0, $search="")
   {
      $this->db->select("id,name,created_at , if(type_category = '1','Vendor',if(type_category = '2','Contract & Company Doc.',if(type_category = '3','Procedure',if(type_category = '4','Employee Position','Contact')))) as type_category");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search) {
         $this->db->like("name", $search);
      }
      $q =  $this->db->get("setting_category");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("id", $id);
      $q =  $this->db->get("setting_category");
      
      return $q->row();
   }
   public function getCategoryByTipe($tipe)
   {
      $this->db->where("type_category", $tipe);
      $q =  $this->db->get("setting_category");
      
      return $q->result();
   }
   
   public function getCount($search)
   {
      $search = trim($search);
      if ($search) {
         $this->db->like("name", $search);
      }
      return $this->db->count_all_results("setting_category");
   }

   public function insert($data)
   {
        $this->db->insert("setting_category", $data);
   }
   
   public function update($data)
   {
      $this->db->where("id", $data["id"]);
        $this->db->update("setting_category", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("id", $id);
        $this->db->delete("setting_category");
   }
   
}
