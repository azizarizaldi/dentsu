<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor_report_Model extends CI_Model {

	public function get($orderBy, $dir, $limit=0, $offset=0, $search="",$range_date,$brand_unit_id) {
		
		$this->db->select("DATE_FORMAT(order_created_date, '%d/%m/%Y') as createdDate, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime,
		IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status,
		, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
		
		$this->db->order_by($orderBy, $dir);
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		}
		
		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}
		
		$this->db->select("date(order_created_date) as order_date, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime, order_status, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
		$this->db->where("order_type", 7);
		$this->db->join("_user visitor", "visitor.user_id = order_created_by", "left");
		$this->db->join("_user visiting", "visiting.user_id = order_pickup_by", "left");
		$this->db->join("departement", "departement.departement_id = visiting.user_departement", "left");
		$q = $this->db->get("order");

		return $q->result();
	}
      
	public function getCount($search,$range_date,$brand_unit_id) {
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		}		
		
		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}
		
		$this->db->select("DATE_FORMAT(order_created_date, '%d/%m/%Y') as createdDate, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime,
		IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS order_status,		
		, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
		$this->db->where("order_type", 7);
		$this->db->join("_user visitor", "visitor.user_id = order_created_by", "left");
		$this->db->join("_user visiting", "visiting.user_id = order_pickup_by", "left");
		$this->db->join("departement", "departement.departement_id = visiting.user_departement", "left");
		$q = $this->db->get("order");

		return $q->num_rows();	   
	}   
}
