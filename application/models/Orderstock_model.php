<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderStock_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="",$stationary_order_id)
   {
	  $this->db->select("stationary_item.item_id , stationary_item.item_description , stationary_category.category_name , stationary_item.item_stock , stationary_item.item_min_stock , stationary_item.item_unit_measure ,stationary_order_detail.stationary_order_detail_id , stationary_order_detail.qty");
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0) {
         $this->db->limit($limit, $offset);
      }
      
	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(item_description LIKE ${search} OR category_name LIKE ${search})", null, false);
      }
	  
	  $this->db->where("stationary_order_id",$stationary_order_id);

      $this->db->join("stationary_category", "category_id = item_category","inner");
      $this->db->join("stationary_order_detail", "stationary_order_detail.item_id = stationary_item.item_id","inner");
      $q =  $this->db->get("stationary_item");
      
      return $q->result();
   }
      
   public function getCount($search,$stationary_order_id)
   {
      $search = trim($search);
      if ($search) {
         $this->db->or_like("item_description", $search);
         $this->db->or_like("category_name", $search);
      }
	  
	  $this->db->where("stationary_order_id",$stationary_order_id);	  
      $this->db->join("stationary_category", "category_id = item_category","inner");
	  $this->db->join("stationary_order_detail", "stationary_order_detail.item_id = stationary_item.item_id","inner");
	  return $this->db->count_all_results("stationary_item");
   }
   
}
