<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_Model extends CI_Model {

   public function get($orderBy, $dir, $limit=0, $offset=0, $search="")
   {
      $this->db->order_by($orderBy, $dir);
      if ($limit > 0)
      {
         $this->db->limit($limit, $offset);
      }
      $search = trim($search);
      if ($search)
      {
         $this->db->like("company_name", $search);
      }
      $q =  $this->db->get("company");
      
      return $q->result();
   }
   
   public function getById($id)
   {
      $this->db->where("company_id", $id);
      $q =  $this->db->get("company");
      
      return $q->row();
   }

   public function getByName($name)
   {
      $this->db->where("company_name", $name);
      $q =  $this->db->get("company");
      
      return $q->row();
   }
   
   public function getCount($search="")
   {
      $search = trim($search);
      if ($search)
      {
         $this->db->like("company_name", $search);
      }
      return $this->db->count_all_results("company");
   }

   public function insert($data)
   {
        $this->db->insert("company", $data);
   }
   
   public function update($data)
   {
      $this->db->where("company_id", $data["company_id"]);
        $this->db->update("company", $data);
   }
   
   public function delete($id)
   {
      $this->db->where("company_id", $id);
        $this->db->delete("company");
   }
   
   public function isAlreadyExist($data)
   {
      if (isset($data["company_id"]))
      {
         $this->db->where("company_id <>", $data["company_id"]);
      }
      $this->db->where("company_name", $data["company_name"]);
      return $this->db->count_all_results("company");
   }
}
