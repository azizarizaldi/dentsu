<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dateYYYYMMDDToString'))
{
    function dateYYYYMMDDToString($var)
    {
        $y = $var/10000;
        $m = ($var%10000)/100;
        $d = ($var%10000)%100;
        
        return sprintf("%d-%02d-%02d", $y, $m, $d);
    }
}

if ( ! function_exists('mysqlDateToInt'))
{
    function mysqlDateToInt($var)
    {
        $arr = explode(" ", $var);

        $hours = explode(":", $arr[1]);
        $dates = explode("-", $arr[0]);
        
        return mktime($hours[0], $hours[1], 0, $dates[1], $dates[2], $dates[0]);
    }
}

if ( ! function_exists('dateDMYHItoInt'))
{
    function dateDMYHItoInt($date)
    {
        $arr = explode(" ", $date);

        if (count($arr) != 2) return 0;
        
        $dates = explode("/", $arr[0]);
        if (count($dates) != 3) return 0;
        
        $hours = explode(":", $arr[1]);
        if (Count($hours) != 2) return 0;
        
        return mktime($hours[0], $hours[1], 0, $dates[1], $dates[0], $dates[2]);
        
    }
}

if ( ! function_exists('greetingTime'))
{
    function greetingTime()
    {
        $h = date("H");

        if ($h < 9) return "Pagi";
        if ($h < 15) return "Siang";
        if ($h < 18) return "Sore";

        return "Malam";
    }
}

if ( ! function_exists('fcm'))
{
    function fcm($token, $title, $body, $photo="")
    {
        
        $msg = array
          (
		'body' 	=> $body,
		'title'	=> $title
          );
          
          if ($photo)
          {
              $msg['photo'] = $photo;
          }
	$fields = array
			(
				'registration_ids'		=> $token,
				'data'	=> $msg
			);
	
	
	        $headers = array
			(
				'Authorization: key=AIzaSyDRkhobt5eJCvjLX4HJggavtuDnEVGhTG0',
				'Content-Type: application/json'
			);
        
        $ch = curl_init();

		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		//
		curl_setopt($ch,CURLOPT_POST, true );
		//curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = @curl_exec($ch );
		curl_close( $ch );
		
		return $result;
    }
}

if ( ! function_exists('kirimEmail'))
{
    function kirimEmail($to, $subject, $message, $cc = array(), $from="owner@adilahsoft.com", $fromName="Universe Admin")
    {
        $CI =& get_instance();
        
        //$config['protocol'] = 'sendmail';
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        //$config['wordwrap'] = TRUE;
        $config['mailtype'] = "html";
        //$config['useragent'] = "X-Mailer: PHP". phpversion();

        $CI->email->initialize($config);
        
        $CI->email->from($from, $fromName);
        $CI->email->to($to);
        if (count($cc))
        {
            $CI->email->cc($cc);
        }
        $CI->email->bcc('owner@adilahsoft.com');

        $CI->email->subject($subject);
        $CI->email->message($message);

        $CI->email->send();
    }
}

if(!function_exists('tgl')){
		
		function tgl($tgl,$type="01")
		{
			if(substr($tgl,11,8) == '00:00:00' or substr($tgl,11,8) == '')
			{
				$jam='';
			}
			else
			{
				$jam=substr($tgl,11,8);
			}
			
			if(substr($tgl,0,10) == '0000-00-00' or substr($tgl,0,10) == '')
			{
				return "-";
			}
			else
			{
				$bulan = array ('01'=>'Januari', // array bulan konversi
				'02'=>'Februari',
				'03'=>'Maret',
				'04'=>'April',
				'05'=>'Mei',
				'06'=>'Juni',
				'07'=>'Juli',
				'08'=>'Agustus',
				'09'=>'September',
				'10'=>'Oktober',
				'11'=>'November',
				'12'=>'Desember'
				);
				
				$hari=array('Sunday'=>'Minggu','Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jumat','Saturday'=>'Sabtu');
				
				$date=date('l-d-m-Y',strtotime($tgl));
				$tanggal=explode('-',$date);
				
				if($type == "01")
				{
					return $hari[$tanggal[0]]." , ".$tanggal[1]." ".$bulan[$tanggal[2]]." ".$tanggal[3]." ".$jam;
				}
				
				if($type == "06")
				{
					return $tanggal[1]." ".$bulan[$tanggal[2]]." ".$tanggal[3];
				}
				
				if($type == "02")
				{
					return $tanggal[1]." ".$bulan[$tanggal[2]]." ".$tanggal[3]." ".$jam;
				}
				
				if($type == "03")
				{
					return $tanggal[1]." ".substr($bulan[$tanggal[2]],0,3)." ".$tanggal[3]." ".$jam;
				}
				
				if($type == "04")
				{
					return $tanggal[1]."/".$tanggal[2]."/".$tanggal[3];
				}
				
				if($type == "05")
				{
					$tanggal=explode('/',$tgl);
					$date=date('Y-m-d',mktime(0,0,0,$tanggal[1],$tanggal[0],$tanggal[2]));
					return $date;
				}
			}
		}
		
	}