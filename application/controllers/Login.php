<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/login');
	}
	
	public function loginAdmin()
	{
		$this->auth("admin");
	}
	
	public function authExit()
	{
	    $headers = $this->input->request_headers();
	    if (! isset($headers['Useremail']) || ! isset($headers['Usertoken']))
	    {
	         $json = new stdClass();
	     
	        $json->status = false;
		    $json->message = "success";
		 
		    echo json_encode($json);
		 
	        return;
	    }
	    
	     $this->load->model("user_model");
	     $row = $this->user_model->logout($headers['Useremail'], $headers['Usertoken']);
	     
	     $json = new stdClass();
	     
	     $json->status = true;
		 $json->message = "success";
		 
		 echo json_encode($json);
	}
	
	public function authAuto()
	{
	    $json = new stdClass();
	    
	    $headers = $this->input->request_headers();
	    if (! isset($headers['Useremail']) || ! isset($headers['Usertoken']))
	    {
			$json->status = false;
			$json->message = "Session is expired";
	    }
	    else
	    {
	        $this->load->model("user_model");
	        $row = $this->user_model->checkToken($headers['Useremail'], $headers['Usertoken']);
	        
	        if (! $row)
	        {
	            $json->status = false;
			    $json->message = "Session is expired";
	        }
	        else
	        {
	            $data['row'] = $row;
				$row->user_token = $headers['Usertoken']; //uniqid();
				
				$this->user_model->updateToken($row->user_id, $row->user_token);
				
				$this->session->set_userdata($data);
				
				$json->status = true;
				$json->message = "success";
				$json->session = $row;
	        }
	    }
	    
	    echo json_encode($json);
	}
	
	public function auth($type="")
	{
		$posts = $this->input->post();
		
		if (! isset($posts['username']) || ! $posts['username'] || ! isset($posts['pass']) || ! $posts['pass'])
		{
			return;
		}
		
		$this->load->model("user_model");
		
		$json = new stdClass();
		
		$row = $this->user_model->getByLogin($posts['username'], $posts['pass']);
		if (! $row)
		{
			$json->status = false;
			$json->message = "Invalid username or password";
		}
		else
		{
			$success = true;

			if ($type == "admin")
			{
				if (! $this->user_model->isAdmin($row->user_type))
				{
					$success = false;

					$json->status = false;
					$json->message = "Permission Denied";
				}
			}
			
			if ($success)
			{
				$data['row'] = $row;
				$row->user_token = uniqid();
				
				$this->user_model->updateToken($row->user_id, $row->user_token, isset($posts['fcm']) ? $posts['fcm'] : "");
				
				$this->session->set_userdata($data);
				
				$json->status = true;
				$json->message = "success";
				$json->session = $row;
			}
		}
		
		echo json_encode($json);
	}
}
