<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Orderstock extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();

		$inIds = isset($gets['inIds']) ? $gets['inIds'] : false;
		$notInIds = isset($gets['notInIds']) ? $gets['notInIds'] : false;
		
			
		if (isset($gets))
		{
			if (isset($gets["order"]))
			{
				$order = $gets["order"];
			}
			if (isset($gets["columns"]))
			{
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value']))
			{
				$search = $gets['search']['value'];
			}
			else
			if (isset($gets['search']))
			{
				$search = $gets['search'];
			}
		}
		//print_r($gets); exit;
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "description")
			{
				$column_order_by = "item_description";
			}
			else
			if ($column["data"] == "stock")
			{
				$column_order_by = "item_stock";
			}
			else
			if ($column["data"] == "minstock")
			{
				$column_order_by = "item_min_stock";
			}
			else
			{
				$column_order_by = $column["data"];
			}
		}
		
		$stationary_order_id = isset($gets["stationary_order_id"]) ? $gets["stationary_order_id"] : 0;	
		
		$this->load->model("orderstock_model");
		$rows = $this->orderstock_model->get(isset($column_order_by) ? $column_order_by : "item_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$stationary_order_id);

		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->description 	= $rows[$i]->item_description;
			$rows[$i]->stock 		= $rows[$i]->item_stock." ".$rows[$i]->item_unit_measure;
			$rows[$i]->minstock 	= $rows[$i]->item_min_stock." ".$rows[$i]->item_unit_measure;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->orderstock_model->getCount(isset($search) ? $search : "",$stationary_order_id);
		$json["recordsFiltered"] = $this->orderstock_model->getCount(isset($search) ? $search : "",$stationary_order_id);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
	function changeQty() {
		$post 						= $this->input->post(null,true);
		$stationary_order_detail_id = $post['stationary_order_detail_id'];
		$qty 						= $post['qty'];
		
		$update['qty']	= $qty;
		$this->db->where("stationary_order_detail_id",$stationary_order_detail_id);
		$this->db->update("stationary_order_detail",$update);
		
		$result['status']		= true;
		$result['message']		= "Data qty change successfully!";

		echo die(json_encode($result));						
	}
	
	function deleteOrder() {
		$post 					= $this->input->post(null,true);
		$stationary_order_id	= $post['stationary_order_id'];
		
		$this->db->where("stationary_order_id",$stationary_order_id);
		$this->db->where("status","draft");
		$getData = $this->db->get("stationary_order")->row();
		
		if($getData) {
			$this->db->where("stationary_order_id",$stationary_order_id);
			$this->db->delete("stationary_order");
			$result['status']		= true;
			$result['message']		= "Order delete successfully!";
		}
		else {
			$result['status']		= false;
			$result['message']		= "Order failed to be deleted!";			
		}

		echo die(json_encode($result));				
	}
	
	function recordOrder() {
		$post 					= $this->input->post(null,true);
		$stationary_order_id	= $post['stationary_order_id'];
		
		$update['status']		= 'hold';
		$this->db->where("stationary_order_id",$stationary_order_id);		
		$this->db->update("stationary_order",$update);
		
		$result['status']		= true;
		$result['message']		= "Order successfully recorded!";
		echo die(json_encode($result));						
	}

	function addTempItem() {
	    $session 	= $this->validLoginApps();
		$post 		= $this->input->post(null,true);
		$checkID 	= $post['checkID'];
		$user_id	= $session->user_id;

		for($i=0;$i<count($checkID);$i++) {
			$qtyNumber 				 = $post['qtyNumber_'.$checkID[$i]];
			if(!empty($qtyNumber)) {
				$data['stationary_item'] = $checkID[$i];
				$data['qty'] 		 	= $qtyNumber;
				$data['created_by']  	= $user_id;
				$this->db->insert("stationary_order_item_temp",$data);
			}
		}

		$result['status']		= true;
		$result['message']		= "Item data successfully added!";
		echo die(json_encode($result));						
	}

	function deleteTempItem() {
		$post 	= $this->input->post(null,true);
		$id		= $post['id'];
		
		$this->db->where("id",$id);
		$this->db->delete("stationary_order_item_temp");
		$result['status']		= true;
		$result['message']		= "This Order item delete successfully!";

		echo die(json_encode($result));				
	}

	function saveNewItem() {
		$session 	= $this->validLoginApps();
		$user_id	= $session->user_id;
		
		$this->db->where("created_by",$user_id);
		$getTempItem = $this->db->get("stationary_order_item_temp")->result();

		$insert['code'] 	 	= "ORD";
		$insert['order_date'] 	= date("Y-m-d H:i:s");
		$insert['status'] 		= "draft";
		$insert['order_by'] 	= $user_id;
		
		$this->db->insert("stationary_order",$insert);
		$idStationary	= $this->db->insert_id();

		foreach($getTempItem as $get) {
			$data['item_id'] 				= $get->stationary_item;
			$data['qty'] 	 				= $get->qty;
			$data['stationary_order_id'] 	= $idStationary;
			$this->db->insert("stationary_order_detail",$data);
		}

		$this->db->where("created_by",$user_id);
		$this->db->delete("stationary_order_item_temp");

		$result['status']		= true;
		$result['message']		= "Item added successfully!";

		echo die(json_encode($result));				

	}
}
