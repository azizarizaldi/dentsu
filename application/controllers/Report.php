<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Report extends Mycontroller {
	public function pdf_stationary_item() {
		$get 		= $this->input->get(null,true);
		$keyword 	= $get['keyword'];
		$categoryID = $get['categoryID'];
		$number 	= $get['number'];
		
		if ($keyword) {
			$keyword = $this->db->escape('%'.$keyword.'%');
			$this->db->where("(item_description LIKE ${keyword} OR category_name LIKE ${keyword})", null, false);
		}
		
		if ($categoryID > 0) {
			$this->db->where("category_id", $categoryID);
		}

		$this->db->order_by("item_description","asc");
		$this->db->join("stationary_category", "category_id = item_category");
		$query =  $this->db->get("stationary_item",$number)->result();
		
		$data['query'] = $query;
		$this->load->view("admin/stationaryitem/pdf",$data);
	}
	
	public function pdf_warning_stock() {
		$get 		= $this->input->get(null,true);
		$keyword 	= $get['keyword'];
		$number 	= $get['number'];
		
		if ($keyword) {
			$keyword = $this->db->escape('%'.$keyword.'%');
			$this->db->where("(item_description LIKE ${keyword} OR category_name LIKE ${keyword})", null, false);
		}
		
		$this->db->where("item_stock < item_min_stock");	  		
		$this->db->order_by("item_description","asc");
		$this->db->join("stationary_category", "category_id = item_category");
		$query =  $this->db->get("stationary_item",$number)->result();
		
		$data['query'] = $query;
		$this->load->view("admin/warningstock/pdf",$data);
	}
	
	public function pdf_order_stock() {
		$get 					= $this->input->get(null,true);
		$search 				= $get['keyword'];
		$number 				= $get['number'];
		$stationary_order_id 	= $get['id'];

		$this->db->select("stationary_item.item_id , stationary_item.item_description , stationary_category.category_name , stationary_item.item_stock , stationary_item.item_min_stock , stationary_item.item_unit_measure ,stationary_order_detail.stationary_order_detail_id , stationary_order_detail.qty");
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search} OR category_name LIKE ${search})", null, false);
		}

		$this->db->where("stationary_order_id",$stationary_order_id);
		$this->db->join("stationary_category", "category_id = item_category","inner");
		$this->db->join("stationary_order_detail", "stationary_order_detail.item_id = stationary_item.item_id","inner");
		$query =  $this->db->get("stationary_item",$number)->result();

		$this->db->select("stationary_order_id , code ,date(order_date) as order_date");		
		$this->db->where("stationary_order_id",$stationary_order_id);
		$data['stationary_order'] = $this->db->get('stationary_order')->row();
		$data['query'] = $query;
		$this->load->view("admin/orderstock/pdf",$data);		
	}
	
	public function pdf_history_stock() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
		}
		$this->db->select("DATE(order_date) AS order_date,code,stationary_order.stationary_order_id,DATE(receive_date) AS receive_date,GROUP_CONCAT(CONCAT(stationary_item.item_description,' - ',stationary_order_detail.qty,' ',stationary_item.item_unit_measure)) AS item_description");
		$this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","inner");
		$this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
		$this->db->group_by("stationary_order_detail.stationary_order_id");
		$data['query']		=  $this->db->get("stationary_order",$number)->result();
		$data['firstDate']	= $firstDate;
		$data['lastDate']	= $lastDate;
		$this->load->view("admin/historyorder/pdf_all",$data);				
	}
	
	public function pdf_history_stock_detail() {
		$get 					= $this->input->get(null,true);
		$stationary_order_id 	= $get['stationary_order_id'];
		
		$this->db->select("stationary_order.* , date(stationary_order.order_date) as order_date");
		$this->db->where("stationary_order_id",$stationary_order_id);
		$stationary_order		= $this->db->get("stationary_order")->row();
		$this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","inner");
		$this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
		$this->db->where("stationary_order_detail.stationary_order_id",$stationary_order_id);
		$data['query']				=  $this->db->get("stationary_order")->result();
		$data['stationary_order']	=  $stationary_order;
		$this->load->view("admin/historyorder/pdf_detail",$data);				
	}
	
	public function pdf_stationary_by_category() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$item_category 			= $get['item_category'];
		$departement_id 		= $get['departement_id'];
		$brand_unit_id 			= $get['brand_unit_id'];
		$company_id 			= $get['company_id'];
		
		$this->db->select("
			order.order_id ,
			order.order_created_date as order_date,
			stationary_item.item_description ,
			order_stationary.order_stationary_qty as qty,
			_user.user_name ,
			brand_unit.brand_unit_name,
			departement.departement_name		
		");
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
		}
			
		if($item_category) {
			$this->db->where("item_category",$item_category);
		}
		
		if($departement_id) {
			$this->db->where("user_departement",$departement_id);
		}
		
		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}
		
		if($company_id) {
			$this->db->where("brand_unit_company",$company_id);
		}		
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		}
		
		$this->db->join("order_stationary","order_stationary.order_stationary_order = order.order_id","inner");
		$this->db->join("stationary_item","stationary_item.item_id = order_stationary.order_stationary_item","inner");
		$this->db->join("_user","_user.user_id = order.order_created_by","inner");
		$this->db->join("departement","_user.user_departement = departement.departement_id","left");
		$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
		
		$data['query'] 			=  $this->db->get("order")->result();		
		$data['firstDate']		= $firstDate;
		$data['lastDate']		= $lastDate;	

		$this->db->where("category_id",$item_category);
		$data['item_category']	= $this->db->get('stationary_category')->row();	

		$this->db->where("departement_id",$departement_id);
		$data['departement']	= $this->db->get('departement')->row();

		$this->db->where("brand_unit_id",$brand_unit_id);
		$data['brand_unit']	= $this->db->get('brand_unit')->row();

		$this->db->where("company_id",$company_id);
		$data['company']	= $this->db->get('company')->row();
		$this->load->view("admin/report_stationary/by_category/pdf",$data);					
	}
	
	public function pdf_inventory_report() {
		$get 								= $this->input->get(null,true);		
		$search 						= $get['keyword'];
		$number 						= $get['number'];

		$this->db->select("
			stationary_item.item_description,
			stationary_item.item_stock , 
			stationary_item.item_min_stock ,
			stationary_item.item_ideal_stock , 
			stationary_item.item_price ,
			(stationary_item.item_price*stationary_item.item_stock) AS total_value,
			stationary_category.category_name AS category
		");
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search})", null, false);
		}

		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$data['query'] 			=  $this->db->get("stationary_item",$number)->result();		

		$data['total_value'] = "";
		foreach($data['query'] as $getData) {
				$data['total_value'] = $data['total_value']+$getData->total_value;
		}
		$this->load->view("admin/inventory_report/pdf",$data);					
	}

	public function pdf_low_Level_report() {
		$get 								= $this->input->get(null,true);		
		$search 						= $get['keyword'];
		$number 						= $get['number'];

		$this->db->select("
			stationary_item.item_description,
			stationary_item.item_stock , 
			stationary_item.item_min_stock ,
			stationary_item.item_ideal_stock , 
			stationary_item.item_price ,
			(stationary_item.item_price*stationary_item.item_stock) AS total_value	,
			stationary_category.category_name AS category
		");
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search}", null, false);
		}
		
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$this->db->where("stationary_item.item_stock < stationary_item.item_min_stock");
		$data['query'] 			=  $this->db->get("stationary_item")->result();		
		$this->load->view("admin/low_level_stock/pdf",$data);					
	}
	
	public function pdf_report_meeting_room() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$order_ref 				= $get['order_ref'];
		
		  $this->db->select("
			  order.order_id,
			  DATE(order_created_date) AS order_date,
			  _user.user_name,
			  meeting_room.meeting_room_name ,
			  IF(order.order_sender_latitude = '2','Internal',IF(order.order_sender_latitude = '1','Vendor',CONCAT('Client - ',order.order_receiver_name))) AS meeting_with,
			  TIME(order_start_date) AS start_date ,
			  TIME(order_end_date) AS end_date,
			  IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status_order	  
		  ");

		  $search = trim($search);
		  if ($search) {
			 $search = $this->db->escape('%'.$search.'%');
			 $this->db->where("(meeting_room_name LIKE ${search} OR user_name LIKE ${search})", null, false);
		  }
		  		  
		  if($order_ref) {
			  $this->db->where("order_ref",$order_ref);
		  }
		  
		  if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);
		
			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);
			
			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
			
			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		  }
		  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
		  $this->db->join("meeting_room","meeting_room.meeting_room_id = order.order_ref","inner");
		  $data['query'] =  $this->db->get("order",$number)->result();
		
		$data['firstDate']		= $firstDate;
		$data['lastDate']		= $lastDate;	
		$data['order_ref']		= $order_ref;	
		$this->load->view("admin/report_meeting_room/pdf",$data);					
	}
	
	public function pdf_report_operational_car() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$order_ref 				= $get['order_ref'];
		
		
		$this->db->select("
		order.order_id,
		DATE(order_created_date) AS order_date,
		_user.user_name,
		car.car_brand , 
		car.car_model , 
		order.order_sender_address AS pickup,  
		order.order_receiver_address AS destination,
		TIME(order_start_date) AS start_date,
		TIME(order_end_date) AS end_date,
		IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status_order	  
	  ");

 	  $search = trim($search);
      if ($search) {
         $search = $this->db->escape('%'.$search.'%');
         $this->db->where("(meeting_room_name LIKE ${search} OR user_name LIKE ${search})", null, false);
      }
	  	  
	  if($order_ref) {
		  $this->db->where("order_ref",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);
	
		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->where("order_type","4");
	  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
	  $this->db->join("car","car.car_id = order.order_ref","inner");
	  $data['query'] =  $this->db->get("order",$number)->result();
		
		$data['firstDate']		= $firstDate;
		$data['lastDate']		= $lastDate;	
		$data['order_ref']		= $order_ref;	
		$this->load->view("admin/report_operational_car/pdf",$data);							
	}
	
	function pdf_report_internal_courier() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$order_ref 				= $get['order_ref'];

	  $this->db->select("
		order_id,
		DATE(order_created_date) AS order_date,
		_user.user_name,
		order.order_photo AS item,
		order_name,
		order_note,
		order_receiver_name,
		order_receiver_phone,
		order_receiver_address,
		order_receiver_address_note,
		order_priority.priority_name,
		courier.user_name AS courier,
		TIME(order_received_date) AS rec_date,
		TIME(order_pickup_date) AS pick_date,
		TIME(order_courier_date) AS found_date		  
	  ");

	  $search = trim($search);
	  if ($search) {
		 $search = $this->db->escape('%'.$search.'%');
		 $this->db->where("(order_name LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
	  }
		  
	  if($order_ref) {
		  $this->db->where("courier.user_id",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);

		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->where("order_type","1");
	  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
	  $this->db->join("_user courier","courier.user_id = order_courier_by ","left");
	  $this->db->join("order_priority","order_priority.`priority_id` = order.order_priority ","left");
	  $q =  $this->db->get("order",$number);
		$data['query'] =  $q->result();

		$data['firstDate']		= $firstDate;
		$data['lastDate']			= $lastDate;	
		$data['order_ref']		= $order_ref;	
		$this->load->view("admin/report_internal_courier/pdf",$data);							
	
	}
	
	public function pdf_report_external_courier() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$order_ref 				= $get['order_ref'];

	  $search = trim($search);
	  if ($search) {
		 $search = $this->db->escape('%'.$search.'%');
		 $this->db->where("(order_name LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
	  }
		  
	  if($order_ref) {
		  $this->db->where("order_status",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);

		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
		$this->db->where("order_type","2");
		$this->db->join("_user reject", "reject.user_id = order_rejected_by", "left");
		$this->db->join("_user sender", "sender.user_id = order_created_by", "left");
		$this->db->join("_user courier", "courier.user_id = order_courier_by", "left");
		$this->db->join("_user pickup", "pickup.user_id = order_pickup_by", "left");
		$this->db->join("departement", "departement_id = sender.user_departement", "left");
		$this->db->join("brand_unit", "departement_brand_unit = brand_unit_id", "left");
		$this->db->join("company", "brand_unit_company = company_id", "left");
		$this->db->join("order_priority", "priority_id = order_priority", "left");
		$this->db->join("courier_package_type", "package_type_id = order_courier_by", "left");
		$this->db->join("car", "car_id = order_ref", "left");
		$this->db->join("meeting_room", "meeting_room_id = order_ref", "left");
		$this->db->join("logistic", "package_type_company = logistic_id", "left");
		$this->db->select("*, sender.user_name as sender_name, sender.user_phone as sender_phone, sender.user_photo as sender_photo, sender.user_visitor_company as sender_visitor_company, courier.user_name as courier_name, courier.user_photo as courier_photo, courier.user_phone as courier_phone, reject.user_name as rejected_by, pickup.user_name as pickup_by, CONCAT(car_brand, ' ', car_model) as car_name_detail
											, DATE_FORMAT(order_created_date, '%d.%m.%Y %H:%i') as order_created_date_fmt
											, DATE_FORMAT(order_courier_date, '%d.%m.%Y %H:%i') as order_courier_date_fmt
											, DATE_FORMAT(order_pickup_date, '%d.%m.%Y %H:%i') as order_pickup_date_fmt
											, DATE_FORMAT(order_received_date, '%d.%m.%Y %H:%i') as order_received_date_fmt
											, DATE_FORMAT(order_rejected_date, '%d/%m/%Y %H:%i') as order_rejected_date_fmt
											, DATE_FORMAT(order_start_date, '%d/%m/%Y %H:%i') as order_start_date_fmt
											, DATE_FORMAT(order_start_date, '%d/%m/%Y') as order_start_date_only
											, DATE_FORMAT(order_end_date, '%d/%m/%Y %H:%i') as order_end_date_fmt
											, DATE_FORMAT(order_end_date, '%H:%i') as order_end_time_fmt
											, CONCAT(DATE_FORMAT(order_start_date, '%h:%i %p'), ' - ', DATE_FORMAT(order_end_date, '%h:%i %p')) as order_range_time
											, DATEDIFF(order_end_date, order_start_date) as order_date_length
											,	DATE(order_created_date) AS order_date,
											order.order_photo AS item,
											order_id,
											IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Received',IF(order.order_status = '5','Rejected','-')))))  AS status_order");
		$q =  $this->db->get("order",$number)->result();	
		for($i=0; $i < count($q); $i++)
		{
			$q[$i]->status  = $this->getStatus($q[$i]);				
		}

		$data['query'] 				=  $q;
		$data['firstDate']		= $firstDate;
		$data['lastDate']			= $lastDate;	
		$this->load->view("admin/report_external_courier/pdf",$data);							
		
	}
	
	public function pdf_report_document_in() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$order_ref 				= $get['order_ref'];
		

	  $this->db->select("
		order_id,
		DATE(order_created_date) AS order_date,
		_user.user_name,
		order.order_photo AS item,
		order_name,
		order_note,
		order_receiver_name,
		order_receiver_phone,
		order_receiver_address,
		order_receiver_address_note,
		courier.user_name AS courier,
		TIME(order_received_date) AS rec_date,
		TIME(order_pickup_date) AS pick_date,
		TIME(order_courier_date) AS found_date,
		order.order_received_date ,
		order.order_rejected_date ,
	    IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status_order		
	  ");

	  $search = trim($search);
	  if ($search) {
		 $search = $this->db->escape('%'.$search.'%');
		 $this->db->where("(order_name LIKE ${search} OR _user.user_name LIKE ${search})", null, false);
	  }
		  
	  if($order_ref) {
		  $this->db->where("order_status",$order_ref);
	  }
	  
	  if($range_date) {
		$string 		= explode('-',$range_date);

		$date1 			= explode('/',$string[0]);
		$date2 			= explode('/',$string[1]);

		$year			= str_replace(' ','',$date1[2]);
		$month			= str_replace(' ','',$date1[0]);
		$day			= str_replace(' ','',$date1[1]);

		$lastYear		= str_replace(' ','',$date2[2]);
		$lastMonth		= str_replace(' ','',$date2[0]);
		$lastDay		= str_replace(' ','',$date2[1]);
		
		$firstDate		= $year.'-'.$month.'-'.$day;
		$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		
		$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
	  }
	  $this->db->where("order_type","3");
	  $this->db->join("_user","_user.user_id = order.order_created_by","inner");
	  $this->db->join("_user courier","courier.user_id = order_courier_by ","left");
	  $q =  $this->db->get("order",$number);
		$data['query'] =  $q->result();

		$data['firstDate']		= $firstDate;
		$data['lastDate']		= $lastDate;	
		$this->load->view("admin/report_document_in/pdf",$data);							
		
	}
	
	public function pdf_visitor_report() {
		$get 					= $this->input->get(null,true);
		
		$search 				= $get['keyword'];
		$range_date 			= $get['range_date'];
		$number 				= $get['number'];
		$brand_unit_id 			= $get['brand_unit_id'];		
		
		$this->db->select("DATE_FORMAT(order_created_date, '%d/%m/%Y') as createdDate, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime,
		IF(order.order_status = '1','New',IF(order.order_status = '2','Approved',IF(order.order_status = '3','Pickup',IF(order.order_status = '4','Done',IF(order.order_status = '5','Rejected','-')))))  AS status,
		, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
				
		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_created_date) between '".$firstDate."' and '".$lastDate."' ");
		}
		
		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}
		
		$this->db->select("date(order_created_date) as order_date, visitor.user_name visitorName, visiting.user_name  visitingName, DATE_FORMAT(order_created_date, '%H:%i') as signInTime, order_status, order_receiver_note, order_rejected_note, DATE_FORMAT(order_end_date, '%H:%i') as signOutTime, order_end_date, DATE_FORMAT(order_end_date, '%d/%m/%Y') as signOutDate, order_id, order_created_date");
		$this->db->where("order_type", 7);
		$this->db->join("_user visitor", "visitor.user_id = order_created_by", "left");
		$this->db->join("_user visiting", "visiting.user_id = order_pickup_by", "left");
		$this->db->join("departement", "departement.departement_id = visiting.user_departement", "left");
		$data['query'] = $this->db->get("order",$number)->result();		
		$data['firstDate']		= $firstDate;
		$data['lastDate']		= $lastDate;	
		$this->load->view("admin/visitor_report/pdf",$data);							
	}

	function pdf_vendor() {
		$get 			= $this->input->get(null,true);
		
		$search 		= $get['keyword'];
		$number 		= $get['number'];
		$id_category 	= $get['id_category'];

		$this->db->select("vendor.* , setting_category.name as category");
		$search = trim($search);
		if ($search) {
		   $this->db->like("vendor_name", $search);
		}
		if ($id_category) {
		   $this->db->where("id_category", $id_category);
		}
		$this->db->join("setting_category","setting_category.id = vendor.id_category","inner");
		$data['query'] =  $this->db->get("vendor",$number)->result();
		$this->load->view("admin/vendor/pdf",$data);							
	  }

	  function pdf_contact() {
		$get 			= $this->input->get(null,true);
		
		$search 		= $get['keyword'];
		$number 		= $get['number'];
		$id_category 	= $get['id_category'];

		$this->db->select("contact.* , setting_category.name as category");
		$search = trim($search);
		if ($search) {
		   $this->db->like("contact_name", $search);
		}
		if ($id_category) {
		   $this->db->where("id_category", $id_category);
		}
		$this->db->join("setting_category","setting_category.id = contact.id_category","inner");
		$data['query'] =  $this->db->get("contact",$number)->result();
		$this->load->view("admin/contact/pdf",$data);							
	  }

	  function pdf_employee() {
		$get 			= $this->input->get(null,true);
		
		$search 		= $get['keyword'];
		$number 		= $get['number'];
		$id_category 	= $get['id_category'];

		$this->db->select("employee.* , setting_category.name as category");
		$search = trim($search);
		if ($search) {
		   $this->db->like("employee_name", $search);
		}
		if ($id_category) {
		   $this->db->where("id_category", $id_category);
		}
		$this->db->join("setting_category","setting_category.id = employee.id_category","inner");
		$data['query'] =  $this->db->get("employee",$number)->result();
		$this->load->view("admin/employee/pdf",$data);							
	  }


		protected function getStatus($row)
		{
			$this->load->model("order_model");
			switch($row->order_status)
				{
					case 2:
						return "New";
					case 3:
						return sprintf("%s", $this->pickUpInfo($row));
					case 4:
						return sprintf("%s<br />Airwaybill No.: %s<br />Delivery Fee: %s<br />Note: %s<br /><br />%s", $this->received($row), $row->order_note, number_format($row->order_receiver_latitude, 0, ",", "."), $row->order_receiver_note, $this->pickUpInfo($row));
					case 5:
						return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
													, $this->order_model->getStatusDesc($row->order_status)
													, $row->rejected_by, $row->order_rejected_date_fmt
													, $row->order_rejected_note);					
					default:
						return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));
	
				}
		}
		
		protected function received($row)
		{
			$this->load->model("order_model");
			return sprintf("%s<br />Date: %s", $this->order_model->getStatusDesc(4), $row->order_received_date_fmt);
		}

		protected function pickUpInfo($row)
		{
			$this->load->model("order_model");
			return sprintf("%s<br />Date: %s", $this->order_model->getStatusDesc(3), $row->order_pickup_date_fmt);
		}
	

		private function courierInfo($row)
		{
			$this->load->model("order_model");
			return sprintf("%s<br />Courier Name: %s<br >Date: %s", $this->order_model->getStatusDesc(2), $row->courier_name, $row->order_courier_date_fmt);
		}
		
		public function pdf_stationary_expense() {
			$get 						= $this->input->get(null,true);
			$brand_unit_id 	= $get["brand_unit_id"];
			$company_id 		= $get["company_id"];
			$range_date 		= $get["range_date"];
			$search 				= $get["keyword"];
			$range_date 		= $get["range_date"];
			$number 				= $get['number'];
		
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''))");

			$this->db->select("
				stationary_item.item_description , stationary_category.category_name , stationary_item.item_price , stationary_order_detail.qty , SUM(stationary_order_detail.qty) AS jumlah_qty
			");

			$search = trim($search);
			if ($search) {
				$search = $this->db->escape('%'.$search.'%');
				$this->db->where("(item_description LIKE ${search})", null, false);
			}
	
			if($brand_unit_id) {
				$this->db->where("departement_brand_unit",$brand_unit_id);
			}
	
			if($company_id) {
				$this->db->where("brand_unit_company",$company_id);
			}
	
			if($range_date) {
				$string 		= explode('-',$range_date);
	
				$date1 			= explode('/',$string[0]);
				$date2 			= explode('/',$string[1]);
	
				$year			= str_replace(' ','',$date1[2]);
				$month			= str_replace(' ','',$date1[0]);
				$day			= str_replace(' ','',$date1[1]);
	
				$lastYear		= str_replace(' ','',$date2[2]);
				$lastMonth		= str_replace(' ','',$date2[0]);
				$lastDay		= str_replace(' ','',$date2[1]);
	
				$firstDate		= $year.'-'.$month.'-'.$day;
				$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
	
				$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
			}
	
			$this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","left");
			$this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
			$this->db->join("_user","_user.user_id = stationary_order.order_by","inner");
			$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
			$this->db->join("departement","departement.departement_id = _user.user_departement","left");
			$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
			$this->db->where("stationary_order.status","receive");
			$this->db->group_by("stationary_item.item_id");
			$rows 			=  $this->db->get("stationary_order",$number)->result();
	
			$data['query'] 					= $rows;		
			$data['firstDate']			= $firstDate;
			$data['lastDate']				= $lastDate;	
			$data['brand_unit_id']	= $brand_unit_id;	
			$data['company_id']			= $company_id;	
			$this->load->view("admin/stationary_expense_report/pdf",$data);					
		}
}
