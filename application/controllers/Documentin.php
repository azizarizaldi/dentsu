<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Documentinternal.php';

class DocumentIn extends DocumentInternal {
	protected function getOrderType()
	{
		return 3;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	protected function getCourierColumnContent($row)
	{
		return sprintf("%s", $row->order_receiver_address);
	}
	
	protected function getDocomentColumnContent($row)
	{
		return sprintf("%s<br />Order ID: %05d<br />Name: %s<br /><a href='%s/%s' target='_blank'><img src='%s/%s' width='80' height='80' /></a>", $row->order_created_date_fmt
										  , $row->order_id, $row->order_name, base_url(), $row->order_photo, base_url(), $row->order_photo);
	}
	
	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s", $row->order_receiver_name);
	}
	
	protected function getStatus($row)
	{
		switch($row->order_status)
			{
				case 2:
					return "New";
				case 3:
					return sprintf("%s", $this->pickUpInfo($row));
				case 4:
					return sprintf("%s<br />", $this->received($row));
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}
	
	public function getSender()
	{
	    $this->load->model("order_model");
	    
	    $rows = $this->order_model->getDocumentInSenders();
	    
	    echo json_encode($rows);
	}
	
	public function getCourier()
	{
	    $this->load->model("order_model");
	    
	    $rows = $this->order_model->getDocumentInCourier();
	    
	    echo json_encode($rows);
	}
	
}
