<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Documentinternal.php';

class DocumentExternal extends DocumentInternal {
    public function getLogistic()
    {
        $this->load->model("logistic_model");
        $rows = $this->logistic_model->get("logistic_company", "asc");
        
        echo json_encode($rows);
    }
    
	protected function getOrderType()
	{
		return 2;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	public function getPackageTypeByCompany()
	{
		$gets = $this->input->get();
		
		//if (! isset($gets['company']) || ! $gets['company']) return;
		
		$company = isset($gets['company']) ? $gets['company'] : 0;
		
		$this->load->model("packagetype_model");
		
		$rows = $this->packagetype_model->getByCompany($company);
		
		echo json_encode($rows);
	}
	
	protected function getDocomentColumnContent($row)
	{
		return sprintf("%s<br />Order ID: %05d<br />Item Description: %s<br /><a href='%s/%s' target='_blank'><img src='%s/%s' width='80' height='80' /></a>", $row->order_created_date_fmt
										  , $row->order_id, $row->order_name, base_url(), $row->order_photo, base_url(), $row->order_photo);
	}
	
	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s<br />Phone: %s<br />Address: %s<br />Post Code: %s", $row->order_receiver_name
										  , $row->order_receiver_phone, $row->order_receiver_address
										  , $row->order_receiver_address_note ? $row->order_receiver_address_note : "-");
	}
	
	protected function getStatus($row)
	{
		switch($row->order_status)
			{
				case 2:
					return "New";
				case 3:
					return sprintf("%s", $this->pickUpInfo($row));
				case 4:
					return sprintf("%s<br />Airwaybill No.: %s<br />Delivery Fee: %s<br />Note: %s<br /><br />%s", $this->received($row), $row->order_note, number_format($row->order_receiver_latitude, 0, ",", "."), $row->order_receiver_note, $this->pickUpInfo($row));
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}
	
	protected function pickUpInfo($row)
	{
		return sprintf("Send to Courier<br />Office Boy: %s<br />Date: %s", $row->pickup_by, $row->order_pickup_date_fmt);
	}

	
}
