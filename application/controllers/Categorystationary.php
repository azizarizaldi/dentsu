<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class CategoryStationary extends MyController {
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
				
		$order = isset($gets["order"]) ? $gets["order"] : false;
		$columns = isset($gets["columns"]) ? $gets["columns"] : false;
		$search = isset($gets['search']['value']) ? $gets['search']['value'] : false;
		
		//print_r($gets); exit;
		
		$column = isset($columns[$order[0]["column"]]) ? $columns[$order[0]["column"]] : false;
		
		$this->load->model("categorystationary_model");
		$rows = $this->categorystationary_model->get(isset($column["data"]) ? $column["data"] : "category_id", isset($order[0]["dir"]) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, $search);

		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->picture = sprintf("<a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", base_url(), $rows[$i]->category_pic, base_url(), $rows[$i]->category_pic);
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->categorystationary_model->getCount($search);
		$json["recordsFiltered"] = $this->categorystationary_model->getCount($search);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
