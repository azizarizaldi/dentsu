<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class StationaryItem extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();

		$inIds = isset($gets['inIds']) ? $gets['inIds'] : false;
		$notInIds = isset($gets['notInIds']) ? $gets['notInIds'] : false;
		
			
		if (isset($gets))
		{
			if (isset($gets["order"]))
			{
				$order = $gets["order"];
			}
			if (isset($gets["columns"]))
			{
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value']))
			{
				$search = $gets['search']['value'];
			}
			else
			if (isset($gets['search']))
			{
				$search = $gets['search'];
			}
		}

		$categoryId = isset($gets["categoryId"]) ? $gets["categoryId"] : 0;


		//print_r($gets); exit;
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "description")
			{
				$column_order_by = "item_description";
			}
			else
			if ($column["data"] == "stock")
			{
				$column_order_by = "item_stock";
			}
			else
			if ($column["data"] == "idealstock")
			{
				$column_order_by = "item_ideal_stock";
			}
			else
			if ($column["data"] == "minstock")
			{
				$column_order_by = "item_min_stock";
			}
			else
			{
				$column_order_by = $column["data"];
			}
		}
		
		$this->load->model("stationaryitem_model");
		$rows = $this->stationaryitem_model->get(isset($column_order_by) ? $column_order_by : "item_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "", $inIds, $notInIds, $categoryId);

		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->description = sprintf("%s<br /><a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", $rows[$i]->item_description, base_url(), $rows[$i]->item_picture, base_url(), $rows[$i]->item_picture);
			$rows[$i]->item_price = "Rp. ".number_format($rows[$i]->item_price, 0, '', '.').',- '; 
			$rows[$i]->stock = $rows[$i]->item_stock." ".$rows[$i]->item_unit_measure;
			$rows[$i]->idealstock = $rows[$i]->item_ideal_stock." ".$rows[$i]->item_unit_measure;
			$rows[$i]->minstock = $rows[$i]->item_min_stock." ".$rows[$i]->item_unit_measure;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->stationaryitem_model->getCount(isset($search) ? $search : "", $categoryId);
		$json["recordsFiltered"] = $this->stationaryitem_model->getCount(isset($search) ? $search : "", $categoryId);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
