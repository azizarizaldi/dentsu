<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Report_internal_courier extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets)) {
			if (isset($gets["order"])) {
				$order = $gets["order"];
			}
			if (isset($gets["columns"])) {
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value'])) {
				$search = $gets['search']['value'];
			}
			else if (isset($gets['search'])) {
				$search = $gets['search'];
			}
			if (isset($gets['order_ref'])) {
				$order_ref = $gets['order_ref'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "order_id")
			{
				$column_order_by = "order_id";
			}
			else if ($column["data"] == "order_date") {
				$column_order_by = "order_date";
			}
			else if ($column["data"] == "user_name") {
				$column_order_by = "user_name";
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("Report_internal_courier_model");
		$rows = $this->Report_internal_courier_model->get(isset($column_order_by) ? $column_order_by : "order_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date,$order_ref);
		
		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->order_id 			= $rows[$i]->order_id;
			$rows[$i]->order_date 			= tgl($rows[$i]->order_date,'2');
			$rows[$i]->user_name 			= $rows[$i]->user_name;
			$rows[$i]->item 				= '<img width="80" height="80" src="'.base_url().$rows[$i]->item.'"/>';
			$rows[$i]->order_name 			= $rows[$i]->order_name.'<br/>Note : '.$rows[$i]->order_note;
			$rows[$i]->order_receiver_name 	= "<input type='hidden' id='more_".$rows[$i]->order_id."' value='".$rows[$i]->order_receiver_address."'/>Receiver Name : ".$rows[$i]->order_receiver_name."<br/>Phone : ".$rows[$i]->order_receiver_phone."<br/><a onclick='more(".$rows[$i]->order_id.")' style='cursor:pointer'>View Address...</a>";
			$rows[$i]->priority_name 		= $rows[$i]->priority_name;
			$rows[$i]->courier 				= $rows[$i]->courier;
			$rows[$i]->status 				= "Received (".$rows[$i]->rec_date.")<br/>Courier Pickup : (".$rows[$i]->pick_date.")<br/>Courier Found (".$rows[$i]->found_date.")";
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->Report_internal_courier_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["recordsFiltered"] = $this->Report_internal_courier_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
