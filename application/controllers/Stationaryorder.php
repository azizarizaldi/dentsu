<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Documentinternal.php';

class StationaryOrder extends DocumentInternal {
	public function getCart()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $rows = array();
	        echo json_encode($rows);
	        return;
	    }
	    
		$gets = $this->input->get();

		if (! isset($gets['orderId'])) return;

        $this->load->model("user_model");
        $isAdmin = $this->user_model->isAdmin($session->user_type);

		$this->load->model("order_model");
		$rows = $this->order_model->getCart($gets['orderId'], $isAdmin ? 0 : $session->user_id);

		if (isset($gets['dt']))
		{
			for($i=0; $i < count($rows); $i++)
			{
				$rows[$i]->description = sprintf("%s<br /><a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", $rows[$i]->item_description, base_url(), $rows[$i]->item_picture, base_url(), $rows[$i]->item_picture);
				$rows[$i]->qty = $rows[$i]->order_stationary_qty." ".$rows[$i]->item_unit_measure;

			}

			$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : "";
			$json["recordsTotal"] = count($rows);
			$json["recordsFiltered"] = count($rows);
			$json["data"] = $rows;
			
			echo json_encode($json);

			return;
		}
		
		for($i=0; $i < count($rows); $i++)
		{
		    $rows[$i]->stock = $rows[$i]->item_stock." ".$rows[$i]->item_unit_measure;
		    
		    $rows[$i]->orderShipping = $rows[$i]->order_sender_latitude == 1 ? "Take it by your self" : "Delivery with Office Boy ".$rows[$i]->pickupUserName;
		}
		
		echo json_encode($rows);
	}

	protected function getOrderType()
	{
		return 6;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	
	protected function getStatusName($row)
	{
	    // doc internal
	    if ($row->order_type == 1)
	    {
	        switch($row->order_status)
	        {
	            case 1:
	                return "NEW ORDER";
	            case 2:
	                return "OUT FOR PICKUP";
	            case 3:
	                return "OUT FOR DELIVERY";
	            case 4:
	                return "DELIVERY COMPLETED";
	            case 5:
	                return "REJECTED";
	           case 6:
	               return "PICK UP";
	        }
	    }
	    
	    	    // doc incoming
	    if ($row->order_type == 3)
	    {
	        switch($row->order_status)
	        {
	            case 1:
	                return "INCOMING DOCUMENT";
	            case 2:
	                return "OUT FOR PICKUP";
	            case 3:
	                return "OUT FOR DELIVERY";
	            case 4:
	                return "RECEIVED";
	            case 5:
	                return "REJECTED";
	        }
	    }
	    
	    // booking car
	    if ($row->order_type == 4)
	    {
	        switch($row->order_status)
	        {
	            case 1:
	                return "REQUEST PROCESSING";
	            case 2:
	                return "REQUEST APPROVED";
	            case 3:
	                return "DRIVER FOUND";
	            case 4:
	                return "BOOKING DONE";
	            case 5:
	                return "BOOKING REJECTED";
	           case 6:
	               return "PICK UP";
	        }
	    }
	    
	    // meeting room
	    if ($row->order_type == 5)
	    {
	        switch($row->order_status)
	        {
	            case 1:
	                return "REQUEST PROCESSING";
	            case 2:
	            case 3:
	                return "REQUEST APPROVED";
	            case 4:
	                return "BOOKING DONE";
	            case 5:
	                return "BOOKING REJECTED";
	        }
	    }
	    
	    if ($row->order_type == 7)
	    {
	        switch($row->order_status)
			{
				case 1:
					return "NEW APPOINTMENT";
				case 2:
					return "Processed";
				case 3:
					return "Order on delivery";
				case 4:
					return "DONE";
				case 5:
					return "REJECTED";					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	    }
	    
	    switch($row->order_status)
			{
				case 1:
					return "New Order";
				case 2:
					return "Processed";
				case 3:
					return "Order on delivery";
				case 4:
					return "Delivered";
				case 5:
					return "Rejected";					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}
	
	protected function getStatusColor($row)
	{
	    switch($row->order_status)
			{
				case 1:
					return "#54c332";
				case 2:
					return "#54c332";
				case 3:
					return "#4e9dda";
				case 4:
					return "#1fbf94";
				case 5:
					return "#ff0000";					
				default:
					return "#e6e41532";

			}
	}

	protected function getStatus($row)
	{
		switch($row->order_status)
			{
				case 1:
					return "New";
				case 2:
					return sprintf("Approved<br />Date: %s", $row->order_created_date);
				case 3:
					return sprintf("%s", $this->pickUpInfo($row));
				case 4:
					return sprintf("%s<br /><br />%s", $this->pickUpInfo($row), $this->received($row));
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}

	protected function getCourierColumnContent($row)
	{
		if ($row->order_sender_latitude == 1)
		{
			return "Self Service";
		}

		return "Delivery with Office Boy";
	}

	protected function pickUpInfo($row)
	{
		if ($row->order_sender_latitude == 1)
		{
			return sprintf("Self Service");
		}
		
		return sprintf("Order on delivery %s<br />Date: %s", $row->pickup_by, $row->order_pickup_date_fmt);
	}
}
