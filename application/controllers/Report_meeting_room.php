<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Report_meeting_room extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets)) {
			if (isset($gets["order"])) {
				$order = $gets["order"];
			}
			if (isset($gets["columns"])) {
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value'])) {
				$search = $gets['search']['value'];
			}
			else if (isset($gets['search'])) {
				$search = $gets['search'];
			}
			if (isset($gets['order_ref'])) {
				$order_ref = $gets['order_ref'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "order_id")
			{
				$column_order_by = "order_id";
			}
			else if ($column["data"] == "order_date") {
				$column_order_by = "order_date";
			}
			else if ($column["data"] == "user_name") {
				$column_order_by = "user_name";
			}
			else {
				$column_order_by = $column["meeting_room_name"];
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("Report_meeting_room_model");
		$rows = $this->Report_meeting_room_model->get(isset($column_order_by) ? $column_order_by : "order_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date,$order_ref);
		
		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->order_id 		= $rows[$i]->order_id;
			$rows[$i]->order_date 		= tgl($rows[$i]->order_date,'2');
			$rows[$i]->user_name 		= $rows[$i]->user_name;
			$rows[$i]->room 			= $rows[$i]->meeting_room_name;
			$rows[$i]->meeting 			= $rows[$i]->meeting_with;
			$rows[$i]->periode 			= $rows[$i]->start_date.' - '.$rows[$i]->end_date;
			$rows[$i]->status 			= $rows[$i]->status_order;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->Report_meeting_room_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["recordsFiltered"] = $this->Report_meeting_room_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
