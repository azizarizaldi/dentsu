<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Billboard extends MyController {
    function get()
	{
		$this->load->model("banner_model");
		
		$rows = $this->banner_model->get("billboard_id", "desc");
		
		echo json_encode($rows);
	}
	

    
}
