<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Order.php';

class DocumentInternal extends Order {
	protected function getOrderType()
	{
		return 1;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
	    $createdBy = $this->isAdminApps($session) ? 0 : $session->user_id;
		$gets = $this->input->get();
		
		//$order = $gets["order"];
		$columns 	= isset($gets["columns"]) ? $gets["columns"] : "";
		$search 	= isset($gets['search']['value']) ? $gets['search']['value'] : "";
		$orderTypes = isset($gets['orderTypes']) ? explode(",", $gets['orderTypes']) : false;

        $originalDate 	= isset($gets["orderDate"]) ? $gets["orderDate"] : "";
		
		if($originalDate) {
			$newDate 		= date("Y-m-d", strtotime($originalDate));
		}
		else {
			$newDate = "";
		}
		
		$status 		= isset($gets['status']) ? $gets['status'] : "";
		$arrstatus 		= isset($gets['arrstatus']) ? $gets['arrstatus'] : "";
		$category 		= isset($gets['category']) ? $gets['category'] : 0;
		$meetingRoomID 	= isset($gets['meetingRoomID']) ? $gets['meetingRoomID'] : "";
		$orderDate 		= isset($newDate) ? $newDate : "";
		
		if ($category == 1)
		{
		   $courierId = isset($gets['courierId']) ? $gets['courierId'] : 0; 
		   $carId = 0;
		}
		else
		{
		    $courierId = 0;
		    $carId = isset($gets['carId']) ? $gets['carId'] : 0;
		}
		
		//print_r($gets); exit;
		
		//$column = $columns[$order[0]["column"]-1];
		
		$this->load->model("order_model");
		//$column["data"], $order[0]["dir"], , 
		$rows = $this->order_model->get($this->getOrderType(), isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, $search, $status, $arrstatus, $createdBy, $orderTypes, $carId, $category, $courierId, $orderDate, $meetingRoomID);

		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->document = $this->getDocomentColumnContent($rows[$i]);
			$rows[$i]->sender = sprintf("%s", $rows[$i]->sender_name);
			$rows[$i]->receiver = $this->getReceiverColumnContent($rows[$i]);
			$rows[$i]->courier = $this->getCourierColumnContent($rows[$i]);
			$rows[$i]->status  = $this->getStatus($rows[$i]);	
			$rows[$i]->statusName = $this->getStatusName($rows[$i]);
			$rows[$i]->statusColor = $this->getStatusColor($rows[$i]);
			$rows[$i]->orderType = $this->getOrderType();
			$rows[$i]->serviceName = $this->getServiceName($rows[$i]);
			
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->order_model->getCount($this->getOrderType(), $status, $search, $arrstatus, $createdBy, $orderTypes, $carId, $category, $courierId, $orderDate, $meetingRoomID);
		$json["recordsFiltered"] = $this->order_model->getCount($this->getOrderType(), $status, $search, $arrstatus, $createdBy, $orderTypes, $carId, $category, $courierId, $orderDate, $meetingRoomID);
		$json["data"] = $rows;
		$json['status'] = true;
		$json['start'] = isset($gets["start"]) ? $gets["start"] : 0;
		
		echo json_encode($json);

	}
	
	public function getActiveCount()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
	    //$session = new stdClass();
	    //$session->user_id = 15;
	    
	    $createdBy = $this->isAdminApps($session) ? 0 : $session->user_id;
		$gets = $this->input->get();
				
		//$order = $gets["order"];
		$columns = isset($gets["columns"]) ? $gets["columns"] : "";
		$search = isset($gets['search']['value']) ? $gets['search']['value'] : "";
		$orderTypes = isset($gets['orderTypes']) ? explode(",", $gets['orderTypes']) : false;
		
		$status = isset($gets['status']) ? $gets['status'] : "";
		$arrstatus = isset($gets['arrstatus']) ? $gets['arrstatus'] : "";
		$category = isset($gets['category']) ? $gets['category'] : 0;
		if ($category == 1)
		{
		   $courierId = isset($gets['carId']) ? $gets['carId'] : 0; 
		   $carId = 0;
		}
		else
		{
		    $courierId = 0;
		    $carId = isset($gets['carId']) ? $gets['carId'] : 0;
		}
		
		$this->load->model("order_model");
		$count = $this->order_model->getCount($this->getOrderType(), $status, $search, $arrstatus, $createdBy, $orderTypes, $carId, $category, $courierId);

        $json = new stdClass();
        $json->count = $count;
        
		echo json_encode($json);
	}

    public function getTaskCount()
	{
	    $json = new stdClass();
	    
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json->status = false;
	        echo json_encode($json);
	        return;
	    }
	    
	    $this->load->model("order_model");
	    
	    $json->status = true;
	    $json->active = $this->order_model->getDocumentInternalCount($session->user_id, array(2, 3, 6));
	    $json->history = $this->order_model->getDocumentInternalCount($session->user_id, array(4, 5));
	    
	    echo json_encode($json);
	    
	}
	
	public function getPriorityData()
	{
	    $this->load->model("orderpriority_model");
	    
	    echo json_encode($this->orderpriority_model->get("priority_name", "asc"));
	}
	
	protected function getServiceName($row)
	{
	    switch($row->order_priority)
	    {
	        case 2:
	            return "Pick up @ Client";
	       case 3:
	            return "Full Service";
	    }
	    
	    return "Drop Only";
	}
	
	protected function getCourierColumnContent($row)
	{
		return sprintf("%s %s", $row->package_type_name, $row->logistic_company);
	}
	
	protected function getStatusName($row)
	{
	    switch($row->order_status)
	    {
	        case 1:
	            return "PROCESSING";
	        case 2:
	            return "OUT FOR PICKUP";
	    }
	    return "";
	}
	
	protected function getStatusColor($row)
	{
	    return "";
	}
	
	protected function getStatus($row)
	{
		switch($row->order_status)
			{
				case 2:
					return $this->courierInfo($row);
				case 3:
					return sprintf("%s<br /><br />%s", $this->pickUpInfo($row), $this->courierInfo($row));
				case 4:
					return sprintf("%s<br /><br />%s<br /><br />%s", $this->received($row), $this->pickUpInfo($row), $this->courierInfo($row));
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}
	
	protected function getDocomentColumnContent($row)
	{
		return sprintf("%s<br />Order ID: %05d<br />Item Description: %s<br />Type: %s<br />Note: %s<br /><a href='%s/%s' target='_blank'><img src='%s/%s' width='80' height='80' /></a>", $row->order_created_date_fmt
										  , $row->order_id, $row->order_name
										  , $row->priority_name, $row->order_note ? $row->order_note : "-", base_url(), $row->order_photo, base_url(), $row->order_photo);
	}
	
	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s<br />Phone: %s<br />Address: %s (%lf, %lf)<br />Detail Receiver Address: %s", $row->order_receiver_name
										  , $row->order_receiver_phone, $row->order_receiver_address, $row->order_receiver_latitude
										  , $row->order_receiver_longitude
										  , $row->order_receiver_address_note ? $row->order_receiver_address_note : "-");
	}
	
	private function courierInfo($row)
	{
		return sprintf("%s<br />Courier Name: %s<br >Date: %s", $this->order_model->getStatusDesc(2), $row->courier_name, $row->order_courier_date_fmt);
	}
	
	protected function pickUpInfo($row)
	{
		return sprintf("%s<br />Date: %s", $this->order_model->getStatusDesc(3), $row->order_pickup_date_fmt);
	}
	
	protected function received($row)
	{
		return sprintf("%s<br />Date: %s", $this->order_model->getStatusDesc(4), $row->order_received_date_fmt);
	}
	
	protected function getPhotoDir()
	{
		return "uploads/document_internal/";
	}
	
}
