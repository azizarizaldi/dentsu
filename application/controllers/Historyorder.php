<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Historyorder extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets))
		{
			if (isset($gets["order"]))
			{
				$order = $gets["order"];
			}
			if (isset($gets["columns"]))
			{
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value']))
			{
				$search = $gets['search']['value'];
			}
			else
			if (isset($gets['search']))
			{
				$search = $gets['search'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "description")
			{
				$column_order_by = "code";
			}
			else
			if ($column["data"] == "stock")
			{
				$column_order_by = "code";
			}
			else
			if ($column["data"] == "minstock")
			{
				$column_order_by = "code";
			}
			else
			{
				$column_order_by = $column["data"];
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("historyorder_model");
		$rows = $this->historyorder_model->get(isset($column_order_by) ? $column_order_by : "code", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date);

		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->order_no 	= $rows[$i]->code.'-'.$rows[$i]->stationary_order_id.'-'.$rows[$i]->order_date;
			$rows[$i]->order_date 	= tgl($rows[$i]->order_date,'02');
			$rows[$i]->receive_date = tgl($rows[$i]->receive_date,'02');
			$rows[$i]->item_order 	= '- '.str_replace(',','<br/>- ',$rows[$i]->item_description);
			$rows[$i]->note 		= "-";
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->historyorder_model->getCount(isset($search) ? $search : "",$range_date);
		$json["recordsFiltered"] = $this->historyorder_model->getCount(isset($search) ? $search : "",$range_date);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
