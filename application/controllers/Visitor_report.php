<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Visitor_report extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets)) {
			if (isset($gets["order"])) {
				$order = $gets["order"];
			}
			if (isset($gets["columns"])) {
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value'])) {
				$search = $gets['search']['value'];
			}
			else if (isset($gets['search'])) {
				$search = $gets['search'];
			}
			if (isset($gets['brand_unit_id'])) {
				$brand_unit_id = $gets['brand_unit_id'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "order_id")
			{
				$column_order_by = "order_id";
			}
			else if ($column["data"] == "order_date") {
				$column_order_by = "order_date";
			}
			else if ($column["data"] == "user_name") {
				$column_order_by = "user_name";
			}
			else {
				$column_order_by = $column["meeting_room_name"];
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("Visitor_report_model");
		$rows = $this->Visitor_report_model->get(isset($column_order_by) ? $column_order_by : "order_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date,$brand_unit_id);
		
		
		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->order_id 			= $rows[$i]->order_id;
			$rows[$i]->order_date 			= tgl($rows[$i]->order_date,'2');
			$rows[$i]->visitorName 			= $rows[$i]->visitorName;
			$rows[$i]->visitingName 		= $rows[$i]->visitingName;
			$rows[$i]->order_receiver_note 	= $rows[$i]->order_receiver_note;
			$rows[$i]->status 				= $rows[$i]->status;
			$rows[$i]->order_rejected_note 	= $rows[$i]->order_rejected_note;
			$rows[$i]->signInTime 			= $rows[$i]->signInTime;
			$rows[$i]->signOutTime 			= ($rows[$i]->signOutTime == '00:00' ? '-' : ''.$rows[$i]->signOutTime);
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->Visitor_report_model->getCount(isset($search) ? $search : "",$range_date,$brand_unit_id);
		$json["recordsFiltered"] = $this->Visitor_report_model->getCount(isset($search) ? $search : "",$range_date,$brand_unit_id);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
