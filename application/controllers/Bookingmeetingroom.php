<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Bookingcar.php';

class BookingMeetingRoom extends BookingCar {
	protected function getOrderType()
	{
		return 5;
	}
	
	protected function getDocomentColumnContent($row)
	{
		return sprintf("%s<br />Booking ID: %05d<br />%s", $row->order_created_date_fmt
										  , $row->order_id, $row->sender_name);
	}

	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s (%s)<br />Time: %s-%s", $row->meeting_room_name, $row->meeting_room_location, $row->order_start_date_fmt, ($row->order_date_length == 0) ? $row->order_end_time_fmt : $row->order_end_date_fmt);
	}
	
	
	protected function getCourierColumnContent($row)
	{
		$this->load->model("order_model");

		return sprintf("%s<br />Meeting with: %s", $row->order_name, $this->order_model->getMeetingWith($row));
	}

	public function getAttendanceList()
	{
		$gets = $this->input->get();
		
		if (! isset($gets['orderId']) || ! $gets['orderId']) return;
		
		$this->load->model("order_model");
		
		echo json_encode($this->order_model->getAttendanceList($gets['orderId']));
	}
	
	public function getMeetingRoomList()
	{
	    $this->load->model("meetingroom_model");
	    
	    $rows = $this->meetingroom_model->get();
	    
	    echo json_encode($rows);
	}
	
	public function getAvailable()
	{
	    $gets = $this->input->get();
	    $this->load->model("meetingroom_model");
	    
	    $rowBooked = $this->meetingroom_model->getBook($gets['meetingRoomId']);
	    $rowAvailable = $this->meetingroom_model->getAvailable($gets['meetingRoomId']);
	    
	    $todays = $this->meetingroom_model->getTodays($gets['meetingRoomId']);
	    /*
	    for($i=0; $i < count($todays); $i++)
	    {
	        switch($todays[$i]->order_status)
	        {
	            case 1:
	                $todays[$i]->statusName = "New Booking";
	                break;
	           case 2:
	           case 3:
	               $todays[$i]->statusName = "New Booking";
	               break;
	        }
	    }*/
	    
	    $obj = new stdClass;
	    $obj->booked = $rowBooked;
	    $obj->available = $rowAvailable;
	    $obj->todays = $todays;
	    
	    echo json_encode($obj);
	}
	
	public function joinAttendee()
	{
	    $posts = $this->input->post();
	    
	    $orderId = $posts['orderId'];
	    $attendees = explode(",", $posts['attendees']);
	    
	    $this->load->model("meetingroom_model");
	    
	    $this->meetingroom_model->absent($orderId, $attendees);
	    
	    $obj = new stdClass;
	    $obj->status = true;
	    
	    echo json_encode($obj);
	}
	
	public function endNow()
	{
	    $posts = $this->input->post();
	    
	    $orderId = $posts['orderId'];
	    
	    $this->load->model("meetingroom_model");
	    $this->meetingroom_model->endNow($orderId);
	    
	    $obj = new stdClass;
	    $obj->status = true;
	    
	    echo json_encode($obj);
	}
	
	public function extend()
	{
	    $posts = $this->input->post();
	    
	    $orderId = $posts['orderId'];
	    $time = $posts['time'];
	    
	    $this->load->model("meetingroom_model");
	    $this->meetingroom_model->extend($orderId, $time);
	    
	    $obj = new stdClass;
	    $obj->status = true;
	    
	    echo json_encode($obj);
	}

}
