<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Receivestock extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();

		$inIds = isset($gets['inIds']) ? $gets['inIds'] : false;
		$notInIds = isset($gets['notInIds']) ? $gets['notInIds'] : false;
		
			
		if (isset($gets))
		{
			if (isset($gets["order"]))
			{
				$order = $gets["order"];
			}
			if (isset($gets["columns"]))
			{
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value']))
			{
				$search = $gets['search']['value'];
			}
			else
			if (isset($gets['search']))
			{
				$search = $gets['search'];
			}
		}
		//print_r($gets); exit;
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "description")
			{
				$column_order_by = "item_description";
			}
			else
			if ($column["data"] == "stock")
			{
				$column_order_by = "item_stock";
			}
			else
			if ($column["data"] == "minstock")
			{
				$column_order_by = "item_min_stock";
			}
			else
			{
				$column_order_by = $column["data"];
			}
		}
		
		$stationary_order_id = isset($gets["stationary_order_id"]) ? $gets["stationary_order_id"] : 0;	
		
		$this->load->model("orderstock_model");
		$rows = $this->orderstock_model->get(isset($column_order_by) ? $column_order_by : "item_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$stationary_order_id);

		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->description 	= $rows[$i]->item_description;
			$rows[$i]->stock 		= $rows[$i]->item_stock." ".$rows[$i]->item_unit_measure;
			$rows[$i]->minstock 	= $rows[$i]->item_min_stock." ".$rows[$i]->item_unit_measure;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->orderstock_model->getCount(isset($search) ? $search : "",$stationary_order_id);
		$json["recordsFiltered"] = $this->orderstock_model->getCount(isset($search) ? $search : "",$stationary_order_id);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
	
	function deleteOrder() {
		$post 					= $this->input->post(null,true);
		$stationary_order_id	= $post['stationary_order_id'];
		
		$update['status']		= 'draft';
		$this->db->where("stationary_order_id",$stationary_order_id);		
		$this->db->update("stationary_order",$update);
		
		$result['status']		= true;
		$result['message']		= "Order successfully deleted!";
		echo die(json_encode($result));						
	}
	
	function receiveOrder() {
	    $session 	= $this->validLoginApps();				
	    if ($session === false) {
			echo 'Invalid';
	    }
		
		$post 					= $this->input->post(null,true);
		$stationary_order_id	= $post['stationary_order_id'];
		
		$this->db->where("stationary_order_id",$stationary_order_id);
		$get_stationary_order_detail = $this->db->get("stationary_order_detail")->result();

		if($get_stationary_order_detail) {		
			$update['status']		= 'receive';
			$update['receive_date']	= date('Y-m-d H:i:s');
			$update['receive_by'] 	= $session->user_id;
			$this->db->where("stationary_order_id",$stationary_order_id);		
			$this->db->update("stationary_order",$update);
			
			foreach($get_stationary_order_detail as $row) {
				$update_stock		= array();
				$this->db->where("item_id",$row->item_id);
				$stationary_item 	= $this->db->get("stationary_item")->row();
				if($stationary_item) {
					$update_stock['item_stock'] = $stationary_item->item_stock+$row->qty;
					$this->db->where("item_id",$stationary_item->item_id);		
					$this->db->update("stationary_item",$update_stock);
				}
			}
			$result['status']		= true;
			$result['message']		= "Order successfully receive!";
		}
		else {
			$result['status']		= false;
			$result['message']		= "Order failed receive!";			
		}
		echo die(json_encode($result));						
	}
}
