<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function fcm()
	{
	    
	    $this->load->model("order_model");
	    
	    
	    print_r(fcm(array("d_7tuTJyp1k:APA91bFjjB4ZMosz7Rw9hqmEi5Y-0_4ByXcK8OJi_QWRIZ-T5vHcdG8C2ibBNOPs0qo2UDeJkHc25bW_c3xGWAXZplosVok-zsWthCprGtdsDcWoMJEp30jEkIbaB05Dp3PrNTv2WBSlv2r1oMLUSDtVHrFiAc7v_Q")
	    , "__chat__", "test abah adilah. klo dapat notif ini tell me", ""));
	}
	
	public function test()
	{
	    $this->load->model("order_model");
	    
	    $this->order_model->notifNewOrder(325);
	}

	public function send_email($to="azizarifrizaldi@gmail.com", $subject="test", $message="ini contoh email", $cc = array(), $from="owner@adilahsoft.com", $fromName="Universe Admin") {
        $CI 				= & get_instance();
        $config['charset'] 	= 'iso-8859-1';
        $config['mailtype'] = "html";
        $CI->email->initialize($config);        
        $CI->email->from($from, $fromName);
        $CI->email->to($to);
        if (count($cc)) {
            $CI->email->cc($cc);
        }
        $CI->email->bcc('owner@adilahsoft.com');
        $CI->email->subject($subject);
        $CI->email->message($message);
        $CI->email->send();
	}

	public function sendReminder() {
		date_default_timezone_set('Asia/Jakarta');
		$getData = $this->db->query("
			SELECT *
			FROM (
			SELECT send_reminder , document.document_id , document.document_name ,  setting_category.name AS category, vendor.vendor_name , start_date , DATE_ADD(end_date,INTERVAL -reminder_day DAY) AS sendDate , end_date , reminder_day
			FROM document
			INNER JOIN setting_category ON setting_category.id = document.id_category
			INNER JOIN vendor ON vendor.vendor_id = document.vendor_id
			) getDocument
			WHERE send_reminder = 'yes' and curdate() BETWEEN sendDate AND end_date
		")->result();

		if(!empty(count($getData))) {
			foreach($getData as $row) {
				$subject 	= "Expiration Reminder '".$row->document_name."'";
				$message	= "<div style='font-family:arial'><p>Your document '".$row->document_name."' will be expired on ".date('l\,jS F Y',strtotime($row->end_date)).'<br/></p>'.'
				DETAILS :<br/>
				Category : '.$row->category.'<br/>
				Doc. name : '.$row->document_name.'<br/>
				Validate Periode : '.date('d/m/Y',strtotime($row->start_date)).' - '.date('d/m/Y',strtotime($row->end_date)).'<br/>
				Vendor : '.$row->vendor_name.'<br/>
				Reminder Days '.$row->reminder_day.' Days before expired
				</div>
				';

				$this->db->where("delivery_date",date("Y-m-d"));
				$this->db->where("document_id",$row->document_id);
				$checkReminder = $this->db->get("reminder_temp")->num_rows();
				if(empty($checkReminder)) {
					$input['document_id'] = $row->document_id;
					$input['delivery_date'] = date("Y-m-d");
					$input['created_date'] = date("Y-m-d H:i:s");
					$this->db->insert("reminder_temp",$input);

					$this->send_email($to="indra.gunawan@dentsuaegis.com", $subject, $message, $cc = array(), $from="owner@adilahsoft.com", $fromName="Universe Admin");
				}
			}
		}
	}

	public function sendEmailDashboard() {
		$post = $this->input->post(null,true);
		$emailto = $post['emailto'];
		$subject = $post['subject'];
		$message = $post['message'];

		$response['status'] 	= "";
		$response['message'] 	= "";

		if(filter_var(''.$emailto, FILTER_VALIDATE_EMAIL)) {
			$this->send_email($emailto, $subject, $message, $cc = array(), $from="owner@adilahsoft.com", $fromName="Universe Admin");
			$response['status'] 	= "success";
			$response['message'] 	= "Email successfully sent!";	
		}
		else {
			$response['status'] 	= "failed";
			$response['message'] 	= "Email address is invalid";	
		}
		echo json_encode($response);
		die();
	}

	public function todolist() {
		$this->db->order_by("id","desc");
		$todolist = $this->db->get("to_do_list")->result();
		if(count($todolist) > 0) {
			foreach($todolist as $row) {
			$color	= "";
			$status	= "";
			$check  = "";
			$value	= "true";
			if($row->priority == 'low') {
				$color="info";
			}
			else if($row->priority == 'medium') {
				$color="warning";
			}
			else if($row->priority == 'high') {
				$color="danger";
			}

			if($row->done == 'true') {
				$check  = "checked";
				$value  = "false";
				$status = "text-decoration:line-through;font-style:italic";
			}
			echo '<li>
			<input type="hidden" id="textTitle_'.$row->id.'" value="'.$row->title.'"/>
			<input type="hidden" id="textPriority_'.$row->id.'" value="'.$row->priority.'"/>
			<input '.$check.' onclick="changeDone('.$row->id.','.$value.')" type="checkbox" value="'.$value.'">
			<span class="text" style="'.$status.'">'.$row->title.'</span>
			<small class="label label-'.$color.'">'.$row->priority.'</small>
			<div class="tools">
				<i class="fa fa-edit" onclick="editTodo('.$row->id.')"></i>
				<i class="fa fa-trash-o" onclick="deleteTodo('.$row->id.')"></i>
			</div>
			</li>';
			}
		}
		else {
			echo 'Data is not available';
		}
	}

	public function update_todolist_status() {
		$post 	= $this->input->post(null,true);
		$id 	= $post['id'];
		$status = $post['status'];
		
		$data['done']	= $status;
		$this->db->where("id",$id);
		$this->db->update("to_do_list",$data);
	}

	public function delete_todolist() {
		$id = $this->input->post('id');
		if($id != '') {
			$this->db->where("id",$id);
			$this->db->delete("to_do_list");	
			$response['status'] 	= "success";
			$response['message'] 	= "Data has been deleted!";	
		}
		else {
			$response['status'] 	= "failed";
			$response['message'] 	= "Data unsuccessfully deleted!";	
		}
		echo json_encode($response);
		die();
	}

	public function saveTodolist() {
		date_default_timezone_set('Asia/Jakarta');
		$post 	= $this->input->post(null,true);
		$id 		= $post['id'];
		$title 		= $post['title'];
		$priority 	= $post['priority'];
		$data['title'] 		= $title;
		$data['priority'] 	= $priority;

		if($id == '') {
			$data['created_at'] 	= date("Y-m-d H:i:s");
			$this->db->insert("to_do_list",$data);
			$response['status'] 	= "success";
			$response['message'] 	= "Data has been added!";		
		}

		else {
			$data['updated_at'] 	= date("Y-m-d H:i:s");
			$this->db->where("id",$id);
			$this->db->update("to_do_list",$data);
			$response['status'] 	= "success";
			$response['message'] 	= "Data has been updated!";		
		}
		echo json_encode($response);
		die();
	}
}
