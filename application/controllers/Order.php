<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

abstract class Order extends MyController {
    abstract protected function getOrderType();
    
    abstract protected function getEmptyOrderNameMessage();
    
    public function setCourier()
    {
        
		date_default_timezone_set('Asia/Jakarta');
        if (! $this->isAdmin())
        {
            die("Access denide");
        }

        $posts = $this->input->post();
        
        //$posts['orderId'] = 2;
        //$posts['courierId'] = 6;
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        $courierId = isset($posts["courierId"]) ? $posts["courierId"] : "";
        
        $json = new stdClass();
        
        if (! $orderId || ! $courierId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            $order = $this->order_model->getOrderById($orderId);
            if (! $order || ! isset($order['order_id']) || ! $order['order_id'])
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {               
                $order['order_status'] = 2;
                $order['order_courier_by'] = $courierId;
                $order['order_courier_date'] = date("Y-m-d H:i:s");
                
                $this->order_model->update($order);
                
                $this->order_model->notifSetCourier($orderId);
                
                $json->status = true;
                $json->message = "Courier has been updated";
            }
        }
        
        echo json_encode($json);
    }
    
    public function approve()
    {
		date_default_timezone_set('Asia/Jakarta');

        if (! $this->isAdmin())
        {
            die("Access denide");
        }

        $posts = $this->input->post();
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        
        $json = new stdClass();
        
        if (! $orderId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            $order = $this->order_model->getOrderById($orderId);
            $orderType = $order['order_type'];
            
            if (! $order || ! isset($order['order_id']) || ! $order['order_id'])
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {                
                $row = $this->session->userdata("row");
                
                if (isset($posts['note']))
                {
                    $order['order_receiver_note'] = $posts['note'];
                }
                
                $session = $this->session->userdata("row");
                
                $order['order_status'] = 2;         
                $order['order_approved_by'] = $session->user_id;
                $order['order_courier_date'] = date("Y-m-d H:i:s");
                
                $this->order_model->update($order);
                
                if ($orderType == 6)
                {
                    $this->load->model("stationaryitem_model");
                    $this->stationaryitem_model->syncStock($orderId);
                    
                }
                
                $this->order_model->notifApproveOrder($orderId);
                
                $json->status = true;
                $json->message = "Order has been approved";
            }
        }
        
        echo json_encode($json);
    }
    
    public function getById()
    {
        $gets = $this->input->get();
        
        if (! isset($gets['orderId']) || ! $gets['orderId'])
        {
            return;
        }
        
        $this->load->model("order_model");
        
        echo json_encode($this->order_model->getById($gets['orderId']));
    }
    
    public function complete()
    {
		date_default_timezone_set('Asia/Jakarta');
        $json = new stdClass();
        
        $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json->sessionExpired = true;

	        echo json_encode(rows);
	        return;
	    }
	    
        $posts = $this->input->post();
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        
        $json = new stdClass();
        
        if (! $orderId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            $order = $this->order_model->getOrderById($orderId);
            if (! $order || ! isset($order['order_id']) || ! $order['order_id'])
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {
                $row = $session;

                if ($this->getOrderType() == 1)
                {
                    if ($order['order_status'] != 3)
                    {
                        die("Access denied");
                    }
                    
                    if (! $this->isAdminApps($session) && ($order['order_courier_by'] != $row->user_id))
                    {
                        die("Access denied");
                        return;
                    }
                    
                    if (isset($posts['orderReceiveName']))
                    {
                        $order['order_receiver_name'] = $posts['orderReceiveName'];
                    }
                    
                    if (isset($_FILES['photo']))
				    {
				        $path = $this->config->item('basePath');
				        $filename = $path.$this->getPhotoDir().$_FILES['photo']['name'];
				    
				        @copy($_FILES['photo']['tmp_name'], $filename);
				    
				        $order['order_receiver_photo'] = $this->getPhotoDir().$_FILES['photo']['name'];

				    }
                }
                else
                if ($this->getOrderType() == 2)
                {
                    if ($order['order_status'] != 3)
                    {
                        die("Access denied");
                    }

                    if (! $this->isAdmin())
                    {
                        die("Access denied");
                    }
                }
                else
                if ($this->getOrderType() == 3)
                {
                    if (! $this->isAdminApps($session) && ($order['order_created_by'] != $row->user_id))
                    {
                        die("Access denied 1");
                    }
                }
                else
                if ($this->getOrderType() == 4)
                {
                    if (($order['order_status'] != 2) && ($order['order_status'] != 3)&& ($order['order_status'] != 6))
                    {
                        die("Access denied");
                    }

                    if (! $this->isAdminApps($row) && ($order['order_created_by'] != $row->user_id))
                    {
                        //die("Access denied 1");
                    }
                }
                if ($this->getOrderType() == 6)
                {
                    if (! $this->isAdminApps($row) && ($order['order_created_by'] != $row->user_id))
                    {
                        die("Access denied 1");
                    }
                }

                
                if (isset($posts['note']) && $posts['note'])
                {
                    $order['order_receiver_note'] = $posts['note'];
                }

                if (isset($posts['airwaybill']) && $posts['airwaybill'])
                {
                    $order['order_note'] = $posts['airwaybill'];
                }         

                if (isset($posts['fee']) && $posts['fee'])
                {
                    $order['order_receiver_latitude'] = $posts['fee'];
                }     
                
                $order['order_status'] = 4;                
                $order['order_received_date'] = date("Y-m-d H:i:s");
                $order['order_received_by'] = $row->user_id;
                
                $this->order_model->update($order);
                $this->order_model->notifCompleteOrder($order['order_id'], $session->user_name);
                
                $json->status = true;
                $json->message = "Order has been completed";
            }
        }
        
        $json->sessionExpired = false;
        echo json_encode($json);
    }
    
    public function pickUp()
    {
		date_default_timezone_set('Asia/Jakarta');
        $session = $this->validLoginApps();
        $sessionUserName = $session->user_name;
        
        $posts = $this->input->post();
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        
        $json = new stdClass();
        
        if (! $orderId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            $order = $this->order_model->getOrderById($orderId);
            
            $orderType = $order['order_type']; 
            
            if (! isset($posts['officeBoy']) || ! $posts['officeBoy'])
            {
                $posts['officeBoy'] = $order['order_courier_by'];
            }
            
            if (! $order || ! isset($order['order_id']) || ! $order['order_id'])
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {
                if ($orderType == 7)
                {
                    if ($order['order_status'] != 1)
                    {
                        die("Access denied");
                    }
                }
                else
                if ($orderType == 4)
                {
                    if ($order['order_status'] != 2 && $order['order_status'] != 3)
                    {
                        die("Access denied");
                    }
                }
                else
                if ($order['order_status'] != 2)
                {
                    die("Access denied");
                }

                if ($orderType == 1)
                {

                    //$session = $this->session->userdata("row");

                    if ((! $this->isAdminApps($session)) && ($order['order_courier_by'] != $session->user_id))
                    {
                        die("Access denied");
                    }
                }
                else
                if (($orderType == 2) || ($orderType == 6) )
                {
                    if (! $this->isAdmin())
                    {
                        die("Access denied");
                    }
                }
                else
                if ($orderType == 7)
                {
                    if ((! $this->isAdmin()) && ($order['order_pickup_by'] != $session->user_id))
                    {
                        die("Access denied");
                    }
                }

                if (isset($posts['addressNote']) && $posts['addressNote'])
                {
                    $order['order_receiver_address_note'] = $posts['addressNote'];
                }  
                
                if ($orderType == 4)
                {
                    $order['order_status'] = $order['order_status'] == 3 ? 6 : 3;
                }
                else
                {
                    $order['order_status'] = 3;
                }
                if ($orderType != 7)
                {
                    $order['order_pickup_by'] = $posts['officeBoy'];
                }
                if (isset($posts['note']))
                {
                    $order['order_sender_address'] = $posts['note'];
                }

                $order['order_pickup_date'] = date("Y-m-d H:i:s");
                
                $this->order_model->update($order);
                
                $json->status = true;
                if ($orderType == 7)
                {
                    $json->message = "Order has been accepted";
                }
                else
                if ($orderType == 4 && $order['order_status'] == 3)
                {
                    $json->message = "Order has been processed";
                }
                else
                {
                    $json->message = "Order has been pick up";
                }
                
                $this->order_model->notifPickupOrder($order['order_id'], $sessionUserName);
                
            }
        }
        
        echo json_encode($json);
    }
    
    public function rejectOrder()
    {
		date_default_timezone_set('Asia/Jakarta');
        $json = new stdClass();
        
        $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json->sessionExpired = true;

	        echo json_encode(rows);
	        return;
	    }
	    
        $posts = $this->input->post();
        
		$this->db->where("order_id",$posts['orderId']);
		$checkOrder = $this->db->get("order")->row();
		
		if($checkOrder) {
			if($checkOrder->order_type == '6') {
				$this->db->where("order_stationary_order",$posts['orderId']);
				$order_stationary = $this->db->get("order_stationary")->result();
				foreach($order_stationary as $get_order) {
					$this->db->where("item_id",$get_order->order_stationary_item);
					$stationary_item 	= $this->db->get("stationary_item")->row();			
					$stok_update		= ($get_order->order_stationary_qty+$stationary_item->item_stock);
					$update_stok['item_stock']	= $stok_update;
					$this->db->where("item_id",$get_order->order_stationary_item);
					$this->db->update("stationary_item",$update_stok);
				}
			}
		}
		
        //$posts['orderId'] = 2;
        //$posts['note'] = 'test doang';
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        $note = isset($posts["note"]) ? $posts["note"] : "";
        
        if (! $orderId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            $order = $this->order_model->getOrderById($orderId);
            if (! $order || ! isset($order['order_id']) || ! $order['order_id'])
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {
                $row = $session;

                if ($order['order_status'] == 4)
                {
                    die("Access denied");
                    return;
                }

                if (! $this->isAdminApps($row) && ($order['order_created_by'] != $row->user_id) && ($order['order_courier_by'] != $row->user_id) && ($order['order_pickup_by'] != $row->user_id))
                {
                    die("Access denied");
                    return;
                }

                if (isset($posts["reason"]))
                {
                    $order['order_rejected_note'] = $posts["reason"];
                }
                else
                {
                    $order['order_rejected_note'] = $note;
                }
                
                $order['order_status'] = 5;                
                $order['order_rejected_by'] = $row->user_id;
                $order['order_rejected_date'] = date("Y-m-d H:i:s");
                
                $this->order_model->update($order);
                $this->order_model->notifRejectedOrder($order['order_id']);
                
                $json->status = true;
                $json->message = "Order has been rejected";
            }
        }
        
        $json->sessionExpired = false;
        echo json_encode($json);

    }
    
    public function create()
    {
		date_default_timezone_set('Asia/Jakarta');

        //exit;
        $posts = $this->input->post();
     /*
      $posts["order_id"] = 2;
     $posts['order_receiver_latitude'] = -6.174838312036681;
$posts['order_receiver_longitude'] = 106.86493635177612;
     $posts['order_name'] = "Perjanjian";
$posts['order_created_by'] = "3";
$posts['order_priority'] = "1";
$posts['order_receiver_name'] = "adasda";
$posts['order_receiver_phone'] = "sadasda";
$posts['order_receiver_address'] = "sadasda";
$posts['order_receiver_address_note'] = "jauh";
$posts['order_note'] = "penting";*/

        $json = new stdClass();
        
        if ($this->getOrderType() != 3)
        {
            $session = $this->validLoginApps();
	        if ($session === false)
	        {
	            $json->sessionExpired = true;
	            echo json_encode($json);
	            return;
	        }
        }
        
        if (! isset($posts['order_name']) || ! $posts['order_name'])
        {
            $json->status = false;
            $json->message = $this->getEmptyOrderNameMessage();
        }
        else
        if ((! isset($posts['order_receiver_name']) || ! $posts['order_receiver_name']) && ($this->getOrderType() != 6) && ($this->getOrderType() != 7) && ($this->getOrderType() != 5))
        {
            $json->status = false;
            if ($this->getOrderType()  == 4)
            {
               $json->message = "Please input client name"; 
            }
            else
            {
                $json->message = "Please input a receiver name";
            }
        }
        else
        if ((! isset($posts['order_receiver_phone']) || ! $posts['order_receiver_phone']) && ($this->getOrderType() != 6) && ($this->getOrderType() != 7))
        {
            $json->status = false;
            $json->message = "Please input a receiver phone";
        }        
        else
        if ((! isset($posts['order_receiver_latitude']) || ! $posts['order_receiver_latitude'] || ! isset($posts['order_receiver_longitude']) || ! $posts['order_receiver_longitude']) && ($this->getOrderType() != 6)&& ($this->getOrderType() != 7))
        {
            $json->status = false;
            $json->message = "Please input a receiver location";
        }
        else
        if ((! isset($posts['order_receiver_address']) || ! $posts['order_receiver_address']) && ($this->getOrderType() != 6))
        {
            $json->status = false;
            $json->message = "Please input a receiver address";
        }
        else
        {
            if (isset($posts['pickupLocation']))
            {
                if ($posts['pickupLocation'] == 1)
                {
                    $posts['order_sender_address'] = "Office";
                    $posts['order_sender_latitude'] = 0;
                    $posts['order_sender_longitude'] =  0;
                }
                
                unset($posts['pickupLocation']);
            }

            if (isset($posts['orderDelivery']))
            {
                $posts['order_sender_latitude'] = $posts['orderDelivery'];
                unset($posts['orderDelivery']);
            }
            
            if (isset($posts['order_start_date']))
            {
                $posts['order_start_date'] = date('Y-m-d H:i:00', dateDMYHItoInt($posts['order_start_date']));;
                $posts['order_end_date'] = date('Y-m-d H:i:59', dateDMYHItoInt($posts['order_end_date']));;
            }
            
            	if (isset($posts['order_photo']) && $posts['order_photo'])
				{
                    $path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["order_photo"]);
				}
				else
				if (isset($_FILES['photo']))
				{
				    $path = $this->config->item('basePath');
				    $filename = $path.$this->getPhotoDir().$_FILES['photo']['name'];
				    
				    @copy($_FILES['photo']['tmp_name'], $filename);
				    
				    $posts['order_photo'] = $this->getPhotoDir().$_FILES['photo']['name'];

				}
            unset($posts['tmp_photo']);
            $posts['order_type'] = $this->getOrderType();
            
            if (isset($posts['attendaces']))
            {
                $attendaces = $posts['attendaces'];
                unset($posts['attendaces']);
            }
            
            if (isset($posts['arrAttendees']))
            {
                $arrAttendees = $posts['arrAttendees'];
                $attendaces = explode(",", $arrAttendees);
                
                unset($posts['arrAttendees']);
            }
            
            if (isset($posts['itemIds']))
            {
                $itemIds = explode(",", $posts['itemIds']);
                $itemQtys = explode(",", $posts['itemQtys']);

                unset($posts['itemIds']);
                unset($posts['itemQtys']);
            }
            
            $this->load->model("order_model");
            
            if (isset($posts["order_id"]))
            {
                if (isset($posts['order_photo']) && ! $posts['order_photo'])
				{
					unset($posts['order_photo']);
				}
                
                $this->order_model->update($posts);
                
                if (isset($attendaces))
                {
                    $this->load->model("meetingroom_model");
                    $this->meetingroom_model->updateAttendance($posts["order_id"], $attendaces);
                }

                if (isset($itemIds))
                {
                    $this->order_model->deleteStationaryCart($posts["order_id"]);
                    for($i=0; $i < count($itemIds); $i++)
                    {
                        $data['order_stationary_item'] = $itemIds[$i];
                        $data['order_stationary_qty'] = $itemQtys[$i];
                        $data['order_stationary_order'] = $posts["order_id"];

                        $this->order_model->updateStationaryCart($data);
                    }
                }
                
                $json->status = true;
                $json->message = "Order has been updated";
            }
            else
            {
                $this->order_model->insert($posts);
                $insert_id = $this->db->insert_id();
                
                if (isset($attendaces))
                {
                    $this->load->model("meetingroom_model");
                    $this->meetingroom_model->updateAttendance($insert_id, $attendaces);
                }

                if (isset($itemIds))
                {
                    $this->order_model->deleteStationaryCart($insert_id);
                    for($i=0; $i < count($itemIds); $i++)
                    {
                        $data['order_stationary_item'] = $itemIds[$i];
                        $data['order_stationary_qty'] = $itemQtys[$i];
                        $data['order_stationary_order'] = $insert_id;

                        $this->order_model->updateStationaryCart($data);
                    }
                }

                
                $json->status = true;
                $json->message = "Order has been created";
                
                $this->order_model->notifNewOrder($insert_id);
            }        
        }
        
        $json->sessionExpired = false;
        
        echo json_encode($json);
    }
    
    public function getCourierPosition()
    {
        $posts = $this->input->post();
        
        $orderId = isset($posts["orderId"]) ? $posts["orderId"] : "";
        
        $json = new stdClass();
        if (! $orderId)
        {
            $json->status = false;
            $json->message = "Invalid arguments";
        }
        else
        {
            $this->load->model("order_model");
            
            $order = $this->order_model->getOrderById($orderId);
            if (! $order)
            {
                $json->status = false;
                $json->message = "Invalid arguments";
            }
            else
            {
                $json->status = true;
                $json->order_pickup_latitude = $order["order_pickup_latitude"];
                $json->order_pickup_longitude = $order["order_pickup_longitude"];
            }                        
        }
        
        echo json_encode($json);
    }
    
    public function getBooking()
	{
		$json = new stdClass();
		
		$gets = $this->input->get();
		
		if (! isset($gets['date1']) || ! isset($gets['date2']))
		{
			return;
		}

		$bookingId = isset($gets['bookingId']) ? $gets['bookingId'] : 0;
		$date1 = dateDMYHItoInt($gets['date1']);
		$date2 = dateDMYHItoInt($gets['date2']);
		$apps = isset($gets['apps']) ? $gets['apps'] : 0;
		
		$error = false;
		if ($date1 == 0 || $date2 == 0|| $date1 >= $date2)
		{
		    if (! $apps)
		    {
		        $error =true;
			    $json->status = false;
			    $json->message = "Invalid date range";
		    }
		    else
		    {
		        $error =true;
		        
		        $json->status = true;
		        $json->suggestions = array();
		        $json->availables = array();
		    }
		}

        if (! $error)
		{
            date_default_timezone_set('Asia/Jakarta');
    		$this->load->model("order_model");
    		
    		$sdate1 = date("Y-m-d H:i:00", $date1);
    		$sdate2 = date("Y-m-d H:i:59", $date2);
    		
    		$data = $this->order_model->getBooking($this->getOrderType(), $sdate1, $sdate2, $bookingId);
            
            $avails = isset($data['avails']) ? $data['avails'] : array();
            if ($apps)
            {
                if ($this->getOrderType() == 4)
                {
                    for($i=0; $i < count($avails); $i++)
                    {
                        $avails[$i]->meeting_room_id = $avails[$i]->car_id;
                        $avails[$i]->meeting_room_name = sprintf("%s %s", $avails[$i]->car_brand, $avails[$i]->car_model);
                        $avails[$i]->meeting_room_pic = $avails[$i]->car_pic;
                        $avails[$i]->meeting_room_location = $avails[$i]->car_no;
                        $avails[$i]->meeting_room_capacity = $avails[$i]->car_seat;
                        $avails[$i]->meeting_room_desc = $avails[$i]->car_color;
                        
                    }
                }
            
            }
            
            $json->status = true;
            $json->availables = $avails;
            
            if ($apps)
            {
                $suggest = isset($data['suggest']) ? $data['suggest'] : array();
                $suggestions = array();
                for($i=0; $i < count($suggest); $i++)
                {
                    if (isset($suggest[$i]->isstart) && $suggest[$i]->isstart == 1)
                    {
                    	$a = clone $suggest[$i];
                    
                        $a->meeting_room_suggest_startdate = $suggest[$i]->startdate_fmt;
                        $a->meeting_room_suggest_enddate = $suggest[$i]->order_start_date_fmt;
                        
                        if ($this->getOrderType() == 4)
                        {
                            $a->meeting_room_id = $a->car_id;
                            $a->meeting_room_name = sprintf("%s - %s %s", $a->car_no, $a->car_brand, $a->car_model);
                            $a->meeting_room_pic = $a->car_pic;
                            $a->meeting_room_location = $a->car_no;
                            $a->meeting_room_capacity = $a->car_seat;
                            $a->meeting_room_desc = $a->car_color;
                        }
    
                        $suggestions[] = $a;
                       
                    }
                    if (isset($suggest[$i]->isend) && $suggest[$i]->isend == 1)
                    {
                    	$b = clone $suggest[$i];
                    
                        $b->meeting_room_suggest_startdate = $suggest[$i]->order_end_date_fmt;
                        $b->meeting_room_suggest_enddate = $suggest[$i]->enddate_fmt;
                        
                        if ($this->getOrderType() == 4)
                        {
                            $b->meeting_room_id = $b->car_id;
                            $b->meeting_room_name = sprintf("%s - %s %s", $b->car_no, $b->car_brand, $b->car_model);
                            $b->meeting_room_pic = $b->car_pic;
                            $b->meeting_room_location = $b->car_no;
                            $b->meeting_room_capacity = $b->car_seat;
                            $b->meeting_room_desc = $b->car_color;
                        }
                       
                       	$suggestions[] = $b;
                    }
                    
                }
                
                $json->suggestions = $suggestions;
            }
            else
            {
                $json->suggestions = isset($data['suggest']) ? $data['suggest'] : array();   
            }
		}
		
		
		echo json_encode($json);
	}
	
	protected function getPhotoDir()
	{
		return "uploads/document_external/";
	}
}
