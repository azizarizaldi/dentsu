<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Documentinternal.php';

class BookingCar extends DocumentInternal {
	protected function getOrderType()
	{
		return 4;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	protected function getDocomentColumnContent($row)
	{
		$address = $row->order_sender_address;
		if ($row->order_sender_latitude != 0)
		{
			$address  .= sprintf(" (%f, %f)", $row->order_sender_latitude, $row->order_sender_longitude);
		}
		
		return sprintf("%s<br />Booking ID: %05d<br />%s<br />Pickup Location: %s<br />Note: %s", $row->order_created_date_fmt
										  , $row->order_id, $row->sender_name, $address, $row->order_note ? $row->order_note : "-", base_url());
	}
	
	protected function getCourierColumnContent($row)
	{
		return sprintf("Booking Date: %s-%s<br />%s %s %s", $row->order_start_date_fmt, ($row->order_date_length == 0) ? $row->order_end_time_fmt : $row->order_end_date_fmt, $row->car_no, $row->car_brand, $row->car_model);
	}
	
	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s (%s)<br />Address: %s", $row->order_receiver_address_note, $row->order_receiver_name, $row->order_receiver_address);
	}
	
	protected function getStatus($row)
	{
		switch($row->order_status)
			{
				case 2:
					return sprintf("Approved<br />%s", $row->order_courier_date);
				case 3:
					return sprintf("Driver found<br />%s", $row->order_pickup_date);
				case 6:
					return sprintf("Pick Up<br />%s", $row->order_pickup_date);
				case 4:
					return sprintf("Done<br />%s", $row->order_received_date);
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}
	
	public function getDriver()
	{
	    $gets = $this->input->get();
	    
	    $carId = isset($gets["carId"]) ? $gets["carId"] : 0;
	    
	    $json = new stdClass();
	    if (! $carId)
	    {
	        echo json_encode("");
	    }
	    else
	    {
	        $this->load->model("cardriver_model");
	        $row = $this->cardriver_model->getDriver($carId);
	        
	        echo json_encode($row);
	    }
	}
	
	public function getTaskCount()
	{
	    $json = new stdClass();
	    
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json->status = false;
	        echo json_encode($json);
	        return;
	    }
	    
	    $this->load->model("cardriver_model");
	    $rows = $this->cardriver_model->getCars($session->user_id);
	    if (! count($rows))
	    {
	        $json->status = false;
	        echo json_encode($json);
	        return;
	    }
	    
	    $carIds = array();
	    for($i=0; $i < count($rows); $i++)
	    {
	        $carids[] = $rows[$i]->car_driver_car;
	    }
	    
	    $this->load->model("order_model");
	    
	    $json->status = true;
	    $json->active = $this->order_model->getBookingCarCount($carids, array(2, 3, 6));
	    $json->history = $this->order_model->getBookingCarCount($carids, array(4, 5));
	    
	    echo json_encode($json);
	    
	}
	
	public function getCar()
	{
	    $gets = $this->input->get();
	    
	    $driverId = isset($gets["driverId"]) ? $gets["driverId"] : 0;
	    
	    $json = new stdClass();
	    if (! $driverId)
	    {
	        echo json_encode("");
	    }
	    else
	    {
	        $this->load->model("cardriver_model");
	        $row = $this->cardriver_model->getCar($driverId);
	        
	        echo json_encode($row);
	    }
	}
	
	public function getCars()
	{
	    $gets = $this->input->get();
	    
	    $driverId = isset($gets["driverId"]) ? $gets["driverId"] : 0;
	    
	    $json = new stdClass();
	    if (! $driverId)
	    {
	        echo json_encode("");
	    }
	    else
	    {
	        $this->load->model("cardriver_model");
	        $rows = $this->cardriver_model->getCars($driverId);
	        
	        echo json_encode($rows);
	    }
	}
	
	public function getCarList()
	{
	    $this->load->model("car_model");
	    
	    $rows = $this->car_model->get();
	    for($i=0; $i < count($rows); $i++)
	    {
	        $rows[$i]->meeting_room_id = $rows[$i]->car_id;
	        $rows[$i]->meeting_room_name = sprintf("%s %s", $rows[$i]->car_brand, $rows[$i]->car_model);
	        $rows[$i]->meeting_room_location = $rows[$i]->car_no;
	        $rows[$i]->meeting_room_capacity = $rows[$i]->car_seat;
	        $rows[$i]->meeting_room_desc = $rows[$i]->car_color;
	        $rows[$i]->meeting_room_pic = $rows[$i]->car_pic;
	    }
	    
	    echo json_encode($rows);
	}

	
}
