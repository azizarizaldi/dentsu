<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Stationary_expense_report extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/stationary_expense_report/form.js", $data, true);
		}
        
		return $this->load->view("admin/stationary_expense_report/list.js", "", true);
	}
	
	protected function getTitle() {
		return "Inventory Report List";
	}
		
	protected function getMainContent() {
		return $this->load->view("admin/stationary_expense_report/list", "", true);
	}

	public function get() {			
		$gets = $this->input->get();

		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''))");
		$order 				= $gets["order"];
		$columns 			= $gets["columns"];
		$brand_unit_id 		= $gets["brand_unit_id"];
		$company_id 		= $gets["company_id"];
		$range_date 		= $gets["range_date"];
		$search 			= $gets['search']['value'];		
		$column 			= $columns[$order[0]["column"]];	
	
		$limit				= $gets["length"];
		$offset				= $gets["start"];
		
		$this->db->select("
			stationary_item.item_description , stationary_category.category_name , stationary_item.item_price , stationary_order_detail.qty , SUM(stationary_order_detail.qty) AS jumlah_qty
		");

		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search})", null, false);
		}

		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}

		if($company_id) {
			$this->db->where("brand_unit_company",$company_id);
		}

		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
		}

		if ($limit > 0) {
			$this->db->limit($limit, $offset);		
		}

		$this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","left");
		$this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
		$this->db->join("_user","_user.user_id = stationary_order.order_by","inner");
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$this->db->join("departement","departement.departement_id = _user.user_departement","left");
		$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
		$this->db->where("stationary_order.status","receive");
		$this->db->group_by("stationary_item.item_id");
		$rows 			=  $this->db->get("stationary_order")->result();
		
		for($i=0; $i < count($rows); $i++) {
			$total_cost				= 0;
			$total_cost				= $rows[$i]->jumlah_qty*$rows[$i]->item_price;
			$rows[$i]->item_price 	= "Rp. ".number_format(($rows[$i]->item_price), 0, '', '.').''; 
			$rows[$i]->total_cost 	= "Rp. ".number_format($total_cost, 0, '', '.').''; 
		}
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search})", null, false);
		}

		if($brand_unit_id) {
			$this->db->where("departement_brand_unit",$brand_unit_id);
		}

		if($company_id) {
			$this->db->where("brand_unit_company",$company_id);
		}

		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  

			$this->db->where("date(order_date) between '".$firstDate."' and '".$lastDate."' ");
		}

		$this->db->join("stationary_order_detail","stationary_order_detail.stationary_order_id = stationary_order.stationary_order_id","left");
		$this->db->join("stationary_item","stationary_item.item_id = stationary_order_detail.item_id","inner");
		$this->db->join("_user","_user.user_id = stationary_order.order_by","inner");
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$this->db->join("departement","departement.departement_id = _user.user_departement","left");
		$this->db->join("brand_unit","brand_unit.brand_unit_id = departement.departement_brand_unit","left");
		$this->db->where("stationary_order.status","receive");
		$this->db->group_by("stationary_item.item_id");
		$rows_number 			=  $this->db->get("stationary_order")->num_rows();

		$json["draw"] 				= $gets["draw"]+1;
		$json["recordsTotal"] 		= $rows_number;
		$json["recordsFiltered"] 	= $rows_number;
		$json["data"] 				= $rows;
		$json['status'] 			= true;
		
		echo json_encode($json);
	}

	public function addForm() {

	}
	public function doSave() {

	}
}
