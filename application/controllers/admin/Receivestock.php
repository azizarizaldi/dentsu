<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Receivestock extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/receivestock/form.js", $data, true);
		}
        
		return $this->load->view("admin/receivestock/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Receive Stock";
	}
	
	protected function getMainContent() {
		$this->db->select("stationary_order_id , code ,date(order_date) as order_date");
		$this->db->where("status",'hold');
		$this->db->order_by("stationary_order_id","desc");
		$data['stationary_order'] =  $this->db->get("stationary_order")->result();
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/receivestock/form", $data, true);
		}
		return $this->load->view("admin/receivestock/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
