<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Warningstock extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/warningstock/form.js", $data, true);
		}
        
		return $this->load->view("admin/warningstock/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Warning";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/warningstock/form", $data, true);
		}
		return $this->load->view("admin/warningstock/list", "", true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
