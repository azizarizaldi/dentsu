<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Report_document_in extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_document_in/form.js", $data, true);
		}
        
		return $this->load->view("admin/report_document_in/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Book / Month";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_document_in/form", $data, true);
		}
		return $this->load->view("admin/report_document_in/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
