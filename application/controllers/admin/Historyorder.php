<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Historyorder extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/historyorder/form.js", $data, true);
		}
        
		return $this->load->view("admin/historyorder/list.js", "", true);
    }
	
	protected function getTitle() {
		return "History Order";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/historyorder/form", $data, true);
		}
		return $this->load->view("admin/historyorder/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
