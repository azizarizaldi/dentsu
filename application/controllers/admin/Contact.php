<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Contact extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/contact/form.js", $data, true);
		}
        
		return $this->load->view("admin/contact/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Contact List";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/contact/form", $data, true);
		}
		return $this->load->view("admin/contact/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$id_category 	= $gets["id_category"];
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "contact_name";
		
		$this->load->model("contact_model");
		$rows = $this->contact_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search,$id_category);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->contact_id = sprintf("%05d", $rows[$i]->contact_id);
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->contact_model->getCount($search,$id_category);
		$json["recordsFiltered"] = $this->contact_model->getCount($search,$id_category);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('5');
		$this->data["data_category"] = $rows;

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("contact_model");
		
		$row = $this->contact_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('5');
		$this->data["data_category"] = $rows;

		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave() {
		$post 				= $this->input->post();
		$json 				= new stdClass();
	
		$action				= $post['action'];
		$id_category		= $post['id_category'];
		$contact_name		= $post['contact_name'];
		$phone_primary		= $post['phone_primary'];
		$phone_alternative	= $post['phone_alternative'];
		$address			= $post['address'];
		$note				= $post['note'];

		$data['id_category']		= $id_category;
		$data['contact_name']		= $contact_name;		
		$data['phone_primary']		= $phone_primary;
		$data['phone_alternative']	= $phone_alternative;
		$data['address']			= $address;
		$data['note']				= $note;

		if($action == 'add') {
			$this->db->insert("contact",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$contact_id		= $post['contact_id'];
			$this->db->where("contact_id",$contact_id);
			$this->db->update("contact",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$contact_id	= $post['contact_id'];

		$this->db->where("contact_id",$contact_id);
		$this->db->delete("contact");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
