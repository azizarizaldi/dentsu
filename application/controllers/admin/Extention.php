<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Extention extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/extention/form.js", $data, true);
		}
        
		return $this->load->view("admin/extention/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Extention List";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/extention/form", $data, true);
		}
		return $this->load->view("admin/extention/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$id_category 	= $gets["id_category"];
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "extention_name";
		
		$this->load->model("extention_model");
		$rows = $this->extention_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search,$id_category);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->extention_id = sprintf("%05d", $rows[$i]->extention_id);
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->extention_model->getCount($search,$id_category);
		$json["recordsFiltered"] = $this->extention_model->getCount($search,$id_category);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('3');
		$this->data["data_category"] = $rows;

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("extention_model");
		
		$row = $this->extention_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('3');
		$this->data["data_category"] = $rows;

		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave() {
		date_default_timezone_set('Asia/Jakarta');

		$post 				= $this->input->post();
		$json 				= new stdClass();

		$config['upload_path']          = 'uploads/extention';
		$config['allowed_types']        = '*';
		$config['encrypt_name'] 		= TRUE;
	
		$this->load->library('upload', $config);
	
		$action				= $post['action'];
		$id_category		= $post['id_category'];
		$extention_name		= $post['extention_name'];
		$process_duration	= $post['process_duration'];
		$website			= $post['website'];
		$terms_condition	= $post['terms_condition'];
		$note				= $post['note'];
		$estimated_cost		= $post['estimated_cost'];

		$data['id_category']		= $id_category;
		$data['extention_name']		= $extention_name;
		$data['terms_condition']	= $terms_condition;
		$data['note']				= $note;
		$data['estimated_cost']		= $estimated_cost;
		$data['website']			= $website;
		$data['process_duration']	= $process_duration;

		if($action == 'add') {
			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
			}
			$data['created_at'] = date("Y-m-d H:i:s");
			$this->db->insert("extention",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$data['updated_at'] = date("Y-m-d H:i:s");

			$extention_id		= $post['extention_id'];
			$this->db->where("extention_id",$extention_id);
			$getextention = $this->db->get('extention')->row();

			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					if(!empty($getextention->file_attachment)) {
						$path_to_file = 'uploads/extention/'.$getextention->file_attachment;
						unlink($path_to_file);	
					}
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
			}
			$this->db->where("extention_id",$extention_id);
			$this->db->update("extention",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$extention_id	= $post['extention_id'];

		$this->db->where("extention_id",$extention_id);
		$getextention = $this->db->get('extention')->row();

		if(!empty($getextention->file_attachment)) {
			$path_to_file = 'uploads/extention/'.$getextention->file_attachment;
			unlink($path_to_file);	
		}

		$this->db->where("extention_id",$extention_id);
		$this->db->delete("extention");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
