<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class DocumentIn extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_in/form.js", $data, true);
		}
        
		return $this->load->view("admin/document_in/list.js", "", true);
    }
	
	public function index()
	{
		$this->load->model("packagetype_model");
		$rows = $this->packagetype_model->getCourierCompanies();
		
		$this->data["couriers"] = $rows;
		
		parent::index();

	}
	
	protected function getTitle()
	{
		return "Document In";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_in/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/document_in/list", "", true);
	}
	
	
	protected function addForm()
	{
		$this->load->model("order_model");
		
		$rows = $this->order_model->getReceiverNames(3);
		$this->data['documentInSenders'] = $rows;
		
		$rows = $this->order_model->getReceiverAddress(3);
		$this->data['documentInCouriers'] = $rows;
		
		parent::addForm();
	}
	
	public function edit()
	{
		$this->load->model("order_model");
		
		$rows = $this->order_model->getReceiverNames(3);
		$this->data['documentInSenders'] = $rows;
		
		$rows = $this->order_model->getReceiverAddress(3);
		$this->data['documentInCouriers'] = $rows;
		
		parent::edit();
	}
	
	protected function getPhotoDir()
	{
		return "document_in";
	}
	

}
