<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Inventory_report extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/inventory_report/form.js", $data, true);
		}
        
		return $this->load->view("admin/inventory_report/list.js", "", true);
	}
	
	protected function getTitle() {
		return "Inventory Report List";
	}
		
	protected function getMainContent() {
		return $this->load->view("admin/inventory_report/list", "", true);
	}
	public function get() {
		$gets 				= $this->input->get();				
		$order 				= $gets["order"];
		$columns 			= $gets["columns"];
		$search 			= $gets['search']['value'];		
		$column 			= $columns[$order[0]["column"]];	
	
		$limit				= $gets["length"];
		$offset				= $gets["start"];

		$this->db->select("
			stationary_item.item_description,
			stationary_item.item_stock , 
			stationary_item.item_min_stock ,
			stationary_item.item_ideal_stock , 
			stationary_item.item_price ,
			(stationary_item.item_price*stationary_item.item_stock) AS total_value,
			stationary_category.category_name
		");
		
		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search})", null, false);
		}

		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		
		}
   
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$rows 			=  $this->db->get("stationary_item")->result();		

		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->item_price 	= "Rp. ".number_format($rows[$i]->item_price, 0, '', '.').',- '; 
			$rows[$i]->total_value 	= "Rp. ".number_format($rows[$i]->total_value, 0, '', '.').',- '; 
		}

		$search = trim($search);
		if ($search) {
			$search = $this->db->escape('%'.$search.'%');
			$this->db->where("(item_description LIKE ${search})", null, false);
		}
   
		$this->db->join("stationary_category","stationary_category.category_id = stationary_item.item_category","left");
		$rows_number 			=  $this->db->get("stationary_item")->num_rows();		

		$json["draw"] 				= $gets["draw"]+1;
		$json["recordsTotal"] 		= $rows_number;
		$json["recordsFiltered"] 	= $rows_number;
		$json["data"] 				= $rows;
		
		echo json_encode($json);

	}

	public function addForm() {

	}
	public function doSave() {

	}
}
