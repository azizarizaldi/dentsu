<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Logistic extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/logistic/form.js", $data, true);
		}
        
		return $this->load->view("admin/logistic/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Logistic Company";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/logistic/form", $data, true);
		}
		return $this->load->view("admin/logistic/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("logistic_model");
		$rows = $this->logistic_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->logistic_model->getCount($search);
		$json["recordsFiltered"] = $this->logistic_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("logistic_model");
		
		$row = $this->logistic_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		/*
		$posts['logistic_company'] = "JNE";
		$posts['logistic_branch'] = "JNE Jatiasih";
		$posts['logistic_cp'] = "Abah";
		$posts['logistic_phone'] = "081385745997";
*/
		$json = new stdClass();
		
		if (! isset($posts["logistic_company"]) || ! isset($posts["logistic_branch"]) || ! $posts["logistic_company"]|| ! $posts["logistic_branch"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("logistic_model");
			
			if ($this->logistic_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "External Courier Branch is alread exist";

			}
			else
			{
				if (isset($posts["logistic_id"]))
				{
					$this->logistic_model->update($posts);
				
					$json->status = true;
					$json->message = "External Courier has been updated";
				}
				else
				{
					$posts["logistic_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->logistic_model->insert($posts);
				
					$json->status = true;
					$json->message = "External Courier has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['logistic_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("logistic_model");
		$this->logistic_model->delete($posts['logistic_id']);
							
		$json->status = true;
		$json->message = "External Courier has been deleted.";
		
		echo json_encode($json);
	}
	
	function getCompanies()
	{
		$this->load->model("logistic_model");
		
		$rows = $this->logistic_model->getCompanies();
		
		echo json_encode($rows);
	}
}
