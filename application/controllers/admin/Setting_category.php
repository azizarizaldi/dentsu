<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Setting_category extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/setting_category/form.js", $data, true);
		}
        
		return $this->load->view("admin/setting_category/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Setting Category";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/setting_category/form", $data, true);
		}
		return $this->load->view("admin/setting_category/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "id";
		
		$this->load->model("setting_category_model");
		$rows = $this->setting_category_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->id = sprintf("%05d", $rows[$i]->id);
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->setting_category_model->getCount($search);
		$json["recordsFiltered"] = $this->setting_category_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("setting_category_model");
		
		$row = $this->setting_category_model->getById($gets['id']);
		
		$this->data["row"] 	= $row;
		$this->action 		= "edit";
		$this->index();
	}
	
	protected function doSave() {
		$post 				= $this->input->post();
		$json 				= new stdClass();
			
		$action				= $post['action'];
		$name				= $post['name'];
		$type_category		= $post['type_category'];

		$data['name']				= $name;
		$data['type_category']		= $type_category;

		if($action == 'add') {
			$data['created_at']		= date("Y-m-d H:i:s");
			$this->db->insert("setting_category",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$id		= $post['id'];
			$this->db->where("id",$id);
			$this->db->update("setting_category",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$id			= $post['id'];

		$this->db->where("id",$id);
		$this->db->delete("setting_category");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
