<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class AppointmentOrder extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/appointmentorder/form.js", $data, true);
		}
        
		return $this->load->view("admin/appointmentorder/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Appointment";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/appointmentorder/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/appointmentorder/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("user_model");
		
		$rows = $this->user_model->get("user_name", "asc", 0, 0, "", true);
		
		$this->data["visitors"] = $rows;
		
		parent::addForm();
	}

	public function edit()
	{
		$this->load->model("user_model");
		
		$rows = $this->user_model->get("user_name", "asc", 0, 0, "", true);
		
		$this->data["visitors"] = $rows;

		parent::edit();
	}
}
