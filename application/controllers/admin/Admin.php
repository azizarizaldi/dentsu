<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Admin extends CI_Controller {
    var $data;

    abstract protected function getTitle();
    abstract protected function getMainContent();
    abstract protected function addForm();
    abstract protected function doSave();
    protected function getPhotoDir() {}
    
    public function __construct(){
        parent::__construct();
        
        if (! $this->session->userdata('row'))
        {
            header("Location: ".base_url());
            return;
        }
        
        $row  = $this->session->userdata('row');
        
        $this->load->model("user_model");
        if (! $this->user_model->isAdmin($row->user_type))
        {
            header("Location: ".base_url());
            return;
        }

        $this->data["created"] = date("M, Y", mysqlDateToInt($row->user_created_date));
 
    }
    
    protected function getJSContent()
    {
        return "";
    }
    
    public function index()
	{
        $this->load->model("user_model");
     
        $this->data['levelName'] = $this->user_model->getLevelName($this->session->userdata("row")->user_type);
        $this->data['profilePicture'] = $this->session->userdata("row")->user_photo ? sprintf("%s%s", base_url(), $this->session->userdata("row")->user_photo) : sprintf("%sassets/app/userdef.png", base_url());
        $this->data["session"] = $this->session->userdata("row");
        $this->data["canModify"] = $this->user_model->canModify($this->data["session"]->user_type);
        $this->data["isSuperAdmin"] = $this->user_model->isSuperAdmin($this->data["session"]->user_type);
        $this->data["title"] = $this->getTitle();
        $this->data["menu"] = $this->load->view("admin/menu", $this->data, true);
        $this->data["mainContent"] = $this->getMainContent();
        $this->data["jsContent"] = $this->getJSContent();
        
		$this->load->view('admin/index', $this->data);
	}
    
    public function add()
    {
        $this->load->model("user_model");
        
        $row = $this->session->userdata("row");
        if (! $this->user_model->canModify($row->user_type))
        {
            return;
        }
        
        $this->addForm();
    }
    
    public function save()
    {
        $this->load->model("user_model");
        
        $row = $this->session->userdata("row");
        if (! $this->user_model->canModify($row->user_type))
        {
            return;
        }
        
        $this->doSave();
    }
    
    function savePhoto()
	{
        $path = $this->config->item('basePath');
        
		foreach($_FILES as $file)
		{
			$name = sprintf("%s%s%s", uniqid(),date("YmdHisv"),basename($file['name']));
			
            $dir = sprintf("uploads/tmp/%s", $this->getPhotoDir());
			$filename = sprintf("uploads/tmp/%s/%s", $this->getPhotoDir(), $name);
            
            if (! is_dir($path.$dir))
            {
                mkdir($path.$dir);
                chmod($path.$dir, 0777);
            }
            
            $dir = sprintf("uploads/%s", $this->getPhotoDir());
			$targetfilename = sprintf("uploads/%s/%s", $this->getPhotoDir(), $name);
            
            if (! is_dir($path.$dir))
            {
                mkdir($path.$dir);
                chmod($path.$dir, 0777);
            }
			
			move_uploaded_file($file['tmp_name'], "${path}/${filename}");
			
			$json = new stdClass();
			$json->src = $filename;
			$json->photo = $targetfilename;
			$json->error = false;
			
			echo json_encode($json);
			return;
		}
	}
    
}
