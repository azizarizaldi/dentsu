<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Document extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document/form.js", $data, true);
		}
        
		return $this->load->view("admin/document/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Document List";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document/form", $data, true);
		}
		return $this->load->view("admin/document/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$id_category 	= $gets["id_category"];
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "document_name";
		
		$this->load->model("document_model");
		$rows = $this->document_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search,$id_category);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->document_id = sprintf("%05d", $rows[$i]->document_id);
			$rows[$i]->validate_period = tgl($rows[$i]->start_date,'02').' s/d '.tgl($rows[$i]->end_date,'02');
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->document_model->getCount($search,$id_category);
		$json["recordsFiltered"] = $this->document_model->getCount($search,$id_category);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('2');
		$this->data["data_category"] = $rows;

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("document_model");
		
		$row = $this->document_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('2');
		$this->data["data_category"] = $rows;

		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave() {
		date_default_timezone_set('Asia/Jakarta');

		$post 				= $this->input->post();
		$json 				= new stdClass();

		$config['upload_path']          = 'uploads/document';
		$config['allowed_types']        = '*';
		$config['encrypt_name'] 		= TRUE;
	
		$this->load->library('upload', $config);
	
		$action				= $post['action'];
		$id_category		= $post['id_category'];
		$document_name		= $post['document_name'];
		$range_date			= $post['range_date'];
		$terms_condition	= $post['terms_condition'];
		$note				= $post['note'];
		$vendor_id			= $post['vendor_id'];
		$reminder_day		= $post['reminder_day'];

		if($range_date) {
			$string 		= explode('-',$range_date);

			$date1 			= explode('/',$string[0]);
			$date2 			= explode('/',$string[1]);

			$year			= str_replace(' ','',$date1[2]);
			$month			= str_replace(' ','',$date1[0]);
			$day			= str_replace(' ','',$date1[1]);

			$lastYear		= str_replace(' ','',$date2[2]);
			$lastMonth		= str_replace(' ','',$date2[0]);
			$lastDay		= str_replace(' ','',$date2[1]);

			$firstDate		= $year.'-'.$month.'-'.$day;
			$lastDate		= $lastYear.'-'.$lastMonth.'-'.$lastDay;		  
		}

		$data['id_category']		= $id_category;
		$data['document_name']		= $document_name;
		$data['terms_condition']	= $terms_condition;
		$data['note']				= $note;
		$data['vendor_id']			= $vendor_id;
		$data['reminder_day']		= $reminder_day;
		$data['start_date']			= $firstDate;
		$data['end_date']			= $lastDate;

		if($action == 'add') {
			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
				/*else{				
					$error = array('error' => $this->upload->display_errors());
					var_dump($error);
				}*/
			}
			$data['created_at'] = date("Y-m-d H:i:s");
			$this->db->insert("document",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$data['updated_at'] = date("Y-m-d H:i:s");

			$document_id		= $post['document_id'];
			$this->db->where("document_id",$document_id);
			$getdocument = $this->db->get('document')->row();

			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					if(!empty($getdocument->file_attachment)) {
						$path_to_file = 'uploads/document/'.$getdocument->file_attachment;
						unlink($path_to_file);	
					}
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
				/*else{				
					$error = array('error' => $this->upload->display_errors());
					var_dump($error);
				}*/
			}
			$this->db->where("document_id",$document_id);
			$this->db->update("document",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$document_id	= $post['document_id'];

		$this->db->where("document_id",$document_id);
		$getdocument = $this->db->get('document')->row();

		if(!empty($getdocument->file_attachment)) {
			$path_to_file = 'uploads/document/'.$getdocument->file_attachment;
			unlink($path_to_file);	
		}

		$this->db->where("document_id",$document_id);
		$this->db->delete("document");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
