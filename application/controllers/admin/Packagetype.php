<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class PackageType extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/package_type/form.js", $data, true);
		}
        
		return $this->load->view("admin/package_type/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Courier Package Type";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/package_type/form", $data, true);
		}
		return $this->load->view("admin/package_type/list", "", true);
	}
	
	public function index()
	{
		$this->load->model("logistic_model");
		
		$companies = $this->logistic_model->get("logistic_company", "asc");
		$this->data["companies"] = $companies;
		
		parent::index();
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		$company = isset($gets['company']) ? $gets['company'] : "";
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("packagetype_model");
		$rows = $this->packagetype_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search, $company);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->packagetype_model->getCount($company, $search);
		$json["recordsFiltered"] = $this->packagetype_model->getCount($company, $search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	public function getByCompany()
	{
		$gets = $this->input->get();
		
		$this->load->model("brand_unit_model");
		echo json_encode($this->brand_unit_model->getByCompany($gets["id"]));
	}
	
	protected function addForm()
	{
		$this->load->model("logistic_model");
		
		$companies = $this->logistic_model->get("logistic_company", "asc");
		$this->data["companies"] = $companies;
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("logistic_model");
		
		$companies = $this->logistic_model->get("logistic_company", "asc");
		$this->data["companies"] = $companies;
		
		$this->load->model("packagetype_model");
		$row = $this->packagetype_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		$json = new stdClass();
		
		if (! isset($posts["package_type_company"]) || ! isset($posts["package_type_name"]) || ! $posts['package_type_company'] || !  $posts['package_type_name'])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("packagetype_model");
			
			if ($this->packagetype_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Package Name is alread exist";

			}
			else
			{
				if (isset($posts["package_type_id"]))
				{
					$this->packagetype_model->update($posts);
				
					$json->status = true;
					$json->message = "Package Type has been updated";
				}
				else
				{
					$posts["package_type_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->packagetype_model->insert($posts);
				
					$json->status = true;
					$json->message = "Package type has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['package_type_id'])) return;
		
		$json = new stdClass();		

		$this->db->where("package_type_id", $posts['package_type_id']);
        $this->db->delete("courier_package_type");
							
		$json->status = true;
		$json->message = "Data has been deleted.";
		
		echo json_encode($json);
	}
}
