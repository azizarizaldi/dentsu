<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class StationaryOrder extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/stationaryorder/form.js", $data, true);
		}
        
		return $this->load->view("admin/stationaryorder/list.js", "", true);
    }

    public function index()
	{
		$this->load->model("user_model");
        
		$this->data["officeboys"] = $this->user_model->getOfficeBoys();	
		
		parent::index();

	}

	protected function getTitle()
	{
		return "Stationary Order";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/stationaryorder/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/stationaryorder/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("user_model");
		
		$rows = $this->user_model->getOfficeBoys();
		
		$this->data["officeBoys"] = $rows;
		
		parent::addForm();
	}

	public function edit()
	{
		$rows = $this->user_model->getOfficeBoys();
		
		$this->data["officeBoys"] = $rows;

		parent::edit();
	}

		protected function getPhotoDir()
	{
		return "document_external";
	}
}
