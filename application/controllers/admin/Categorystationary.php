<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class CategoryStationary extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/categorystationary/form.js", $data, true);
		}
        
		return $this->load->view("admin/categorystationary/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Category Stationary";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/categorystationary/form", $data, true);
		}
		return $this->load->view("admin/categorystationary/list", "", true);
	}
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("categorystationary_model");
		
		$row = $this->categorystationary_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		/*
		$posts['logistic_company'] = "JNE";
		$posts['logistic_branch'] = "JNE Jatiasih";
		$posts['logistic_cp'] = "Abah";
		$posts['logistic_phone'] = "081385745997";
*/
		$json = new stdClass();
		
		if (! isset($posts["category_name"]) || ! $posts["category_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("categorystationary_model");
			
			if ($this->categorystationary_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Stationary category is alread exist";

			}
			else
			{
				if ($posts['category_pic'])
				{
					$path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["category_pic"]);
				}
				else
				{
					unset($posts['category_pic']);
				}

				unset($posts['tmp_photo']);


				if (isset($posts["category_id"]))
				{
					$this->categorystationary_model->update($posts);
				
					$json->status = true;
					$json->message = "Stationary Category has been updated";
				}
				else
				{
					$posts["category_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->categorystationary_model->insert($posts);
				
					$json->status = true;
					$json->message = "Stationary Category has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['category_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("categorystationary_model");
		$this->categorystationary_model->delete($posts['category_id']);
							
		$json->status = true;
		$json->message = "Stationary Category has been deleted.";
		
		echo json_encode($json);
	}
	
	function setDriver()
	{
		$posts = $this->input->post();
		
		if (! isset($posts['carId']) || ! isset($posts['date1']) || ! isset($posts['driverId']) || ! $posts['carId'] || ! $posts['date1'] || ! $posts['driverId'])
		{
			return;
		}
		
		$date1 = dateYYYYMMDDToString($posts['date1']);
		$date2 = (isset($posts['date2']) && $posts['date2'] > 0) ? dateYYYYMMDDToString($posts['date2']) : "2999-12-31";
		
		$json = new stdClass();
	
		$this->load->model("cardriver_model");
		
		$json->status = $this->cardriver_model->isValidData($posts['carId'], $date1, $date2, $posts['driverId']);
		$json->message = $json->status ? "Driver has been set" : "Driver already exist";
		
		if ($json->status)
		{
			$data['car_driver_car'] = $posts['carId'];
			$data['car_driver_user'] = $posts['driverId'];
			$data['car_driver_start_date'] = $date1." 00:00:00";
			$data['car_driver_end_date'] = $date2." 23:59:59";
			
			$this->cardriver_model->insert($data);
		}
		
		echo json_encode($json);
	}

	protected function getPhotoDir()
	{
		return "stationary";
	}
	
}
