<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class BrandUnit extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/brandunit/form.js", $data, true);
		}
        
		return $this->load->view("admin/brandunit/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Brand Unit";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/brandunit/form", $data, true);
		}
		return $this->load->view("admin/brandunit/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("brand_unit_model");
		$rows = $this->brand_unit_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->brand_unit_model->getCount($search);
		$json["recordsFiltered"] = $this->brand_unit_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	public function getByCompany()
	{
		$gets = $this->input->get();
		
		$this->load->model("brand_unit_model");
		echo json_encode($this->brand_unit_model->getByCompany($gets["id"]));
	}
	
	protected function addForm()
	{
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->load->model("brand_unit_model");
		$row = $this->brand_unit_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		$json = new stdClass();
		
		if (! isset($posts["brand_unit_company"]) || ! isset($posts["brand_unit_name"]) || ! $posts["brand_unit_name"]|| ! $posts["brand_unit_company"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("brand_unit_model");
			
			if ($this->brand_unit_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Brand Unit is alread exist";

			}
			else
			{
				if (isset($posts["brand_unit_id"]))
				{
					$this->brand_unit_model->update($posts);
				
					$json->status = true;
					$json->message = "Brand Unit has been updated";
				}
				else
				{
					$posts["brand_unit_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->brand_unit_model->insert($posts);
				
					$json->status = true;
					$json->message = "Brand Unit has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['brand_unit_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("dept_model");
		$count = $this->dept_model->getCountByBrandUnit($posts['brand_unit_id']);
		if ($count > 0)
		{
			$json->status = false;
			$json->message = "You can't delete this data.";	
		}
		else
		{
			$this->load->model("brand_unit_model");
			$this->brand_unit_model->delete($posts['brand_unit_id']);
								
			$json->status = true;
			$json->message = "Brand Unit has been deleted.";
		}
		
		echo json_encode($json);
	}
}
