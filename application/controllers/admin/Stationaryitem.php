<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class StationaryItem extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/stationaryitem/form.js", $data, true);
		}
        
		return $this->load->view("admin/stationaryitem/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Stationary";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/stationaryitem/form", $data, true);
		}
		return $this->load->view("admin/stationaryitem/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("categorystationary_model");

		$this->data['categories'] = $this->categorystationary_model->get("category_name", "asc");

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();

		$this->load->model("categorystationary_model");

		$this->data['categories'] = $this->categorystationary_model->get("category_name", "asc");
		
		$this->load->model("stationaryitem_model");
		
		$row = $this->stationaryitem_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		$json = new stdClass();
		
		if (! isset($posts["item_description"]) || ! $posts["item_description"] || ! isset($posts["item_category"]) || ! $posts["item_category"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("stationaryitem_model");
			
			if ($this->stationaryitem_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Stationary item is alread exist";

			}
			else
			{
				if ($posts['item_picture'])
				{
					$path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["item_picture"]);
				}
				else
				{
					unset($posts['item_picture']);
				}

				unset($posts['tmp_photo']);


				if (isset($posts["item_id"]))
				{
					$this->stationaryitem_model->update($posts);
				
					$json->status = true;
					$json->message = "Stationary Item has been updated";
				}
				else
				{
					$posts["item_update_by"] = $this->session->userdata("row")->user_id;
					
					$this->stationaryitem_model->insert($posts);
				
					$json->status = true;
					$json->message = "Stationary Item has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['item_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("stationaryitem_model");
		$this->stationaryitem_model->delete($posts['item_id']);
							
		$json->status = true;
		$json->message = "Stationary Item has been deleted.";
		
		echo json_encode($json);
	}
	

	protected function getPhotoDir()
	{
		return "stationary_item";
	}
	
	public function warning() {
		echo 'Page is under maintenance...';
	}
}
