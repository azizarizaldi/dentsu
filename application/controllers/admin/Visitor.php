<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/User.php';

class Visitor extends User {
	protected function getTitle()
	{
		return "Visitor";
	}

	protected function getPath()
    {
    	return "visitor";
    }

    protected function isVisitor()
	{
		return true;
	}

	protected function getPhotoDir()
	{
		return "visitor";
	}
}
