<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Dept extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data["action"] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/dept/form.js", $data, true);
		}
        
		return $this->load->view("admin/dept/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Departement";
	}
	
	protected function getMainContent()
	{
		$data["action"] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/dept/form", $data, true);
		}
		return $this->load->view("admin/dept/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("dept_model");
		$rows = $this->dept_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->dept_model->getCount($search);
		$json["recordsFiltered"] = $this->dept_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	protected function addForm()
	{
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->load->model("dept_model");
		$row = $this->dept_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		$json = new stdClass();
		
		if (! isset($posts["departement_brand_unit"]) || ! isset($posts["departement_name"]) || ! $posts["departement_brand_unit"]|| ! $posts["departement_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("dept_model");
			
			if ($this->dept_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Departement is alread exist";

			}
			else
			{
				if (isset($posts["departement_id"]))
				{
					$this->dept_model->update($posts);
				
					$json->status = true;
					$json->message = "Departement has been updated";
				}
				else
				{
					$posts["departement_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->dept_model->insert($posts);
				
					$json->status = true;
					$json->message = "Departement has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['departement_id'])) return;
		
		$this->load->model("dept_model");
		$this->dept_model->delete($posts['departement_id']);
				
		$json = new stdClass();		
				
		$json->status = true;
		$json->message = "Departement has been deleted.";
		
		echo json_encode($json);
	}
	
	function getByBrandUnit()
	{
		$gets = $this->input->get();
		
		$this->load->model("dept_model");
		$rows = $this->dept_model->getByBrandUnit($gets["id"]);
		
		echo json_encode($rows);
	}
}
