<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Visitor_report extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/visitor_report/form.js", $data, true);
		}
        
		return $this->load->view("admin/visitor_report/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Book / Month";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/visitor_report/form", $data, true);
		}
		return $this->load->view("admin/visitor_report/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
	
	public function changeBrandUnit() {
		$post = $this->input->post(null,true);
		$this->db->where("brand_unit_company",$post['company_id']);
		foreach($this->db->get('brand_unit')->result() as $get) { 
			echo '<option value="'.$get->brand_unit_id.'">'.$get->brand_unit_name.'</option>';
		}
	}
}
