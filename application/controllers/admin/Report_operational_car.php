<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Report_operational_car extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_operational_car/form.js", $data, true);
		}
        
		return $this->load->view("admin/report_operational_car/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Book / Month";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_operational_car/form", $data, true);
		}
		return $this->load->view("admin/report_operational_car/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
