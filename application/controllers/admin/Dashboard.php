<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Dashboard extends Admin {
	protected function getTitle()
	{
		return "Dashboard";
	}
	
	protected function getMainContent()
	{
		$this->db->where("item_stock < item_min_stock");
		$this->db->join("stationary_category", "category_id = item_category");
		$warning_stock =  $this->db->get("stationary_item")->num_rows();

		$this->db->where("user_type","50");
		$visitors =  $this->db->get("_user")->num_rows();

		$this->db->where("user_type <>", 99);
		$this->db->where("user_type <>", 50);
		$total_users =  $this->db->get("_user")->num_rows();

		$stationary_item =  $this->db->get("stationary_item")->num_rows();

		$this->db->where("order_type","5");
		$this->db->where("order_status","1");
		$meeting_room =  $this->db->get("order")->num_rows();

		$this->db->where("order_type","4");
		$this->db->where("order_status","1");
		$operational_car =  $this->db->get("order")->num_rows();

		$this->db->where("order_type","1");
		$this->db->where("order_status","1");
		$doc_in =  $this->db->get("order")->num_rows();

		$this->db->where("order_status","1");
		$this->db->where("order_type","2");
		$doc_ex =  $this->db->get("order")->num_rows();

		$this->db->where("order_type","3");
		$this->db->where("order_status","1");
		$courier =  $this->db->get("order")->num_rows();

		$this->db->where("order_type","6");
		$this->db->where("order_status","1");
		$stationary_order =  $this->db->get("order")->num_rows();

		$this->db->where("order_type","7");
		$this->db->where("order_status","1");
		$appointment =  $this->db->get("order")->num_rows();
		
		$data['warning_stock'] 		= $warning_stock;
		$data['meeting_room'] 		= $meeting_room;
		$data['operational_car'] 	= $operational_car;
		$data['doc_in'] 			= $doc_in;
		$data['doc_ex'] 			= $doc_ex;
		$data['courier'] 			= $courier;
		$data['stationary_order'] 	= $stationary_order;
		$data['visitors'] 			= $appointment;
		$data['total_users'] 		= $total_users;
		$data['stationary_item'] 	= $stationary_item;

		return $this->load->view("admin/dashboard", $data, true);;
	}
	
	protected function addForm() {}
	protected function doSave() {}
}
