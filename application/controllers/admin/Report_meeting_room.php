<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Report_meeting_room extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_meeting_room/form.js", $data, true);
		}
        
		return $this->load->view("admin/report_meeting_room/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Book / Month";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_meeting_room/form", $data, true);
		}
		return $this->load->view("admin/report_meeting_room/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
