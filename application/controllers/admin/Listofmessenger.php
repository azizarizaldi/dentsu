<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Listofmessenger extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/listofmessenger/form.js", $data, true);
		}
        
		return $this->load->view("admin/listofmessenger/list.js", "", true);
    }
	
	protected function getTitle() {
		return "List Of Messenger";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/listofmessenger/form", $data, true);
		}
		return $this->load->view("admin/listofmessenger/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
