<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Billboard extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data["action"] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/".$this->getPath()."/form.js", $data, true);
		}
        
		return $this->load->view("admin/".$this->getPath()."/list.js", "", true);
    }

    protected function getPath()
    {
    	return "billboard";
    }
	
	protected function getTitle()
	{
		return "Dashboard Banner";
	}
	
	protected function getMainContent()
	{
		$data["action"] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/".$this->getPath()."/form", $data, true);
		}
		return $this->load->view("admin/".$this->getPath()."/list", "", true);
	}

	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];

		
		$this->load->model("banner_model");
		$rows = $this->banner_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"]);
		
		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->billboard_banner_url = sprintf("<a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", base_url(), $rows[$i]->billboard_banner, base_url(), $rows[$i]->billboard_banner);
		}
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->banner_model->getCount();
		$json["recordsFiltered"] = $this->banner_model->getCount();
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	protected function addForm()
	{
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("banner_model");
		$row = $this->banner_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		$json = new stdClass();

		if (! $posts['billboard_banner']) return;
		
		$this->load->model("banner_model");
			
		$path = $this->config->item('basePath');
		@copy($path.$posts["tmp_banner"], $path.$posts["billboard_banner"]);

		unset($posts['tmp_banner']);
				
		if (isset($posts["billboard_id"]))
		{
					
			$this->banner_model->update($posts);
				
			$json->status = true;
			$json->message = $this->getTitle()." has been updated";
		}
		else
		{
			$posts["billboard_created_by"] = $this->session->userdata("row")->user_id;
					
			$this->banner_model->insert($posts);
				
			$json->status = true;
			$json->message = $this->getTitle()." has been added.";
		}
		
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['billboard_id'])) return;
		
		$this->load->model("banner_model");
		$this->banner_model->delete($posts['billboard_id']);
				
		$json = new stdClass();		
				
		$json->status = true;
		$json->message = "Data has been deleted.";
		
		echo json_encode($json);
	}
	
	protected function getPhotoDir()
	{
		return "billboard";
	}
}
