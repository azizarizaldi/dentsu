<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class BookingMeetingRoom extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/bookingmeetingroom/form.js", $data, true);
		}
        
		return $this->load->view("admin/bookingmeetingroom/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Booking Meeting Room";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/bookingmeetingroom/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/bookingmeetingroom/list", "", true);
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("order_model");
		
		$rows = $this->order_model->getAttendanceList($gets['id']);
		for($i=0; $i < count($rows); $i++)
		{
			$users['user'.$rows[$i]->user_id] = true;
		}
		
		$this->data["attendances"] = isset($users) ? $users : array();
		
		parent::edit();
	}
}
