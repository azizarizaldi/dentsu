<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Employee extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/employee/form.js", $data, true);
		}
        
		return $this->load->view("admin/employee/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Employee List";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/employee/form", $data, true);
		}
		return $this->load->view("admin/employee/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$id_category 	= $gets["id_category"];
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "employee_name";
		
		$this->load->model("employee_model");
		$rows = $this->employee_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search,$id_category);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->employee_id = sprintf("%05d", $rows[$i]->employee_id);
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->employee_model->getCount($search,$id_category);
		$json["recordsFiltered"] = $this->employee_model->getCount($search,$id_category);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('4');
		$this->data["data_category"] = $rows;

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("employee_model");
		
		$row = $this->employee_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('4');
		$this->data["data_category"] = $rows;

		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave() {
		$post 				= $this->input->post();
		$json 				= new stdClass();
	
		$action				= $post['action'];
		$id_category		= $post['id_category'];
		$employee_name		= $post['employee_name'];
		$phone_primary		= $post['phone_primary'];
		$phone_alternative	= $post['phone_alternative'];
		$address			= $post['address'];
		$note				= $post['note'];
		$title				= $post['title'];

		$data['id_category']		= $id_category;
		$data['title']				= $title;
		$data['employee_name']		= $employee_name;		
		$data['phone_primary']		= $phone_primary;
		$data['phone_alternative']	= $phone_alternative;
		$data['address']			= $address;
		$data['note']				= $note;


		$config['upload_path']          = 'uploads/employee';
		$config['allowed_types']        = '*';
		$config['encrypt_name'] 		= TRUE;
	
		$this->load->library('upload', $config);

		if($action == 'add') {
			if (!empty($_FILES['file_photo']['name'])) {
				if ($this->upload->do_upload('file_photo')){
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_photo'] 	= $file_name;
				}
			}

			$this->db->insert("employee",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$employee_id		= $post['employee_id'];
			$this->db->where("employee_id",$employee_id);
			$getemployee = $this->db->get('employee')->row();

			if (!empty($_FILES['file_photo']['name'])) {
				if ($this->upload->do_upload('file_photo')){
					if(!empty($getemployee->file_photo)) {
						$path_to_file = 'uploads/employee/'.$getemployee->file_photo;
						unlink($path_to_file);	
					}
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_photo'] 	= $file_name;
				}
			}

			$this->db->where("employee_id",$employee_id);
			$this->db->update("employee",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$employee_id	= $post['employee_id'];

		$this->db->where("employee_id",$employee_id);
		$getemployee = $this->db->get('employee')->row();

		if(!empty($getemployee->file_photo)) {
			$path_to_file = 'uploads/employee/'.$getemployee->file_photo;
			unlink($path_to_file);	
		}

		$this->db->where("employee_id",$employee_id);
		$this->db->delete("employee");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
