<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Company extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/company/form.js", $data, true);
		}
        
		return $this->load->view("admin/company/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Company";
	}
	
	protected function getMainContent()
	{
		switch($this->action)
		{
			case "add":
				return $this->load->view("admin/company/add", "", true);
				break;
			case "edit":
				return $this->load->view("admin/company/edit", "", true);
				break;
		}
		return $this->load->view("admin/company/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("company_model");
		$rows = $this->company_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->company_model->getCount($search);
		$json["recordsFiltered"] = $this->company_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("company_model");
		
		$row = $this->company_model->getById($gets["id"]);
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		
		$json = new stdClass();
		
		//$posts['company_name'] = "test";
		
		if (! isset($posts["company_name"]) || ! $posts["company_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("company_model");
			
			if ($this->company_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Company \"${posts['company_name']}\" is already exist.";
			}
			else
			{
				if (isset($posts["company_id"]))
				{
					$this->company_model->update($posts);
				
					$json->status = true;
					$json->message = "Company has updated.";
				}
				else
				{
					$posts["company_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->company_model->insert($posts);
				
					$json->status = true;
					$json->message = "Company has added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['company_id'])) return;
		
		$json = new stdClass();
		
		$this->load->model("brand_unit_model");
		$count = $this->brand_unit_model->getCountByCompany($posts['company_id']);
		if ($count > 0)
		{
			$json->status = false;
			$json->message = "You can't delete this data.";	
		}
		else
		{
			$this->load->model("company_model");
			$this->company_model->delete($posts['company_id']);
				
			$json->status = true;
			$json->message = "Company has been deleted.";	
		}
		
		echo json_encode($json);
	}

}
