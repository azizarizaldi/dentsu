<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Stationary_by_category extends Admin {
	var $action;

	protected function getJSContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_stationary/by_category/form.js", $data, true);
		}
        
		return $this->load->view("admin/report_stationary/by_category/list.js", "", true);
    }
	
	protected function getTitle() {
		return "Report Stationary";
	}
	
	protected function getMainContent() {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/report_stationary/by_category/form", $data, true);
		}
		return $this->load->view("admin/report_stationary/by_category/list", $data, true);
	}
	
	protected function addForm() {
	}
	
	protected function doSave()
	{
	}
}
