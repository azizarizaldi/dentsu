<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class MeetingRoom extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/meetingroom/form.js", $data, true);
		}
        
		return $this->load->view("admin/meetingroom/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Meeting Room";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/meetingroom/form", $data, true);
		}
		return $this->load->view("admin/meetingroom/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		if ($column["data"] == "name")
		{
			$orderBy = "meeting_room_name";
		}
		else
		{
			$orderBy = $column["data"];
		}
		$this->load->model("meetingroom_model");
		$rows = $this->meetingroom_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search);

		for($i=0; $i < count($rows); $i++)
		{
			if ($rows[$i]->meeting_room_pic)
			{
				$rows[$i]->name = sprintf("%s<br /><a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", $rows[$i]->meeting_room_name, base_url(), $rows[$i]->meeting_room_pic, base_url(), $rows[$i]->meeting_room_pic);
			}
			else
			{
				$rows[$i]->name = $rows[$i]->meeting_room_name;
			}
		}
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->meetingroom_model->getCount($search);
		$json["recordsFiltered"] = $this->meetingroom_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("meetingroom_model");
		
		$row = $this->meetingroom_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		/*
		$posts['logistic_company'] = "JNE";
		$posts['logistic_branch'] = "JNE Jatiasih";
		$posts['logistic_cp'] = "Abah";
		$posts['logistic_phone'] = "081385745997";
*/
		$json = new stdClass();
		
		if (! isset($posts["meeting_room_name"]) || ! $posts["meeting_room_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("meetingroom_model");
			
			if ($this->meetingroom_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Meeting Room is alread exist";

			}
			else
			{
				if ($posts['meeting_room_pic'])
				{
					$path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["meeting_room_pic"]);
				}
				else
				{
					unset($posts['meeting_room_pic']);
				}

				unset($posts['tmp_photo']);


				if (isset($posts["meeting_room_id"]))
				{
					$this->meetingroom_model->update($posts);
				
					$json->status = true;
					$json->message = "Meeting room has been updated";
				}
				else
				{
					$posts["meeting_room_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->meetingroom_model->insert($posts);
				
					$json->status = true;
					$json->message = "Meeting Room has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['meeting_room_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("meetingroom_model");
		$this->meetingroom_model->delete($posts['meeting_room_id']);
							
		$json->status = true;
		$json->message = "Meeting Room has been deleted.";
		
		echo json_encode($json);
	}
	
	function getCompanies()
	{
		$this->load->model("logistic_model");
		
		$rows = $this->logistic_model->getCompanies();
		
		echo json_encode($rows);
	}

	protected function getPhotoDir()
	{
		return "meetingroom";
	}
}
