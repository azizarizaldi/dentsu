<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class BookingCar extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/bookingcar/form.js", $data, true);
		}
        
		return $this->load->view("admin/bookingcar/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Booking Car";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/bookingcar/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/bookingcar/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("order_model");
		
		$this->data["services"] = $this->order_model->getBookingCarsServices();
		
		parent::addForm();
	}
	
	public function edit()
	{
		$this->load->model("order_model");
		
		$this->data["services"] = $this->order_model->getBookingCarsServices();
		parent::edit();
	}

		protected function getPhotoDir()
	{
		return "document_external";
	}
}
