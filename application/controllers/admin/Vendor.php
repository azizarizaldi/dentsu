<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Vendor extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/vendor/form.js", $data, true);
		}
        
		return $this->load->view("admin/vendor/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Vendor Databases";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/vendor/form", $data, true);
		}
		return $this->load->view("admin/vendor/list", "", true);
	}
	
	public function get() {
		$gets 			= $this->input->get();				
		$id_category 	= $gets["id_category"];
		$order 			= $gets["order"];
		$columns 		= $gets["columns"];
		$search 		= $gets['search']['value'];		
		$column 		= $columns[$order[0]["column"]];	
		$orderBy 		= "vendor_name";
		
		$this->load->model("vendor_model");
		$rows = $this->vendor_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search,$id_category);
		
		for($i=0; $i < count($rows); $i++) {
			$rows[$i]->vendor_id = sprintf("%05d", $rows[$i]->vendor_id);
		}
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->vendor_model->getCount($search,$id_category);
		$json["recordsFiltered"] = $this->vendor_model->getCount($search,$id_category);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	
	protected function addForm()
	{
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('1');
		$this->data["data_category"] = $rows;

		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("vendor_model");
		
		$row = $this->vendor_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->load->model("setting_category_model");		
		$rows = $this->setting_category_model->getCategoryByTipe('1');
		$this->data["data_category"] = $rows;

		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave() {
		$post 				= $this->input->post();
		$json 				= new stdClass();

		$config['upload_path']          = 'uploads/vendor';
		$config['allowed_types']        = '*';
		$config['encrypt_name'] 		= TRUE;
	
		$this->load->library('upload', $config);
	
		$action				= $post['action'];
		$id_category		= $post['id_category'];
		$vendor_name		= $post['vendor_name'];
		$npwp				= $post['npwp'];
		$email				= $post['email'];
		$contact_person		= $post['contact_person'];
		$phone_primary		= $post['phone_primary'];
		$phone_alternative	= $post['phone_alternative'];
		$address			= $post['address'];
		$note				= $post['note'];

		$data['id_category']		= $id_category;
		$data['vendor_name']		= $vendor_name;
		$data['npwp']				= $npwp;
		$data['email']				= $email;
		$data['contact_person']		= $contact_person;
		$data['phone_primary']		= $phone_primary;
		$data['phone_alternative']	= $phone_alternative;
		$data['address']			= $address;
		$data['note']				= $note;

		if($action == 'add') {
			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
				/*else{				
					$error = array('error' => $this->upload->display_errors());
					var_dump($error);
				}*/
			}
			$this->db->insert("vendor",$data);
			$json->status = true;
			$json->message = "Data has been added successfully!";
		}
		else {
			$vendor_id		= $post['vendor_id'];
			$this->db->where("vendor_id",$vendor_id);
			$getVendor = $this->db->get('vendor')->row();

			if (!empty($_FILES['file_attachment']['name'])) {
				if ($this->upload->do_upload('file_attachment')){
					if(!empty($getVendor->file_attachment)) {
						$path_to_file = 'uploads/vendor/'.$getVendor->file_attachment;
						unlink($path_to_file);	
					}
					$upload_data 				= $this->upload->data();				
					$file_name 					= $upload_data['file_name'];
					$data['file_attachment'] 	= $file_name;
				}
				/*else{				
					$error = array('error' => $this->upload->display_errors());
					var_dump($error);
				}*/
			}
			$this->db->where("vendor_id",$vendor_id);
			$this->db->update("vendor",$data);
			$json->status 	= true;
			$json->message	= "Data has been updated successfully!";
		}
		echo json_encode($json);
	}
	
	function remove() {
		$post 		= $this->input->post(null,true);
		$json 		= new stdClass();
		$vendor_id	= $post['vendor_id'];

		$this->db->where("vendor_id",$vendor_id);
		$getVendor = $this->db->get('vendor')->row();

		if(!empty($getVendor->file_attachment)) {
			$path_to_file = 'uploads/vendor/'.$getVendor->file_attachment;
			unlink($path_to_file);	
		}

		$this->db->where("vendor_id",$vendor_id);
		$this->db->delete("vendor");
		
		$json->status 	= true;
		$json->message 	= "Data has been deleted.";			
		echo json_encode($json);
	}
}
