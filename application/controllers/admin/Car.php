<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class Car extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/car/form.js", $data, true);
		}
        
		return $this->load->view("admin/car/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Car";
	}
	
	protected function getMainContent()
	{
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/car/form", $data, true);
		}
		return $this->load->view("admin/car/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		IF ($column["data"] == "no")
		{
			$orderBy = "car_no";
		}
		else
		{
			$orderBy = $column["data"];
		}
		
		$this->load->model("car_model");
		$rows = $this->car_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search);

		for($i=0; $i < count($rows); $i++)
		{
			if ($rows[$i]->car_pic)
			{
				$rows[$i]->no = sprintf("%s<br /><a href='%s/%s'><img src='%s/%s' width='40' height='40' /></a>", $rows[$i]->car_no, base_url(), $rows[$i]->car_pic, base_url(), $rows[$i]->car_pic);
			}
			else
			{
				$rows[$i]->no = $rows[$i]->car_no;
			}
		}
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->car_model->getCount($search);
		$json["recordsFiltered"] = $this->car_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	public function index()
	{
		$this->load->model("user_model");
		
		$this->data['drivers'] = $this->user_model->getDrivers();
		
		parent::index();
	}
	
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("car_model");
		
		$row = $this->car_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		/*
		$posts['logistic_company'] = "JNE";
		$posts['logistic_branch'] = "JNE Jatiasih";
		$posts['logistic_cp'] = "Abah";
		$posts['logistic_phone'] = "081385745997";
*/
		$json = new stdClass();
		
		if (! isset($posts["car_no"]) || ! isset($posts["car_brand"])  || ! isset($posts["car_model"]) || ! $posts["car_no"]|| ! $posts["car_brand"]|| ! $posts["car_model"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("car_model");
			
			if ($this->car_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Card No is alread exist";

			}
			else
			{
				if ($posts['car_pic'])
				{
					$path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["car_pic"]);
				}
				else
				{
					unset($posts['car_pic']);
				}

				unset($posts['tmp_photo']);


				if (isset($posts["car_id"]))
				{
					$this->car_model->update($posts);
				
					$json->status = true;
					$json->message = "Car has been updated";
				}
				else
				{
					$posts["car_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->car_model->insert($posts);
				
					$json->status = true;
					$json->message = "Car has been added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['car_id'])) return;
		
		$json = new stdClass();		

		$this->load->model("car_model");
		$this->car_model->delete($posts['car_id']);
							
		$json->status = true;
		$json->message = "Car has been deleted.";
		
		echo json_encode($json);
	}
	
	function setDriver()
	{
		$posts = $this->input->post();
		
		if (! isset($posts['carId']) || ! isset($posts['date1']) || ! isset($posts['driverId']) || ! $posts['carId'] || ! $posts['date1'] || ! $posts['driverId'])
		{
			return;
		}
		
		$date1 = dateYYYYMMDDToString($posts['date1']);
		$date2 = (isset($posts['date2']) && $posts['date2'] > 0) ? dateYYYYMMDDToString($posts['date2']) : "2999-12-31";
		
		$json = new stdClass();
	
		$this->load->model("cardriver_model");
		
		$json->status = $this->cardriver_model->isValidData($posts['carId'], $date1, $date2, $posts['driverId']);
		$json->message = $json->status ? "Driver has been set" : "Driver already exist";
		
		if ($json->status)
		{
			$data['car_driver_car'] = $posts['carId'];
			$data['car_driver_user'] = $posts['driverId'];
			$data['car_driver_start_date'] = $date1." 00:00:00";
			$data['car_driver_end_date'] = $date2." 23:59:59";
			
			$this->cardriver_model->insert($data);
		}
		
		echo json_encode($json);
	}

	protected function getPhotoDir()
	{
		return "car";
	}
	
}
