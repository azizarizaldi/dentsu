<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class OrderPriority extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/orderpriority/form.js", $data, true);
		}
        
		return $this->load->view("admin/orderpriority/list.js", "", true);
    }
	
	protected function getTitle()
	{
		return "Company";
	}
	
	protected function getMainContent()
	{
		switch($this->action)
		{
			case "add":
				return $this->load->view("admin/orderpriority/add", "", true);
				break;
			case "edit":
				return $this->load->view("admin/orderpriority/edit", "", true);
				break;
		}
		return $this->load->view("admin/orderpriority/list", "", true);
	}
	
	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];
		
		$this->load->model("orderpriority_model");
		$rows = $this->orderpriority_model->get($column["data"], $order[0]["dir"], $gets["length"], $gets["start"], $search);
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->orderpriority_model->getCount($search);
		$json["recordsFiltered"] = $this->orderpriority_model->getCount($search);
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	protected function addForm()
	{
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("orderpriority_model");
		
		$row = $this->orderpriority_model->getById($gets["id"]);
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		
		$json = new stdClass();
		
		//$posts['priority_name'] = "Urgent 4";
		//$posts['priority_id'] = 2;
		
		if (! isset($posts["priority_name"]) || ! $posts["priority_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("orderpriority_model");
			
			if ($this->orderpriority_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Priority \"${posts['priority_name']}\" is already exist.";
			}
			else
			{
				if (isset($posts["priority_id"]))
				{
					$this->orderpriority_model->update($posts);
				
					$json->status = true;
					$json->message = "Order Priority has updated.";
				}
				else
				{
					$posts["priority_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->orderpriority_model->insert($posts);
				
					$json->status = true;
					$json->message = "Order Priority Data has added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['priority_id'])) return;
		
		$json = new stdClass();
		
		$this->load->model("order_model");
		$count = $this->order_model->getCountByPriority($posts['priority_id']);
		if ($count > 0)
		{
			$json->status = false;
			$json->message = "You can't delete this data.";	
		}
		else
		{
			$this->load->model("orderpriority_model");
			$this->orderpriority_model->delete($posts['priority_id']);
				
			$json->status = true;
			$json->message = "Order Priority Data has been deleted.";	
		}
		
		echo json_encode($json);
	}

}
