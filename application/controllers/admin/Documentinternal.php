<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class DocumentInternal extends Admin {
	var $action;

	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_internal/form.js", $data, true);
		}
        
		return $this->load->view("admin/document_internal/list.js", "", true);
    }
	
	public function index()
	{        
		$this->load->model("user_model");
		
		$this->data["drivers"] = $this->user_model->getCouriers();		
		parent::index();

	}
	
	protected function getTitle()
	{
		return "Internal Courier";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_internal/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/document_internal/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("user_model");
		
		$rows = $this->user_model->getEmployees();
		$this->data["senders"] = $rows;

		$this->load->model("orderpriority_model");
		
		$rows = $this->orderpriority_model->get("priority_id", "asc");
		$this->data["priorities"] = $rows;
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("order_model");
		
		$row = $this->order_model->getById($gets["id"]);
		$this->data["row"] = $row;
		
		$this->load->model("user_model");
		
		$rows = $this->user_model->getEmployees();
		$this->data["senders"] = $rows;
		
		$this->load->model("orderpriority_model");
		
		$rows = $this->orderpriority_model->get("priority_id", "asc");
		$this->data["priorities"] = $rows;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();
		
		$json = new stdClass();
		
		//$posts['company_name'] = "test";
		
		if (! isset($posts["company_name"]) || ! $posts["company_name"])
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("company_model");
			
			if ($this->company_model->isAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Company \"${posts['company_name']}\" is already exist.";
			}
			else
			{
				if (isset($posts["company_id"]))
				{
					$this->company_model->update($posts);
				
					$json->status = true;
					$json->message = "Company has updated.";
				}
				else
				{
					$posts["company_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->company_model->insert($posts);
				
					$json->status = true;
					$json->message = "Company has added.";
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['company_id'])) return;
		
		$json = new stdClass();
		
		$this->load->model("brand_unit_model");
		$count = $this->brand_unit_model->getCountByCompany($posts['company_id']);
		if ($count > 0)
		{
			$json->status = false;
			$json->message = "You can't delete this data.";	
		}
		else
		{
			$this->load->model("company_model");
			$this->company_model->delete($posts['company_id']);
				
			$json->status = true;
			$json->message = "Company has been deleted.";	
		}
		
		echo json_encode($json);
	}

	protected function getPhotoDir()
	{
		return "document_internal";
	}
}
