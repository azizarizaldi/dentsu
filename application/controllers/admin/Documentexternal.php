<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Documentinternal.php';

class DocumentExternal extends DocumentInternal {
	protected function getJSContent()
    {
		$data['action'] = $this->action;
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_external/form.js", $data, true);
		}
        
		return $this->load->view("admin/document_external/list.js", "", true);
    }
	
	public function index()
	{
		$this->load->model("packagetype_model");
		$rows = $this->packagetype_model->getCourierCompanies();
		
		$this->data["couriers"] = $rows;
		
		$this->load->model("user_model");
        
		$this->data["officeboys"] = $this->user_model->getOfficeBoys();	
		
		parent::index();

	}
	
	protected function getTitle()
	{
		return "External Courier";
	}
	
	protected function getMainContent()
	{
		$this->data['action'] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/document_external/form", $this->data, true);
				break;
		}
		return $this->load->view("admin/document_external/list", "", true);
	}
	
	protected function addForm()
	{
		$this->load->model("packagetype_model");
		
		$rows = $this->packagetype_model->getCourierCompanies();
		
		$this->data["couriers"] = $rows;
		
		parent::addForm();
	}

		protected function getPhotoDir()
	{
		return "document_external";
	}
}
