<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/Admin.php';

class User extends Admin {
	var $action;

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	protected function getJSContent()
    {
		$data["action"] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/".$this->getPath()."/form.js", $data, true);
			case "import":
				return $this->load->view("admin/user/import.js", $data, true);
		}
        
		return $this->load->view("admin/".$this->getPath()."/list.js", "", true);
    }

    protected function getPath()
    {
    	return "user";
    }
	
	protected function getTitle()
	{
		return "User";
	}
	
	protected function getMainContent()
	{
		$data["action"] = $this->action;
		
		switch($this->action)
		{
			case "add":
			case "edit":
				return $this->load->view("admin/".$this->getPath()."/form", $data, true);
			case "import":
				return $this->load->view("admin/user/import", $data, true);
		}
		return $this->load->view("admin/".$this->getPath()."/list", "", true);
	}
	
	protected function isVisitor()
	{
		return false;
	}

	public function get()
	{
		$gets = $this->input->get();
				
		$order = $gets["order"];
		$columns = $gets["columns"];
		$search = $gets['search']['value'];
		
		//print_r($gets); exit;
		
		$column = $columns[$order[0]["column"]];

		if ($column["data"] == 'type_name')
		{
			$orderBy = "user_type";
		}
		else
		{
			$orderBy = $column["data"];
		}
		
		$this->load->model("user_model");
		$rows = $this->user_model->get($orderBy, $order[0]["dir"], $gets["length"], $gets["start"], $search, $this->isVisitor());
		
		for($i=0; $i < count($rows); $i++)
		{
			$rows[$i]->type_name = $this->user_model->getLevelName($rows[$i]->user_type);
			$rows[$i]->status_name = $this->user_model->getStatusName($rows[$i]->user_status);
			$rows[$i]->departement = $rows[$i]->departement_name  ? sprintf("%s, %s, %s", $rows[$i]->company_name, $rows[$i]->brand_unit_name
											, $rows[$i]->departement_name) : "-";
		}
		
		$json["draw"] = $gets["draw"]+1;
		$json["recordsTotal"] = $this->user_model->getCount($search, $this->isVisitor());
		$json["recordsFiltered"] = $this->user_model->getCount($search, $this->isVisitor());
		$json["data"] = $rows;
		
		echo json_encode($json);

	}
	
	protected function addForm()
	{
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->action = "add";
		$this->index();
	}
	
	public function edit()
	{
		$gets = $this->input->get();
		
		$this->load->model("company_model");
		
		$companies = $this->company_model->get("company_name", "asc");
		$this->data["companies"] = $companies;
		
		$this->load->model("user_model");
		$row = $this->user_model->getById($gets['id']);
		
		$this->data["row"] = $row;
		
		$this->action = "edit";
		$this->index();
	}
	
	protected function doSave()
	{
		$posts = $this->input->post();

		if ($this->isVisitor())
		{
			$posts['user_password'] = "abah.adilah.135020";
		}

		/*
		//$posts['user_photo'] = "uploads/photo/5a5c6fb64047920180115100910000062659900_1466160633-20160617-Stok-uang-AY-3.jpg";
		//$posts['tmp_photo'] ="uploads/tmp/5a5c6fb64047920180115100910000062659900_1466160633-20160617-Stok-uang-AY-3.jpg";
		$posts['user_id'] = 3;
		$posts['user_photo'] = "";
		$posts['tmp_photo'] = "";
		$posts['user_nik'] = "135020";
		$posts['user_name'] = "Toni";
		$posts['user_phone'] = '087874802517';
		$posts['user_departement'] = "1";
		$posts['user_email'] = "owner@adilahsoft.com";
		//$posts['user_password'] = "password";
		$posts['user_type'] = 97;
		$posts['user_status'] = 2;
		//$posts['confirm_password'] = "password";
		*/

		$json = new stdClass();
		
		if (! isset($posts["user_email"]) || ! isset($posts["user_email"]) )
		{
			$json->status = false;
			$json->message = "Terjadi kesalahan";
		}
		else
		{
			$this->load->model("user_model");
			
			if (! $this->isVisitor() && $this->user_model->isNIKAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "NIK is alread exist";

			}
			else
			if ($this->user_model->isPhoneAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Phone is alread exist";

			}
			else
			if ($this->user_model->isEmailAlreadyExist($posts))
			{
				$json->status = false;
				$json->message = "Email account is alread exist";

			}
			else
			{
				if ($posts['user_photo'])
				{
					$path = $this->config->item('basePath');
					@copy($path.$posts["tmp_photo"], $path.$posts["user_photo"]);
				}
				else
				{
					unset($posts['user_photo']);
				}

				unset($posts['tmp_photo']);
				
				if (isset($posts["user_id"]))
				{
					
					$this->user_model->update($posts);
				
					$json->status = true;
					$json->message = $this->getTitle()." has been updated";
				}
				else
				{
					$password = $posts['user_password'];
					
					unset($posts['confirm_password']);
					unset($posts['user_password']);
					
					$posts["user_created_by"] = $this->session->userdata("row")->user_id;
					
					$this->user_model->insert($posts, $password);
				
					$json->status = true;
					$json->message = $this->getTitle()." has been added.";

					$row = $this->user_model->getById($this->db->insert_id());
					$this->createUserEmail($row);
				}
			}			
		}
		
		echo json_encode($json);
	}
	
	function remove()
	{
		$posts = $this->input->post();

		if (! isset($posts['user_id'])) return;
		
		$this->load->model("user_model");
		$this->user_model->delete($posts['user_id']);
				
		$json = new stdClass();		
				
		$json->status = true;
		$json->message = sprintf("%s has been deleted.", $this->isVisitor() ? "Visitor" : "User");
		
		echo json_encode($json);
	}
	
	function changePassword()
	{
		$posts = $this->input->post();
		
		$posts['user_id'] = 3;
		$posts['user_password'] = "password";
		$posts['confirm_password'] = "password";
		
		if (! isset($posts["user_id"]) || ! $posts['user_id'] || ! isset($posts["user_password"]) || ! $posts['user_password']) return;
		
		unset($posts['confirm_password']);
		
		$this->load->model("user_model");
		$this->user_model->changePassword($posts);
		
		$json = new stdClass();		
				
		$json->status = true;
		$json->message = sprintf("%s has been changed.", $this->isVisitor() ? "Token" : "Password");
		
		echo json_encode($json);
	}
	
	protected function getPhotoDir()
	{
		return "photo";
	}

	public function aa()
	{
		$gets = $this->input->get();

		$this->load->model("user_model");
		$row = $this->user_model->getById($gets["id"]);

		$this->createUserEmail($row);
	}

	private function createUserEmail($user)
	{

		$link = sprintf("%suser/changepassword?token=%s&phone=%s", base_url(), $user->user_verification_code, $user->user_phone);
		$message = read_file($this->config->item("basePath")."assets/template/create_user_email.html");
		
		$message = str_replace("__app_name__", $this->config->item("app_name"), $message);
		$message = str_replace("__time__", greetingTime(), $message);
		$message = str_replace("__name__", $user->user_name, $message);
		$message = str_replace("__phone__", $user->user_phone, $message);
		$message = str_replace("__password__", $user->user_token, $message);
		$message = str_replace("__link__", $link, $message);
		
		$config['mailtype'] = "html";

		$this->load->library('email');
		$this->email->initialize($config);

		$this->email->from($this->config->item("admin_email"), $this->config->item("admin_name"));
		$this->email->to($user->user_email);
		$this->email->subject($this->config->item('create_user_email_subject'));
		$this->email->message($message);

		@$this->email->send();
	}

	public function toolsimport()
	{
		$this->action = "import";
		$this->index();
	}

	public function doimport()
	{	
		$json = new stdClass(); 

		if (! $_FILES['file']['name'])
		{
			$json->message = "Please input file import";
		}
		else
		if (($_FILES['file']['type'] != "text/csv") && ($_FILES['file']['type'] != "application/vnd.ms-excel"))
		{
			$json->message = "Invalid file type.";
		}
		else
		{
			$content = read_file($_FILES['file']['tmp_name']);
			if (! $content)
			{
				$json->message = "File is empty";
			}
			else
			{
				$this->load->model("user_model");
				$this->load->model("company_model");
				$this->load->model("dept_model");
				$this->load->model("brand_unit_model");

				$contents = explode("\n", $content);
				$success = 0;
				$failed = 0;
				for($i=1; $i < count($contents); $i++)
				{
					$row = trim($contents[$i]);
					if ($row == "") continue;

					$cols = explode(";", $row);

					if (count($cols) != 7)
					{
						$failed++;
						continue;
					}

					$rand = rand(1000000, 999999);

					unset($departement_id);
					if ($cols[4] != "" && $cols[5] != "" && $cols[6] != "")
					{
						$row = $this->company_model->getByName($cols[4]);
						if (isset($row->company_id))
						{
							$company_id = $row->company_id;
						}
						else
						{
							$companies['company_name'] = $cols[4];
							$this->company_model->insert($companies);

							$company_id = $this->db->insert_id();
						}

						$row = $this->brand_unit_model->getByName($company_id, $cols[5]);
						if (isset($row->brand_unit_id))
						{
							$brand_unit_id = $row->brand_unit_id;
						}
						else
						{
							$brandunits['brand_unit_name'] = $cols[5];
							$brandunits['brand_unit_company'] = $company_id;

							$this->brand_unit_model->insert($brandunits);

							$brand_unit_id = $this->db->insert_id();
						}

						$row = $this->dept_model->getByName($brand_unit_id, $cols[6]);
						if (isset($row->departement_id))
						{
							$departement_id = $row->departement_id;
						}
						else
						{
							$depts['departement_name'] = $cols[6];
							$depts['departement_brand_unit'] = $brand_unit_id;

							$this->dept_model->insert($depts);

							$departement_id = $this->db->insert_id();
						}
					}

					if (isset($departement_id))
					{
						$data['user_departement'] = $departement_id;
					}
					$data['user_email'] = $cols[0];
					$data['user_phone'] = $cols[1];
					$data['user_nik'] = $cols[2];
					$data['user_name'] = $cols[3];
					$data['user_token'] = $rand;
					$data['user_verification_code'] = md5(uniqid());

					$row = $this->user_model->getByUniqValue($data['user_email'], $data['user_phone'], $data['user_nik']);

					if (isset($row->user_id))
					{
						if ($_POST['method'] == 1)
						{
							$failed++;
						}
						else
						{
							$success++;
							$data['user_id'] = $row->user_id;
							$this->user_model->update($data);
						}

						$this->createUserEmail($row);
					}
					else
					{
						$this->user_model->insert($data, $rand);
						$row = $this->user_model->getById($this->db->insert_id());

						$this->createUserEmail($row);

						$success++;
					}

				}
				
				$json->message = sprintf("File has been imported. %d data succes, %d data failed", $success, $failed);
			}
		}




		//print_r($_FILES);



		echo json_encode($json);
	}
}
