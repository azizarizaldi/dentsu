<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Stationary_by_category extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
		
		if (isset($gets)) {
			if (isset($gets["order"])) {
				$order 			= $gets["order"];
			}
			if (isset($gets["columns"])) {
				$columns 		= $gets["columns"];
			}
			if (isset($gets['search']['value'])) {
				$search 		= $gets['search']['value'];
			}
			if (isset($gets["item_category"])) {
				$item_category 	= $gets["item_category"];
			}
			if (isset($gets["departement_id"])) {
				$departement_id 	= $gets["departement_id"];
			}
			if (isset($gets["company_id"])) {
				$company_id 	= $gets["company_id"];
			}
			if (isset($gets["brand_unit_id"])) {
				$brand_unit_id 	= $gets["brand_unit_id"];
			}
			else if (isset($gets['search'])) {
				$search 		= $gets['search'];
			}
		}
		
		if (isset($columns)) {
			$column = $columns[$order[0]["column"]];
			if ($column["data"] == "id_order") {
				$column_order_by = "stationary_order_id";
			}
			else if ($column["data"] == "order_date") {
				$column_order_by = "order_date";
			}
			else if ($column["data"] == "qty") {
				$column_order_by = "qty";
			}
			else {
				$column_order_by = $column["data"];
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("reportstationary_model");
		$rows = $this->reportstationary_model->get(isset($column_order_by) ? $column_order_by : "code", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date,isset($item_category) ? $item_category : "",isset($departement_id) ? $departement_id : "",isset($company_id) ? $company_id : "",isset($brand_unit_id) ? $brand_unit_id : "");

		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->order_no 			= str_pad ($rows[$i]->order_id, 5, '0', STR_PAD_LEFT);
			$rows[$i]->order_date 			= tgl($rows[$i]->order_date,'02');
			$rows[$i]->item_description 	= $rows[$i]->item_description;
			$rows[$i]->qty 					= $rows[$i]->qty;
			$rows[$i]->user_name 			= $rows[$i]->user_name;
			$rows[$i]->brand_unit_name 		= $rows[$i]->brand_unit_name;
			$rows[$i]->departement_name 	= $rows[$i]->departement_name;
		}
		
		$json["draw"] 				= isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] 		= $this->reportstationary_model->getCount(isset($search) ? $search : "",$range_date,isset($item_category) ? $item_category : "",isset($departement_id) ? $departement_id : "",isset($company_id) ? $company_id : "",isset($brand_unit_id) ? $brand_unit_id : "");
		$json["recordsFiltered"]	= $this->reportstationary_model->getCount(isset($search) ? $search : "",$range_date,isset($item_category) ? $item_category : "",isset($departement_id) ? $departement_id : "",isset($company_id) ? $company_id : "",isset($brand_unit_id) ? $brand_unit_id : "");
		$json["data"] 				= $rows;
		$json['status'] 			= true;
		
		echo json_encode($json);

	}
	
}
