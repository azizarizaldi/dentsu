<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Documentinternal.php';

class AppointmentOrder extends DocumentInternal {
    
    public function get()
    {
        $gets = $this->input->get();
        if (isset($gets['draw']))
        {
            parent::get();
            return;
        }
        
        $this->load->model("order_model");
        
        $rows = $this->order_model->getAppointmentToday();
        for($i=0; $i < count($rows); $i++)
        {
            $rows[$i]->note = $rows[$i]->order_status == 5 ? $rows[$i]->order_rejected_note : $rows[$i]->order_receiver_note;
            
            $rows[$i]->canSignOut = $rows[$i]->order_status > 1;
            $rows[$i]->hasSignOut = $rows[$i]->signOutDate != "00/00/0000";
        }
        
        echo json_encode($rows);
    }
    
    public function signOut()
    {
        $posts = $this->input->post();
        
        
        $this->load->model("order_model");
        $this->order_model->appointmentSignOut($posts['orderId'], $posts['signInTime']);
        
        $json = new stdClass();
        $json->status = true;
        $json->message = "Your had sign out. Thanks";
        
        echo json_encode($json);
    }
    
    public function register()
    {
        $posts = $this->input->post();
        $json = new stdClass();
 
        $visitor["user_name"] = $posts['name'];
        $visitor["user_visitor_company"] = $posts['company'];
        $visitor["user_email"] = $posts['email'];
        $visitor["user_phone"] = $posts['phone'];
        $visitor['user_type'] = 50;
  
		if (isset($_FILES['photo']))
		{
			$path = $this->config->item('basePath');
			$filename = $path.$this->getPhotoDir().$_FILES['photo']['name'];
				    
			@copy($_FILES['photo']['tmp_name'], $filename);
			$visitor['user_photo'] = $this->getPhotoDir().$_FILES['photo']['name'];

		}
	

		$this->load->model("user_model");
		$visitorId = $this->user_model->insert($visitor, $posts['phone']);
		
		$order['order_name'] = "Appointment"; 
		$order['order_created_by'] = $visitorId;
		$order['order_pickup_by'] = $posts['host'];
		$order['order_receiver_address'] = $posts['purpose'];
		$order['order_type'] = $this->getOrderType();

		$this->load->model("order_model");
		$this->order_model->insert($order);
		
		$json->status = true;
		$json->message = "Appointment created successfully";
		
		echo json_encode($json);
    }
    
    public function createOrder()
    {
        $posts = $this->input->post();
        
        $order['order_name'] = "Appointment"; 
		$order['order_created_by'] = $posts['userId'];
		$order['order_pickup_by'] = $posts['host'];
		$order['order_receiver_address'] = $posts['purpose'];
		$order['order_type'] = $this->getOrderType();

		$this->load->model("order_model");
		$this->order_model->insert($order);
		
		$insert_id = $this->db->insert_id();
		
		$this->order_model->notifNewOrder($insert_id);
		
		$json = new stdClass();
		$json->status = true;
		$json->message = "Appointment created successfully";
		
		echo json_encode($json);
    }
    
    public function isEmailPhoneUnique()
    {
        $gets = $this->input->get();
     
        $this->load->model("user_model");
        $row = $this->user_model->getByUniqValue($gets['email'], $gets['phone'], FALSE);   
        
        $json = new stdClass();
        $json->status = ! $row;
        $json->message = $json->status ? "" : "Email or phone no is already exist";
        
        echo json_encode($json);
    }
    
	protected function getOrderType()
	{
		return 7;
	}
	
	protected function getEmptyOrderNameMessage()
	{
		return "Please input a document name";
	}
	
	
	protected function getDocomentColumnContent($row)
	{
		return sprintf("%s", $row->order_created_date_fmt);
	}
	
	protected function getReceiverColumnContent($row)
	{
		return sprintf("%s", $row->pickup_by);
	}
	
	protected function getStatus($row)
	{
		$this->load->model("order_model");
		
		switch($row->order_status)
			{
				case 2:
					return "New";
				case 3:
					return sprintf("Accept<br />Date: %s<br />Note: %s. %s", $row->order_pickup_date_fmt, $row->order_receiver_address_note, $row->order_sender_address);
				case 4:
					return sprintf("Done<br />Date: %s<br />Note: %s", $row->order_received_date_fmt, $row->order_receiver_note);
				case 5:
					return sprintf("%s<br />By: %s<br />Date: %s<br />Note: %s %s"
												, $this->order_model->getStatusDesc($row->order_status)
												, $row->rejected_by, $row->order_rejected_date_fmt
												, $this->order_model->getAppointmentRejectedReason($row), $row->order_rejected_note);					
				default:
					return sprintf("%s", $this->order_model->getStatusDesc($row->order_status));

			}
	}

	protected function getCourierColumnContent($row)
	{
		return sprintf("%s", $row->order_receiver_address);
	}
	
	protected function getPhotoDir()
	{
		return "uploads/visitor/";
	}
}
