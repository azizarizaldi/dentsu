<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class ListOfMessenger extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets))
		{
			if (isset($gets["order"]))
			{
				$order = $gets["order"];
			}
			if (isset($gets["columns"]))
			{
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value']))
			{
				$search = $gets['search']['value'];
			}
			else
			if (isset($gets['search']))
			{
				$search = $gets['search'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "user_nik")
			{
				$column_order_by = "user_nik";
			}
			else
			if ($column["data"] == "user_name")
			{
				$column_order_by = "user_name";
			}
			else
			if ($column["data"] == "user_email")
			{
				$column_order_by = "user_email";
			}
		}
				
		$this->load->model("listofmessenger_model");
		$rows = $this->listofmessenger_model->get(isset($column_order_by) ? $column_order_by : "code", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "");

		for($i=0; $i < count($rows); $i++)
		{
			
			$rows[$i]->user_nik 	= $rows[$i]->user_nik;
			$rows[$i]->user_photo 	= "<img src='".base_url()."/".$rows[$i]->user_photo."' width='80' height='80' />";
			$rows[$i]->user_name 	= $rows[$i]->user_name;
			$rows[$i]->user_phone 	= $rows[$i]->user_phone;
			$rows[$i]->user_email 	= $rows[$i]->user_email;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->listofmessenger_model->getCount(isset($search) ? $search : "");
		$json["recordsFiltered"] = $this->listofmessenger_model->getCount(isset($search) ? $search : "");
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
