<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MyController extends CI_Controller {
	var $data;
	

	public function isAdmin()
	{
		$row = $this->session->userdata("row");
		return $row->user_type >= 97;
	}
	
	public function isAdminApps($row)
	{
		return $row->user_type >= 97;
	}
	
	public function validLoginApps()
	{
	    $row = $this->session->userdata("row");
	    
	    if ($row)
	    {
	        return $row;
	    }
	    
	    $headers = $this->input->request_headers();
	    if (! isset($headers['Useremail']) || ! isset($headers['Usertoken']))
	    {
	        return false;
	    }
	    
	    $this->load->model("user_model");
	    $row = $this->user_model->checkToken($headers['Useremail'], $headers['Usertoken']);
	    
	    if (! $row)
	    {
	        return false;
	    }
	    
	    return $row;
	    
	}
}
