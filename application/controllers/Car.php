<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Car extends MyController {
    function getBrands()
	{
		$this->load->model("car_model");
		
		$rows = $this->car_model->getBrands();
		
		echo json_encode($rows);
	}
	
	function getModels()
	{
		$this->load->model("car_model");
		
		$rows = $this->car_model->getModels();
		
		echo json_encode($rows);
	}

	function getRentCompanie()
	{
		$this->load->model("car_model");
		
		$rows = $this->car_model->getRentCompanies();
		
		echo json_encode($rows);
	}
    
}
