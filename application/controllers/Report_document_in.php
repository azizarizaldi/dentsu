<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Report_document_in extends Mycontroller {
	
	public function get()
	{
	    $session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $json['status'] = false;
	        echo json_encode($json);
	        return;
	    }
	    
		$gets = $this->input->get();
			
		if (isset($gets)) {
			if (isset($gets["order"])) {
				$order = $gets["order"];
			}
			if (isset($gets["columns"])) {
				$columns = $gets["columns"];
			}
			if (isset($gets['search']['value'])) {
				$search = $gets['search']['value'];
			}
			else if (isset($gets['search'])) {
				$search = $gets['search'];
			}
			if (isset($gets['order_ref'])) {
				$order_ref = $gets['order_ref'];
			}
		}
		
		if (isset($columns))
		{
			$column = $columns[$order[0]["column"]];


			if ($column["data"] == "order_id")
			{
				$column_order_by = "order_id";
			}
			else if ($column["data"] == "order_date") {
				$column_order_by = "order_date";
			}
			else if ($column["data"] == "user_name") {
				$column_order_by = "user_name";
			}
		}
		
		$range_date 	= isset($gets["range_date"]) ? $gets["range_date"] : "";			
		
		$this->load->model("Report_document_in_model");
		$rows = $this->Report_document_in_model->get(isset($column_order_by) ? $column_order_by : "order_id", isset($order) ? $order[0]["dir"] : "asc", isset($gets["length"]) ? $gets["length"] : 0, isset($gets["start"]) ? $gets["start"] : 0, isset($search) ? $search : "",$range_date,$order_ref);
		
		for($i=0; $i < count($rows); $i++)
		{
			$message = "";
			if($rows[$i]->status_order == 'Done') {
				$message = "<br/>Received Date : ".$rows[$i]->order_received_date;
			}
			else if($rows[$i]->status_order == 'Rejected') {
				$message = "<br/>Rejected Date : ".$rows[$i]->order_rejected_date;				
			}
			
			$rows[$i]->order_id 			= $rows[$i]->order_id;
			$rows[$i]->order_date 			= tgl($rows[$i]->order_date,'2');
			$rows[$i]->user_name 			= $rows[$i]->user_name;
			$rows[$i]->item 				= '<img width="80" height="80" src="'.base_url().$rows[$i]->item.'"/>';
			$rows[$i]->order_name 			= "Document Name : ".$rows[$i]->order_name.'<br/>Sender : '.$rows[$i]->order_receiver_name.'<br/>Courier : '.$rows[$i]->order_receiver_address;
			$rows[$i]->status 				= '<b>'.$rows[$i]->status_order.'</b>'.$message;
		}
		
		$json["draw"] = isset($gets["draw"]) ? $gets["draw"]+1 : 0;
		$json["recordsTotal"] = $this->Report_document_in_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["recordsFiltered"] = $this->Report_document_in_model->getCount(isset($search) ? $search : "",$range_date,$order_ref);
		$json["data"] = $rows;
		$json['status'] = true;
		
		echo json_encode($json);

	}
	
}
