<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class Chat extends MyController {
    public function getLast()
    {
        $json = new stdClass();
        $session = $this->validLoginApps();
        
        //$session = new stdClass();
        //$session->user_id = 17;
        
        if ($session === false)
	    {
	        $json->sessionExpired = true;

	        echo json_encode($json);
	        return;
	    }
	    
	    $gets = $this->input->get();
	    
	    $search = isset($gets['userName']) ? $gets['userName'] : "";
	    
	    $this->load->model("chat_model");
	    $rows = $this->chat_model->getLast($session->user_id, $search);
	    for($i=0; $i < count($rows); $i++)
	    {
	        $rows[$i]->time = date("d/m/Y") == $rows[$i]->chat_create_date_1_fmt ? $rows[$i]->chat_create_date_2_fmt : $rows[$i]->chat_create_date_fmt;
	        if ($rows[$i]->user_type == 3)
	        {
	            $rows[$i]->userType = "Driver";
	        }
	        else
	        if ($rows[$i]->user_type == 4)
	        {
	            $rows[$i]->userType = "Courier";
	        } 
	        else
	        {
	            $rows[$i]->userType = "Employee";
	        }
	    }
	    
	    $json->rows = $rows;
	    echo json_encode($json);
    }
    
    public function get()
    {
        $json = new stdClass();
        $session = $this->validLoginApps();
        
        //$session = new stdClass();
        //$session->user_id = 17;
        
        if ($session === false)
	    {
	        $json->sessionExpired = true;

	        echo json_encode($json);
	        return;
	    }
	    
	    $gets = $this->input->get();
	    if (! isset($gets['to'])) return;
	    
	    $this->load->model("chat_model");
	    $rows = $this->chat_model->get($session->user_id, $gets['to']);
	    for($i=0; $i < count($rows); $i++)
	    {
	        $rows[$i]->fromMySelf = $rows[$i]->chat_from ==  $session->user_id;
	        $rows[$i]->time = date("d/m/Y") == $rows[$i]->chat_create_date_1_fmt ? $rows[$i]->chat_create_date_2_fmt : $rows[$i]->chat_create_date_fmt;
	    }
	    
	    $json->rows = $rows;
	    echo json_encode($json);   
    }
    
    public function send()
    {
        $json = new stdClass();
        
        $session = $this->validLoginApps();
        
        if ($session === false)
	    {
	        $json->sessionExpired = true;

	        echo json_encode($json);
	        return;
	    }
	    
	    $posts = $this->input->post();
	    if (! isset($posts['to']) || ! $posts['to'] || ! isset($posts['message']) || ! $posts['message'])
	    {
	        $json->status = false;

	        echo json_encode($json);
	        return;
	    }
	    
	    $this->load->model("chat_model");
	    $this->chat_model->save($session->user_id, $posts['to'], $posts['message']);
	    
	    
	   $json->status = true;

	   echo json_encode($json);
	   return;
    }
    
}
