<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Mycontroller.php';

class User extends MyController {
    public function getEmployees()
    {
        /*$session = $this->validLoginApps();
	    if ($session === false)
	    {
	        $rows = array();
	        echo json_encode($rows);
	        return;
	    }
	    */
        $this->load->model("user_model");
        
        $rows = $this->user_model->getEmployees();
        
        echo json_encode($rows);
    }
    
    public function getCouriers()
    {
        $this->load->model("user_model");
        
        $rows = $this->user_model->getCouriers();
        
        echo json_encode($rows);
    }

    public function changePassword()
    {
    	$gets = $this->input->get();

        if (! isset($gets['token']) || ! $gets['token'] || ! isset($gets['phone']) || ! $gets['phone'])
        {
            die("Access denied");
        }

        $this->load->model("user_model");

        $row = $this->user_model->getByVerificationCode($gets['token'], $gets['phone']);
        if (! $row)
        {
            die("Access denied");
        }

        $data['token'] = $gets['token'];
        $data['phone'] = $gets['phone'];

        $this->load->view("changepassword", $data);
    }

    public function dochangePassword()
    {
        $json = new stdClass();
        $posts = $this->input->post();

        if (! isset($posts['token']) || ! $posts['token'] || ! isset($posts['phone']) || ! $posts['phone'] || ! isset($posts['newpassword']) || ! $posts['newpassword'] || (strlen($posts['newpassword']) < 6))
        {
            $json->message = "Access denied (1)";
        }
        else
        {
            $this->load->model("user_model");
            $row = $this->user_model->getByVerificationCode($posts['token'], $posts['phone']);
            if (! $row)
            {
                $json->message = "Access denied (2)";
            }
            else
            {
                $data['user_id'] = $row->user_id;
                $data["user_password"] = $posts['newpassword'];
                $data['user_verification_code'] = "";

                $this->user_model->changePassword($data);

                $json->message = "Password has been changed";
            }
        }

        echo json_encode($json);
    }
}
